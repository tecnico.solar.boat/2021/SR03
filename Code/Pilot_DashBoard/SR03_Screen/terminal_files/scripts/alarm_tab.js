var alarms_active = getVariableValue("alarms_active");
var alarm_current = getVariableValue("@AlarmCurrent");

if(alarms_active){
    if(alarm_current>0) setVariableValue("@AlarmShow",0x01);
    else setVariableValue("@AlarmShow",0x8001);
}