// Energy
updateTime();
calculateEnergy();

// Calculate solar power
calculatesolarpower();

// Calculate new variables
// Foils
calculateMag();

// BMS
decodeStatus();
calculateMaxMinCellTemp();
calculateMaxTabTemp();

// Send message to activate encoder
sendMsgsAfter10secs();

// Detect warnings
detectThrottleValueWithoutZero();

// Clock
includeFile("update_time.js");




// Time with a scale of 0.1.
// Time = 100 -> RealTime = 10s
function updateTime(){
    var time = getVariableValue("Elapsed_Time");
    time = time + 1;
    setVariableValue("Elapsed_Time",time); // Should be applied a 0.1 factor
    
    time_string = sec2time(Math.round(time*0.1));
    setVariableValue("Elapsed_Time_string", time_string);
}


function sec2time(timeInSeconds) {
    var pad = function(num, size) { return ('000' + num).slice(size * -1); },
    time = parseFloat(timeInSeconds).toFixed(3),
    hours = Math.floor(time / 60 / 60),
    minutes = Math.floor(time / 60) % 60,
    seconds = Math.floor(time - minutes * 60),
    milliseconds = time.slice(-3);

    return pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2);
}


// Calculate Energy 
// Needs 100ms period
function calculateEnergy(){
    var energy_joules = getVariableValue("energy_joules");
    var energy_Wh = getVariableValue("energy_Wh");

    var voltage = getVariableValue("Voltage") * 0.002;
    var current = getVariableValue("Current") * 0.01;
    
    energy_joules = energy_joules + (voltage * current)* 0.1*10; // 0.1 comes from 100ms period and 10 for more accuracy
    energy_Wh = (energy_joules / 3600)/10; // /10 for more accuracy
    setVariableValue("energy_Wh",energy_Wh);
    setVariableValue("energy_joules",energy_joules);
}




// Magnetic field - Same scale that magX, magY, magZ
function calculateMag(){
magX = getVariableValue("magX");
magY = getVariableValue("magY");
magZ = getVariableValue("magZ");

mag = Math.sqrt(square(magX)+square(magY)+square(magZ));

setVariableValue("mag", mag);
}

//Status Decode
function decodeStatus(){
   var charging = getVariableValue("Charging");
   var discharging = getVariableValue("Discharging");
   var idling = getVariableValue("Idling");
   var balancing = getVariableValue("Balancing");

   if(charging) setVariableValue("BMS_status_string","Charging");
   if(discharging) setVariableValue("BMS_status_string","Discharging");
   if(idling) setVariableValue("BMS_status_string","Idling");
   if(balancing) setVariableValue("BMS_status_string","Balancing");

   if(!(charging || discharging || idling || balancing))
       setVariableValue("BMS_status_string","Off");   
}


// Max and min cell temperature calculation
function calculateMaxMinCellTemp(){
    var cell_temps= [];
    var sum_cell_temps = 0;

    for(i = 0; i < 24; i++){
        cell_temps[i] = getVariableValue("Cell" + String(i+1) + "_temp");
        sum_cell_temps = sum_cell_temps + cell_temps[i];
    }

    var max_cell_temp = Math.max.apply(Math, cell_temps);
    setVariableValue("MaxCell_temp", max_cell_temp);

    var min_cell_temp = Math.min.apply(Math, cell_temps);
    setVariableValue("MinCell_temp", min_cell_temp);
}

// Max tab temperature calculation
function calculateMaxTabTemp(){
    var tabs_temps= [];

    for(i = 0; i < 13; i++){
        tabs_temps[i] = getVariableValue("BusBar" + String(i+1) + "_temp");
    }

    var max_tabs_temp = Math.max.apply(Math, tabs_temps);
    setVariableValue("MaxTab_temp", max_tabs_temp);    
}

function sendMsgsAfter10secs(){
    var time = getVariableValue("elapsed_time_10secs_msg");
    time = time + 1;
    setVariableValue("elapsed_time_10secs_msg",time); 
    
    if(time === 100){ // 10 seconds
       includeFile("activate_encoder.js");
       includeFile("sendPIDgains.js");
       includeFile("send_last_angles.js");
    }
}
        
function messageSuccessCAN(result){
    if (result){
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","CAN message sent successfully!");
   }
   else{
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","Failed to send CAN message!");
       }
}   
        
        
function detectThrottleValueWithoutZero(){
    value = getVariableValue("throttle_value");
    zero = getVariableValue("throttle_zero");
    
    if (value !== 0 || !zero) {
        setVariableValue("Warning_Throttle", 1);
    }
    else{
        setVariableValue("Warning_Throttle", 0);
    }
}

function calculatesolarpower(){
    var voltage = getVariableValue("IVT_Result_U2") * 0.001;
    var current = getVariableValue("IVT_Result_I") * 0.001;
    
    var power = voltage * current;
    
    setVariableValue("solar_power", power);
    print(power);
}


function square(x){
    return x*x;
}