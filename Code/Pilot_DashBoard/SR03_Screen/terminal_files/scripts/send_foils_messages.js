// Get variables
var dist_setpoint = getVariableValue("dist_setpoint");
var front_angle_setpoint = getVariableValue("front_angle_setpoint");
var back_angle_setpoint = getVariableValue("back_angle_setpoint");
var controller_state = getVariableValue("controller_state");
var controller_mode = getVariableValue("controller_mode");
var homing = getVariableValue("homing");
var back_foil_state = getVariableValue("back_foil_state");


var current_threshold = getVariableValue("current_threshold");
var pid_speed_reference = getVariableValue("pid_speed_reference");
var pid_state = getVariableValue("pid_state");


// Separate 2 bytes variables - Big Endian
var back_angle_setpoint_1byte = get1stbyte(back_angle_setpoint);
var back_angle_setpoint_2byte = get2ndbyte(back_angle_setpoint);
var front_angle_setpoint_1byte = get1stbyte(front_angle_setpoint);
var front_angle_setpoint_2byte = get2ndbyte(front_angle_setpoint);

var current_threshold_1byte = get1stbyte(current_threshold);
var current_threshold_2byte = get2ndbyte(current_threshold);
var pid_speed_reference_1byte = get1stbyte(pid_speed_reference);
var pid_speed_reference_2byte = get2ndbyte(pid_speed_reference);


// Concatenate flags
//var state = concatenate(0,0,0,0,controller_state,homing,controller_mode,back_foil_state);
var state = concatenate(0,0,0,0,controller_state,homing,controller_mode,back_foil_state);

// Send messages
var CANPort = 1;
var CANID = 0x625;
var DLC = 6;

var result1 = sendCANMessage(CANPort,
                            CANID,
                            DLC,
                            back_angle_setpoint_1byte,
                            back_angle_setpoint_2byte,
                            front_angle_setpoint_1byte,
                            front_angle_setpoint_2byte,
                            dist_setpoint,
                            state
                            );

var CANPort = 1;
var CANID = 0x605;
var DLC = 5;

var result2 = sendCANMessage(CANPort,
                            CANID,
                            DLC,
                            current_threshold_1byte,
                            current_threshold_2byte,
                            pid_speed_reference_1byte,
                            pid_speed_reference_2byte,  
                            pid_state
                            );
messageSuccessCAN(result1 && result2);


// Reset variables
setVariableValue("controller_state", 0);
setVariableValue("controller_mode", 0);
setVariableValue("homing", 0);



function get1stbyte(variable){
    return ((variable >> 8) & 0xff);
}

function get2ndbyte(variable){
    return variable & 0xff; 
}

function concatenate(b7,b6,b5,b4,b3,b2,b1,b0){
    return (b7<<7) + (b6<<6) + (b5<<5) + (b4<<4) + (b3<<3) + (b2<<2) + (b1<<1) + (b0<<0);
}

function messageSuccessCAN(result){
    if (result){
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","CAN message sent successfully!");
   }
   else{
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","Failed to send CAN message!");
       }
}   
        