// Clock by CAN
year = getVariableValue("year") + 2000;
month = getVariableValue("month");
day = getVariableValue("day");
hour = getVariableValue("hour");
min = getVariableValue("minute");
sec = getVariableValue("second");


date = new Date(year, month, day, hour, min, sec);
time_string = date.toLocaleTimeString(); //hour+':'+min+':'+sec;
date_string = date.toLocaleDateString();
setVariableValue("Date_string_CAN", date_string);
setVariableValue("Time_string_CAN", time_string);