    // Update fault_string based on fault
var fault = getVariableValue("M2_fault");

switch(fault) {
    case 0:
        setVariableValue("M2_Fault_string","NONE");
        break;
    case 1:
        setVariableValue("M2_Fault_string","OVER_VOLTAGE");
        break;        
    case 2:
        setVariableValue("M2_Fault_string","UNDER_VOLTAGE");
        break;        
    case 3:
        setVariableValue("M2_Fault_string","DRV");
        break;        
    case 4:
        setVariableValue("M2_Fault_string","ABS_OVER_CURRENT");
        break;            
    case 5:
        setVariableValue("M2_Fault_string","OVER_TEMP_FET");
        break;            
    case 6:
        setVariableValue("M2_Fault_string","OVER_TEMP_MOTOR");
        break;            
    case 7:
        setVariableValue("M2_Fault_string","GATE_DRIVER_OVER_VOLTAGE");
        break;     
    case 8:
        setVariableValue("M2_Fault_string","GATE_DRIVER_UNDER_VOLTAGE");
        break;            
    case 9:
        setVariableValue("M2_Fault_string","MCU_UNDER_VOLTAGE");
        break;            
    case 10:
        setVariableValue("M2_Fault_string","BOOTING_FROM_WATCHDOG_RESET");
        break;            
    case 11:
        setVariableValue("M2_Fault_string","ENCODER_SPI");
        break;     
    case 12:
        setVariableValue("M2_Fault_string","ENCODER_SINCOS_BELOW_MIN_AMPLITUDE");
        break;            
    case 13:
        setVariableValue("M2_Fault_string","ENCODER_SINCOS_ABOVE_MAX_AMPLITUDE");
        break;            
    case 14:
        setVariableValue("M2_Fault_string","FLASH_CORRUPTION");
        break;            
    case 15:
        setVariableValue("M2_Fault_string","HIGH_OFFSET_CURRENT_SENSOR_1");
        break;     
    case 16:
        setVariableValue("M2_Fault_string","HIGH_OFFSET_CURRENT_SENSOR_2");
        break;            
    case 17:
        setVariableValue("M2_Fault_string","HIGH_OFFSET_CURRENT_SENSOR_3");
        break;            
    case 18:
        setVariableValue("M2_Fault_string","UNBALANCED_CURRENTS");
        break;            
    case 19:
        setVariableValue("M2_Fault_string","BRK");
        break;   
    case 20:
        setVariableValue("M2_Fault_string","RESOLVER_LOT");
        break;     
    case 21:
        setVariableValue("M2_Fault_string","RESOLVER_DOS");
        break;            
    case 22:
        setVariableValue("M2_Fault_string","RESOLVER_LOS");
        break;            
    case 23:
        setVariableValue("M2_Fault_string","FLASH_CORRUPTION_APP_CFG");
        break;            
    case 24:
        setVariableValue("M2_Fault_string","FLASH_CORRUPTION_MC_CFG");
        break;  
    case 25:
        setVariableValue("M2_Fault_string","ENCODER_NO_MAGNET");
        break;     
    default:
        setVariableValue("M2_Fault_string","NOT RECOGNIZED ERROR");
        break;         
}