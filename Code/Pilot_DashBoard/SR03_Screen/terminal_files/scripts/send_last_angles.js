// Get variables
left = getVariableValue("last_left_angle");
right = getVariableValue("last_right_angle");
back = getVariableValue("last_back_angle");

var left_1byte = get1stbyte(left);
var left_2byte = get2ndbyte(left);

var right_1byte = get1stbyte(right);
var right_2byte = get2ndbyte(right);

var back_1byte = get1stbyte(back);
var back_2byte = get2ndbyte(back);


// Send messages
var CANPort = 1;
var CANID = 0x655;
var DLC = 6;

var result = sendCANMessage(CANPort,
                            CANID,
                            DLC,
                            left_1byte,
                            left_2byte,
                            right_1byte,
                            right_2byte,
                            back_1byte,
                            back_2byte
                            );

messageSuccessCAN(result);



function get1stbyte(variable){
    return ((variable >> 8) & 0xff);
}

function get2ndbyte(variable){
    return variable & 0xff; 
}

function messageSuccessCAN(result){
    if (result){
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","CAN message sent successfully!");
   }
   else{
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","Failed to send CAN message!");
       }
}   