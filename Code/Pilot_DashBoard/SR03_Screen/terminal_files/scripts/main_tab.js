//calculateBatteryPower();
calculateMotorPower();
calculateSolarPanelsPower(); 




// Battery Power Calculation
/*
function calculateBatteryPower(){
    var voltage = getVariableValue("Voltage") * 0.002; // Multiplied by scale
    var current = getVariableValue("Current") * 0.01; // Multiplied by scale
    
    var battery_power = voltage * current;
    
    setVariableValue("battery_power",battery_power); // In W 
}
 */

// Motor Power Calculation
function calculateMotorPower(){
    var motor1_voltage = getVariableValue("M1_Voltage") * 0.1; // Multiplied by scale
    var motor1_current = getVariableValue("M1_input_current") * 0.01; // Multiplied by scale

    var motor2_voltage = getVariableValue("M2_Voltage") * 0.1; // Multiplied by scale
    var motor2_current = getVariableValue("M2_input_current") * 0.01; // Multiplied by scale

    var motor_power = (motor1_voltage * motor1_current) + (motor2_voltage * motor2_current);

    setVariableValue("motor_power",motor_power);   // In W 
}

// Solar Panels Power Calculation
function calculateSolarPanelsPower(){
    var solar_current = getVariableValue("Solar_Current") * 0.01; // Multiplied by scale
    var voltage = getVariableValue("Voltage") * 0.002; // Multiplied by scale
    
    var solar_power = voltage * solar_current;

    setVariableValue("solar_power",solar_power);
}

//Speed m/s to knots
function ms2knot(){
    var speed_ms = getVariableValue("Speed");
    var speed_knots = speed_ms * 0.01 * 1.94384449 * 10; // 10 is because of factor
    
    setVariableValue("speed_knots",speed_knots);
}
