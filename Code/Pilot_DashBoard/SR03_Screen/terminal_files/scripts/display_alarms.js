// Read Warnings
Warning_Over_Voltage = getVariableValue("Warning_Over_Voltage");
Warning_Over_Temp = getVariableValue("Warning_Over_Temp");
Warning_Over_Current = getVariableValue("Warning_Over_Current");
Warning_Under_Voltage = getVariableValue("Warning_Under_Voltage");
no_flow = getVariableValue("no_flow");
M1_overtemp_fault = getVariableValue("M1_overtemp_fault");
M1_overtemp_warn = getVariableValue("M1_overtemp_warn");
M2_overtemp_fault = getVariableValue("M2_overtemp_fault");
M2_overtemp_warn = getVariableValue("M2_overtemp_warn");

nr = 0;

// Display Warnings
nr = check_warning(Warning_Over_Voltage, "Battery Overvoltage", nr);
nr = check_warning(Warning_Over_Temp, "Battery Overtemperature", nr);
nr = check_warning(Warning_Over_Current, "Battery Overcurrent", nr);
nr = check_warning(Warning_Under_Voltage, "Battery Undervoltage", nr);
nr = check_warning(no_flow, "0 L/min flow, shutdown mandatory", nr);

nr = check_warning(M1_overtemp_fault, "Low Power Motor above 90 ºC, shutdown mandatory", nr);
nr = check_warning(M1_overtemp_warn, "Low Power Motor above 85 ºC, reduce throttle", nr);
nr = check_warning(M2_overtemp_fault, "High Power Motor above 90 ºC, shutdown mandatory", nr);
nr = check_warning(M2_overtemp_warn, "High Power Motor above 85 ºC, reduce throttle", nr);

// Set number of Warnings
setVariableValue("nr_warnings", nr);

function check_warning(warning, msg, nr){
    if (warning === 1){
        setVariableValue("@AlarmShow",0x8001);
        setVariableValue("warning_string", msg);
        return nr + 1;
    }
    return nr;
}
