var state = getVariableValue("state");
var mode = getVariableValue("mode");

switch(state){
    case 0:
        setVariableValue("foils_state_string","OFF");
        break;
    case 1:
        setVariableValue("foils_state_string","HOMING");
        break;
    case 2:
        setVariableValue("foils_state_string","READY");
        break;
    case 3:
        setVariableValue("foils_state_string","ON");
        break;
    case 4:
        setVariableValue("foils_state_string","ERROR");
        break;
    default:
        setVariableValue("foils_state_string","Undefined State");
        break;
}

switch(mode){
    case 0:
        setVariableValue("foils_mode_string","MANUAL");
        break;
    case 1:
        setVariableValue("foils_mode_string","ROLL");
        break;
    case 2:
        setVariableValue("foils_mode_string","HEAVE");
        break;
    case 3:
        setVariableValue("foils_mode_string","FULL");
        break;
    default:
        setVariableValue("foils_mode_string","UNDEF");
        break;
}


