var angle = getVariableValue("screen_foils_angle_setpoint");
var water_distance = getVariableValue("screen_foils_water_distance_setpoint");

var result = sendCANMessage(1, 0x615, 6, 0x00, 0x00, 0x00, angle, water_distance, 0x00, 0x00, 0x00);

print(angle);

if (result){
    setVariableValue("@AlarmShow",0x8002);
    setVariableValue("sent_message_info_string","CAN message sent successfully!");
}
else{
    setVariableValue("@AlarmShow",0x8002);
    setVariableValue("sent_message_info_string","Failed to send CAN message!");
}
