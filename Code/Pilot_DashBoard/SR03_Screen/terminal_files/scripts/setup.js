// Activate Roll Over
setProperty(443, "Roll Over", 1);
setProperty(359, "Roll Over", 1);
setProperty(371, "Roll Over", 1);
setProperty(61, "Roll Over", 1);
setProperty(367, "Roll Over", 1); // Foils
setProperty(3737, "Roll Over", 1); // PID Gains
setProperty(1519, "Roll Over", 1); // Confirm Alarm
setProperty(1376, "Roll Over", 1); // Foils Gains


// Set Light and mode
setVariableValue("@CurrentThemeIndex",1); // Night mode
setVariableValue("@DispBacklightIntensity",80);
setVariableValue("@KeybBacklightIntensity",0);
setVariableValue("@BeeperMaxVolume", 100);

// Set Time to LowPower and Sleep
setVariableValue("@PWR_TimeToSleepMode", 1);
setVariableValue("@PWR_TimeToPowerOff", 2332800);