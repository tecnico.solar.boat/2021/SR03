var mode = getVariableValue("mode"); //mode ==1: vorgabe, mode ==2: player, mode == 0: init
var pattern = getVariableValue("pattern");
var level = getVariableValue ("level");
var play_level = getVariableValue ("G1_play_level");
var blink = getVariableValue ("G1_blink");
var blinklevel = getVariableValue ("G1_blinklevel");
var repeatrate = getVariableValue ("G1_repeatrate");
var waittimer = getVariableValue ("G1_waittimer");

var buttonbase = 2073;
var show_move = getVariableValue("G1_show_move");
switch (mode)
{
    case 0:
        setVariableValue("G1_buttons_off",1);
        setVariableValue("@Visibility00",0);
        var color_number = Math.floor(Math.random()*(4)) + 1;
        switch (color_number)
        {
            case 1: pattern = pattern + "a"; break;//rot
            case 2: pattern = pattern + "b"; break;//blau
            case 3: pattern = pattern + "c"; break;//gelb
            case 4: pattern = pattern + "d"; break;//gr�n
        }
        setVariableValue ("G1_mode",1);
        setVariableValue ("G1_pattern",pattern);
        setVariableValue ("G1_blinklevel",0);
        setVariableValue ("G1_blink",0);
        break;
    
    case 1:
        if (waittimer == repeatrate)
        {
            setVariableValue("G1_buttons_off",1);
            var current_color = pattern[blinklevel];
            switch (blink)
            {
            case 0: 
                switch (current_color)
                {
                case "a": 
                    current_color_number = 1; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [0] = 255;
                    break;
                case "b": 
                    current_color_number = 2; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [2] = 255;
                    break;
                case "c": 
                    current_color_number = 3; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [0] = 255;
                    color [1] = 255;
                    break;
                case "d": 
                    current_color_number = 4; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [1] = 255;
                    break;    
                }
                setProperty(buttonbase+current_color_number,"Background Color",color);
                blink = 1;
                setVariableValue("G1_show_move",blinklevel+1);
                setVariableValue ("G1_waittimer",1);
                break;
                
            case 1:
                switch (current_color)
                {
                case "a": 
                    current_color_number = 1; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [0] = 100;
                    break;
                case "b": 
                    current_color_number = 2; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [2] = 100;
                    break;
                case "c": 
                    current_color_number = 3; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [0] = 100;
                    color [1] = 100;
                    break;
                case "d": 
                    current_color_number = 4; 
                    var color = getProperty(buttonbase+current_color_number,"Background Color");
                    color [1] = 100;
                    break;    
                }
                setVariableValue ("G1_waittimer",1);
                setProperty(buttonbase+current_color_number,"Background Color",color);
                blinklevel++;
                if (blinklevel == level)//fertig mit zeigen, input mode
                {
                    setVariableValue("G1_mode",2);
                    setVariableValue("@Visibility00",4);
                    setVariableValue("G1_show_move",0);
                    setVariableValue("G1_buttons_off",0);
                    blinklevel = 0;
                }
                else//es kommt noch eine Zahl
                {
                    setVariableValue("G1_buttons_off",1);
                }
                blink = 0;
                break;
            }
        }
        else
        {
            setVariableValue ("G1_waittimer",waittimer+1);
        }
        setVariableValue ("G1_blink",blink);
        setVariableValue ("G1_blinklevel",blinklevel);
        setVariableValue ("G1_play_level",1);
        setVariableValue ("G1_button_pressed","f");
        break;
    
    case 2:
        var button = getVariableValue ("G1_button_pressed");
        var buttons_off = getVariableValue ("G1_buttons_off");
        var right_button = pattern [play_level-1];
        switch (button)
        {
            case "f"://warten
            break;
            
            case right_button://richtig
                setVariableValue("@Visibility00",0);
                setVariableValue("G1_show_move",show_move+1);
                setVariableValue ("G1_play_level",play_level +1);
                setVariableValue ("G1_button_pressed","f");
                if (play_level == level)//alles richtig, neues level
                {
                    setVariableValue("G1_show_move",show_move+1);
                    setVariableValue ("G1_level",level +1);
                    setVariableValue ("G1_mode",0);
                    setVariableValue ("G1_blinklevel",0);
                    setVariableValue ("G1_button_pressed","f");
                    setVariableValue ("G1_play_level",1);
                }
                break;
                
            default://falsch
                var number_games = getVariableValue ("G1_number_games");
                setVariableValue("G1_show_move",0);
                setVariableValue ("G1_number_games",number_games+1);
                setVariableValue("@Visibility00",1);
                setVariableValue("G1_buttons_off",1);
                var record = getVariableValue(("G1_record" + repeatrate));
                if (record < level-1)
                {
                    setVariableValue(("G1_record" + repeatrate),level-1);
                }
                setVariableValue ("G1_mode",3);
		setProperty(2072, "G1_Flashing", true);
                setVariableValue("G1_stage_vis", 0);
        }
        break;  
    case 4:
        break;
}
    




