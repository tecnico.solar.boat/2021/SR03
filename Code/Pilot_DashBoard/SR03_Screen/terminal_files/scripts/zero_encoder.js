// Send message
var CANPort = 1;
var CANID = 0x609;
var DLC = 8;

var result = sendCANMessage(CANPort,
                            CANID,
                            DLC,
                            0x23,
                            0x03,
                            0x60,
                            0x00,
                            0x00,
                            0x00,
                            0x00,
                            0x00
                            );
messageSuccessCAN(result);

function messageSuccessCAN(result){
    if (result){
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","CAN message sent successfully!");
   }
   else{
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","Failed to send CAN message!");
       }
}   