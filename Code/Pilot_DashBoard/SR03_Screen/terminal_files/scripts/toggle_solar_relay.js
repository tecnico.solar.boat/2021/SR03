// Send messages
var CANPort = 1;
var CANID = 0x635;
var DLC = 6;

var result = sendCANMessage(CANPort,
                            CANID,
                            DLC
                            );
messageSuccessCAN(result);


function messageSuccessCAN(result){
    if (result){
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","CAN message sent successfully!");
   }
   else{
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","Failed to send CAN message!");
       }
}   
        