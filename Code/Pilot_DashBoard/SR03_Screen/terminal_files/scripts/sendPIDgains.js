// Get variables
KD = getVariableValue("PID_KD");
KI = getVariableValue("PID_KI");
KP = getVariableValue("PID_KP");

var KD_1byte = get1stbyte(KD);
var KD_2byte = get2ndbyte(KD);

var KI_1byte = get1stbyte(KI);
var KI_2byte = get2ndbyte(KI);

var KP_1byte = get1stbyte(KP);
var KP_2byte = get2ndbyte(KP);


// Send messages
var CANPort = 1;
var CANID = 0x645;
var DLC = 6;

var result = sendCANMessage(CANPort,
                            CANID,
                            DLC,
                            KI_1byte,
                            KI_2byte,
                            KD_1byte,
                            KD_2byte,
                            KP_1byte,
                            KP_2byte
                            );

messageSuccessCAN(result);



function get1stbyte(variable){
    return ((variable >> 8) & 0xff);
}

function get2ndbyte(variable){
    return variable & 0xff; 
}

function messageSuccessCAN(result){
    if (result){
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","CAN message sent successfully!");
   }
   else{
       setVariableValue("@AlarmShow",0x8002);
       setVariableValue("sent_message_info_string","Failed to send CAN message!");
       }
}   