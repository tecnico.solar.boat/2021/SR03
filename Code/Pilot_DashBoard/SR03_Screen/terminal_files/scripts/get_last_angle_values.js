// Get variables
left = getVariableValue("left_angle");
right = getVariableValue("right_angle");
back = getVariableValue("back_angle");

// Set variables
setVariableValue("last_left_angle", left);
setVariableValue("last_right_angle", right);
setVariableValue("last_back_angle", back);