/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Arduino.h>


byte status[] = {0xAC, 0x00,0x01, 0x05, 0x7D, 0x00, 0x03, 0x0F 0x00, 0x75, 0xAD};

byte SOC_Voltage_Currents[] = {0xAC, 0x00,0x02, 0x08, 0x19,0x84, 0xB0,0x43, 0x0B,0xC4, 0x75,0xA9, 0x77, 0xAD};

byte cellVoltage0[] = {0xAC, 0x00,0x03, 0x08, 0x0D,0xEF, 0x0D,0xF0, 0x0D,0xF1, 0x0D,0xF2, 0x17, 0xAD};
byte cellVoltage1[] = {0xAC, 0x00,0x04, 0x08, 0x0D,0xEF, 0x0D,0xF0, 0x0D,0xF1, 0x0D,0xF2, 0x10, 0xAD};
byte cellVoltage2[] = {0xAC, 0x00,0x05, 0x08, 0x0D,0xEF, 0x0D,0xF0, 0x0D,0xF1, 0x0D,0xF2, 0x11, 0xAD};

byte SolarPanelVoltages0[] = {0xAC, 0x00,0x06, 0x08, 0x56,0x6B, 0x56,0x6C, 0x56,0x6D, 0x56,0x6E, 0x0A, 0xAD};
byte SolarPanelVoltages1[] = {0xAC, 0x00,0x07, 0x02, 0x56,0x6F, 0x3C ,0xAD};

byte temperatures0[] = {0xAC, 0x00,0x08, 0x08, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x08, 0xAD};
byte temperatures1[] = {0xAC, 0x00,0x09, 0x08, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x39, 0xAD};
byte temperatures2[] = {0xAC, 0x00,0x0A, 0x08, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x0A, 0xAD};
byte temperatures3[] = {0xAC, 0x00,0x0B, 0x08, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x3B, 0xAD};


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  // Send status
  for(size_t i = 0; i < sizeof(status); i++)
  {
    Serial.write(status[i]);
  }
  delay(50); // Be gentil for testing
  
  // Send SOC_Voltage_Currents
  for(size_t i = 0; i < sizeof(SOC_Voltage_Currents); i++)
  {
    Serial.write(SOC_Voltage_Currents[i]);
  }
  delay(50); // Be gentil for testing

  // Send cellVoltage0
  for(size_t i = 0; i < sizeof(cellVoltage0); i++)
  {
    Serial.write(cellVoltage0[i]);
  }
  delay(50); // Be gentil for testing
  
  // Send cellVoltage1
  for(size_t i = 0; i < sizeof(cellVoltage1); i++)
  {
    Serial.write(cellVoltage1[i]);
  }
  delay(50); // Be gentil for testing
  
  // Send cellVoltage2
  for(size_t i = 0; i < sizeof(cellVoltage2); i++)
  {
    Serial.write(cellVoltage2[i]);
  }
  delay(50); // Be gentil for testing
  
  // Send SolarPanelVoltages0
  for(size_t i = 0; i < sizeof(SolarPanelVoltages0); i++)
  {
    Serial.write(SolarPanelVoltages0[i]);
  }
  delay(50); // Be gentil for testing

  // Send SolarPanelVoltages0
  for(size_t i = 0; i < sizeof(SolarPanelVoltages1); i++)
  {
    Serial.write(SolarPanelVoltages1[i]);
  }
  delay(50); // Be gentil for testing

  // Send temperatures0
  for(size_t i = 0; i < sizeof(temperatures0); i++)
  {
    Serial.write(temperatures0[i]);
  }
  delay(50); // Be gentil for testing

  // Send temperatures1
  for(size_t i = 0; i < sizeof(temperatures1); i++)
  {
    Serial.write(temperatures1[i]);
  }
  delay(50); // Be gentil for testing

  // Send temperatures2
  for(size_t i = 0; i < sizeof(temperatures2); i++)
  {
    Serial.write(temperatures2[i]);
  }
  delay(50); // Be gentil for testing

  // Send temperatures0
  for(size_t i = 0; i < sizeof(temperatures3); i++)
  {
    Serial.write(temperatures3[i]);
  }
  delay(50); // Be gentil for testing


  delay(1000); // Be gentil for testing
}