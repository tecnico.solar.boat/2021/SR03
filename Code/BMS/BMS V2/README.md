# BMSSPV V2

Copyright (C) 2021  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute 
it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat

TSB BMS V2 code.

### ❗️ In order to properly view the comments in the code install VSCode's Better Comments Extension.

### This code was developed by our colleague Sebastião Beirão during his Master Thesis entitled Battery Management System for Solar Powered Vehicles applied to Técnico Solar Boat prototype which is publicly available [here](https://fenix.tecnico.ulisboa.pt/cursos/meec/dissertacao/846778572213540).


