/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

/**
 * @file BMS.cpp
 * @author Sebastião Beirão (sebastiao.beirao@tecnico.ulisboa.pt)
 * @brief This file contains the main BMS functions, it uses the functions provied by Analog Devices to communicate with the LTC68111 and than it implements TSB's own logic to get all the necessary information from the battery.
 * @version BMSV2
 * @date 2021-12-22
 * 
 * @copyright Copyright (c) 2021 Técnico Solar Boat
 * 
 */

#include "BMS.h"

uint8_t num_cells_balancing = 0; // Number of cells that are currently balancing
/**
 * @brief Array to hold the balancing status of each cell
 * 
 */
bool cells_balancing[12] = {false,false,false,false,false,false,false,false,false,false,false,false};

/**
 * @brief Variable from the NTC_Thermistor to compute the temperature value acquired form the different thermistors
 * 
 */
NTC_Thermistor *t_ambient;
NTC_Thermistor *t_HS_1;
NTC_Thermistor *t_HS_2;
NTC_Thermistor *t_tabs;
NTC_Thermistor *t_body;

/**
 * @brief This function should be called in the Setup() and it initialized all the Teensy's pins gives them a pre-defined state, initialized the NTC_Thermistor variables and initialies the LTC6811 itself 
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 */
void BMS_init( cell_asic ic[] ){
	setOutput(EXTERNAL_LED);
	setOutput(BUZZER);
	setOutput(FANS);
	setOutput(CTRL_SD);
	setOutput(CS_SD);
	setOutput(LED_BUILTIN);
	setOutput(MAIN_RELAY);
	setOutput(PRE_CHARGE_RELAY);
	setOutput(CHARGER_RELAY);
	setOutput(SOLAR_RELAY);
	setOutput(CTRL_K5); // * Even though its not being used should be configures as OUTPUT to reduce power while sleeping see: https://forum.pjrc.com/threads/23660-Low-Power-quot-Green-quot-Battery-Operation-Solutions-For-The-Teensy-3?p=34235&viewfull=1#post34235
	setOutput(SOLAR_MOS);
	setInput(SENSE_ON);
	setInput(SENSE_MOTOR);
	setInput(SENSE_CHARGER);
	// // setInput(SENSE_SPARE); Currently not being used
	setInput(PreChargeEndSignal);
	setOutput(MS0);
	setOutput(MS1);
	setOutput(MS2);
	setInputPullDown(VUSB);
	
	output_low(FANS);

	output_low(MAIN_RELAY);
	output_low(PRE_CHARGE_RELAY);
	output_low(CHARGER_RELAY);
	output_low(SOLAR_RELAY);
	output_low(SOLAR_MOS);

	output_low(MS0);
	output_low(MS1);
	output_low(MS2);

	// Teensy ADC configs
	analogReadRes(12); // Set Teensy's ADC resolution to 12 Bits
	analogReadAveraging(10); // Enable 10 samples averaging the the Teensy's ADC readings
	
	// Thermistor configuration:
	// Thermistors directly connected to the Teensy's ADC
	t_ambient = new NTC_Thermistor(AMBIENT, REF_AMBIENT, NOM_AMBIENT, T0, BETA_AMBIENT, ADC_RESOLUTION);
	t_HS_1 = new NTC_Thermistor(HEATSINK_1, REF_HS, NOM_HS, T0, BETA_HS, ADC_RESOLUTION);
	t_HS_2 = new NTC_Thermistor(HEATSINK_2, REF_HS, NOM_HS, T0, BETA_HS, ADC_RESOLUTION);
	// Thermistors acquired by the LTC6811 whose value is then computed to a temperature by the Teensy
	t_tabs = new NTC_Thermistor(REF_TAB, NOM_TAB, T0, BETA_TAB, REF_VOLTAGE_TEMPS);
	t_body = new NTC_Thermistor(REF_BODY, NOM_BODY, T0, BETA_BODY, REF_VOLTAGE_TEMPS);
	
	// Make noise to signalize that I'm working
	tone(BUZZER,700);
	delay (50);
	noTone(BUZZER);
	delay (50);
	tone(BUZZER,2000);
	delay (75);
	noTone(BUZZER);

	SPI.setSCK(SPI_SCK); // SPI_CLK pin is different than the default one so that the Teensy's LED is not constantly ON
	spi_enable(SPI_CLOCK_DIV16); // * This will set the Teensy's SPI to have a 1MHz Clock
	
	// Initialize LTC6811 registers
	LTC681x_init_cfg(TOTAL_IC, ic);
	LTC6811_reset_crc_count(TOTAL_IC,ic);
	LTC6811_init_reg_limits(TOTAL_IC,ic);
	wakeup_sleep(TOTAL_IC);
	LTC6811_clrcell();
	wakeup_sleep(TOTAL_IC);
	LTC6811_clraux();
	wakeup_sleep(TOTAL_IC);
	LTC6811_clrstat();
}

/**
 * @brief Set over and under voltage threshold on the ell_assic struct
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 */
void BMS_set_OV_UV( cell_asic ic[] ){
	for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++){
		LTC681x_set_cfgr_uv(current_ic, ic, UV_THRESHOLD);
		LTC681x_set_cfgr_ov(current_ic, ic, OV_THRESHOLD);
	}
	BMS_Wake_Write_Config(ic);
}

/**
 * @brief Wake up the LTC6811 and write its configuration to it. The LTC6811 enters a sleep state if we don't communicate with it for more than 2 second and resets its configuration registers. So each time we communicate with it it's better to wake it up and write the configuration to it
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 */
void BMS_Wake_Write_Config( cell_asic ic[] ){
	wakeup_sleep(TOTAL_IC);
	LTC6811_wrcfg(TOTAL_IC, ic);
}

/**
 * @brief Runs the different self tests that the LTC681 offers and reports the result to the @param error
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param error variable that holds the LTC_Status
 */
void BMS_selfCheck( cell_asic ic[], uint16_t *error ){
	/*
	Bit0: errors detected in Digital Filter and CELL Memory;
	Bit1: errors detected in Digital Filter and AUX Memory;
	Bit2: errors detected in Digital Filter and STAT Memory;
	Bit3: Mux Test;
	Bit4: ADC Overlap self test;
	Bit5: AUX Measurement;
	Bit6: STAT Measurement;
	Bit7: Open wire error;
	Bit8: Thermal shutdown status (1 means True 0 means False);
	Bit9: A PEC error was detected in the received data;
	Bit10: Unused;
	Bit11: Unused;
	Bit12 to Bit15: Cell that is open, this is a 4 bit decimal number to indicate the cell connection that is open.
	*/
	//! The configuration is not written in this function because we want the ADC options to have the default value 7 kHz
	int8_t aux = 0;
	// ADCs Self Test Cell Memory
	wakeup_sleep(TOTAL_IC);
	aux = LTC6811_run_cell_adc_st(CELL,TOTAL_IC,ic);
	if ( aux != 0 ) {
		*error = *error | 0x0001;
		aux = 0;
	}
	// ADCs Self Test AUX Memory
	wakeup_sleep(TOTAL_IC);
	aux = LTC6811_run_cell_adc_st(AUX,TOTAL_IC, ic);
	if ( aux != 0 ) {
		*error = *error | 0x0002;
		aux = 0;
	}
	// ADCs Self Test Stat Memory
	wakeup_sleep(TOTAL_IC);
	aux = LTC6811_run_cell_adc_st(STAT,TOTAL_IC, ic);
	if ( aux != 0 ) {
		*error = *error | 0x0004;
		aux = 0;
	}
	// Mux Self Test
	wakeup_sleep(TOTAL_IC);
	LTC6811_diagn();
	delay(5);
	aux = LTC6811_rdstat(0,TOTAL_IC,ic); // Set to read back all aux registers
	if ( aux != 0 ) { // PEC Error detected
		*error = *error | 0x0200;
		aux = 0;
	}
	for (int iccount = 0; iccount<TOTAL_IC; iccount++)
	{
		if (ic[iccount].stat.mux_fail[0] != 0) aux++;
	}
	if ( aux != 0 ) {
		*error = *error | 0x0008;
		aux = 0;
	}

	// Run ADC Overlap self test
	wakeup_sleep(TOTAL_IC);
	aux = (int8_t)LTC6811_run_adc_overlap(TOTAL_IC,ic);
	if ( aux != 0 ) {
		*error = *error | 0x0010;
		aux = 0;
	}

	// ADC Digital Redundancy self test
	wakeup_sleep(TOTAL_IC);
	aux = LTC6811_run_adc_redundancy_st(ADC_CONVERSION_MODE,AUX,TOTAL_IC, ic);
	if ( aux != 0 ) {
		*error = *error | 0x0020;
		aux = 0;
	}

	wakeup_sleep(TOTAL_IC);
	aux = LTC6811_run_adc_redundancy_st(ADC_CONVERSION_MODE,STAT,TOTAL_IC, ic);
	if ( aux != 0 ) {
		*error = *error | 0x0040;
		aux = 0;
	}

	// Open wire test
	// ! Only detects one open cell if multiple are open
	// TODO: Update Analog Library so that multiple open cells can be detected
	LTC6811_run_openwire(TOTAL_IC, ic);
	for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
	{
		if (ic[current_ic].system_open_wire != 0)
		{
			*error = *error | 0x0080;
			for (int cell=0; cell<ic[0].ic_reg.cell_channels+1; cell++)
			{
				if ((ic[current_ic].system_open_wire &(1<<cell))>0)
				{
					*error = *error | (cell << 12 );
				}
			}
		}
	}

	// LTC6811 Thermal Shutdown
	wakeup_sleep(TOTAL_IC);
	LTC6811_rdstat(2,TOTAL_IC,ic); // ? Is this really necessary here ?!?!
	delay(200); // ? Is this really necessary here ?!?!
	aux = LTC6811_rdstat(2,TOTAL_IC,ic); // Set to read back all aux registers
	if ( aux != 0 ) { // PEC Error detected
		*error = *error | 0x0200;
		aux = 0;
	}
	if ( ic[0].stat.thsd[0] != 0 ){
		*error = *error | 0x0100;
	}

	// Serial.print("BMS_selfCheck: ");
	// Serial.println(*error);
	// Send error to serial
	// Trigger buzzer if error != 0 and block code
	// TODO: Decide what to do if errors are detected, errors detected here are most probably critical... 
}

/**
 * @brief Read the cell's voltages and retrived the actual current measured by the Isabellenhütte's current shunts form the boat's CAN Bus.
 * ! Use this function if Isabellenhütte's current shunts are used
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param error variable that holds the LTC_Status
 * @param current Currents struct to save the currents in
 * @param canBus TSB_CAN object so that the currents can be retrived from it
 * @retval true If PEC errors are detected
 * @retval false If no PEC errors are detected
 */
bool BMS_readCells( cell_asic ic[], uint16_t *error, Currents *current, TSB_CAN *canBus){
	int8_t PEC_error = 0;
	// // Debug stuff
		// //uint32_t conv_time = 0;

	// Send command to measure the all cells
	BMS_Wake_Write_Config( ic );
	LTC6811_adcv(ADC_CONVERSION_MODE, ADC_DCP, CELL_CH_ALL);
	LTC6811_pollAdc();

	// // Debug stuff
		// // conv_time = LTC6811_pollAdc();
		// // Serial.print(F("cell conversion completed in:"));
		// // Serial.print(((float)conv_time/1000), 1);
		// // Serial.println(F("mS"));
		// // Serial.println();

	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdcv(0, TOTAL_IC,ic); // Set to read back all cell voltage registers
	bms_check_error(PEC_error);
	// // Debug stuff
		// // bms_print_cells( ic );

	if ( PEC_error != 0) { // PEC errors detected
		*error = *error | 0x0200;
		// Send Status Message 0x01
		// Buzzer
		return true;
	}

	// Retrive Isabellenhütte's current shunts values from the CAN Bus:
	current -> bat = canBus->shunt_bat.current.value / 1000.0; // Convert to Amperes
	current -> solar = canBus->shunt_solar.current.value / 1000.0; // Convert to Amperes
	
	return false;
}

/**
 * @brief This is the function to be used if the in the case analog current transducers are used. It measures both the cells and GPIO 1 and 2 so that synchronizing the battery voltage measured with the battery current is possible.
 * ! Use this function if Analog Current Transducers are used
 *     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// !!!! KEEP IN MIND THAT IN ORDER TO USE THIS SOME JUMPERS SHOULD BE BROKEN ON THE PCB  !!!
	// !!!! REFER TO THE SCHEMATIC IN ORDER TO DO SO 										 !!!
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param error variable that holds the LTC_Status
 * @param current Currents struct to save the currents in
 * @retval true If PEC errors are detected
 * @retval false If no PEC errors are detected
 */
bool BMS_readCells_GPIO_1_2( cell_asic ic[], uint16_t *error, Currents *current){
	
	int8_t PEC_error = 0;
	// // Debug stuff
		// // uint32_t conv_time = 0;

	// Send command to measure the Cells and GPIO 1 and 2 voltages
	BMS_Wake_Write_Config( ic );
	LTC6811_adcvax(ADC_CONVERSION_MODE, ADC_DCP);
	LTC6811_pollAdc();
	// // Debug stuff
		// // conv_time = LTC6811_pollAdc();
		// // Serial.print(F("cell conversion completed in:"));
		// // Serial.print(((float)conv_time/1000), 1);
		// // Serial.println(F("mS"));
		// // Serial.println();

	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdcv(0, TOTAL_IC,ic); // Set to read back all cell voltage registers
	bms_check_error(PEC_error);
	// // Debug stuff
		// // bms_print_cells( ic );

	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdaux(0,TOTAL_IC,ic); // Set to read back all aux registers
	bms_check_error(PEC_error);
	// // Debug stuff
		// //bms_print_aux( ic );

	if ( PEC_error != 0) {
		*error = *error | 0x0200;
		// Send Status Message 0x01
		// Buzzer
		return true;
	}

	// This code convers the voltage reading of GPIOs 1 and 2 of LTC to current in case
	// an analog current transducer is use. The values shown are for LEM DHAB S/143
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// !!!! KEEP IN MIND THAT IN ORDER TO USE THIS SOME JUMPERS SHOULD BE BROKEN ON THE PCB  !!!
	// !!!! REFER TO THE SCHEMATIC IN ORDER TO DO SO 										 !!!
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// For the High currnet channel the Sensitivity is 5.2 mV/A
	// For the Low currnet channel the Sensitivity is 40 mV/A
	// The result of this equation corresponds to 10 times the real current, for example 498 corresponds a 49,8 A
	current -> batHigh = (int16_t)roundf (((500.0/13.0)*(5*(ic[0].aux.a_codes[1]/10000.0)-14))*10);
	current -> batLow =  (int16_t)roundf ((-(125.0/2.0 - 25 * ic[0].aux.a_codes[0]/10000.0))*10);
	if (current -> batLow > 498 || current -> batLow < -498) // ABS( Battery current) > 50 -> use high current channel
	{
		current -> bat = current -> batHigh;
	}
	else{ //  ABS( Battery current) < 50 -> use low current channel
		current -> bat = current -> batLow;
	}
	if( current -> bat < -5384){ // Sensor is not connected -5384 !!! (means that the signal is pulled low to GND)
		current -> bat = 0; // ! (maybe this should not be considered as 0, if sensor gets disconnected during operation something must be done)
	}
	return false;
}

/**
 * @brief Functions to check if any cell needs to be balanced. The function first computes the mininum and maximum cell's voltage and than checks if this value is above or below the imbalance_thershold.
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param min minimum cell voltage
 * @retval true If battery needs to be balanced
 * @retval false If battery is already balanced
 */
bool BMS_needs_balance( cell_asic ic[], uint16_t *min ){
	uint16_t max = 0;
	uint16_t imbalance = 0;
	*min = OV_THRESHOLD;
	for(int i = 0; i < ic[0].ic_reg.cell_channels; i++){
		if ( ic[0].cells.c_codes[i] > max ){
			max = ic[0].cells.c_codes[i];
		}
		if ( ic[0].cells.c_codes[i] < *min ){
			*min = ic[0].cells.c_codes[i];
		}
	}
	imbalance = max - *min;
	// // Debug stuff
		// // Serial.println((String) "Imbalance = " + imbalance);
		// // Serial.println((String) "Minimum Cell Voltage is: " + *min);
	if ( imbalance > imbalance_thershold) {
		// // Serial.println("I need balance");
		return true;
	}
	// // Serial.println("I don't need balance");
	return false;
}

/**
 * @brief Activates balance on cells that need it. The functions loops through all cells, first checking if that cell is already balancing, if it is not it checks if the voltage of that cell is above the minimum voltage for balancing, if the cell voltage is above the lowest cell by more than the imbalance threshold, if the number of cell is below the maximum allowed, if the balancing resistors thermistors temperature is below the maximum allowed. If all the conditions are satisfied the function activates the balancing load on that cell and procedes to check the next cell. After checking all cells it writes the configuration to the BMS so that the balance effectively starts.
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param min minimum cell voltage as computed by BMS_needs_balance
 */
void BMS_balance( cell_asic ic[], uint16_t min ){
	// // Serial.println(num_cells_balancing);
	for(int i = 0; i < ic[0].ic_reg.cell_channels; i++){
		// // if (ic[0].cells.c_codes[i] > ( min + imbalance_thershold) )
		// // Serial.println((String)"Cell " + (i+1) + " needs to balance!");
		if ( cells_balancing[i] == false ){
			if ( ic[0].cells.c_codes[i] > balancingThreshold ){
				if ( ic[0].cells.c_codes[i] > ( min + imbalance_thershold) ){
					if ( num_cells_balancing < MAX_CELLS_BALANCING ){
						if ( temps.heatsink_1 < (heatsink_warn_temp - 10) && temps.heatsink_2 < (heatsink_warn_temp - 10) ){
							// // Serial.println((String)"Cell " + (i+1) + " is set to balance!");
							LTC6811_set_discharge(i+1,TOTAL_IC,ic);
							num_cells_balancing++;
							cells_balancing[i] = true;
							// * +1 because LTC6811_set_discharge is done to receive cell number from 1 to 12
						}	
					}
				}	
			}
		}
	}
	BMS_Wake_Write_Config( ic );
}

/**
 * @brief Actives the balancing load of the cells that are in an overvoltage state
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 */
void BMS_discharge_OV( cell_asic ic[] ){
	for(int i = 0; i < ic[0].ic_reg.cell_channels; i++){
		if ( ic[0].cells.c_codes[i] > OV_THRESHOLD ){
			LTC6811_set_discharge(i+1,TOTAL_IC,ic);
			// * +1 because LTC6811_set_discharge is done to receive cell number from 1 to 12
		}
	}
}

// 
/**
 * @brief Turns off the balancing load of the cells that are already within the imbalance_thershold or bellow minimum voltage allowed for balancing.
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param setpoint minimum cell voltage as computed by BMS_needs_balance
 */
void BMS_check_balance( cell_asic ic[], uint16_t setpoint){
	bool haveChanged = false;
	// // Serial.println((String)"Cells Balancing: " + num_cells_balancing);
	for(int i = 0; i < ic[0].ic_reg.cell_channels; i++){
		// // Serial.print((String)"\nCell: " + i + " if condition: ");
		// // Serial.println(ic[0].cells.c_codes[i] <= balancingThreshold && cells_balancing[i]==true);
		if ( ic[0].cells.c_codes[i] <= balancingThreshold && cells_balancing[i]==true ){
			// // Serial.println((String)"Cell " + (i+1) + " stoped balancing! " + "Due to being below " + balancingThreshold + " V" );
			haveChanged = true;
			LTC6811_clear_discharge(i+1,TOTAL_IC,ic);
			num_cells_balancing--;
			// //Serial.println((String)"Cells Balancing after: " + num_cells_balancing); 
			cells_balancing[i]=false;
			// * +1 because LTC6811_clear_discharge is done to receive cell number from 1 to 12
		}
		if (  (ic[0].cells.c_codes[i] <= ( setpoint + (imbalance_thershold))) && cells_balancing[i]==true ) {
			
			// // Serial.println((String)"Cell " + (i+1) + " stoped balancing! " + ic[0].cells.c_codes[i] + " " + ( setpoint + (imbalance_thershold-80)) );
			haveChanged = true;
			LTC6811_clear_discharge(i+1,TOTAL_IC,ic);
			num_cells_balancing--;
			// // Serial.println((String)"Cells Balancing after: " + num_cells_balancing); 
			cells_balancing[i]=false;
			// * +1 because LTC6811_clear_discharge is done to receive cell number from 1 to 12
		}	
	}
	if (haveChanged) { // If anything changes, than write configuration the the LTC6811
		BMS_Wake_Write_Config(ic);
		BMS_balance( ic, minimumCellVoltage );
	}
}

/**
 * @brief Checks if there are still any cell balancing and updates to cells balancing 0 and 1 so that it matches the cells that are actually being balanced
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param cells0 uint8_t from the CAN / Serial status message byte 3 (Check communication documentation in doc folder)
 * @param cells1 uint8_t from the CAN / Serial status message byte 4 (Check communication documentation in doc folder)
 * @retval true If there are still cells balancing
 * @retval false If no cells are balancing
 */
bool BMS_any_balancing(cell_asic ic[], uint8_t *cells0, uint8_t *cells1  ){
	bool bal[12];
	
	for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
	{
		uint16_t current_mask = 0x01;
		for(int i = 0; i < 8; i++)
		{
			bal[i] = (ic[current_ic].config.rx_data[4] & current_mask) >> i;
			current_mask = current_mask << 1;
		}
		current_mask = 0x01;
		for(int i = 8; i < 12; i++)
		{
			bal[i] = (ic[current_ic].config.rx_data[5] & current_mask) >> (i-8);
			current_mask = current_mask << 1;
		}
		
		for(int i = 0; i < 8; i++)
		{
			if (bal[i]) {
				*cells0 ^= (-1 ^ *cells0) & (1UL << i);
			}
			else{
				*cells0 ^= (-0 ^ *cells0) & (1UL << i);
			}
				
		}
		for(int i = 8; i < 12; i++)
		{
			if (bal[i]) {
				*cells1 ^= (-1 ^ *cells1) & (1UL << (i-8));
			}
			else{
				*cells1 ^= (-0 ^ *cells1) & (1UL << (i-8));
			}		
		}

		
		for(int i = 0; i < 12; i++)
		{
			if (bal[i]) {
				return true;
			}
				
		}
		return false;
	}
	return false;
}

/**
 * @brief Stops balancing in all cells by clearing the DCC bit of the configuration registers and writing the configuration the the LTC6811
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 */
void BMS_clear_discharge(cell_asic ic[]){
	num_cells_balancing = 0;
	for (uint8_t i = 0; i < sizeof(cells_balancing); i++){
		cells_balancing[i] = false;
	}
	for (int i=0; i<TOTAL_IC; i++)
	{
		ic[i].config.tx_data[4] = 0;
		ic[i].config.tx_data[5] = 0;
	}
	wakeup_sleep(TOTAL_IC);
	LTC6811_wrcfg(TOTAL_IC,ic);
	LTC6811_rdcfg(TOTAL_IC, ic);
}

/**
 * @brief Instructs the LTC6811 to measure the SOC (Sum of All Cells), Die temperature, VregA, VregD and GPIOs 1 to 5
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param error variable that holds the LTC_Status
 * @retval true If there are PEC errors
 * @retval false If there are no PEC errors
 */
bool BMS_read_aux_GPIO_1_to_5( cell_asic ic[], uint16_t *error){
	int8_t PEC_error = 0;
	
	// // wakeup_sleep(TOTAL_IC);
	// // LTC6811_clraux();
	// // LTC6811_clrstat();

	BMS_Wake_Write_Config( ic );
	LTC6811_adax(ADC_CONVERSION_MODE, AUX_CH_ALL); // This will convert GPIOs 1-5 and Vref2
	LTC6811_pollAdc();

	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdaux(REG_ALL,TOTAL_IC,ic);
	bms_check_error(PEC_error);
	// // bms_print_aux( ic );

	wakeup_sleep(TOTAL_IC);
	LTC6811_adstat(ADC_CONVERSION_MODE, STAT_CH_TO_CONVERT);
	LTC6811_pollAdc();

	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdstat(REG_ALL,TOTAL_IC,ic);
	bms_check_error(PEC_error);
	// // bms_print_stat( ic );
	
	if ( PEC_error != 0) {
		*error = *error | 0x0200;
		// Send Status Message 0x01
		// Buzzer
		return true;
	}
	return false;
}

/**
 * @brief Instructs the LTC6811 to measure the SOC (Sum of All Cells), Die temperature, VregA, VregD and GPIOs 3 to 5
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param error variable that holds the LTC_Status
 * @retval true If there are PEC errors
 * @retval false If there are no PEC errors
 */
bool BMS_read_aux_GPIO_3_to_5( cell_asic ic[], uint16_t *error, Currents *current){
	int8_t PEC_error = 0;
	
	// // wakeup_sleep(TOTAL_IC);
	// // LTC6811_clraux();
	// // LTC6811_clrstat();

	BMS_Wake_Write_Config( ic );
	for(uint8_t CH_TO_CONVERT = 3; CH_TO_CONVERT < 6; CH_TO_CONVERT++){ // This will convert GPIO 3 to 5
		LTC681x_adaxd(MD_7KHZ_3KHZ, CH_TO_CONVERT);
		LTC6811_pollAdc();
	}


	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdaux(0,TOTAL_IC,ic);
	bms_check_error(PEC_error);
	// // bms_print_aux( ic );

	wakeup_sleep(TOTAL_IC);
	LTC6811_adstat(MD_7KHZ_3KHZ, STAT_CH_TO_CONVERT);
	LTC6811_pollAdc();

	wakeup_sleep(TOTAL_IC);
	PEC_error = LTC6811_rdstat(0,TOTAL_IC,ic);
	bms_check_error(PEC_error);
	// // bms_print_stat( ic );
	
	if ( PEC_error != 0) {
		*error = *error | 0x0200;
		// Send Status Message 0x01
		// Buzzer
		return true;
	}

	return false;
}

/**
 * @brief Checks the over and under voltage flags of the LTC6811
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param warn uint8_t from the CAN / Serial status message byte 5 (Check communication documentation in doc folder)
 * @param error variable that holds the LTC_Status
 * @retval TSB_NONE if there are no nither over or under voltage
 * @retval OVER_VOLTAGE in case of an over voltage situation
 * @retval UNDER_VOLTAGE in case of an under voltage situation
 */
uint8_t BMS_OV_UV( cell_asic ic[], uint8_t *warn, uint16_t *error){
	int8_t PEC_error = 0;
	uint8_t aux = TSB_NONE;
	bool OV[12];
	bool UV[12];

	BMS_Wake_Write_Config( ic );
	PEC_error = LTC6811_rdstat(REG_2,TOTAL_IC,ic); // Set to read back all aux registers
	// // LTC6811_rdcfg(1,ic);
	// // bms_print_rxconfig( ic );
	// // bms_print_stat(ic);
	if ( PEC_error != 0) { // PEC Error detected
		*error = *error | 0x0200;
		// Send Status Message 0x01
		// Buzzer
	}
	
	for (int current_ic =0 ; current_ic < TOTAL_IC; current_ic++)
	{
		for(int i = 0, k = 0; i < 4; i++, k++)
		{
			UV[i] = ( ic[current_ic].stat.flags[0] >> k ) & 0x01;
			k++;
			OV[i] = ( ic[current_ic].stat.flags[0] >> k ) & 0x01;	
		}
		for(int i = 4, k = 0; i < 8; i++, k++)
		{
			UV[i] = ( ic[current_ic].stat.flags[1] >> k ) & 0x01;
			k++;
			OV[i] = ( ic[current_ic].stat.flags[1] >> k ) & 0x01;		
		}
		for(int i = 8, k = 0; i < 12; i++, k++)
		{
			UV[i] = ( ic[current_ic].stat.flags[2] >> k ) & 0x01;
			k++;
			OV[i] = ( ic[current_ic].stat.flags[2] >> k ) & 0x01;
		}
		
		for(int i = 0; i < 12; i++)
		{
			if (OV[i]!= 0) {
				*warn = *warn | 0x04;
				aux = OVER_VOLTAGE;
				break;
			}
		}
		for(int i = 0; i < 12; i++)
		{
			if (UV[i]!= 0) {
				*warn = *warn | 0x08;
				aux = UNDER_VOLTAGE;
				break;
			}
		}
	}
	return aux;
} 

/**
 * @brief Check if any cells have reached the fully charged state, by checking if its voltage is >= OV_THRESHOLD
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @retval true if any cell is >= OV_THRESHOLD
 * @retval false if any cell is < OV_THRESHOLD
 */
bool BMS_is_cell_charged( cell_asic ic[]){
	for (int i=0; i< ic[0].ic_reg.cell_channels; i++){
		if (ic[0].cells.c_codes[i] >= OV_THRESHOLD) {
			return true;
		}
	}
	return false;
}

/**
 * @brief This functions reads all the temperatures measured both by the Teensy and the LTC6811. It also updates the variable that hold the maximum temperature measured and the maximum cell temperature measured.
 * 
 * @param temps Struct containing all the measured temperatures
 * @param ic cell_assic struct containing the battery / LTC6811 data
 * @param current current mux channel selected
 */
void BMS_read_temps(Temperatures *temps, cell_asic ic[], uint8_t current){
	temps -> maxTemp = 0;
	temps -> maxTempCells = 0;
	temps -> ambient = (uint8_t) t_ambient->readCelsius(); temps -> maxTemp = max(temps -> maxTemp, temps -> ambient);
	temps -> heatsink_1 = (uint8_t) t_HS_1->readCelsius(); temps -> maxTemp = max(temps -> maxTemp, temps -> heatsink_1);
	temps -> heatsink_2 = (uint8_t) t_HS_2->readCelsius(); temps -> maxTemp = max(temps -> maxTemp, temps -> heatsink_2);

	switch (current){
	case 0:
		temps -> bus_bars[10] = (uint8_t) t_tabs->readCelsiusFromADC( (double) ic[0].aux.a_codes[0] / 10000.0 );
		temps -> bus_bars[3] = (uint8_t) t_tabs->readCelsiusFromADC( (double) ic[0].aux.a_codes[1] / 10000.0 );

		temps -> cells[10] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[2] / 10000.0 );
		temps -> cells[3] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[3] / 10000.0 );
		temps -> cells[21] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[4] / 10000.0 );
		break;
	case 1:
		temps -> bus_bars[12] = (uint8_t) t_tabs->readCelsiusFromADC( (double)  ic[0].aux.a_codes[0] / 10000.0 );
		temps -> bus_bars[5] = (uint8_t)  t_tabs->readCelsiusFromADC( (double) ic[0].aux.a_codes[1] / 10000.0 );

		temps -> cells[12] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[2] / 10000.0 );
		temps -> cells[5] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[3] / 10000.0 );
		temps -> cells[19] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[4] / 10000.0 );
		break;
	case 2:
		// // temps -> bus_bars[T39] = ; Unused thermistor input
		temps -> bus_bars[7] = (uint8_t) t_tabs->readCelsiusFromADC( (double)  ic[0].aux.a_codes[1] / 10000.0 );

		temps -> cells[14] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[2] / 10000.0 );
		temps -> cells[7] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[3] / 10000.0 );
		temps -> cells[17] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[4] / 10000.0 );
		break;
	case 3:
		temps -> bus_bars[8] = (uint8_t) t_tabs->readCelsiusFromADC( (double)  ic[0].aux.a_codes[0] / 10000.0 );
		temps -> bus_bars[1] = (uint8_t) t_tabs->readCelsiusFromADC( (double)  ic[0].aux.a_codes[1] / 10000.0 );

		temps -> cells[8] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[2] / 10000.0 );
		temps -> cells[1] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[3] / 10000.0 );
		temps -> cells[23] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[4] / 10000.0 );
		break;
	case 4:
		temps -> bus_bars[0] = (uint8_t) t_tabs->readCelsiusFromADC( (double)  ic[0].aux.a_codes[0] / 10000.0 );
		temps -> bus_bars[9] = (uint8_t) t_tabs->readCelsiusFromADC( (double)  ic[0].aux.a_codes[1] / 10000.0 );

		temps -> cells[0] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[2] / 10000.0 );
		temps -> cells[9] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[3] / 10000.0 );
		temps -> cells[16] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[4] / 10000.0 );
		break;
	case 5:
		temps -> bus_bars[6] = (uint8_t) t_tabs->readCelsiusFromADC( (double)  ic[0].aux.a_codes[0] / 10000.0 );
		// // temps -> bus_bars[T40] = ; Unused thermistor input

		temps -> cells[6] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[2] / 10000.0 );
		temps -> cells[15] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[3] / 10000.0 );
		temps -> cells[20] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[4] / 10000.0 );
		break;
	case 6:
		temps -> bus_bars[2] = (uint8_t) t_tabs->readCelsiusFromADC( (double)  ic[0].aux.a_codes[0] / 10000.0 );
		temps -> bus_bars[11] = (uint8_t) t_tabs->readCelsiusFromADC( (double)  ic[0].aux.a_codes[1] / 10000.0 );

		temps -> cells[2] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[2] / 10000.0 );
		temps -> cells[11] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[3] / 10000.0 );
		temps -> cells[18] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[4] / 10000.0 );
		break;
	case 7:
		temps -> bus_bars[4] = (uint8_t) t_tabs->readCelsiusFromADC( (double)  ic[0].aux.a_codes[0] / 10000.0 );
		// // temps -> bus_bars[T38] = ; Unused thermistor input

		temps -> cells[4] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[2] / 10000.0 );
		temps -> cells[13] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[3] / 10000.0 );
		temps -> cells[22] = (uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[4] / 10000.0 );
		break;
	}

	// // Debug code to print the temperatures measured. It was used to check that readCelsiusFromADC was properly working
		// // Serial.println();
		// // Serial.println((String) "Current Temp: " + current + " -> " + ic[0].aux.a_codes[4] / 10000.0);
		// // Serial.println((uint8_t) t_body->readCelsiusFromADC( (double) ic[0].aux.a_codes[4] / 10000.0 ));
		// // Serial.println();

		// // for (size_t i = 0; i < 13; i++){
		// // 	Serial.print("\t\t\tB ");
		// // 	Serial.print(i+1);
		// // 	Serial.print(": ");
		// // 	Serial.print(temps->bus_bars[i]);
		// // 	if ((i+1)%5 == 0)
		// // 		Serial.println();	
		// // }
		// // Serial.println();
		// // Serial.println();

		// // for (size_t i = 0; i < 24; i++){
		// // 	Serial.print("\t\tC ");
		// // 	Serial.print(i+1);
		// // 	Serial.print(": ");
		// // 	Serial.print(temps->cells[i]);
		// // 	if ((i+1)%5 == 0)
		// // 		Serial.println();
		// // }
		// // Serial.println();
		// // Serial.println();
	

	for (uint8_t i = 0; i < 24; i++){
		temps -> maxTemp = max(temps -> maxTemp, temps -> cells[i]);
		temps -> maxTempCells = max(temps -> maxTempCells, temps -> cells[i]);
	}
	for (uint8_t i = 0; i < 13; i++){
		temps -> maxTemp = max(temps -> maxTemp, temps -> bus_bars[i]);
		temps -> maxTempCells = max(temps -> maxTempCells, temps -> bus_bars[i]);
	}

}

/**
 * @brief Checks all temperatures agains their respective threshold
 * 
 * @retval true if any of the temperatures is above the over temperature threshold
 * @retval false if no over temperature has been detected
 */
bool BMS_check_temperatures(){
	if ( temps.ambient > warning_temp || temps.LTC > warning_temp ){
		return true;
	}
	for (uint8_t i = 0; i < 24; i++){
		if (temps.cells[i] > warning_temp){
			return true;
		}
	}
	for (uint8_t i = 0; i < 13; i++){
		if (temps.bus_bars[i] > warning_temp){
			return true;
		} 	
	}
	if ( temps.heatsink_1 > heatsink_warn_temp || temps.heatsink_2 > heatsink_warn_temp ){
		return true;
	}
	return false;	
}

/**
 * @brief Prints the voltage of each cell to the serial port.
 * ! DEBUG FUNCTION NOT TO BE USED IN PRODUCTION CODE
 *
 * @param ic cell_assic struct containing the battery / LTC6811 data
 */
void bms_print_cells( cell_asic ic[] )
{
	for (int current_ic = 0 ; current_ic < TOTAL_IC; current_ic++)
	{	
		Serial.print(" IC ");
		Serial.print(current_ic+1,DEC);
		Serial.print(", ");
		for (int i=0; i< ic[0].ic_reg.cell_channels; i++)
		{

			Serial.print(" C");
			Serial.print(i+1,DEC);
			Serial.print(":");
			Serial.print(ic[current_ic].cells.c_codes[i]*0.0001,4);
			Serial.print(",");
		}
		Serial.println();
		
	}
	Serial.println();
}

/**
 * @brief Prints the voltages of the GPIOs to the serial port.
 * ! DEBUG FUNCTION NOT TO BE USED IN PRODUCTION CODE
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 */
void bms_print_aux( cell_asic ic[] )
{
	for (int current_ic =0 ; current_ic < TOTAL_IC; current_ic++)
	{
		Serial.print(" IC ");
		Serial.print(current_ic+1,DEC);
		for (int i=0; i < 5; i++) // Ler ate 5 para ler o resto dos GPIOS
		{
			Serial.print(F(" GPIO-"));
			Serial.print(i+1,DEC);
			Serial.print(":");
			Serial.print(ic[current_ic].aux.a_codes[i]*0.0001,4);
			Serial.print(",");
		}
		// Serial.print(F(" Vref2"));
		// Serial.print(":");
		// Serial.print(ic[current_ic].aux.a_codes[5]*0.0001,4);
		// Serial.println();
	}
	Serial.println();
}

/**
 * @brief Prints the voltages stored in the STAT registers to the serial port.
 * ! DEBUG FUNCTION NOT TO BE USED IN PRODUCTION CODE
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 */
void bms_print_stat( cell_asic ic[] )
{

	for (int current_ic =0 ; current_ic < TOTAL_IC; current_ic++)
	{
		Serial.print(F(" IC "));
		Serial.print(current_ic+1,DEC);
		Serial.print(F(" SOC:"));
		Serial.print(ic[current_ic].stat.stat_codes[0]*0.0001*20,4);
		Serial.print(F(","));
		Serial.print(F(" Itemp:"));   
		Serial.print(ic[current_ic].stat.stat_codes[1]*0.0001,4);
		Serial.print(F(","));
		Serial.print(F(" LTC Temp:"));   
		Serial.print(ic[current_ic].stat.stat_codes[1]*0.01333333-273,4);
		Serial.print(F(","));
		Serial.print(F(" VregA:"));
		Serial.print(ic[current_ic].stat.stat_codes[2]*0.0001,4);
		Serial.print(F(","));
		Serial.print(F(" VregD:"));
		Serial.print(ic[current_ic].stat.stat_codes[3]*0.0001,4);
		Serial.println();
		Serial.print(F(" Flags:"));
		Serial.print(F(" 0x"));
		serial_print_hex(ic[current_ic].stat.flags[0]);
		Serial.print(F(", 0x"));
		serial_print_hex(ic[current_ic].stat.flags[1]);
		Serial.print(F(", 0x"));
		serial_print_hex(ic[current_ic].stat.flags[2]);
		Serial.print(F("   Mux fail flag:"));
		Serial.print(F(" 0x"));
		serial_print_hex(ic[current_ic].stat.mux_fail[0]);
		Serial.print(F("   THSD:"));
		Serial.print(F(" 0x"));
		serial_print_hex(ic[current_ic].stat.thsd[0]);
		Serial.println("\n");  
	}

	Serial.println();
}

/**
 * @brief Prints the configuration data that was read back from the LTC6811 to the serial port.
 * ! DEBUG FUNCTION NOT TO BE USED IN PRODUCTION CODE
 * 
 * @param ic cell_assic struct containing the battery / LTC6811 data
 */
void bms_print_rxconfig( cell_asic ic[] )
{
  Serial.println(F("Received Configuration "));
  for (int current_ic=0; current_ic<TOTAL_IC; current_ic++)
  {
    Serial.print(F("CFGA IC "));
    Serial.print(current_ic+1,DEC);
    for(int i = 0; i < 6; i++)
    {
      Serial.print(F(", 0x"));
      serial_print_hex(ic[current_ic].config.rx_data[i]);
    }
    Serial.print(F(", Received PEC: 0x"));
    serial_print_hex(ic[current_ic].config.rx_data[6]);
    Serial.print(F(", 0x"));
    serial_print_hex(ic[current_ic].config.rx_data[7]);
    Serial.println("\n");

	float vuv = ( ( ( ( ic[current_ic].config.rx_data[2] & 0x0F ) << 8 ) | ic[current_ic].config.rx_data[1] ) + 1 ) * 16 / 10000.0;
	float vov = ( ( ic[current_ic].config.rx_data[3] << 4 ) | (( ic[current_ic].config.rx_data[2] & 0xF0 ) >> 4) ) * 16 / 10000.0;
	Serial.print("Over Voltage Threshold: ");
	Serial.print(vov, 4);
	Serial.print(" Under Voltage Threshold: ");
	Serial.println(vuv, 4);
  }
}

/**
 * @brief Function to print in HEX form
 * ! DEBUG FUNCTION NOT TO BE USED IN PRODUCTION CODE
 * 
 * @param data 
 */
void serial_print_hex(uint8_t data){
  if (data< 16)
  {
    Serial.print("0");
    Serial.print((byte)data,HEX);
  }
  else
    Serial.print((byte)data,HEX);
}

/**
 * @brief Function to check PEC error flag and print PEC error message to the serial port.
 * 
 * @param error PEC_error variable as returned from the functions that interact with th LTC6811
 */
void bms_check_error(int error)
{
	if (error == -1)
	{
		Serial.println(F("A PEC error was detected in the received data"));
	}
}