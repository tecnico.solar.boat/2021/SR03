/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

/**
 * @file WDT.c
 * @author Sebastião Beirão (sebastiao.beirao@tecnico.ulisboa.pt)
 * @brief This file defines the routines to handle the Watch Dog Timer.
 *        This file is based on the information found on: 
 *        https://bigdanzblog.wordpress.com/2017/10/27/watch-dog-timer-wdt-for-teensy-3-1-and-3-2/
 * @version BMSV2
 * @date 2021-12-22
 * 
 * @copyright Copyright (c) 2021 Técnico Solar Boat
 * 
 */

#include "WDT.h"

/**
 * @brief Setup Watch Dog Timer, should only be called once
 * 
 */
void WDT_setup(){
	// Setup WDT
    noInterrupts();                                         // don't allow interrupts while setting up WDOG
    WDOG_UNLOCK = WDOG_UNLOCK_SEQ1;                         // unlock access to WDOG registers
    WDOG_UNLOCK = WDOG_UNLOCK_SEQ2;
    delayMicroseconds(1);                                   // Need to wait a bit..

    // We will use 1 second WDT timeout (e.g. you must kick the dog in < 1 sec or a reboot occurs)
    // With a prescale of 0x400 the WDT timer runs at 7.2 MHz so to do 1 s we need 7 200 000 ticks -> 0x006DDD00 
    WDOG_TOVALH = 0x006d;
    WDOG_TOVALL = 0xdd00;

    // This sets prescale clock so that the watchdog timer ticks at 7.2MHz
    WDOG_PRESC  = 0x400;

    // Set options to enable WDT. You must always do this as a SINGLE write to WDOG_CTRLH
    WDOG_STCTRLH |= WDOG_STCTRLH_ALLOWUPDATE |
        WDOG_STCTRLH_WDOGEN | WDOG_STCTRLH_WAITEN |
        WDOG_STCTRLH_STOPEN | WDOG_STCTRLH_CLKSRC;
        // To call WDOG interrupt also do an or with:
        //WDOG_STCTRLH_IRQRSTEN;            // Tell WDOG to trigger the interrupt
	// and call (void watchdog_isr() need to be difined):
	// NVIC_ENABLE_IRQ(IRQ_WDOG);       		// enable the call to the watchdog_isr function

    
    interrupts();
}

/**
 * @brief Kick the dog. Call this in the loop, otherwise the Teensy will reset
 * 
 */
void WDT_kick(){
	noInterrupts();                                     
    WDOG_REFRESH = 0xA602;
    WDOG_REFRESH = 0xB480;
    interrupts();
}

/**
 * @brief Disable the wathchdog timer
 * 
 */
void WDT_disable(){
	NVIC_DISABLE_IRQ(IRQ_WDOG);
	noInterrupts(); // don't allow interrupts while setting up WDOG
	WDOG_UNLOCK = WDOG_UNLOCK_SEQ1; // unlock access to WDOG registers
	WDOG_UNLOCK = WDOG_UNLOCK_SEQ2;
	delayMicroseconds(1); // Need to wait a bit..

	// Set options to disable WDT. You must always do this as a SINGLE write to WDOG_CTRLH
	WDOG_STCTRLH = WDOG_STCTRLH_ALLOWUPDATE;
	
	interrupts();
}

/**
 * @brief Call this in the setup and it will print the reason for the last reset.
 * 
 */
void WDT_reason_last_reset(){
    Serial.println();
    Serial.println("Reason for last Reset: ");

    if (RCM_SRS1 & RCM_SRS1_SACKERR)   Serial.println("Stop Mode Acknowledge Error Reset");
    if (RCM_SRS1 & RCM_SRS1_MDM_AP)    Serial.println("MDM-AP Reset");
    if (RCM_SRS1 & RCM_SRS1_SW)        Serial.println("Software Reset");                   // reboot with SCB_AIRCR = 0x05FA0004
    if (RCM_SRS1 & RCM_SRS1_LOCKUP)    Serial.println("Core Lockup Event Reset");
    if (RCM_SRS0 & RCM_SRS0_POR)       Serial.println("Power-on Reset");                   // removed / applied power
    if (RCM_SRS0 & RCM_SRS0_PIN)       Serial.println("External Pin Reset");               // Reboot with software download
    if (RCM_SRS0 & RCM_SRS0_WDOG)      Serial.println("Watchdog(COP) Reset");              // WDT timed out
    if (RCM_SRS0 & RCM_SRS0_LOC)       Serial.println("Loss of External Clock Reset");
    if (RCM_SRS0 & RCM_SRS0_LOL)       Serial.println("Loss of Lock in PLL Reset");
    if (RCM_SRS0 & RCM_SRS0_LVD)       Serial.println("Low-voltage Detect Reset");
    Serial.println();

}