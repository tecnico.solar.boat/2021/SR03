/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

/**
 * @file WDT.h
 * @author Sebastião Beirão (sebastiao.beirao@tecnico.ulisboa.pt)
 * @brief This file defines the routines to handle the Watch Dog Timer.
 *        This file is based on the information found on: 
 *        https://bigdanzblog.wordpress.com/2017/10/27/watch-dog-timer-wdt-for-teensy-3-1-and-3-2/
 * @version BMSV2
 * @date 2021-12-22
 * 
 * @copyright Copyright (c) 2021 Técnico Solar Boat
 * 
 */

#ifndef WDT_H
#define WDT_H

#include <Arduino.h>
#include <kinetis.h>

void WDT_setup();
void WDT_kick();
void WDT_disable();
void WDT_reason_last_reset();

#endif //WDT_H