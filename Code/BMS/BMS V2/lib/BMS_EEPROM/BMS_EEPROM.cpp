/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

/**
 * @file BMS_EEPROM.cpp
 * @author Sebastião Beirão (sebastiao.beirao@tecnico.ulisboa.pt)
 * @brief  This files holds the code for the BMS's EEPROM related stuff
 * @version BMSV2
 * @date 2021-12-22
 * 
 * @copyright Copyright (c) 2021 Técnico Solar Boat
 * 
 */

#include <BMS_EEPROM.h>

/**
 * @brief Calculates the CRC of the data in the EEPROM so that it can be 
 * later compared with the CRC values stored in the EEPROM
 * 
 * @param length number of EEPROM bytes to calculate the CRC for. // ! Always starts at position 0
 * @return unsigned long calculated CRC
 */
unsigned long eeprom_crc( uint16_t length ){
  unsigned long crc = ~0L;

  for( unsigned int index = 0 ; index < length  ; ++index ){
    crc = crc_table[( crc ^ EEPROM[index] ) & 0x0f] ^ (crc >> 4);
    crc = crc_table[( crc ^ ( EEPROM[index] >> 4 )) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }
  return crc;
}

/**
 * @brief Save BMS parameters to the Teesny's EEPROM
 * 
 */
void saveToEEPROM(){
    
    EEPROM.put(BALANCING_ALLOWED_ADDR, balancingAllowed);
    EEPROM.put(BALANCING_THRESH_ADDR, balancingThreshold);
    EEPROM.put(IMBALANCE_THRESH_ADDR, imbalance_thershold);
    EEPROM.put(OV_THRESH_ADDR, OV_THRESHOLD);
    EEPROM.put(UV_THRESH_ADDR, UV_THRESHOLD);
    EEPROM.put(WARN_TEMP_ADDR, warning_temp);
    EEPROM.put(DOC_ADDR, DOC);
    EEPROM.put(COC_ADDR, COC);
    EEPROM.put(REFRESH_RATE_ADDR, refresh_rate);
    EEPROM.put(EEPROM_CRC_ADDR, eeprom_crc(EEPROM_CRC_ADDR));
}

/**
 * @brief Read BMS parameters from the Teensy's EEPROM. This checks whether the data is valid or not.
 * @if data is valid
 * data is restored from the EEPROM
 * @else 
 * default values are loaded and saved to the EEPROM.
 * @endif 
 */
void readFromEEPROM(){
    unsigned long crc = ~0L;
    EEPROM.get(EEPROM_CRC_ADDR, crc);
    if ( crc == eeprom_crc(EEPROM_CRC_ADDR) ){
        EEPROM.get(BALANCING_ALLOWED_ADDR, balancingAllowed); 
        EEPROM.get(BALANCING_THRESH_ADDR, balancingThreshold); 
        EEPROM.get(IMBALANCE_THRESH_ADDR, imbalance_thershold);
        EEPROM.get(OV_THRESH_ADDR, OV_THRESHOLD);
        EEPROM.get(UV_THRESH_ADDR, UV_THRESHOLD);
        EEPROM.get(WARN_TEMP_ADDR, warning_temp);
        EEPROM.get(DOC_ADDR, DOC);
        EEPROM.get(COC_ADDR, COC);
        EEPROM.get(REFRESH_RATE_ADDR, refresh_rate);
    }
    else{
        // Serial.println("EEPROM CRC Error!");
        clearEEPROM();
        setDefaults();
        saveToEEPROM();
    }
}

/**
 * @brief Set BMS parameteres to their default values
 * 
 */
void setDefaults(){
    balancingAllowed = false;
    balancingThreshold = 37000;
    imbalance_thershold = 100;
    OV_THRESHOLD = 42000; // Over voltage threshold ADC Code. If OV_THRESHOLD is not divisible by 16 it will be rounded
    UV_THRESHOLD = 36000; // Under voltage threshold ADC Code. If (UV_THRESHOLD - 1) is not divisible by 16 it will be rounded
    warning_temp = 55;
    DOC = 300; // Maximum discharge current
    COC = 33.6; // Maximum charge current
    refresh_rate = 300; // Unit is ms
}

/**
 * @brief Set all EEPROM positions to 0
 * 
 */
void clearEEPROM(){
    for (int i = 0 ; i < EEPROM.length() ; i++) {
        EEPROM.update(i, 0);
    }
}