/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "CAN_TSB.h"

int32_t ind = 0;

TSB_CAN::TSB_CAN()
{
	this->bms.cellvoltages = new float[12]();
	this->bms.solar_voltages = new float[5]();
	this->bms.temperatures = new float[41]();
	this->bms.cells_balancing = new bool[12]();
	this->screen.solarRelay = false;
	this->screen.pumpMOSFET = false;

	//initialize array of valid nodes
	for (int i = 0; i < MAX_NUM_NODES; i++)
		this->received_time[i] = 0;
}

void TSB_CAN::receive_Message(CAN_message_t &frame)
{
	//validate received node id into valid nodes array
	this->received_time[frame.id & MAX_NUM_NODES] = millis();

	switch (frame.id)
	{

		//*****************
		//****** Throttle ******
		//*****************

		case 0x10:
			ind = 0;
			this->throttle.value = buffer_get_int8(frame.buf, &ind);
			this->throttle.status = buffer_get_uint8(frame.buf, &ind);
			break;

		//*****************
		//****** BMS ******
		//*****************

		case 0x601:
			ind = 0;
			this->bms.status = buffer_get_uint8(frame.buf, &ind);
			this->bms.ltc_status = buffer_get_uint16(frame.buf, &ind);
			cells_balancing = buffer_get_uint8(frame.buf, &ind);
			for(int i = 0; i< 12; i++){
				this->bms.cells_balancing[i] = (bool)(cells_balancing >> i);
			}
			this->bms.warnings = buffer_get_uint8(frame.buf, &ind);
			break;

		case 0x611:
			ind = 0;
			this->bms.soc = buffer_get_float16(frame.buf, 1e2, &ind);
			this->bms.voltage = buffer_get_float16(frame.buf, 5e2, &ind);
			this->bms.current = buffer_get_float16(frame.buf, 1e2, &ind);
			this->bms.solar_current = buffer_get_float16(frame.buf, 1e2, &ind);
			break;

		case 0x621:
			for(int i=0; i<4; i++){
				this->bms.cellvoltages[i] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/10000.0;
			}
			break;

		case 0x631:
			for(int i=0; i<4; i++){
				this->bms.cellvoltages[i+4] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/10000.0;
			}
			break;

		case 0x641:
			for(int i=0; i<4; i++){
				this->bms.cellvoltages[i+8] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/10000.0;
			}
			break;

		case 0x651:
			for(int i=0; i<4; i++){
				this->bms.solar_voltages[i] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/1000.0;
			}
			break;

		case 0x661:
			this->bms.solar_voltages[4] = (uint16_t)((frame.buf[0]<<8) | frame.buf[1])/1000.0;
			break;

		case 0x671:
			ind = 0;
			for (int i = 0; i < 8; i++)
			{
				this->bms.temperatures[i] = buffer_get_uint8(frame.buf, &ind);
			}
			break;

		case 0x681:
			ind = 0;
			for (int i = 8; i < 16; i++)
			{
				this->bms.temperatures[i] = buffer_get_uint8(frame.buf, &ind);
			}
			break;

		case 0x691:
			ind = 0;
			for (int i = 16; i < 24; i++)
			{
				this->bms.temperatures[i] = buffer_get_uint8(frame.buf, &ind);
			}
			break;

		case 0x6A1:
			ind = 0;
			for (int i = 24; i < 32; i++)
			{
				this->bms.temperatures[i] = buffer_get_uint8(frame.buf, &ind);
			}
			break;

		case 0x6B1:
			ind = 0;
			for (int i = 32; i < 37; i++)
			{
				this->bms.temperatures[i] = buffer_get_uint8(frame.buf, &ind);
			}
			bms.maxTemp = buffer_get_uint8(frame.buf, &ind);
			break;

		case 0x6C1:
			ind = 0;
			for (int i = 37; i < 41; i++)
			{
				this->bms.temperatures[i] = buffer_get_uint8(frame.buf, &ind);
			}
			break;

		//*********************
		//******* Foils *******
		//*********************

		case 0x602:
			ind = 0;
			this->foils.dist = buffer_get_float16(frame.buf, 1e2, &ind);
			this->foils.left_angle = buffer_get_float16(frame.buf, 1e2, &ind);
			this->foils.right_angle = buffer_get_float16(frame.buf, 1e2, &ind);
			this->foils.back_angle = buffer_get_float16(frame.buf, 1e2, &ind);
			break;

		case 0x612:
			ind = 0;
			this->foils.state = buffer_get_uint8(frame.buf, &ind);
			this->foils.mode = buffer_get_uint8(frame.buf, &ind);
			this->foils.height_ref = buffer_get_uint8(frame.buf, &ind);
			this->foils.angle_ref = buffer_get_float16(frame.buf, 1e1, &ind);
			this->foils.speed = buffer_get_float16(frame.buf, 1e2, &ind);
			this->foils.teensyTemp = buffer_get_uint8(frame.buf, &ind);
			break;

		case 0x622:
			ind = 0;
			this->foils.fake_dist=buffer_get_float16(frame.buf, 1e2, &ind);
			break;
		//*********************
		//****** Motores ******
		//*********************

		case 0x901:
			ind = 0;		
			this->motor1.motor_current = buffer_get_float32(frame.buf, 1e2, &ind);
			this->motor1.input_current = buffer_get_float32(frame.buf, 1e2, &ind);
			break;

		case 0xE01:
			ind = 0;
			this->motor1.duty_cycle = buffer_get_float16(frame.buf, 1e3, &ind);
			this->motor1.rpm = buffer_get_int32(frame.buf, &ind)/10;
			this->motor1.voltage = buffer_get_float16(frame.buf, 1e1, &ind);
			break;

		case 0xF01:
			ind = 0;
			this->motor1.temp_mos_1 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor1.temp_mos_2 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor1.temp_mos_3 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor1.temp_mos_max = buffer_get_float16(frame.buf, 1e1, &ind);
			break;
		case 0x1001:
			ind = 0;
			this->motor1.fault = buffer_get_uint8(frame.buf, &ind);
			this->motor1.id = buffer_get_uint8(frame.buf, &ind);
			this->motor1.temp_mot = buffer_get_float16(frame.buf, 1e1, &ind);
			break;
		case 0x902:
			ind = 0;		
			this->motor2.motor_current = buffer_get_float32(frame.buf, 1e2, &ind);
			this->motor2.input_current = buffer_get_float32(frame.buf, 1e2, &ind);
			break;

		case 0xE02:
			ind = 0;
			this->motor2.duty_cycle = buffer_get_float16(frame.buf, 1e3, &ind);
			this->motor2.rpm = buffer_get_int32(frame.buf, &ind)/10;
			this->motor2.voltage = buffer_get_float16(frame.buf, 1e1, &ind);
			break;

		case 0xF02:
			ind = 0;
			this->motor2.temp_mos_1 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor2.temp_mos_2 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor2.temp_mos_3 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor2.temp_mos_max = buffer_get_float16(frame.buf, 1e1, &ind);
			break;
		case 0x1002:
			ind = 0;
			this->motor2.fault = buffer_get_uint8(frame.buf, &ind);
			this->motor2.id = buffer_get_uint8(frame.buf, &ind);
			this->motor2.temp_mot = buffer_get_float16(frame.buf, 1e1, &ind);
			break;

		//*********************
		//*** Xsens MTi680G ***
		//*********************
		#if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
		case 0x04:
			ind = 0;
			
				this->ahrs.utc_time.Year = buffer_get_uint8(frame.buf, &ind);
				this->ahrs.utc_time.Month = buffer_get_uint8(frame.buf, &ind);
				this->ahrs.utc_time.Day = buffer_get_uint8(frame.buf, &ind);
				this->ahrs.utc_time.Hour = buffer_get_uint8(frame.buf, &ind);
				this->ahrs.utc_time.Minute = buffer_get_uint8(frame.buf, &ind);
				this->ahrs.utc_time.Second = buffer_get_uint8(frame.buf, &ind);
				this->ahrs.utc_time_centisecond = buffer_get_float16(frame.buf, 1e4, &ind);
				this->ahrs.unix_time = makeTime(ahrs.utc_time);
			

				if (this->receivedTime == false) // Sets RTC Clock time when time is received from GPS and creates new log file
				{
					uint32_t time = ahrs.unix_time;
					// set the Time library to use Teensy 3.0's RTC to keep time
					setSyncProvider(getTeensy3Time);
					if (timeStatus() != timeSet)
					{
						//Serial.println("Unable to sync with the RTC");
					}
					else
					{
						//Serial.println("RTC has set the system time");
					}
					const signed long DEFAULT_TIME = 1357041600; // Jan 1 2013
					if (time < DEFAULT_TIME)
					{			   // check the value is a valid time (greater than Jan 1 2013)
						time = 0L; // return 0 to indicate that the time is not valid
					}
					else
					{
						this->receivedTime = true;
					}
					Teensy3Clock.set(time); // set the RTC
					setTime(time);
					// this->screen.newFile = true;  If SD is used this variable should be used
				}
			break;
			#endif

		case 0x434:
			ind = 0;
			this->ahrs.roll = buffer_get_float16(frame.buf, pow(2,7), &ind);
			this->ahrs.pitch = buffer_get_float16(frame.buf, pow(2,7), &ind);
			this->ahrs.yaw = buffer_get_float16(frame.buf, pow(2,7), &ind);
			break;

		case 0x444:
			ind = 0;
			this->ahrs.gyrX = buffer_get_float16(frame.buf, pow(2,9), &ind);
			this->ahrs.gyrY = buffer_get_float16(frame.buf, pow(2,9), &ind);
			this->ahrs.gyrZ = buffer_get_float16(frame.buf, pow(2,9), &ind);
			break;

		case 0x454:
			ind = 0;
			this->ahrs.accX = buffer_get_float16(frame.buf, pow(2,8), &ind);
			this->ahrs.accY = buffer_get_float16(frame.buf, pow(2,8), &ind);
			this->ahrs.accZ = buffer_get_float16(frame.buf, pow(2,8), &ind);
			break;

		case 0x494:
			ind = 0;
			this->ahrs.velX = buffer_get_float16(frame.buf, pow(2,6), &ind);
			this->ahrs.velY = buffer_get_float16(frame.buf, pow(2,6), &ind);
			this->ahrs.velZ = buffer_get_float16(frame.buf, pow(2,6), &ind);
			break;
			
		case 0x4A4:
			ind = 0;
			this->ahrs.free_accX = buffer_get_float16(frame.buf, pow(2,8), &ind);
			this->ahrs.free_accY = buffer_get_float16(frame.buf, pow(2,8), &ind);
			this->ahrs.free_accZ = buffer_get_float16(frame.buf, pow(2,8), &ind);
			break;

		case 0x614:
			ind = 0;
			this->ahrs.status_word = buffer_get_uint32(frame.buf, &ind);
			break;

		case 0x624:
			ind = 0;
			this->ahrs.error_code = buffer_get_uint8(frame.buf, &ind);
			break;

		case 0x664:
			ind = 0;
			this->ahrs.temperature = buffer_get_float16(frame.buf, pow(2,8), &ind);
			break;

		case 0x674:
			ind = 0;
			this->ahrs.lat = buffer_get_float32(frame.buf, pow(2,24), &ind);
			this->ahrs.lon = buffer_get_float32(frame.buf, pow(2,23), &ind);
			break;

		case 0x684:
			ind = 0;
			this->ahrs.magx = buffer_get_float16(frame.buf, pow(2,10), &ind);
			this->ahrs.magy = buffer_get_float16(frame.buf, pow(2,10), &ind);
			this->ahrs.magz = buffer_get_float16(frame.buf, pow(2,10), &ind);
			break;

		//**********************
		//******* Screen *******
		//**********************
		#ifdef IS_MC
			case 0x605:
				ind = 0;
				this->screen.current_threshold = buffer_get_int16(frame.buf, &ind);
				this->screen.pid_speed_setpoint = buffer_get_float16(frame.buf, 1e1, &ind);
				this->screen.pid_state = frame.buf[ind] & 0x01;
				ind++;
				break;
			case 0x645:
				ind = 0;
				this ->screen.speed_Ki = buffer_get_float16(frame.buf, 1e1, &ind);
				this ->screen.speed_Kd = buffer_get_float16(frame.buf, 1e1, &ind);
				this ->screen.speed_Kp = buffer_get_float16(frame.buf, 1e1, &ind);
				this ->screen.newGains = true;
				break;
		#endif
		// case 0x615: If SD card is used, this case should be used
		// 	this->screen.new_kvaser_file = true;
		// 	break;
		#ifdef IS_FOILS
			case 0x625:
				ind = 0;
				this->screen.back_angle_setpoint = buffer_get_float16(frame.buf, 1e1, &ind);
				this->screen.front_angle_setpoint = buffer_get_float16(frame.buf, 1e1, &ind);
				this->screen.dist_setpoint = buffer_get_uint8(frame.buf, &ind);
				this->screen.back_foil_state = frame.buf[ind] & 0x01; 
				this->screen.mode_setpoint = frame.buf[ind] >> 1 & 0x01; 
				this->screen.homing = frame.buf[ind] >> 2 & 0x01; 
				this->screen.controller_state = frame.buf[ind] >> 3 & 0x01;
				ind++;
				break;
			case 0x655:
				ind = 0;
				this->screen.last_left_angle = buffer_get_float16(frame.buf, 1e2, &ind);
				this->screen.last_right_angle = buffer_get_float16(frame.buf, 1e2, &ind);
				this->screen.last_back_angle = buffer_get_float16(frame.buf, 1e2, &ind);
				break;
			case 0x675:
				ind = 0;
				this->screen.heave_gains[0] = buffer_get_float8(frame.buf, 1e1, &ind);
				this->screen.heave_gains[1] = buffer_get_float8(frame.buf, 1e1, &ind);
				this->screen.heave_gains[2] = buffer_get_float8(frame.buf, 1e1, &ind);
				this->screen.heave_gains[3] = buffer_get_float8(frame.buf, 1e1, &ind);
				this->screen.heave_gains[4] = buffer_get_float8(frame.buf, 1e1, &ind);
				this->screen.heave_gains[5] = buffer_get_float8(frame.buf, 1e1, &ind);
				break;
			case 0x685:
				ind = 0;
				this->screen.roll_gains[0] = buffer_get_float8(frame.buf, 1e1, &ind);
				this->screen.roll_gains[1] = buffer_get_float8(frame.buf, 1e1, &ind);
				this->screen.roll_gains[2] = buffer_get_float8(frame.buf, 1e1, &ind);
				break;
		#endif
		#ifdef IS_BMS
			case 0x635:
				this->screen.solarRelay = true;
				break;
		#endif
		#ifdef IS_CCU
			case 0x665:
				this->screen.pumpMOSFET = true;
				break;
		#endif


		//*************************
		//******* Shunt_Bat *******
		//*************************

		case 0x606:
			if (frame.buf[0] == 0x00)
			{
				ind = 2;
				this->shunt_bat.current.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_bat.current.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_bat.current.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_bat.current.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_bat.current.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x616:
			if (frame.buf[0] == 0x01)
			{
				ind = 2;
				this->shunt_bat.voltage1.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_bat.voltage1.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_bat.voltage1.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_bat.voltage1.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_bat.voltage1.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x626:
			if (frame.buf[0] == 0x02)
			{
				ind = 2;
				this->shunt_bat.voltage2.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_bat.voltage2.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_bat.voltage2.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_bat.voltage2.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_bat.voltage2.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x636:
			if (frame.buf[0] == 0x03)
			{
				ind = 2;
				this->shunt_bat.voltage3.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_bat.voltage3.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_bat.voltage3.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_bat.voltage3.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_bat.voltage3.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x646:
			if (frame.buf[0] == 0x04)
			{
				ind = 2;
				this->shunt_bat.temperature.value = buffer_get_int32(frame.buf, &ind) / 10;
				this->shunt_bat.temperature.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_bat.temperature.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_bat.temperature.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_bat.temperature.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x656:
			if (frame.buf[0] == 0x05)
			{
				ind = 2;
				this->shunt_bat.power.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_bat.power.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_bat.power.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_bat.power.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_bat.power.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x666:
			if (frame.buf[0] == 0x06)
			{
				ind = 2;
				this->shunt_bat.As.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_bat.As.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_bat.As.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_bat.As.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_bat.As.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x676:
			if (frame.buf[0] == 0x07)
			{
				ind = 2;
				this->shunt_bat.Wh.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_bat.Wh.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_bat.Wh.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_bat.Wh.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_bat.Wh.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;

		//*********************
		//******* Fuse Board *******
		//*********************

		case 0x607:
			ind = 0;
			this->fuse_board.current_24 = buffer_get_float8(frame.buf, 1e1, &ind);
			this->fuse_board.current_48 = buffer_get_float8(frame.buf, 1e1, &ind);
			break;

		//*********************
		//******* Shunt_Solar *******
		//*********************
		case 0x608:
			if (frame.buf[0] == 0x00)
			{
				ind = 2;
				this->shunt_solar.current.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_solar.current.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_solar.current.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_solar.current.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_solar.current.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x618:
			if (frame.buf[0] == 0x01)
			{
				ind = 2;
				this->shunt_solar.voltage1.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_solar.voltage1.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_solar.voltage1.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_solar.voltage1.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_solar.voltage1.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x628:
			if (frame.buf[0] == 0x02)
			{
				ind = 2;
				this->shunt_solar.voltage2.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_solar.voltage2.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_solar.voltage2.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_solar.voltage2.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_solar.voltage2.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x638:
			if (frame.buf[0] == 0x03)
			{
				ind = 2;
				this->shunt_solar.voltage3.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_solar.voltage3.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_solar.voltage3.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_solar.voltage3.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_solar.voltage3.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x648:
			if (frame.buf[0] == 0x04)
			{
				ind = 2;
				this->shunt_solar.temperature.value = buffer_get_int32(frame.buf, &ind) / 10;
				this->shunt_solar.temperature.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_solar.temperature.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_solar.temperature.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_solar.temperature.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x658:
			if (frame.buf[0] == 0x05)
			{
				ind = 2;
				this->shunt_solar.power.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_solar.power.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_solar.power.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_solar.power.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_solar.power.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x668:
			if (frame.buf[0] == 0x06)
			{
				ind = 2;
				this->shunt_solar.As.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_solar.As.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_solar.As.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_solar.As.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_solar.As.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;
		case 0x678:
			if (frame.buf[0] == 0x07)
			{
				ind = 2;
				this->shunt_solar.Wh.value = buffer_get_int32(frame.buf, &ind);
				this->shunt_solar.Wh.OCS = ((frame.buf[1] >> 4) >> 0) & 0x01;
				this->shunt_solar.Wh.this_result = ((frame.buf[1] >> 4) >> 1) & 0x01;
				this->shunt_solar.Wh.any_result = ((frame.buf[1] >> 4) >> 2) & 0x01;
				this->shunt_solar.Wh.system = ((frame.buf[1] >> 4) >> 3) & 0x01;
			}
			break;

		//*********************
		//******* CCU *******
		//*********************

		case 0x60A:
			ind = 4;
			// this->ccu.temp_M1 = buffer_get_float16(frame.buf, 1e1, &ind);
			// this->ccu.temp_M2 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->ccu.temp_WC_IN = buffer_get_float16(frame.buf, 1e1, &ind);
			this->ccu.temp_WC_OUT = buffer_get_float16(frame.buf, 1e1, &ind);
			break;

		case 0x61A:
			ind = 0;
			this->ccu.pump_pwm = buffer_get_uint8(frame.buf, &ind);
			this->ccu.pump_tach = buffer_get_uint16(frame.buf, &ind);
			this->ccu.water_flow = buffer_get_float8(frame.buf, 1e1, &ind);
			break;

		//********************************
		//******* Steering Encoder *******
		//********************************

		case 0x619:
			ind=0;
			this->encoder.value=buffer_get_int32(frame.buf,&ind);
			break;

		//***********************
		//******* CANWiFi *******
		//***********************

		case 0x60B:
			ind=0;
			this->canWiFi.TeensyTemp=buffer_get_float16(frame.buf,1e1,&ind);
			break;

	}
}

void TSB_CAN::printFrame(CAN_message_t &msg,  int mailbox)
{
	// For AVR:
	#if defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)
	Serial.print("MB: ");
	if (mailbox > -1) Serial.print(mailbox);
	else Serial.print("???");
	Serial.print(" ID: 0x");
	Serial.print(msg.id, HEX);
	Serial.print(" Len: ");
	Serial.print(msg.len);
	Serial.print(" Data: 0x");
	for (int count = 0; count < msg.len; count++) {
		Serial.print(msg.buf[count], HEX);
		Serial.print(" ");
	}
	Serial.print("\r\n");  

	// For Teensy:
	#elif defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
	Serial.print("MB "); Serial.print(msg.mb);
	Serial.print("  OVERRUN: "); Serial.print(msg.flags.overrun);
	Serial.print("  LEN: "); Serial.print(msg.len);
	Serial.print(" EXT: "); Serial.print(msg.flags.extended);
	Serial.print(" TS: "); Serial.print(msg.timestamp);
	Serial.print(" ID: "); Serial.print(msg.id, HEX);
	Serial.print(" Buffer: ");
	for ( uint8_t i = 0; i < msg.len; i++ ) {
		Serial.print(msg.buf[i], HEX); Serial.print(" ");
	} Serial.println();
	#endif
}

#if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
bool TSB_CAN::frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller)
{
		// printFrame(frame, mailbox);
		receive_Message(frame);
		return true;
}

#elif defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)
void TSB_CAN::gotFrame(CAN_message_t *frame, int mailbox)
{
	// printFrame(*frame, mailbox);
	receive_Message(*frame);
	return;
}
#endif

StaticJsonDocument<256> Throttle_Data::toJson()
{
	StaticJsonDocument<256> payload;
	payload["asked_power"] = value;
	payload["status"] = status;
	return payload;
}

StaticJsonDocument<4096> BMS_Data::toJson()
{
	StaticJsonDocument<4096> payload;

	JsonArray cellVoltagesJSON = payload.createNestedArray("cellVoltages");
	JsonArray solarVoltagesJSON = payload.createNestedArray("solarVoltages");
	JsonArray temperaturesJSON = payload.createNestedArray("temperatures");
	JsonArray cells_balancingJSON = payload.createNestedArray("cells_balancing");

	for (size_t i = 0; i < 12; i++)
	{
		cellVoltagesJSON.add(cellvoltages[i]);
	}
	for (size_t i = 0; i < 5; i++)
	{
		solarVoltagesJSON.add(solar_voltages[i]);
	}
	for (size_t i = 0; i < 41; i++)
	{
		temperaturesJSON.add(temperatures[i]);
	}
	for (size_t i = 0; i < 12; i++)
	{
		cells_balancingJSON.add(cells_balancing[i]);
	}

	payload["status"] = status;
	payload["ltc_status"] = ltc_status;
	payload["warnings"] = warnings;
	payload["soc"] = soc;
	payload["voltage"] = voltage;
	payload["current"] = current;
	payload["solar_current"] = solar_current;
	payload["max_temp"] = maxTemp;

	return payload;
}

StaticJsonDocument<512> Foils_Data::toJson()
{
	StaticJsonDocument<512> payload;

	payload["dist"] = dist;
	payload["left_angle"] = left_angle;
	payload["right_angle"] = right_angle;
	payload["back_angle"] = back_angle;
	payload["state"] = state;
	payload["mode"] = mode;
	payload["height_ref"] = height_ref;
	payload["angle_ref"] = angle_ref;
	payload["speed"] = speed;
	payload["teensyTemp"] = teensyTemp;

	return payload;
}

StaticJsonDocument<512> Motor_Data::toJson()
{
	StaticJsonDocument<512> payload;
	payload["motor_current"] = motor_current;
	payload["input_current"] = input_current;

	payload["dutycycle"] = duty_cycle;
	payload["rpm"] = rpm;
	payload["voltage"] = voltage;

	payload["temp_mos_1"] = temp_mos_1;
	payload["temp_mos_2"] = temp_mos_2;
	payload["temp_mos_3"] = temp_mos_3;
	payload["temp_mos_max"] = temp_mos_max;

	payload["temp_mot"] = temp_mot;
	
	payload["fault"] = fault;
	payload["id"] = id;
	return payload;
}

StaticJsonDocument<512> Xsens_MTi680G_Data::toJson(){
	StaticJsonDocument<512> payload;
	// payload["time_year"] = utc_time.Year;
	// payload["time_month"] = utc_time.Month;
	// payload["time_hour"] = utc_time.Hour;
	// payload["time_minute"] = utc_time.Minute;
	// payload["time_second"] = utc_time.Second;
	// payload["time_centisecond"] = utc_time_centisecond;
	// payload["time_unix"] = unix_time;
	// payload["status_word"] = status_word;
	// payload["error_code"] = error_code;
	payload["temperature"] = temperature;
	payload["pitch"] = pitch;
	payload["roll"] = roll;
	payload["yaw"] = yaw;
	// payload["accX"] = accX;
	// payload["accY"] = accY;
	// payload["accZ"] = accY;
	payload["gyrX"] = gyrX;
	payload["gyrY"] = gyrY;
	payload["gyrZ"] = gyrZ;
	payload["velX"] = velX;
	payload["velY"] = velY;
	payload["velZ"] = velZ;
	payload["free_accX"] = free_accX;
	payload["free_accY"] = free_accY;
	payload["free_accZ"] = free_accZ;
	payload["lat"] = lat;
	payload["lon"] = lon;
	payload["mag"] = sqrt(pow(magx,2)+pow(magy,2)+pow(magz,2));
	return payload;

}

StaticJsonDocument<512> IVT_S_Data::toJson()
{
	StaticJsonDocument<512> payload;

	payload["current"] = current.value/1000.0;
	payload["voltage1"] = voltage1.value/1000.0;
	payload["voltage2"] = voltage2.value/1000.0;
	payload["voltage3"] = voltage3.value/1000.0;
	payload["temperature"] = temperature.value;
	payload["power"] = power.value;
	payload["As"] = As.value;
	payload["Wh"] = Wh.value;
	return payload;
}

StaticJsonDocument<256> Fuse_Board_Data::toJson()
{
	StaticJsonDocument<256> payload;
	payload["current_24"] = current_24;
	payload["current_48"] = current_48;
	return payload;
}

StaticJsonDocument<512> Cooling_Control_Unit_Data::toJson(){
	StaticJsonDocument<512> payload;
	payload["temp_WC_IN"] = temp_WC_IN;
	payload["temp_WC_OUT"] = temp_WC_OUT;

	payload["pump_pwm"] = pump_pwm;
	payload["pump_tach"] = pump_tach;
	payload["water_flow"] = water_flow;
	return payload;
}

StaticJsonDocument<256> Steering_Encoder_Data::toJson(){
	StaticJsonDocument<256> payload;
	payload["encoder_value"] = value;
	return payload;
}

StaticJsonDocument<256> CANWiFi_Data::toJson(){
	StaticJsonDocument<256> payload;
	payload["TeensyTemp"] = TeensyTemp;
	return payload;
}

#if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
time_t TSB_CAN::getTeensy3Time()
{
	return Teensy3Clock.get();
}
#endif

void TSB_CAN::check_valid_data()
{
	for (int i = 0; i < MAX_NUM_NODES; i++)
	{	
		if (i == CAN_ID)
			continue;		
		if ((millis() - this->received_time[i] > 2000))
			reset_data(i);
	}
}

void TSB_CAN::reset_data(uint8_t source_id)
{
	switch (source_id)
	{
	//*****************
	//****** Throttle ******
	//*****************
	case 0x00:
		this->throttle.value = 0;
		break;
	//*****************
	//****** BMS ******
	//*****************
	case 0x01:
		this->bms.status = 0;
		this->bms.ltc_status = 0;
		this->bms.warnings = 0;
		this->bms.soc = 0;
		this->bms.voltage = 0;
		this->bms.voltage = 0;
		this->bms.current = 0;
		this->bms.solar_current = 0;
		memset(bms.cellvoltages, 0, sizeof(bms.cellvoltages[0])*12);
		memset(bms.solar_voltages, 0, sizeof(bms.solar_voltages[0])*5);
		memset(bms.temperatures, 0, sizeof(bms.temperatures[0])*41);
		memset(bms.cells_balancing, 0, sizeof(bms.cells_balancing[0])*12);
		break;
	//*********************
	//******* Foils *******
	//*********************
	case 0x02:
		this->foils.speed = 0;
		this->foils.dist = 0;
		this->foils.state = 0;
		this->foils.mode = 0;
		this->foils.left_angle = 0;
		this->foils.right_angle = 0;
		this->foils.back_angle = 0;
		this->foils.angle_ref = 0;
		this->foils.height_ref = 0;

		break;
	//*********************
	//****** Motores ******
	//*********************
	// case 0x03:
	// 	this->motor1.motor_current = 0;
	// 	this->motor1.input_current = 0;
	// 	this->motor1.duty_cycle = 0;
	// 	this->motor1.rpm = 0;
	// 	this->motor1.voltage = 0;
	// 	this->motor1.temp_mos_1 = 0;
	// 	this->motor1.temp_mos_2 = 0;
	// 	this->motor1.temp_mos_3 = 0;
	// 	this->motor1.temp_mos_max = 0;
	// 	this->motor1.fault = 0;
	// 	this->motor1.id = 0;
	// 	this->motor1.asked_current = 0;
	// 	this->motor1.pid_speed = 0;
	// 	this->motor2.motor_current = 0;
	// 	this->motor2.input_current = 0;
	// 	this->motor2.duty_cycle = 0;
	// 	this->motor2.rpm = 0;
	// 	this->motor2.voltage = 0;
	// 	this->motor2.temp_mos_1 = 0;
	// 	this->motor2.temp_mos_2 = 0;
	// 	this->motor2.temp_mos_3 = 0;
	// 	this->motor2.temp_mos_max = 0;
	// 	this->motor2.fault = 0;
	// 	this->motor2.id = 0;
	// 	this->motor2.asked_current = 0;
	// 	this->motor2.pid_speed = 0;

	// 	break;

	//**********************
	//******* Screen *******
	//**********************
	case 0x05:
		this->screen.pid_speed_setpoint = 0;
		this->screen.pid_state = 0;
		
		this->screen.controller_state = 0;
		this->screen.homing = 0;
		this->screen.mode_setpoint = 0;
		this->screen.front_angle_setpoint = 0;
		this->screen.back_angle_setpoint = 0;

		this->screen.dist_setpoint = 0;
		this->screen.back_foil_state = 0;

		this->screen.new_kvaser_file = 0;
		this->screen.solarRelay = 0;

		memset(screen.heave_gains, 0, sizeof(screen.heave_gains[0])*6);
		memset(screen.roll_gains, 0, sizeof(screen.roll_gains[0])*3);


		break;
	//*********************
	//******* Shunt_bat *******
	//*********************
	case 0x06:
		this->shunt_bat.current.value = 0;
		this->shunt_bat.current.OCS = 0;
		this->shunt_bat.current.this_result = 0;
		this->shunt_bat.current.any_result = 0;
		this->shunt_bat.current.system = 0;
		this->shunt_bat.voltage1.value = 0;
		this->shunt_bat.voltage1.OCS = 0;
		this->shunt_bat.voltage1.this_result = 0;
		this->shunt_bat.voltage1.any_result = 0;
		this->shunt_bat.voltage1.system = 0;
		this->shunt_bat.voltage2.value = 0;
		this->shunt_bat.voltage2.OCS = 0;
		this->shunt_bat.voltage2.this_result = 0;
		this->shunt_bat.voltage2.any_result = 0;
		this->shunt_bat.voltage2.system = 0;
		this->shunt_bat.voltage3.value = 0;
		this->shunt_bat.voltage3.OCS = 0;
		this->shunt_bat.voltage3.this_result = 0;
		this->shunt_bat.voltage3.any_result = 0;
		this->shunt_bat.voltage3.system = 0;
		this->shunt_bat.temperature.value = 0;
		this->shunt_bat.temperature.OCS = 0;
		this->shunt_bat.temperature.this_result = 0;
		this->shunt_bat.temperature.any_result = 0;
		this->shunt_bat.temperature.system = 0;
		this->shunt_bat.power.value = 0;
		this->shunt_bat.power.OCS = 0;
		this->shunt_bat.power.this_result = 0;
		this->shunt_bat.power.any_result = 0;
		this->shunt_bat.power.system = 0;
		this->shunt_bat.As.value = 0;
		this->shunt_bat.As.OCS = 0;
		this->shunt_bat.As.this_result = 0;
		this->shunt_bat.As.any_result = 0;
		this->shunt_bat.As.system = 0;
		#ifndef IS_BMS
			this->shunt_bat.Wh.value = 0;
		#endif
		this->shunt_bat.Wh.OCS = 0;
		this->shunt_bat.Wh.this_result = 0;
		this->shunt_bat.Wh.any_result = 0;
		this->shunt_bat.Wh.system = 0;
		break;
	//*********************
	//******* Fuse_Board *******
	//*********************
	case 0x07:
		this->fuse_board.current_24 = 0;
		this->fuse_board.current_48 = 0;
		break;
	//*********************
	//******* Shunt_solar *******
	//*********************
	case 0x08:
		this->shunt_solar.current.value = 0;
		this->shunt_solar.current.OCS = 0;
		this->shunt_solar.current.this_result = 0;
		this->shunt_solar.current.any_result = 0;
		this->shunt_solar.current.system = 0;
		this->shunt_solar.voltage1.value = 0;
		this->shunt_solar.voltage1.OCS = 0;
		this->shunt_solar.voltage1.this_result = 0;
		this->shunt_solar.voltage1.any_result = 0;
		this->shunt_solar.voltage1.system = 0;
		this->shunt_solar.voltage2.value = 0;
		this->shunt_solar.voltage2.OCS = 0;
		this->shunt_solar.voltage2.this_result = 0;
		this->shunt_solar.voltage2.any_result = 0;
		this->shunt_solar.voltage2.system = 0;
		this->shunt_solar.voltage3.value = 0;
		this->shunt_solar.voltage3.OCS = 0;
		this->shunt_solar.voltage3.this_result = 0;
		this->shunt_solar.voltage3.any_result = 0;
		this->shunt_solar.voltage3.system = 0;
		this->shunt_solar.temperature.value = 0;
		this->shunt_solar.temperature.OCS = 0;
		this->shunt_solar.temperature.this_result = 0;
		this->shunt_solar.temperature.any_result = 0;
		this->shunt_solar.temperature.system = 0;
		this->shunt_solar.power.value = 0;
		this->shunt_solar.power.OCS = 0;
		this->shunt_solar.power.this_result = 0;
		this->shunt_solar.power.any_result = 0;
		this->shunt_solar.power.system = 0;
		this->shunt_solar.As.value = 0;
		this->shunt_solar.As.OCS = 0;
		this->shunt_solar.As.this_result = 0;
		this->shunt_solar.As.any_result = 0;
		this->shunt_solar.As.system = 0;
		this->shunt_solar.Wh.value = 0;
		this->shunt_solar.Wh.OCS = 0;
		this->shunt_solar.Wh.this_result = 0;
		this->shunt_solar.Wh.any_result = 0;
		this->shunt_solar.Wh.system = 0;
		break;
	//*********************
	//******* CCU *********
	//*********************
	case 0x0A:
		this->ccu.temp_M1 = 0;
		this->ccu.temp_M2 = 0;
		this->ccu.temp_WC_IN = 0;
		this->ccu.temp_WC_OUT = 0;
		this->ccu.pump_pwm = 0;
		this->ccu.pump_tach = 0;
		this->ccu.water_flow = 0;
		break;
	default:
		break;
	}
}
