/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

/**
 * @file main.cpp
 * @author Sebastião Beirão (sebastiao.beirao@tecnico.ulisboa.pt)
 * @brief This file is the main cpp file
 * @version BMSV2
 * @date 2021-12-21
 * 
 * @copyright Copyright (c) 2021 Técnico Solar Boat
 * 
 */

#include <Arduino.h>
#include <SPI.h>
#include <stdint.h>
#include <Snooze.h>
#include "../include/BMS_config.h"
#include "LT_SPI.h"
#include "LTC681x.h"
#include "LTC6811.h"
#include "BMS.h"
#include "tsb_serial.h"
#include "CAN_TSB.h"
#include "BMS_EEPROM.h"

/*****************************************
 ********** Global Variables*  ***********
*****************************************/
// SOC
bool isFull = true;
bool chargingAllowed = true;
volatile bool isCharging = false;

// Balancing
bool timeToBalance = false; // Variable to track whether there are cells that need to be balanced
bool isBalancing = true;
uint16_t minimumCellVoltage = OV_THRESHOLD; // Just for initializing BMS_needs_balance() changes this value

// Balancing parameters configurable over the GUI 
bool balancingAllowed = false;
uint16_t balancingThreshold = 35000; // Voltage above which balance is allowed
uint16_t imbalance_thershold = 100; // 100 means 10 mV - Maximum allowed difference between cell voltages

// MUX Bits
uint8_t currentTemp = 0; // Current MUX channel being read
// Combination of bits to loop through the all the MUX channels
bool S0[8] = {0,1,0,1,0,1,0,1}; 
bool S1[8] = {0,0,1,1,0,0,1,1};
bool S2[8] = {0,0,0,0,1,1,1,1};

/*******************************************
 *********** Under / Over voltage **********
 ************** configuration **************
********************************************/
uint16_t OV_THRESHOLD = 42000; // Over voltage threshold ADC Code. If OV_THRESHOLD is not divisible by 16 it will be rounded
uint16_t UV_THRESHOLD = 34000; // Under voltage threshold ADC Code. If (UV_THRESHOLD - 1) is not divisible by 16 it will be rounded
const uint16_t FULL_BATTERY = OV_THRESHOLD*12/20; // Full charged battery voltage SOC (Sum of All Cells) = REGISTER * 0.0001 * 20

// Warning temperature threshold
uint8_t warning_temp = 55; // Configurable over the GUI - Cells overtemperature threshold
uint8_t heatsink_warn_temp = 70; // Balancing heatsink overtemperature thershold

bool anyWarning = false; // Variable to store if there are any warning

// Maximum allowed current configurable over the GUI
float DOC = 300; // Means 300 A - Maximum discharge current - Discharge Over Current Threshold
float COC = 33.6; // Means 33,6 A - Maximum charge current - Charge Over current Threshold

/*****************************************
 ********** Refresh Rate BMS *************
*****************************************/
// BMS refresh rate configurable over the GUI
uint16_t refresh_rate = 1000; // Unit is ms 

// Voltage sensors
volatile bool hasVoltageCharger = false; // Variable to hold the status of the Charger (Sense Channel 1) voltage sensor
volatile bool killSwitchPlugged = false; // Variable to hold the status of the Motor (Sense Channel 2) voltage sensor
volatile bool systemON = true; // Variable to hold the status of the ON (Sense Channel 3) voltage sensor
volatile bool serialConnected = true; // Variable to hold the status of the VUSB voltage sensor

// Relays
volatile bool motorRelayON = false; // Variable to hold the status of the motor relay
volatile bool preChargeRelayON = false; // Variable to hold the status of the precharge relay
volatile bool chargerRelayON = false; // Variable to hold the status of the charger relay
volatile bool solarRelayON = false; // Variable to hold the status of the solar relay


// Temperatures
Temperatures temps; // Stores the temperatures

// Currents
Currents currents; // Stores the currents

// LTC Status Variable
// Check Messages Format.pdf in the doc folder for more info
// TODO: Pay more attention to this variable during system operation to check for errors
uint16_t LTC_Status = 0; // LTC Status bits variable (CURRENTLY NOT BEING USED outside BMS' Teensy)

// BMS Status
// Check Messages Format.pdf in the doc folder for more info
//	|       Bit 7      |     Bit 6     |    Bit 5    |    Bit 4    |   Bit 3   | Bit 2 |    Bit 1    |   Bit 0  |
//	| Pre charge Relay | Charger Relay | Solar Relay | Motor Relay | Balancing |  Idle | Discharging | Charging |
uint8_t bms_status = 0; // BMS Status bits variable

// BMS Warnings Variable
// Check Messages Format.pdf in the doc folder for more info
//	|  Bit 7   |  Bit 6   |   Bit 5  |   Bit 4  |     Bit 3     |     Bit 2    |       Bit 1      |     Bit 0    |
//	| Not used | Not used | Not used | Not used | Under Voltage | Over Voltage | Over Temperature | Over Current |  
uint8_t warnings = 0; // BMS Warning bits variable

// BMS Timer
IntervalTimer BMS_Timer; // This times is the one used to run the BMS_callback function
volatile bool shouldRun = false; // Variable that tells loop to run BMS_callback()

// Charger Timer
IntervalTimer charger_timer; // This timer is only active during charging it periodically checks if the charging process is fineshed

// Serial Timer
IntervalTimer serial_timer; // This timer is only active if USB is connected to a PC and is used to send the Serial Messages to the GUI

// BMS Serial Messages:
// Check Messages Format.pdf in the doc folder for more info
TSB_SERIAL serial_interface;
tsb_serial_msg_t status;
tsb_serial_msg_t SOC_Voltage_Currents;
tsb_serial_msg_t cellvoltages0;
tsb_serial_msg_t cellvoltages1;
tsb_serial_msg_t cellvoltages2;
tsb_serial_msg_t SolarPanelVoltages0;
tsb_serial_msg_t SolarPanelVoltages1;
tsb_serial_msg_t temperatures0;
tsb_serial_msg_t temperatures1;
tsb_serial_msg_t temperatures2;
tsb_serial_msg_t temperatures3;
tsb_serial_msg_t temperatures4;
tsb_serial_msg_t temperatures5;
tsb_serial_msg_t cellParameters0;
tsb_serial_msg_t securityParameters;

// CAN Stuff:
// Declare your CAN interface, CAN0 available for Teensy 3.2/5 CAN 0 and 1 for Teensy 3.6 
// CAN 1, 2 and 3 available for Teensy 4.X
FlexCAN_T4<CAN0, RX_SIZE_256, TX_SIZE_16> Can0; // <- This name can be whatever you like but this is the name used to send commands to the interface

// BMS CAN Messages:
// Check Messages Format.pdf in the doc folder for more info
TSB_CAN m_can;
CAN_message_t status_balance_warnings;
CAN_message_t can_SOC_Volt_Cur;
CAN_message_t can_cell0;
CAN_message_t can_cell1;
CAN_message_t can_cell2;
CAN_message_t can_solarVoltage0;
CAN_message_t can_solarVoltage1;
CAN_message_t can_temp0;
CAN_message_t can_temp1;
CAN_message_t can_temp2;
CAN_message_t can_temp3;
CAN_message_t can_temp4;
CAN_message_t can_temp5;

// Snooze related Variables used for managing the Sleep mode of the BMS
// Load Drivers
SnoozeDigital s_digital; // Allows the Teensy to wake up with digital pin interrupts
SnoozeAlarm s_alarm; // Allows the Teensy to wake up every x time
SnoozeSPI s_SPI;  // This driver does not wake Teensy, only configures SPI for low power
// Install Drivers
SnoozeBlock config_teensy32(s_digital, s_alarm, s_SPI);
int whoWokeMe; // Value correspond to pin who woke up Teensy or 34 for compare, 35 for RTC, 36 for low power timer or 37 for Touch drivers

/******************************************************
 This variable stores the results from the LTC6811
 register reads and the array lengths must be based
 on the number of ICs on the stack
 ******************************************************/

cell_asic bms_ic[TOTAL_IC];

void chargerISR();
void charger_Timer_callback();
void motorISR();
void serialISR();
void senseOnISR();
void preChargeISR();
void BMS_Timer_callback();
void BMS_callback();
void initSerialMsg();
void initCANMsg();
void updateSerialMsg();
void updateMessages();
void send_can_messages();
void send_serial_messages();

/**
 * @brief Initializes hardware and variables
 * 
 */
void setup()
{
	Serial.begin(9600); // USB is always 12 Mbit/sec 9600 is ignored on Teensy
	initSerialMsg(); // Initialize the static part of the Serial Messages
	
	// If for any reason you need to set the EEPROM values to the default ones uncomment the following 3 lines:
		// setDefaults();	
		// clearEEPROM();
		// saveToEEPROM();
	
	readFromEEPROM(); // Read stored values from the EEPROM

	BMS_init(bms_ic);
	BMS_selfCheck(bms_ic, &LTC_Status);
	if ( LTC_Status != 0 ) {
		// If BMS_selfCheck fails than critical error stop execution!
		// TODO: Decide what should be done here in the future
	}
	
	// Configure CAN
	initCANMsg(); // Initialize the static part of the CAN Messages

	Can0.begin(); // Initialise CAN BUS
  	Can0.setBaudRate(1000000); // Define CAN BaudRate to 1 Mbit/s

	// FIFO allows ordered receptions, Mailboxes can receive in any order as long as a slot is empty. 
	// You can have FIFO and Mailboxes combined, for example, assign a Mailbox to receive a filtered ID in 
	// it's own callback, so a separate callback will fire when your critical frame is received. 
	// With FIFO alone you have only 1 callback essentially. 
	// In either case Teensy is fast enough to receive all critical frames without filters 
	// unless you are in non-interrupt mode or your loop code is causing delays...

	// Either use Mailboxes or FIFO if you are not using filters
	Can0.setMaxMB(16); // Teensy 3.X have only 16 Mailboxes per CAN Channel Teensy 4.X has 64
	Can0.enableMBInterrupts();
	
	// Can0.enableFIFO();
	// Can0.enableFIFOInterrupt();
	
	Can0.attachObj(&m_can); // Register CAN_TSB as a CAN Listener
	m_can.attachGeneralHandler(); // Attach CAN_TSB as a General Handler for all Mailboxes
	// This can also be done for individual Mailboxes
	
	// Can0.mailboxStatus(); // This prints the mailboxes status, is also works when using FIFO

	BMS_Timer.begin(BMS_Timer_callback, refresh_rate * 1000); // Start BMS_Timer

	// Attach voltage sensor interrupts
	attachInterrupt(digitalPinToInterrupt(SENSE_MOTOR), motorISR, CHANGE);
	attachInterrupt(digitalPinToInterrupt(SENSE_CHARGER), chargerISR, RISING);
	attachInterrupt(digitalPinToInterrupt(PreChargeEndSignal), preChargeISR, RISING);

	// Start serial Timer since serial is plugged in for programing
	serialConnected = true;
	output_high(LED_BUILTIN);
	serial_timer.begin(send_serial_messages, 1*1e6); // Send serial messages, refresh rate is fixed at 1s otherwise GUI will crash

	// Configure Snooze drivers
	s_digital.pinMode(VUSB, INPUT_PULLDOWN, RISING);
	s_digital.pinMode(SENSE_ON, INPUT, RISING);
	s_alarm.setRtcTimer(0,10,0); // Wake up Every 10 minutes
	s_SPI.setClockPin(SPI_SCK); // This driver does not wake Teensy, only configures SPI for low power
	
	systemON = input(SENSE_ON); // Check wether the system aka Boat is ON or not

	attachInterrupt(digitalPinToInterrupt(VUSB), serialISR, CHANGE); // Attach Interrupt for when USB cable is unplugged or plugged
	// TODO: Check if the interrupt can be configured as FALLING
	attachInterrupt(digitalPinToInterrupt(SENSE_ON), senseOnISR, CHANGE); // Attach Interrupt for when boat is turned off
}

/**
 * @brief Teensy main Loop function
	The main loop is where the BMS is at when it is not sleeping nor processing ISRs. 
	Here is where valid messages received from the serial port or CAN bus that perform 
	changes on the BMS are processed.

	The main loop also monitors the state of the different flags that are activated by 
	either time-based or pin-change interruptions. One of these flags is the one used to 
	inform that it is time to acquire and process BSM data. If the flag is activated, the 
	function BMS routine is called.

	Another flag processed by the main loop is the one that tells that the boat is off and 
	the USB cable is not connected, meaning that it is time to place the BMS in a low power 
	state. This is done with the help of the Snooze Library.
	
	The last task of the main loop is to call the TSB’s CAN Library routine to check the 
	validity of each device’s data struct so that only valid data is used by the BMS.
 * 
 */
void loop(){
	// Handles configuration messages sent from the BMS GUI
	if(incomingMessage.valid_msg == true){ // incomingMessage is a global variable defined in tsb_serial_msg_t 
		noInterrupts();
		switch (incomingMessage.data_id)
        {
            case 0x0C: // Cell parameters0 - Check Messages Format.pdf in the doc folder for more info
            OV_THRESHOLD = (incomingMessage.data[0] << 8) | incomingMessage.data[1];
			UV_THRESHOLD = (incomingMessage.data[2] << 8) | incomingMessage.data[3];
			balancingThreshold = (incomingMessage.data[4] << 8) | incomingMessage.data[5];
			imbalance_thershold = (incomingMessage.data[6] << 8) | incomingMessage.data[7];
			BMS_set_OV_UV( bms_ic );
            break;

			case 0x0E: // Security parameters - Check Messages Format.pdf in the doc folder for more info
			DOC = (float) ((incomingMessage.data[0] << 8) | incomingMessage.data[1])/10.0;
			COC = (float) ((incomingMessage.data[2] << 8) | incomingMessage.data[3])/10.0;
			warning_temp = incomingMessage.data[4];
			balancingAllowed = incomingMessage.data[5];
			refresh_rate = (incomingMessage.data[6] << 8) | incomingMessage.data[7];
			BMS_Timer.update(refresh_rate * 1000);
			break;
        }
		saveToEEPROM(); // Save newly received values to EEPROM
        incomingMessage.valid_msg = false; // Message is already processed, so set flag to false so that we don't come here again
		interrupts();
    }

	// Code to toggle solar relays with the pilot dashboard
	if ( m_can.screen.solarRelay == true ){ 
		noInterrupts();
		if( solarRelayON ){ // Turn off solar
			output_low(SOLAR_RELAY);
			solarRelayON = false;
			// Charging won't be allowed
			chargingAllowed = false;
		}
		else{ // Turn on solar
			if( !chargerRelayON ){ // Makes sure the boat is not charging with shore power
				output_high(SOLAR_RELAY);
				solarRelayON = true;
				// Charging won't be allowed
				chargingAllowed = true;
			}
		}
		m_can.screen.solarRelay = false;
		interrupts();
	}

	// No charging if above safe temperature
	// TODO: This should be moved to BMS Callback
	if (temps.maxTemp > MAX_TEMP_FOR_CHARGING && currents.bat > CHARGING_THRESHOLD ){
		noInterrupts();
		// Turn off charging
		output_low(CHARGER_RELAY);
		output_low(SOLAR_RELAY);
		solarRelayON = false;
		chargerRelayON = false;
		isCharging = false;
		// Charging won't be allowed
		chargingAllowed = false;
		interrupts();
	}	

	// Call BMS_callback() if it is time for that and send CAN Messages
	if (shouldRun){
		shouldRun = false;
		BMS_callback();
		updateMessages(); // Update both Serial and CAN Messages
		send_can_messages(); // Send CAN Messages (Serial Messages are sent in their own timer)
	}

	if (whoWokeMe == VUSB){ // Teensy woke up due to USB cable being plugged in
		output_high(LED_BUILTIN);
		serialConnected = true;
		serial_timer.begin(send_serial_messages, 1*1e6); // Send serial messages, refresh rate is fixed at 1s otherwise GUI will crash
		whoWokeMe = 100; // Give whoWokeMe a non-usable value, so it does not enter here again
	}
	else if (whoWokeMe == SENSE_ON){ // Teensy woke up due to system turn on
		systemON = true;
		whoWokeMe = 100; // Give whoWokeMe a non-usable value, so it does not enter here again
	}
	
	if ( !serialConnected && !systemON ){ // System  is OFF and Serial is not connected so go to Sleep
		output_low(SOLAR_RELAY);
		solarRelayON = false;
		output_low(CHARGER_RELAY);
		chargerRelayON = false;
		output_low(PRE_CHARGE_RELAY);
		preChargeRelayON = false;
		output_low(MAIN_RELAY); // Should already be OFF since !systemON but better be safe then sorry
		motorRelayON = false;
		whoWokeMe = Snooze.hibernate(config_teensy32);
	}

	m_can.check_valid_data(); // Chick if CAN Messages stored in each device's struct are still valid
}

/**
 * @brief Charger ISR This function is called when the charger is connected to the charging port of the battery box. If charging is allowed, and the battery is not ballancing it opens the solar panels, closes the charger relay and relay and starts the charger_times
 * 
 */

void chargerISR(){
	if (digitalReadFast(SENSE_CHARGER) && chargingAllowed && !isBalancing ) // Charger was connected
	{
		output_low(SOLAR_RELAY);
		solarRelayON = false;
		output_high(FANS);
		output_high(CHARGER_RELAY);
		charger_timer.begin(charger_Timer_callback, 3000000);
		chargerRelayON = true;
		isCharging = true;
	}
}

/**
 * @brief charger_timer callback. This callback is only active during charging, it periodically checks if the charging process is fineshed
 * 
 */
void charger_Timer_callback(){
	if (chargerRelayON){
		if (currents.bat < 0 ) // TODO: The condition should probably be  > 0 && < 0.05 C since this is the condition fo end of charging. Think about it.
		{
			output_low(CHARGER_RELAY);
			chargerRelayON = false;
			output_low(FANS);
			charger_timer.end();
		}
	}
}

/**
 * @brief Killswitch ISR. This callback is called every time the killswitch is removed or plugged in. It takes care of precharge, if the killswitch was plugged in or opens the motors' relay if killswitch was removed
 * 
 */
void motorISR(){
	if (input(SENSE_MOTOR)) // Kill Switch is plugged -> close pre-charge relay closed
	{
		output_high(PRE_CHARGE_RELAY);
		preChargeRelayON = true;
		killSwitchPlugged = true;
	}
	else{ // Kill Switch is open -> open motor and pre-charge relay
		output_low(MAIN_RELAY);
		output_low(PRE_CHARGE_RELAY); // Pre-charge should already be open but better be safe than sorry
		killSwitchPlugged = false;
		motorRelayON = false;
		preChargeRelayON = false;	
	}
}

/**
 * @brief Callback for VUSB pin change interruption. It activates or deactivates the broadcast of USB Serial messages for the BMS GUI
 * 
 */
void serialISR(){
	if (digitalReadFast(VUSB)){ // USB was connected
		output_high(LED_BUILTIN);
		serialConnected = true;
		serial_timer.begin(send_serial_messages, 1*1e6); // Send serial messages, refresh rate is fixed at 1s otherwise GUI will crash
	}
	else{
		// USB  Removed, deactivate serial broadcast
		output_low(LED_BUILTIN);
		serialConnected = false;
		serial_timer.end();
	}
}

/**
 * @brief Callback for ON voltage sensor pin change interruption. It is called every time the boat is turned ON or OFF
 * 
 */
void senseOnISR(){
	if ( !digitalReadFast(SENSE_ON) ){ // System Turned OFF
		systemON = false;
	}
	else{
		systemON = true;
	}
}

/**
 * @brief Callback for PreChargeEndSense pin change interruption. It is called when the voltage in the motors controllers reaches ~90% of the battery voltage, meaning that the precharge is finished and the motors' relay can be closed
 * 
 */
void preChargeISR(){
	if ( killSwitchPlugged ){
		output_high(MAIN_RELAY);
		output_low(PRE_CHARGE_RELAY);
		motorRelayON = true;
		preChargeRelayON = false;
	}
}

/**
 * @brief BMS Timer callback. It only sets a flag that is than processed in the main loop so that the Teensy doesn't get stucked in an interruption for too long.
 * 
 */
void BMS_Timer_callback(){
	shouldRun = true;
}

/**
 * @brief This is the main function of the BMS. It is responsible for communicating with the BSM, acquiring its data and acquiring data from the rest of the devices on the board. The acquired data is then processed to verify if all parameters are within the SOA (Safe Operating Area). The main loop calls this function after BMS_timer_callback has set the respective flag.
 * 
 * To understanf the logic behind this function check Sebastião Beirão's Master Thesis: Battery Management System for Solar Powered Vehicles applied to Técnico Solar Boat prototype -  Figure 4.24
 */
void BMS_callback(){

	// // Stuff used for debugging proposes:
		// // Serial.println();
		// // Serial.println();
		// // Serial.println(m_can.shunt_bat.Wh.value);
		// // Serial.println(m_can.shunt_bat.current.value);
		// // Serial.println();
		// // Serial.println();
		// // digitalWriteFast(EXTERNAL_LED, !digitalReadFast(EXTERNAL_LED));

	int8_t error = 0;
	uint8_t OV_UV = TSB_NONE;

	// Read Cells and import current from the CAN BUS to BMS data structure
	error = BMS_readCells(bms_ic, &LTC_Status, &currents, &m_can);
	while (error != 0) {
		error = BMS_readCells(bms_ic, &LTC_Status, &currents, &m_can);
		// TODO: CRC error detected handle this
	}
	LTC6811_rdcfg(TOTAL_IC,bms_ic);
	
	// Read SOC (Sum of All Cells), LTC temperature, VregA, VregD and GPIOs 1 to 5 which are connected to Temperature MUXs
	BMS_read_aux_GPIO_1_to_5( bms_ic, &LTC_Status);

	// Read temperatures
	BMS_read_temps( &temps, bms_ic, currentTemp);
	currentTemp++; // Iterate to the next temperature
	if (currentTemp > 7) currentTemp = 0;
	
	// Prepare Temperature MUXs for next iteration so that the MUX inputs can settle and we can mitigate crosstalk
	digitalWrite(MS0, S0[currentTemp]);
	digitalWrite(MS1, S1[currentTemp]);
	digitalWrite(MS2, S2[currentTemp]);


	// Checks if values are ok
	if ( (OV_UV = BMS_OV_UV( bms_ic, &warnings, &LTC_Status)) != TSB_NONE) { // OV or UV conditions
		anyWarning = true;
		// Emergency stuff
		if (OV_UV == OVER_VOLTAGE ) {
			// Turn off charging
			output_low(CHARGER_RELAY);
			chargerRelayON = false;
			output_low(SOLAR_RELAY);
			solarRelayON = false;
			// Charging won't be allowed
			chargingAllowed = false;
			// ! This code is not working very well due to the variation in the cells voltage measurements
			// // Turn on discharge on OV cells
			// // BMS_discharge_OV( bms_ic );
			// // BMS_any_balancing( bms_ic, &status.data[3], &status.data[4] );
			warnings ^= (-1 ^ warnings) & (1UL << 2);	
		}
		else{ // Undervoltage condition
			output_low(MAIN_RELAY);
			motorRelayON = false;
			chargingAllowed = true;
			output_high(SOLAR_RELAY);
			solarRelayON = true;
			warnings ^= (-1 ^ warnings) & (1UL << 3);
		}
		tone(BUZZER,1300);
		// reset WDT
		return;
	}
	else if(OV_UV == TSB_NONE && !isBalancing && !isCharging){
		anyWarning = false;
		BMS_clear_discharge(bms_ic);
		isBalancing = false;
		chargingAllowed = true;
	}
	if( BMS_check_temperatures() ){ // Check Temperatures
		anyWarning = true;
		warnings ^= (-1 ^ warnings) & (1UL << 1);
		BMS_clear_discharge(bms_ic);
		isBalancing = false;
		tone(BUZZER,1300);
		// If Cells' overtemperature threshold is exceed by more than 5 ºC than shutdown the motors!
		if (temps.maxTempCells > (warning_temp + 5)){
			output_low(MAIN_RELAY);
			motorRelayON = false;
		}
		// reset WDT
		return;
	}
	if( currents.bat < (- DOC) ){ // Check discharge current threshold
		anyWarning = true;
		output_low(MAIN_RELAY);
		motorRelayON = false;
		warnings ^= (-1 ^ warnings) & (1UL << 0);
		tone(BUZZER,1300);
		// reset WDT
		return;
	}
	// Automatically closes solar relays is pulling too much current
	else if( currents.bat < -(CLOSE_SOLAR_CURRENT_THRESHOLD) && !isCharging ){
		chargingAllowed = true;
		output_high(SOLAR_RELAY);
		solarRelayON = true;
	}
	if (currents.bat > COC){ // Check charge current threshold
		anyWarning = true;
		output_low(CHARGER_RELAY);
		chargerRelayON = false;
		output_low(SOLAR_RELAY);
		solarRelayON = false;
		warnings ^= (-1 ^ warnings) & (1UL << 0);
		tone(BUZZER,1300);
		// reset WDT
		return;
	}
	if ( abs( currents.bat ) > FANS_SPIN_CURRENT_THRESHOLD || temps.maxTemp > FANS_SPIN_TEMP_THERSHOLD ){
		// Control fans state based on Battery current or Box Temperature
		output_high(FANS);
	}
	else if ( ( abs( currents.bat ) < (FANS_SPIN_CURRENT_THRESHOLD - 0.5)) && !isBalancing && (temps.maxTemp < (FANS_SPIN_TEMP_THERSHOLD - 3)) ){ 
		// Give some hysteresis so that fans are not constantly turning ON and OFF
		output_low(FANS);
	}
	
	if( !anyWarning ){
		// clear Warnings
		noTone(BUZZER);
		warnings ^= (-0 ^ warnings) & (1UL << 0);
		warnings ^= (-0 ^ warnings) & (1UL << 1);
		warnings ^= (-0 ^ warnings) & (1UL << 2);
		warnings ^= (-0 ^ warnings) & (1UL << 3);

		// Update minimumCellVoltage and timeToBalance flag
		timeToBalance = BMS_needs_balance( bms_ic, &minimumCellVoltage );


		// Check whether the battery is full
		if ( bms_ic[0].stat.stat_codes[0] >= FULL_BATTERY || BMS_is_cell_charged( bms_ic ) ) {
			// Battery is full since its voltage is > FULL_BATTERY or there is a cell >= OV_THRESHOLD
			isFull = true;
			chargingAllowed = false;
			output_low(CHARGER_RELAY);
			chargerRelayON = false;
			output_low(SOLAR_RELAY);
			solarRelayON = false;
			return;
		}
		else{
			// Battery is not yet full
			isFull = false;
			chargingAllowed = true;
			// Is balancing?
			if ( isBalancing == true ) { // If balancing then check balancing status
				if (!balancingAllowed){ // If balancing is not allowed turn it off
					BMS_clear_discharge(bms_ic);
					isBalancing = false;
					BMS_any_balancing( bms_ic, &status.data[3], &status.data[4] );
					return;
				}
				if ( BMS_needs_balance( bms_ic, &minimumCellVoltage ) ) { 
					// Call this again because if balancingThreshold is changed there may be more cells to balance
					BMS_balance( bms_ic, minimumCellVoltage );
				}
				BMS_check_balance( bms_ic, minimumCellVoltage );
				if ( BMS_any_balancing( bms_ic, &status.data[3], &status.data[4] ) == false) {
					isBalancing = false;
					output_low(FANS);
				}
				return;
			}
			else{ // If not balancing then check if balancing is needed
				if (timeToBalance && balancingAllowed) {
					if ( BMS_needs_balance( bms_ic, &minimumCellVoltage ) ) {
						output_low(SOLAR_RELAY);
						solarRelayON = false;
						output_high(FANS);
						output_low(CHARGER_RELAY);
						chargerRelayON = false;
						isBalancing = true;
						BMS_balance( bms_ic, minimumCellVoltage );
						return;
					}
					return;
				}
				else{ // It's not time to balance yet or balancing is disabled
					if (chargingAllowed == false) { // Charging is not allowed 
						output_low(CHARGER_RELAY);
						chargerRelayON = false;
						output_low(SOLAR_RELAY);
						solarRelayON = false;
						return;
					}
					// ! By removing this solar relays should be mostly open and only closes in case
					// ! too much current is pulled from the battery, this pervents constantly toggling the relay while testing
					// // else{ // Was not charging but charging is allowed
					// // 	if ( isCharging == false && systemON == true) {
					// //		output_high(SOLAR_RELAY);
					// //		solarRelayON = true;
					// //		isCharging = true;
					// //		return;
					// //	}
					// //	return;
					// // }
					return;	
				}
			}
		}	
	}
	anyWarning = false;
}

/**
 * @brief Initialize the static part of the Serial messages
 * 
 */
void initSerialMsg(){
	status.addr = 0x01;
	SOC_Voltage_Currents.addr = 0x01;
	cellvoltages0.addr = 0x01;
	cellvoltages1.addr = 0x01;
	cellvoltages2.addr = 0x01;
	SolarPanelVoltages0.addr = 0x01;
	SolarPanelVoltages1.addr = 0x01;
	temperatures0.addr = 0x01;
	temperatures1.addr = 0x01;
	temperatures2.addr = 0x01;
	temperatures3.addr = 0x01;
	temperatures4.addr = 0x01;
	temperatures5.addr = 0x01;
	cellParameters0.addr = 0x01;
	securityParameters.addr = 0x01;

	status.data_id = 0x01;
	SOC_Voltage_Currents.data_id = 0x11;
	cellvoltages0.data_id = 0x21;
	cellvoltages1.data_id = 0x31;
	cellvoltages2.data_id = 0x41;
	SolarPanelVoltages0.data_id = 0x51;
	SolarPanelVoltages1.data_id = 0x61;
	temperatures0.data_id = 0x71;
	temperatures1.data_id = 0x81;
	temperatures2.data_id = 0x91;
	temperatures3.data_id = 0xA1;
	temperatures4.data_id = 0xB1;
	temperatures5.data_id = 0xC1;
	cellParameters0.data_id = 0x0C;
	securityParameters.data_id = 0x0E;


	status.data_size = 6;
	SOC_Voltage_Currents.data_size = 8;
	cellvoltages0.data_size = 8;
	cellvoltages1.data_size = 8;
	cellvoltages2.data_size = 8;
	SolarPanelVoltages0.data_size = 8;
	SolarPanelVoltages1.data_size = 2;
	temperatures0.data_size = 8;
	temperatures1.data_size = 8;
	temperatures2.data_size = 8;
	temperatures3.data_size = 8;
	temperatures4.data_size = 5;
	temperatures5.data_size = 4;
	cellParameters0.data_size = 8;
	securityParameters.data_size = 8;
}

/**
 * @brief Initialize the static part of the CAN messages
 * 
 */
void initCANMsg(){
	status_balance_warnings.flags.extended = 0;
	status_balance_warnings.id = 0x601;
	status_balance_warnings.len = 6;

	can_SOC_Volt_Cur.flags.extended = 0;
	can_SOC_Volt_Cur.id = 0x611;
	can_SOC_Volt_Cur.len = 8;

	can_cell0.flags.extended = 0;	
	can_cell0.id = 0x621;
	can_cell0.len = 8;

	can_cell1.flags.extended = 0;	
	can_cell1.id = 0x631;
	can_cell1.len = 8;

	can_cell2.flags.extended = 0;	
	can_cell2.id = 0x641;
	can_cell2.len = 8;

	can_solarVoltage0.flags.extended = 0;
	can_solarVoltage0.id = 0x651;
	can_solarVoltage0.len = 8;

	can_solarVoltage1.flags.extended = 0;
	can_solarVoltage1.id = 0x661;
	can_solarVoltage1.len = 2;

	can_temp0.flags.extended = 0;	
	can_temp0.id = 0x671;
	can_temp0.len = 8;

	can_temp1.flags.extended = 0;	
	can_temp1.id = 0x681;
	can_temp1.len = 8;

	can_temp2.flags.extended = 0;	
	can_temp2.id = 0x691;
	can_temp2.len = 8;

	can_temp3.flags.extended = 0;	
	can_temp3.id = 0x6A1;
	can_temp3.len = 8;

	can_temp4.flags.extended = 0;	
	can_temp4.id = 0x6B1;
	can_temp4.len = 6;

	can_temp5.flags.extended = 0;	
	can_temp5.id = 0x6C1;
	can_temp5.len = 4;
}

/**
 * @brief Updates both Serial and CAN messages
 * 
 */
void updateMessages(){
	int32_t ind = 0;
	// // Debug Stuff 
		// // Serial.println((String)"Charging: " + isCharging + " Discharging: " + currents.bat + " Idle: " + !systemON + " Balancing: " + isBalancing + " BMS Status: " + bms_status);
		// // Serial.println((String)"Battery Current: " + currents.bat + " From CAN: " + m_can.shunt_bat.current.value);
	
	bms_status ^= (-(currents.bat > 0) ^ bms_status) & (1UL << 0);
	bms_status ^= (-(currents.bat < 0) ^ bms_status) & (1UL << 1);
	bms_status ^= (-((!systemON)) ^ bms_status) & (1UL << 2);
	bms_status ^= (-isBalancing ^ bms_status) & (1UL << 3);
	bms_status ^= (-motorRelayON ^ bms_status) & (1UL << 4);
	bms_status ^= (-solarRelayON ^ bms_status) & (1UL << 5);
	bms_status ^= (-chargerRelayON ^ bms_status) & (1UL << 6);
	bms_status ^= (-preChargeRelayON ^ bms_status) & (1UL << 7);
	status.data[0] = bms_status;
	status.data[1] = highByte(LTC_Status);
	status.data[2] = lowByte(LTC_Status);
	status.data[5] = warnings;

	status_balance_warnings.buf[0] = status.data[0];
	status_balance_warnings.buf[1] = status.data[1];
	status_balance_warnings.buf[2] = status.data[2];
	status_balance_warnings.buf[3] = status.data[3];
	status_balance_warnings.buf[4] = status.data[4];
	status_balance_warnings.buf[5] = status.data[5];
	
	ind = 0;
	// TODO: The following line should be battery SOC and not the shunt Wh
	buffer_append_int16(SOC_Voltage_Currents.data, (int16_t)m_can.shunt_bat.Wh.value, &ind); // Send Isabel Wh to BMS GUI
	buffer_append_uint16(SOC_Voltage_Currents.data, bms_ic[0].stat.stat_codes[0], &ind); // Voltage
	buffer_append_float16(SOC_Voltage_Currents.data, currents.bat, 1e2, &ind); // Battery Current
	buffer_append_float16(SOC_Voltage_Currents.data, currents.solar, 1e2, &ind); // Solar Current

	for(int32_t i=0, ind1=0, ind2=0, ind3=0, ind4=0, ind5=0, ind6=0 ; i < 4 ; i++)
	{
		buffer_append_uint16(cellvoltages0.data, bms_ic[0].cells.c_codes[i], &ind1);
		buffer_append_uint16(can_cell0.buf, bms_ic[0].cells.c_codes[i], &ind2);

		buffer_append_uint16(cellvoltages1.data, bms_ic[0].cells.c_codes[i+4], &ind3);
		buffer_append_uint16(can_cell1.buf, bms_ic[0].cells.c_codes[i+4], &ind4);

		buffer_append_uint16(cellvoltages2.data, bms_ic[0].cells.c_codes[i+8], &ind5);
		buffer_append_uint16(can_cell2.buf, bms_ic[0].cells.c_codes[i+8], &ind6);
	}

	ind = 2;
	// TODO: Missing Battery SOC here
	buffer_append_uint16(can_SOC_Volt_Cur.buf, bms_ic[0].stat.stat_codes[0], &ind); // Voltage
	buffer_append_float16(can_SOC_Volt_Cur.buf, currents.bat, 1e2, &ind); // Battery Current
	buffer_append_float16(can_SOC_Volt_Cur.buf, currents.solar, 1e2, &ind); // Solar Current

	
	for (size_t i = 0; i < 8; i++){
		temperatures0.data[i] = temps.bus_bars[i];
		temperatures2.data[i] = temps.cells[i+3];
		temperatures3.data[i] = temps.cells[i+11];
		
		can_temp0.buf[i] = temps.bus_bars[i];
		can_temp2.buf[i] = temps.cells[i+3];
		can_temp3.buf[i] = temps.cells[i+11];
	}

	for (size_t i = 0; i < 5; i++){
		temperatures1.data[i] = temps.bus_bars[i+8];
		temperatures4.data[i] = temps.cells[i+19];
		
		can_temp1.buf[i] = temps.bus_bars[i+8];
		can_temp4.buf[i] = temps.cells[i+19];
	}

	can_temp4.buf[5] = temps.maxTempCells;
	
	for (size_t i = 5; i < 8; i++){
		temperatures1.data[i] = temps.cells[i-5];

		can_temp1.buf[i] = temps.cells[i-5];
	}

	
	temperatures5.data[0] = temps.ambient;
	temperatures5.data[1] = temps.heatsink_1;
	temperatures5.data[2] = temps.heatsink_2;
	temperatures5.data[3] = (uint8_t)round(bms_ic[0].stat.stat_codes[1]*0.01333333-273); // LTC Temperature (conversion taken from LTC6811's datasheet)

	can_temp5.buf[0] = temps.ambient;
	can_temp5.buf[1] = temps.heatsink_1;
	can_temp5.buf[2] = temps.heatsink_2;
	can_temp5.buf[3] = temperatures5.data[3];


	cellParameters0.data[0] = highByte(OV_THRESHOLD);
	cellParameters0.data[1] = lowByte(OV_THRESHOLD);
	cellParameters0.data[2] = highByte(UV_THRESHOLD);
	cellParameters0.data[3] = lowByte(UV_THRESHOLD);
	cellParameters0.data[4] = highByte(balancingThreshold);
	cellParameters0.data[5] = lowByte(balancingThreshold);
	cellParameters0.data[6] = highByte(imbalance_thershold);
	cellParameters0.data[7] = lowByte(imbalance_thershold);

	ind = 0;
	buffer_append_float16(securityParameters.data, DOC, 1e1, &ind);
	buffer_append_float16(securityParameters.data, COC, 1e1, &ind);
	securityParameters.data[4] = warning_temp;
	securityParameters.data[5] = balancingAllowed;
	securityParameters.data[6] = highByte(refresh_rate);
	securityParameters.data[7] = lowByte(refresh_rate);	
}

/**
 * @brief Send CAN Messages
 * 
 */
void send_can_messages(){
	Can0.write(status_balance_warnings);
	Can0.write(can_SOC_Volt_Cur);
	Can0.write(can_cell0);
	Can0.write(can_cell1);
	Can0.write(can_cell2);
	Can0.write(can_temp0);
	Can0.write(can_temp1);
	Can0.write(can_temp2);
	Can0.write(can_temp3);
	Can0.write(can_temp4);
	Can0.write(can_temp5);
}

/**
 * @brief Send Serial Messages
 * 
 */
void send_serial_messages(){
	if ( cellvoltages0.data[0] != 0 ){ // Makes sure messages have content 
		serial_interface.send_msg(status);
		serial_interface.send_msg(SOC_Voltage_Currents);
		serial_interface.send_msg(cellvoltages0);
		serial_interface.send_msg(cellvoltages1);
		serial_interface.send_msg(cellvoltages2);
		serial_interface.send_msg(temperatures0);
		serial_interface.send_msg(temperatures1);
		serial_interface.send_msg(temperatures2);
		serial_interface.send_msg(temperatures3);
		serial_interface.send_msg(temperatures4);
		serial_interface.send_msg(temperatures5);

		serial_interface.send_msg(cellParameters0);
		serial_interface.send_msg(securityParameters);
	}
}