/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

/**
 * @file BMS_config.h
 * @author Sebastião Beirão (sebastiao.beirao@tecnico.ulisboa.pt)
 * @brief This file mainly hold configuration parameters and declares global variables
 * @version BMSV2
 * @date 2021-12-21
 * 
 * @copyright Copyright (c) 2021 Técnico Solar Boat
 * 
 */

#ifndef BMS_CONFIG_H
#define BMS_CONFIG_H

#include <Arduino.h>  // typedefs use types defined in this header file.
#include "LTC681x.h"

// Struct to hold all the measured temperatures
typedef struct{
    uint8_t ambient=0;
    uint8_t heatsink_1=0;
    uint8_t heatsink_2=0;
    uint8_t LTC=0;
    uint8_t bus_bars[13]={0};
    uint8_t cells[24]={0};
    uint8_t maxTempCells = 0;
    uint8_t maxTemp=0;
} Temperatures;

// Struct to hold the currents
typedef struct{
    float batLow; // to be used when analog transducers with two ranges are used
    float batHigh; // to be used when analog transducers with two ranges are used
    float bat;
    float solar;    
} Currents;

/*****************************************
 ************* Teensy Pins  **************
*****************************************/
#define EXTERNAL_LED 0
#define BUZZER 5
#define FANS 6
#define CTRL_SD 8
#define CS_SD 9
#define LTC_CS 10
#define SPI_SCK 14
#define MAIN_RELAY 15           // CTRL_K1
#define PRE_CHARGE_RELAY 16     // CTRL_K2
#define CHARGER_RELAY 17        // CTRL_K3
#define SOLAR_RELAY 18          // CTRL_K4
#define CTRL_K5 19              // CTRL_K5
#define SOLAR_MOS 20            // CTRL_K6
#define SENSE_ON 21             // PCB Sense Channel 3
#define SENSE_MOTOR 22          // PCB Sense Channel 2
#define SENSE_CHARGER 23        // PCB Sense Channel 1
#define SENSE_SPARE 24          // PCB Sense Channel 4
#define PreChargeEndSignal 25   // PCB Sense Channel 5
#define MS0 30                  // MUX Selection Bits
#define MS1 31                  // MUX Selection Bits
#define MS2 32                  // MUX Selection Bits
#define VUSB 33

#define HEATSINK_1 A10          // Balancing Heatsink Bottom Thermistor
#define HEATSINK_2 A11          // Balancing Heatsink Top Thermistor
#define AMBIENT A14             // Ambient Temperature Thermistos
#define SOLAR_A A12             // Analog Current Sensor Input

/**********************************************
 ************* Hardcoded Values **************
***********************************************/
#define CLOSE_SOLAR_CURRENT_THRESHOLD 5 // Means 5 A - Battery current above which the solar panels relay will be closed
#define FANS_SPIN_CURRENT_THRESHOLD 2 // Means 2 A  - Battery current above which the FANs will turn ON
#define FANS_SPIN_TEMP_THERSHOLD 30 // Means 30 ºC - Temperature (anywhere) above which the FANs will turn ON
#define MAX_TEMP_FOR_CHARGING 45 // Means 45 ºC - Maximum allowed temperature for charging
#define CHARGING_THRESHOLD 1 // Means 1 A - Current above which it is considered that the battery is charging

/**********************************************
 ************* EEPROM Addresses ***************
***********************************************/
// EEPROM Address of the different variables stored in the Teensy's EEPROM
#define BALANCING_ALLOWED_ADDR 0
#define BALANCING_THRESH_ADDR 1
#define IMBALANCE_THRESH_ADDR 3
#define OV_THRESH_ADDR 5
#define UV_THRESH_ADDR 7
#define WARN_TEMP_ADDR 9
#define DOC_ADDR 10
#define COC_ADDR 14
#define REFRESH_RATE_ADDR 18
#define EEPROM_CRC_ADDR 20

/**********************************************
 ************* Thermistor Stuff ***************
***********************************************/
// Bus Bars Thermistor Parameters - MPN: NXFT15WF104FA1B140
#define BETA_TAB 3435
#define NOM_TAB 10000 // Thermistor resistance at T0
#define REF_TAB 10000 // Resistance of the voltage diveder resistor
// Cells' Body Thermistor Parameters - MPN: 103JT-025
#define BETA_BODY 4250
#define NOM_BODY 100000 // Thermistor resistance at T0
#define REF_BODY 100000 // Resistance of the voltage diveder resistor
// Balancing Heatsink Thermistor Parameters - MPN: NCP18XH103F03RB
#define BETA_HS 3380
#define NOM_HS 10000 // Thermistor resistance at T0
#define REF_HS 8450 // Resistance of the voltage diveder resistor
// Ambient Thermistor Parameters - MPN: NTCLE100E3103HT1
#define BETA_AMBIENT 3977
#define NOM_AMBIENT 10000 // Thermistor resistance at T0
#define REF_AMBIENT 10000 // Resistance of the voltage diveder resistor

#define ADC_RESOLUTION 4095 // ADC Resolution for the Temperuters read by the Teensy (Ambient and Balancing Heatsink)
#define REF_VOLTAGE_TEMPS 5 // Voltage applied to the Bus Bar and Cells Thermistors
#define T0 25 // Temperature for the nominal resistance

/*****************************************
 ********** Global  Variables  ***********
*****************************************/
// SOC
extern bool isFull;
extern bool chargingAllowed;

// Balancing parameters configurable over the GUI 
extern bool timeToBalance; // Variable to track whether there are cells that need to be balanced
extern bool balancingAllowed;
extern bool isBalancing;
extern uint16_t minimumCellVoltage;
extern uint16_t balancingThreshold; // Voltage above which balance is allowed
extern uint16_t imbalance_thershold; // Maximum allowed difference between cell voltages


// Voltage sensors
volatile extern bool hasVoltageCharger; // Variable to hold the status of the Charger (Sense Channel 1) voltage sensor
volatile extern bool killSwitchPlugged; // Variable to hold the status of the Motor (Sense Channel 2) voltage sensor
volatile extern bool systemON; // Variable to hold the status of the ON (Sense Channel 3) voltage sensor
volatile extern bool serialConnected; // Variable to hold the status of the VUSB voltage sensor

// Relays
volatile extern bool motorRelayON; // Variable to hold the status of the motor relay
volatile extern bool solarRelayON; // Variable to hold the status of the solar relay

// Temperatures
extern Temperatures temps; // Stores the temperatures


/*****************************************
 ********** Refresh Rate BMS *************
*****************************************/
extern uint16_t refresh_rate; // Unit is ms

/*****************************************
 ********** Balancing Defines ************
*****************************************/
#define MAX_CELLS_BALANCING 11 // Maximum number of cells balancing at the same time

/*****************************************
 ************** OV/UV Defines ************
*****************************************/
#define TSB_NONE 0
#define OVER_VOLTAGE 1
#define UNDER_VOLTAGE 2

/*****************************************
 *********** LTC6811-2 Parameters ********
*****************************************/

#define ENABLED 1
#define DISABLED 0
#define DATALOG_ENABLED 1
#define DATALOG_DISABLED 0

const uint8_t TOTAL_IC = 1; // number of LTC6811 ICs in the daisy chain

/*****************************************
 *********** ADC configurations **********
*****************************************/
// ADC_OPT and ADC_CONVERSION_MODE sets the ADC mode
const uint8_t ADC_OPT = ADC_OPT_0; // Selects Modes 27kHz, 7kHz, 422Hz or 26Hz with MD[1:0] Bits in ADC Conversion Commands

// Sets to 7 kHz (normal mode) since ADC_OPT = 0
const uint8_t ADC_CONVERSION_MODE = MD_7KHZ_3KHZ;

// Do not permit discharge while measuring
const uint8_t ADC_DCP = DCP_DISABLED; 

// Measure all cells 
const uint8_t CELL_CH_TO_CONVERT = CELL_CH_ALL; 

// Measure all  GPIOs and 2nd reference
const uint8_t AUX_CH_TO_CONVERT = AUX_CH_ALL; 

// Measure SC (Sum of all cells), Internal temperature, Analog supply voltage
// and Digital supply voltage
const uint8_t STAT_CH_TO_CONVERT = STAT_CH_ALL;

/*******************************************
 *********** Under / Over voltage **********
 ************** configuration **************
********************************************/
extern uint16_t OV_THRESHOLD; // Over voltage threshold ADC Code. If OV_THRESHOLD is not divisible by 16 it will be rounded
extern uint16_t UV_THRESHOLD; // Under voltage threshold ADC Code. If (UV_THRESHOLD - 1) is not divisible by 16 it will be rounded
extern const uint16_t FULL_BATTERY; // Full charged battery voltage SOC = REGISTER * 0.0001 * 20

// Warning temperature threshold
extern uint8_t warning_temp; // Configurable over the GUI - Cells overtemperature threshold
extern uint8_t heatsink_warn_temp; // Balancing heatsink overtemperature thershold

// Control if warning happened
extern bool anyWarning; // Variable to store if there are any warning

// Maximum allowed current configurable over the GUI
extern float DOC; // Means 300 A - Maximum discharge current - Discharge Over Current Threshold
extern float COC; // Means 33,6 A - Maximum charge current - Charge Over current Threshold


/*****************************************
 ***************** Macros ****************
*****************************************/
// This set of macro allow to call certain Teensy functions in a more straightforward away
// Set "pin" low
// @param pin pin to be driven LOW
#define output_low(pin)   digitalWriteFast(pin, LOW)
// Set "pin" high
// @param pin pin to be driven HIGH
#define output_high(pin)  digitalWriteFast(pin, HIGH)
// Return the state of pin "pin"
// @param pin pin to be read (HIGH or LOW).
// @return the state of pin "pin"
#define input(pin)        digitalReadFast(pin)
// Set "pin" as output
// @param pin pin to be set as output
#define setOutput(pin)    pinMode(pin, OUTPUT)
// Set "pin" as input
// @param pin pin to be set as input
#define setInput(pin)    pinMode(pin, INPUT)
// Set "pin" as input pulldow
// @param pin pin to be set as input
#define setInputPullDown(pin)    pinMode(pin, INPUT_PULLDOWN)

#endif  // BMS_CONFIG_H