# BMSSPV V2

TSB's BMS V2 code.

Copyright (C) 2021  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute 
it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat

- BMS V2 contains the actual BMS code the PCB for this code is available [here](https://gitlab.com/tecnico.solar.boat/2021/SR03/-/tree/main/PCB's/BMSV2);
- BMS_simulator contains piece of code that simulates the serial messages sent by the BMS so that the GUI can be testes;
- PEC C++ Contains a C++ code to calculate the PEC of LTC6811 messages according to LTC6811's datasheet page 76;
- Serial CheckSum contains a C++ implementation to calculate the PEC of TSB Serial Library compliant messages.


### ❗️ In order to properly view the comments in the code install VSCode's Better Comments Extension.

### This code was developed by our colleague Sebastião Beirão during his Master Thesis entitled Battery Management System for Solar Powered Vehicles applied to Técnico Solar Boat prototype which is publicly available [here](https://fenix.tecnico.ulisboa.pt/cursos/meec/dissertacao/846778572213540).