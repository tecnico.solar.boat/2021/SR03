# CANWiFi

This repo contains the code used in the CAN sniffer to read the BUS and publish, every X time units, the data to the server using MQTT via Wifi. 

Copyright (C) 2021  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute 
it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat

## Requirements

- Teensy 3.2
- ESP32
- PubSubClient library (MQTT) Version 2.7
- Adafruit WiFiNINA library (ESP communication)

## Connections

For mor info check the PCB for this project [here](https://gitlab.com/tecnico.solar.boat/2021/SR03/-/tree/main/PCB's/CANWiFi) and the [Adafruit board](https://www.adafruit.com/product/4201) from which this design is based on.

| Teensy| ESP |
| --- | --- |
| 3.3 V  |  VCC |
| GND  | GND  |
| MOSI | IO14  |
| MISO  | IO23  |
| SCK | IO18 |
| CS (10) | IO5 |
| ESP_BUSY (8) | IO33 |
 

## Configuration

### WiFi

In order for this code to work you need to set the credentials of the WiFi [here](include/config.h#L23).

### MQTT Broker

We are using a private broker, hosted on our server, in case you want to setup your own broker follow this [tutorial](https://obrienlabs.net/how-to-setup-your-own-mqtt-broker/) and then change the credentials [here](include/config.h#L29) and server address [here](lib/MQTT_TSB/MQTT_TSB.h#L45). 

### Adding new components

If you want to add more components to the system you need to follow this steps

1. Add the new component class to the CAN TSB library and implement the toJson method function.
2. Setup a topic in [config.h](include/config.h#L32) following the form TSB/\<vessel>\/\<component>
3. Call the mqtt publish function in [main.cpp](src/main.cpp#L69) like it is done for the other components.

### Changing the Publishing interval

The sniffer reads all the messages sent to the CAN BUS but the interval between messages to the server is controlled by the Interval timer send_timer in [main.cpp](src/main.cpp#L142).
