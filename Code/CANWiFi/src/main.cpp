/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <MQTT_TSB.h>
#include <InternalTemperature.h>

bool ledState = false;
MQTT_TSB mqttHandler;

IntervalTimer blink_timer;
IntervalTimer time_timer;
IntervalTimer send_timer;

// CANWiFi CAN Messages:
FlexCAN_T4<CAN0, RX_SIZE_256, TX_SIZE_16> Can0;
TSB_CAN canBus;
CAN_message_t temp;

const signed long DEFAULT_TIME_MAIN = 1357041600; // Jan 1 2013

void blink_led(){
	int32_t ind=0;
	ledState = !ledState;
	digitalWrite(LED_BUILTIN, ledState);
	canBus.canWiFi.TeensyTemp = InternalTemperature.readTemperatureC();
	buffer_append_float16(temp.buf, canBus.canWiFi.TeensyTemp, 1e1, &ind);
	Can0.write(temp);
}

void initCAN_Messages(){
	temp.flags.extended = 0;
	temp.id = 0x60B;
	temp.len = 2;
}

// void sendTime(){
// 	unsigned long time_now = now();
// 	int32_t ind = 0;
// 	if (time_now > DEFAULT_TIME_MAIN){
// 		buffer_append_uint32(time_msg.buf, time_now, &ind);
// 		Can0.write(time_msg);
// 	}

// }

void sendData(){
	// Ensure the connection to the MQTT server is alive (this will make the first
	// connection and automatically reconnect when disconnected).
	mqttHandler.MQTT_connect();

	// Publish the data obtained from the CAN
	mqttHandler.MQTT_publishMessage(THROTTLE_TOPIC, canBus.throttle.toJson());
	mqttHandler.MQTT_publishMessage(BMS_TOPIC, canBus.bms.toJson());
	mqttHandler.MQTT_publishMessage(FOILS_TOPIC, canBus.foils.toJson());
	mqttHandler.MQTT_publishMessage(MOTOR_1_TOPIC, canBus.motor1.toJson());
	mqttHandler.MQTT_publishMessage(MOTOR_2_TOPIC, canBus.motor2.toJson());
	mqttHandler.MQTT_publishMessage(AHRS_TOPIC, canBus.ahrs.toJson());
	// mqttHandler.MQTT_publishMessage(DISPLAY_TOPIC, canBus.screen.toJson());
	mqttHandler.MQTT_publishMessage(BAT_SHUNT_TOPIC, canBus.shunt_bat.toJson());
	mqttHandler.MQTT_publishMessage(FUSE_BOARD, canBus.fuse_board.toJson());
	mqttHandler.MQTT_publishMessage(SOLAR_SHUNT_TOPIC, canBus.shunt_solar.toJson());
	// mqttHandler.MQTT_publishMessage(ENCODER_TOPIC, canBus.encoder.toJson());
	mqttHandler.MQTT_publishMessage(CCU_TOPIC, canBus.ccu.toJson());
	mqttHandler.MQTT_publishMessage(CANWiFi_TOPIC, canBus.canWiFi.toJson());
}


void setup() {
	//Setup serial for USB communication
	Serial.begin(115200);
	
	pinMode(RESET_ESP, OUTPUT);
	digitalWriteFast(RESET_ESP, LOW);
	SPI.setSCK(14); // Change SCK pin to pin 14 (13 is the default one)

	// Setup the CAN
	initCAN_Messages();
	Can0.begin();
	Can0.setBaudRate(1000000);
	Can0.setMaxMB(16); // Teensy 3.X have only 16 Mailboxes per CAN Channel Teensy 4.X has 64
  	Can0.enableMBInterrupts();
	Can0.attachObj(&canBus);
	canBus.attachGeneralHandler();

	// Setup Teensy State LED
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWriteFast(LED_BUILTIN, HIGH);

	// Connect to the Internet
	mqttHandler.WiFi_connect();

	// Get time from the internet
	mqttHandler.WiFi_getTime();

	// // Setup the MQTT handler
	mqttHandler.MQTT_setup();

	// time_timer.begin(sendTime, 1*1e6);
	// // Send time during 10 seconds
	// uint32_t now = millis();
	// while (millis() < now + 10000){
	// 	/* code */
	// }
	// time_timer.update(10*1e6);

	// // Setup WDT
    // noInterrupts();                                         // don't allow interrupts while setting up WDOG
    // WDOG_UNLOCK = WDOG_UNLOCK_SEQ1;                         // unlock access to WDOG registers
    // WDOG_UNLOCK = WDOG_UNLOCK_SEQ2;
    // delayMicroseconds(1);                                   // Need to wait a bit..

    // // for this demo, we will use 10 seconds WDT timeout (e.g. you must reset it in < 1 sec or a boot occurs)
    // WDOG_TOVALH = 0x044a;
    // WDOG_TOVALL = 0xa200;

    // // This sets prescale clock so that the watchdog timer ticks at 7.2MHz
    // WDOG_PRESC  = 0x400;

    // // Set options to enable WDT. You must always do this as a SINGLE write to WDOG_CTRLH
    // WDOG_STCTRLH |= WDOG_STCTRLH_ALLOWUPDATE |
    //     WDOG_STCTRLH_WDOGEN | WDOG_STCTRLH_WAITEN |
    //     WDOG_STCTRLH_STOPEN | WDOG_STCTRLH_CLKSRC;
    // interrupts(); 

	send_timer.begin(sendData, 500000);
	// ALL initialization OK star blinking
	pinMode(LED_BUILTIN, OUTPUT);
	blink_timer.priority(255);
	blink_timer.begin(blink_led, 1000000);
}



void loop() {
	canBus.check_valid_data();
	// Reset watch dog timer
    noInterrupts();
    WDOG_REFRESH = 0xA602;
    WDOG_REFRESH = 0xB480;
    interrupts();
}
