/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <SPI.h>
#include "WiFiNINA.h"
#include "WiFiUdp.h"
#include "PubSubClient.h"
#include "CAN_TSB.h"
#include "../../include/config.h"
#include <TimeLib.h>

class MQTT_TSB
{
private: 
	// Create an ESP8266 WiFiClient class to connect to the MQTT server.
	const char* ssid = WLAN_SSID;        // your network SSID (name)
	const char* pass = WLAN_PASS;    	 // your network password (use for WPA, or use as key for WEP)
	int keyIndex = 0;            		// your network key Index number (needed only for WEP)
	int status = WL_IDLE_STATUS;
	// Initialize the Ethernet client library
	// with the IP address and port of the server
	// that you want to connect to (port 80 is default for HTTP):
	WiFiClient net;
	
	
	PubSubClient mqttClient;
	// const char* MQTT_BROKER  = "test.mosquitto.org"; // Your broker ip
	const char* MQTT_BROKER  = "BROKER IP GOES HERE"; // Your broker ip

	// Time Related stuff
	byte packetBuffer[NTP_PACKET_SIZE]; // buffer to hold incoming and outgoing packets
	// A UDP instance to let us send and receive packets over UDP
	WiFiUDP Udp;
	void sendNTPpacket(char *ntpSrv);
	time_t getTime();
	static time_t getTeensy3Time();
	void defTime();
	void printWifiStatus();
	void printTime();


public:
	void WiFi_connect();
	void WiFi_getTime();
	void MQTT_connect();
	MQTT_TSB();
	void MQTT_setup();
	void MQTT_loop();
	void MQTT_publishMessage(const char* topic, StaticJsonDocument<MQTT_MAX_PACKET_SIZE>);
	void MQTT_publishStringMessage(const char* topic, char* payload );
};
