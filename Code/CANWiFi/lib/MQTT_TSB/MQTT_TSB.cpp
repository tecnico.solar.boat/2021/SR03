/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "MQTT_TSB.h"

MQTT_TSB::MQTT_TSB()
{
}

void MQTT_TSB::MQTT_connect()
{


	// Stop if already connected.
	if (mqttClient.connected())
	{
		return;
	}

	// Loop until we're reconnected
	while (!mqttClient.connected()) {
		Serial.println("Attempting MQTT connection...");
		// Create a random mqttClient ID
		String mqttClientId = "SR03-";
		mqttClientId += String(random(0xffff), HEX);
		// Attempt to connect
		// if ( mqttClient.connect(mqttClientId.c_str() ) ) {
		if ( mqttClient.connect(mqttClientId.c_str(), MQTT_USER, MQTT_PASS ) ) {
			Serial.println("connected");
			// Once connected, publish an announcement...
			mqttClient.publish("outTopic", "hello world");
			// ... and resubscribe

		} else {
			Serial.print("failed, rc=");
			Serial.print(mqttClient.state());
			Serial.println(" try again in 5 seconds");
			// Wait 5 seconds before retrying
			delay(5000);
		}
	}
}

void MQTT_TSB::WiFi_connect(){
	// check for the WiFi module:
	WiFi.setPins(SPIWIFI_SS, SPIWIFI_ACK, ESP32_RESETN, ESP32_GPIO0, &SPIWIFI);
	while (WiFi.status() == WL_NO_MODULE) {
		Serial.println("Communication with WiFi module failed!");
		// don't continue
		delay(1000);
	}
	WiFi.setLEDs(254,254,150);

	String fv = WiFi.firmwareVersion();
	if (fv < "1.0.0") {
		Serial.println("Please upgrade the firmware");
	}
		Serial.print("Found firmware "); Serial.println(fv);

	// attempt to connect to Wifi network:
	Serial.print("Attempting to connect to SSID: ");
	Serial.println(ssid);
	// Connect to WPA/WPA2 network. Change this line if using open or WEP network:
	do {
		status = WiFi.begin(ssid, pass);
		delay(100);     // wait until connection is ready!
	} while (status != WL_CONNECTED);

	Serial.println("Connected to wifi");
	MQTT_TSB::printWifiStatus();
}

void MQTT_TSB::WiFi_getTime(){
	Udp.begin(localPort);

	// Time related stuff
	// set the Time library to use Teensy 3.0's RTC to keep time
  	setSyncProvider(getTeensy3Time);
	if (timeStatus()!= timeSet) {
		Serial.println("Unable to sync with the RTC");
	} else {
		Serial.println("RTC has set the system time");
	}
	defTime();
	Udp.stop();
	MQTT_TSB::printTime();
}

void MQTT_TSB::MQTT_setup()
{
	mqttClient.setClient(net);
	mqttClient.setServer(MQTT_BROKER, 1883);
}

void MQTT_TSB::MQTT_publishMessage(const char* topic, StaticJsonDocument<MQTT_MAX_PACKET_SIZE> payload )
{
		char bufferB[MQTT_MAX_PACKET_SIZE];
		serializeJson(payload, bufferB);
		mqttClient.publish(topic, bufferB);
}

void MQTT_TSB::MQTT_publishStringMessage(const char* topic, char* payload )
{
		mqttClient.publish(topic, payload);
}


// send an NTP request to the time server at the given address
void MQTT_TSB::sendNTPpacket(char *ntpSrv)
{
	// set all bytes in the buffer to 0
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	// (see URL above for details on the packets)

	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
	// 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12]  = 49;
	packetBuffer[13]  = 0x4E;
	packetBuffer[14]  = 49;
	packetBuffer[15]  = 52;

	// all NTP fields have been given values, now
	// you can send a packet requesting a timestamp:
	Udp.beginPacket(ntpSrv, 123); //NTP requests are to port 123

	Udp.write(packetBuffer, NTP_PACKET_SIZE);

	Udp.endPacket();
}

time_t MQTT_TSB::getTime(){
	char timeServer[] = "time.nist.gov";  // NTP server
	while (Udp.parsePacket() > 0) ; // discard any previously received packets
	sendNTPpacket(timeServer); // send an NTP packet to a time server
  
	// wait for a reply for UDP_TIMEOUT miliseconds
	unsigned long startMs = millis();
	while (!Udp.available() && (millis() - startMs) < UDP_TIMEOUT) {}

	if (Udp.parsePacket() >= NTP_PACKET_SIZE) {
		Serial.println("Receive NTP Response");
		// We've received a packet, read the data from it into the buffer
		Udp.read(packetBuffer, NTP_PACKET_SIZE);
		// the timestamp starts at byte 40 of the received packet and is four bytes,
		// or two words, long. First, extract the two words:
		unsigned short highWord = word(packetBuffer[40], packetBuffer[41]);
		unsigned short lowWord = word(packetBuffer[42], packetBuffer[43]);
		// combine the four bytes (two words) into a long integer
		// this is NTP time (seconds since Jan 1 1900):
		// Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
		// subtract seventy years:
		return (highWord << 16 | lowWord) - 2208988800UL;
	}
	else{
		Serial.println("No NTP Response :-(");
		return 0L;
	}

}

time_t MQTT_TSB::getTeensy3Time()
{
  return Teensy3Clock.get();
}

void MQTT_TSB::defTime(){
	time_t time = getTime();
	const signed long DEFAULT_TIME = 1357041600; // Jan 1 2013
	if( time < DEFAULT_TIME) { // check the value is a valid time (greater than Jan 1 2013)
       time = 0L; // return 0 to indicate that the time is not valid
    }
	Teensy3Clock.set(time); // set the RTC
	setTime(time);
}

void MQTT_TSB::printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void MQTT_TSB::printTime(){
	Serial.println((String)"Day: " + day() + " Month: " + month() + " Year: " + year() + " Hour: " + hour() + " Minutes: " + minute() + " Seconds: " + second() );
}