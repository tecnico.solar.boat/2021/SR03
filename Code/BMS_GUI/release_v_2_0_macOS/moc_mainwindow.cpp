/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../C++ code/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[44];
    char stringdata0[731];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 8), // "setupgui"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 9), // "updategui"
QT_MOC_LITERAL(4, 31, 8), // "resetgui"
QT_MOC_LITERAL(5, 40, 9), // "scanports"
QT_MOC_LITERAL(6, 50, 16), // "board_connection"
QT_MOC_LITERAL(7, 67, 7), // "connect"
QT_MOC_LITERAL(8, 75, 10), // "board_name"
QT_MOC_LITERAL(9, 86, 19), // "board_disconnection"
QT_MOC_LITERAL(10, 106, 16), // "check_port_valid"
QT_MOC_LITERAL(11, 123, 10), // "show_image"
QT_MOC_LITERAL(12, 134, 17), // "update_board_name"
QT_MOC_LITERAL(13, 152, 12), // "updategraphs"
QT_MOC_LITERAL(14, 165, 10), // "readSerial"
QT_MOC_LITERAL(15, 176, 10), // "sendSerial"
QT_MOC_LITERAL(16, 187, 11), // "updatetemps"
QT_MOC_LITERAL(17, 199, 10), // "parse_data"
QT_MOC_LITERAL(18, 210, 9), // "SERIAL_ID"
QT_MOC_LITERAL(19, 220, 11), // "data_length"
QT_MOC_LITERAL(20, 232, 17), // "parse_data_status"
QT_MOC_LITERAL(21, 250, 31), // "parse_data_SOC_voltage_currents"
QT_MOC_LITERAL(22, 282, 24), // "parse_data_cellvoltages0"
QT_MOC_LITERAL(23, 307, 24), // "parse_data_cellvoltages1"
QT_MOC_LITERAL(24, 332, 24), // "parse_data_cellvoltages2"
QT_MOC_LITERAL(25, 357, 30), // "parse_data_solarpanelvoltages0"
QT_MOC_LITERAL(26, 388, 30), // "parse_data_solarpanelvoltages1"
QT_MOC_LITERAL(27, 419, 24), // "parse_data_temperatures0"
QT_MOC_LITERAL(28, 444, 24), // "parse_data_temperatures1"
QT_MOC_LITERAL(29, 469, 24), // "parse_data_temperatures2"
QT_MOC_LITERAL(30, 494, 24), // "parse_data_temperatures3"
QT_MOC_LITERAL(31, 519, 24), // "parse_data_temperatures4"
QT_MOC_LITERAL(32, 544, 24), // "parse_data_temperatures5"
QT_MOC_LITERAL(33, 569, 18), // "parse_data_values1"
QT_MOC_LITERAL(34, 588, 18), // "parse_data_values3"
QT_MOC_LITERAL(35, 607, 13), // "getvaluesdata"
QT_MOC_LITERAL(36, 621, 19), // "construct_message_1"
QT_MOC_LITERAL(37, 641, 19), // "construct_message_3"
QT_MOC_LITERAL(38, 661, 17), // "readvalues_enable"
QT_MOC_LITERAL(39, 679, 8), // "checksum"
QT_MOC_LITERAL(40, 688, 7), // "message"
QT_MOC_LITERAL(41, 696, 23), // "get_highest_temperature"
QT_MOC_LITERAL(42, 720, 5), // "delay"
QT_MOC_LITERAL(43, 726, 4) // "time"

    },
    "MainWindow\0setupgui\0\0updategui\0resetgui\0"
    "scanports\0board_connection\0connect\0"
    "board_name\0board_disconnection\0"
    "check_port_valid\0show_image\0"
    "update_board_name\0updategraphs\0"
    "readSerial\0sendSerial\0updatetemps\0"
    "parse_data\0SERIAL_ID\0data_length\0"
    "parse_data_status\0parse_data_SOC_voltage_currents\0"
    "parse_data_cellvoltages0\0"
    "parse_data_cellvoltages1\0"
    "parse_data_cellvoltages2\0"
    "parse_data_solarpanelvoltages0\0"
    "parse_data_solarpanelvoltages1\0"
    "parse_data_temperatures0\0"
    "parse_data_temperatures1\0"
    "parse_data_temperatures2\0"
    "parse_data_temperatures3\0"
    "parse_data_temperatures4\0"
    "parse_data_temperatures5\0parse_data_values1\0"
    "parse_data_values3\0getvaluesdata\0"
    "construct_message_1\0construct_message_3\0"
    "readvalues_enable\0checksum\0message\0"
    "get_highest_temperature\0delay\0time"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      37,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  199,    2, 0x08 /* Private */,
       3,    0,  200,    2, 0x08 /* Private */,
       4,    0,  201,    2, 0x08 /* Private */,
       5,    0,  202,    2, 0x08 /* Private */,
       6,    0,  203,    2, 0x08 /* Private */,
       7,    1,  204,    2, 0x08 /* Private */,
       9,    0,  207,    2, 0x08 /* Private */,
      10,    0,  208,    2, 0x08 /* Private */,
      11,    0,  209,    2, 0x08 /* Private */,
      12,    0,  210,    2, 0x08 /* Private */,
      13,    0,  211,    2, 0x08 /* Private */,
      14,    0,  212,    2, 0x08 /* Private */,
      15,    0,  213,    2, 0x08 /* Private */,
      16,    0,  214,    2, 0x08 /* Private */,
      17,    2,  215,    2, 0x08 /* Private */,
      20,    1,  220,    2, 0x08 /* Private */,
      21,    1,  223,    2, 0x08 /* Private */,
      22,    1,  226,    2, 0x08 /* Private */,
      23,    1,  229,    2, 0x08 /* Private */,
      24,    1,  232,    2, 0x08 /* Private */,
      25,    1,  235,    2, 0x08 /* Private */,
      26,    1,  238,    2, 0x08 /* Private */,
      27,    1,  241,    2, 0x08 /* Private */,
      28,    1,  244,    2, 0x08 /* Private */,
      29,    1,  247,    2, 0x08 /* Private */,
      30,    1,  250,    2, 0x08 /* Private */,
      31,    1,  253,    2, 0x08 /* Private */,
      32,    1,  256,    2, 0x08 /* Private */,
      33,    1,  259,    2, 0x08 /* Private */,
      34,    1,  262,    2, 0x08 /* Private */,
      35,    0,  265,    2, 0x08 /* Private */,
      36,    0,  266,    2, 0x08 /* Private */,
      37,    0,  267,    2, 0x08 /* Private */,
      38,    0,  268,    2, 0x08 /* Private */,
      39,    1,  269,    2, 0x08 /* Private */,
      41,    0,  272,    2, 0x08 /* Private */,
      42,    1,  273,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray, QMetaType::Int,   18,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void,
    QMetaType::QByteArray,
    QMetaType::QByteArray,
    QMetaType::Void,
    QMetaType::UChar, QMetaType::QByteArray,   40,
    QMetaType::Double,
    QMetaType::Void, QMetaType::Int,   43,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setupgui(); break;
        case 1: _t->updategui(); break;
        case 2: _t->resetgui(); break;
        case 3: _t->scanports(); break;
        case 4: _t->board_connection(); break;
        case 5: _t->connect((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->board_disconnection(); break;
        case 7: _t->check_port_valid(); break;
        case 8: _t->show_image(); break;
        case 9: _t->update_board_name(); break;
        case 10: _t->updategraphs(); break;
        case 11: _t->readSerial(); break;
        case 12: _t->sendSerial(); break;
        case 13: _t->updatetemps(); break;
        case 14: _t->parse_data((*reinterpret_cast< QByteArray(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 15: _t->parse_data_status((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->parse_data_SOC_voltage_currents((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->parse_data_cellvoltages0((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->parse_data_cellvoltages1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->parse_data_cellvoltages2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->parse_data_solarpanelvoltages0((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->parse_data_solarpanelvoltages1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->parse_data_temperatures0((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->parse_data_temperatures1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 24: _t->parse_data_temperatures2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 25: _t->parse_data_temperatures3((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: _t->parse_data_temperatures4((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: _t->parse_data_temperatures5((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: _t->parse_data_values1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 29: _t->parse_data_values3((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 30: _t->getvaluesdata(); break;
        case 31: { QByteArray _r = _t->construct_message_1();
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = std::move(_r); }  break;
        case 32: { QByteArray _r = _t->construct_message_3();
            if (_a[0]) *reinterpret_cast< QByteArray*>(_a[0]) = std::move(_r); }  break;
        case 33: _t->readvalues_enable(); break;
        case 34: { unsigned char _r = _t->checksum((*reinterpret_cast< QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< unsigned char*>(_a[0]) = std::move(_r); }  break;
        case 35: { double _r = _t->get_highest_temperature();
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = std::move(_r); }  break;
        case 36: _t->delay((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 37)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 37;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 37)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 37;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
