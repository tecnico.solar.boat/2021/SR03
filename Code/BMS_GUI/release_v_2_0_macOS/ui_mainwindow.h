/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionArduino_Uno;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_23;
    QTabWidget *tabWidget;
    QWidget *livedata;
    QVBoxLayout *verticalLayout;
    QCustomPlot *voltage_graph;
    QLabel *bat_current_label;
    QHBoxLayout *horizontalLayout_13;
    QCustomPlot *bat_current_graph_negative;
    QCustomPlot *bat_current_graph_positive;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *battery_status_label;
    QFrame *battery_status_box;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_7;
    QLabel *state_label;
    QLabel *soc_label;
    QLabel *battery_voltage_label;
    QLabel *pack_temp_label;
    QHBoxLayout *horizontalLayout_5;
    QLabel *voltage_warning_label_1;
    QLabel *voltage_warning_label;
    QHBoxLayout *horizontalLayout_6;
    QLabel *current_warning_label_1;
    QLabel *current_warning_label;
    QHBoxLayout *horizontalLayout_7;
    QLabel *temperature_warning_label_1;
    QLabel *temperature_warning_label;
    QHBoxLayout *horizontalLayout_11;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_12;
    QLabel *balancing_label;
    QFrame *balancing_led_frame;
    QHBoxLayout *horizontalLayout_4;
    QLabel *pre_charge_relay_label;
    QWidget *pre_charge_relay_led_widget;
    QHBoxLayout *horizontalLayout_10;
    QLabel *motor_relay_label_1;
    QLabel *motor_relay_label;
    QWidget *motor_relay_led_widget;
    QHBoxLayout *horizontalLayout_9;
    QLabel *solar_relay_label_1;
    QLabel *solar_relay_label;
    QWidget *solar_relay_led_widget;
    QHBoxLayout *horizontalLayout_8;
    QLabel *charger_relay_label_1;
    QLabel *charger_relay_label;
    QWidget *charger_relay_led_widget;
    QVBoxLayout *verticalLayout_3;
    QLabel *solar_panels_label;
    QFrame *solar_panels_box;
    QVBoxLayout *verticalLayout_20;
    QVBoxLayout *verticalLayout_13;
    QLabel *solar_current_label;
    QCustomPlot *solar_current_graph;
    QHBoxLayout *horizontalLayout_18;
    QVBoxLayout *verticalLayout_9;
    QLabel *solar_voltage1_label;
    QLabel *solar_voltage2_label;
    QLabel *solar_voltage3_label;
    QVBoxLayout *verticalLayout_10;
    QLabel *solar_voltage4_label;
    QLabel *solar_voltage5_label;
    QLabel *solar_voltage6_label;
    QLabel *solar_voltage7_label;
    QWidget *temperatures;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_5;
    QCustomPlot *temp_graph;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_14;
    QCustomPlot *bus_temp_graph;
    QVBoxLayout *verticalLayout_14;
    QLabel *ambitemp;
    QLabel *heatsink1;
    QLabel *heatsink2;
    QLabel *ltctemp;
    QWidget *Solar_panels;
    QVBoxLayout *verticalLayout_18;
    QCustomPlot *solar_voltage_graph;
    QLabel *solar_current_label_2;
    QCustomPlot *solar_current_graph_2;
    QWidget *cellset;
    QVBoxLayout *verticalLayout_22;
    QHBoxLayout *horizontalLayout_23;
    QFrame *verticalFrame;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_15;
    QHBoxLayout *horizontalLayout_20;
    QLabel *over_voltage_box_label;
    QLineEdit *over_voltage_box;
    QLabel *voltage_unit_1;
    QHBoxLayout *horizontalLayout_19;
    QLabel *under_voltage_box_label;
    QLineEdit *under_voltage_box;
    QLabel *voltage_unit_2;
    QHBoxLayout *horizontalLayout_27;
    QLabel *discharge_current_box_label;
    QLineEdit *discharge_over_current_box;
    QLabel *current_unit_1;
    QHBoxLayout *horizontalLayout_26;
    QLabel *charge_current_box_label;
    QLineEdit *charge_over_current_box;
    QLabel *current_unit_2;
    QHBoxLayout *horizontalLayout_25;
    QLabel *over_heat_box_label;
    QLineEdit *over_heat_box;
    QLabel *temp_unit_1;
    QHBoxLayout *horizontalLayout_24;
    QLabel *allowed_disbalancing_label;
    QLineEdit *allowed_disbalancing_box;
    QLabel *voltage_unit_5;
    QHBoxLayout *horizontalLayout_22;
    QLabel *min_cell_voltage_balancing_label;
    QLineEdit *min_cell_voltage_balancing_box;
    QLabel *voltage_unit_6;
    QHBoxLayout *horizontalLayout_21;
    QLabel *label_2;
    QLineEdit *refresh_rate_box;
    QLabel *label_3;
    QCheckBox *check_balancing_allowed;
    QFrame *horizontalFrame_5;
    QHBoxLayout *horizontalLayout_15;
    QCommandLinkButton *read_button;
    QCommandLinkButton *upload_button;
    QWidget *log2file;
    QWidget *rawdata;
    QVBoxLayout *verticalLayout_19;
    QTextBrowser *rawdata_box;
    QWidget *board;
    QFrame *battery_status_box_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *Board_label;
    QComboBox *board_list;
    QCommandLinkButton *connect_button;
    QCommandLinkButton *disconnect_button;
    QLabel *label;
    QLabel *board_name;
    QLabel *bms_board_label;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(1254, 800);
        MainWindow->setMinimumSize(QSize(1100, 800));
        actionArduino_Uno = new QAction(MainWindow);
        actionArduino_Uno->setObjectName(QString::fromUtf8("actionArduino_Uno"));
        actionArduino_Uno->setAutoRepeat(false);
        actionArduino_Uno->setVisible(false);
        actionArduino_Uno->setIconVisibleInMenu(false);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setMinimumSize(QSize(800, 500));
        verticalLayout_23 = new QVBoxLayout(centralWidget);
        verticalLayout_23->setSpacing(6);
        verticalLayout_23->setContentsMargins(11, 11, 11, 11);
        verticalLayout_23->setObjectName(QString::fromUtf8("verticalLayout_23"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        tabWidget->setMinimumSize(QSize(800, 500));
        livedata = new QWidget();
        livedata->setObjectName(QString::fromUtf8("livedata"));
        verticalLayout = new QVBoxLayout(livedata);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        voltage_graph = new QCustomPlot(livedata);
        voltage_graph->setObjectName(QString::fromUtf8("voltage_graph"));

        verticalLayout->addWidget(voltage_graph);

        bat_current_label = new QLabel(livedata);
        bat_current_label->setObjectName(QString::fromUtf8("bat_current_label"));
        bat_current_label->setMaximumSize(QSize(300, 20));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        bat_current_label->setFont(font);
        bat_current_label->setLayoutDirection(Qt::LeftToRight);

        verticalLayout->addWidget(bat_current_label);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(0);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        bat_current_graph_negative = new QCustomPlot(livedata);
        bat_current_graph_negative->setObjectName(QString::fromUtf8("bat_current_graph_negative"));
        bat_current_graph_negative->setMinimumSize(QSize(0, 15));
        bat_current_graph_negative->setMaximumSize(QSize(16777215, 60));
        bat_current_graph_negative->setLayoutDirection(Qt::RightToLeft);

        horizontalLayout_13->addWidget(bat_current_graph_negative);

        bat_current_graph_positive = new QCustomPlot(livedata);
        bat_current_graph_positive->setObjectName(QString::fromUtf8("bat_current_graph_positive"));
        bat_current_graph_positive->setMaximumSize(QSize(16777215, 60));

        horizontalLayout_13->addWidget(bat_current_graph_positive);


        verticalLayout->addLayout(horizontalLayout_13);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(30);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_2->setContentsMargins(0, -1, 0, -1);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, -1, 0, -1);
        battery_status_label = new QLabel(livedata);
        battery_status_label->setObjectName(QString::fromUtf8("battery_status_label"));
        battery_status_label->setEnabled(true);
        battery_status_label->setMinimumSize(QSize(150, 30));
        battery_status_label->setMaximumSize(QSize(16777215, 30));
        battery_status_label->setFont(font);

        verticalLayout_2->addWidget(battery_status_label);

        battery_status_box = new QFrame(livedata);
        battery_status_box->setObjectName(QString::fromUtf8("battery_status_box"));
        battery_status_box->setMinimumSize(QSize(400, 170));
        battery_status_box->setMaximumSize(QSize(16777215, 16777215));
        battery_status_box->setFrameShape(QFrame::Box);
        battery_status_box->setFrameShadow(QFrame::Plain);
        horizontalLayout = new QHBoxLayout(battery_status_box);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        state_label = new QLabel(battery_status_box);
        state_label->setObjectName(QString::fromUtf8("state_label"));
        state_label->setEnabled(true);

        verticalLayout_7->addWidget(state_label);

        soc_label = new QLabel(battery_status_box);
        soc_label->setObjectName(QString::fromUtf8("soc_label"));
        soc_label->setEnabled(true);

        verticalLayout_7->addWidget(soc_label);

        battery_voltage_label = new QLabel(battery_status_box);
        battery_voltage_label->setObjectName(QString::fromUtf8("battery_voltage_label"));
        battery_voltage_label->setEnabled(true);

        verticalLayout_7->addWidget(battery_voltage_label);

        pack_temp_label = new QLabel(battery_status_box);
        pack_temp_label->setObjectName(QString::fromUtf8("pack_temp_label"));

        verticalLayout_7->addWidget(pack_temp_label);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(-1, -1, 50, -1);
        voltage_warning_label_1 = new QLabel(battery_status_box);
        voltage_warning_label_1->setObjectName(QString::fromUtf8("voltage_warning_label_1"));
        voltage_warning_label_1->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_5->addWidget(voltage_warning_label_1);

        voltage_warning_label = new QLabel(battery_status_box);
        voltage_warning_label->setObjectName(QString::fromUtf8("voltage_warning_label"));

        horizontalLayout_5->addWidget(voltage_warning_label);


        verticalLayout_7->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(-1, -1, 50, -1);
        current_warning_label_1 = new QLabel(battery_status_box);
        current_warning_label_1->setObjectName(QString::fromUtf8("current_warning_label_1"));
        current_warning_label_1->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_6->addWidget(current_warning_label_1);

        current_warning_label = new QLabel(battery_status_box);
        current_warning_label->setObjectName(QString::fromUtf8("current_warning_label"));

        horizontalLayout_6->addWidget(current_warning_label);


        verticalLayout_7->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(0);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(-1, -1, 0, -1);
        temperature_warning_label_1 = new QLabel(battery_status_box);
        temperature_warning_label_1->setObjectName(QString::fromUtf8("temperature_warning_label_1"));
        temperature_warning_label_1->setMaximumSize(QSize(250, 16777215));

        horizontalLayout_7->addWidget(temperature_warning_label_1);

        temperature_warning_label = new QLabel(battery_status_box);
        temperature_warning_label->setObjectName(QString::fromUtf8("temperature_warning_label"));

        horizontalLayout_7->addWidget(temperature_warning_label);


        verticalLayout_7->addLayout(horizontalLayout_7);


        horizontalLayout->addLayout(verticalLayout_7);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(0);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(0);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(-1, -1, -1, 0);
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        balancing_label = new QLabel(battery_status_box);
        balancing_label->setObjectName(QString::fromUtf8("balancing_label"));
        balancing_label->setEnabled(true);
        balancing_label->setMinimumSize(QSize(0, 28));
        balancing_label->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_12->addWidget(balancing_label);

        balancing_led_frame = new QFrame(battery_status_box);
        balancing_led_frame->setObjectName(QString::fromUtf8("balancing_led_frame"));
        balancing_led_frame->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_12->addWidget(balancing_led_frame, 0, Qt::AlignRight);


        verticalLayout_8->addLayout(horizontalLayout_12);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        pre_charge_relay_label = new QLabel(battery_status_box);
        pre_charge_relay_label->setObjectName(QString::fromUtf8("pre_charge_relay_label"));
        pre_charge_relay_label->setEnabled(true);
        pre_charge_relay_label->setMinimumSize(QSize(0, 28));
        pre_charge_relay_label->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_4->addWidget(pre_charge_relay_label);

        pre_charge_relay_led_widget = new QWidget(battery_status_box);
        pre_charge_relay_led_widget->setObjectName(QString::fromUtf8("pre_charge_relay_led_widget"));
        pre_charge_relay_led_widget->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_4->addWidget(pre_charge_relay_led_widget, 0, Qt::AlignRight);


        verticalLayout_8->addLayout(horizontalLayout_4);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        motor_relay_label_1 = new QLabel(battery_status_box);
        motor_relay_label_1->setObjectName(QString::fromUtf8("motor_relay_label_1"));
        motor_relay_label_1->setEnabled(true);
        motor_relay_label_1->setMinimumSize(QSize(0, 28));
        motor_relay_label_1->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_10->addWidget(motor_relay_label_1);

        motor_relay_label = new QLabel(battery_status_box);
        motor_relay_label->setObjectName(QString::fromUtf8("motor_relay_label"));
        motor_relay_label->setEnabled(true);
        motor_relay_label->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_10->addWidget(motor_relay_label);

        motor_relay_led_widget = new QWidget(battery_status_box);
        motor_relay_led_widget->setObjectName(QString::fromUtf8("motor_relay_led_widget"));
        motor_relay_led_widget->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_10->addWidget(motor_relay_led_widget, 0, Qt::AlignRight);


        verticalLayout_8->addLayout(horizontalLayout_10);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        solar_relay_label_1 = new QLabel(battery_status_box);
        solar_relay_label_1->setObjectName(QString::fromUtf8("solar_relay_label_1"));
        solar_relay_label_1->setEnabled(true);
        solar_relay_label_1->setMinimumSize(QSize(0, 28));
        solar_relay_label_1->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_9->addWidget(solar_relay_label_1);

        solar_relay_label = new QLabel(battery_status_box);
        solar_relay_label->setObjectName(QString::fromUtf8("solar_relay_label"));
        solar_relay_label->setEnabled(true);
        solar_relay_label->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_9->addWidget(solar_relay_label);

        solar_relay_led_widget = new QWidget(battery_status_box);
        solar_relay_led_widget->setObjectName(QString::fromUtf8("solar_relay_led_widget"));
        solar_relay_led_widget->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_9->addWidget(solar_relay_led_widget, 0, Qt::AlignRight);


        verticalLayout_8->addLayout(horizontalLayout_9);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        charger_relay_label_1 = new QLabel(battery_status_box);
        charger_relay_label_1->setObjectName(QString::fromUtf8("charger_relay_label_1"));
        charger_relay_label_1->setEnabled(true);
        charger_relay_label_1->setMinimumSize(QSize(0, 28));
        charger_relay_label_1->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_8->addWidget(charger_relay_label_1);

        charger_relay_label = new QLabel(battery_status_box);
        charger_relay_label->setObjectName(QString::fromUtf8("charger_relay_label"));
        charger_relay_label->setEnabled(true);
        charger_relay_label->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_8->addWidget(charger_relay_label);

        charger_relay_led_widget = new QWidget(battery_status_box);
        charger_relay_led_widget->setObjectName(QString::fromUtf8("charger_relay_led_widget"));
        charger_relay_led_widget->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_8->addWidget(charger_relay_led_widget, 0, Qt::AlignRight);


        verticalLayout_8->addLayout(horizontalLayout_8);


        horizontalLayout_11->addLayout(verticalLayout_8);


        horizontalLayout->addLayout(horizontalLayout_11);


        verticalLayout_2->addWidget(battery_status_box);


        horizontalLayout_2->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        solar_panels_label = new QLabel(livedata);
        solar_panels_label->setObjectName(QString::fromUtf8("solar_panels_label"));
        solar_panels_label->setEnabled(true);
        solar_panels_label->setMinimumSize(QSize(150, 30));
        solar_panels_label->setMaximumSize(QSize(16777215, 30));
        solar_panels_label->setFont(font);

        verticalLayout_3->addWidget(solar_panels_label);

        solar_panels_box = new QFrame(livedata);
        solar_panels_box->setObjectName(QString::fromUtf8("solar_panels_box"));
        solar_panels_box->setMinimumSize(QSize(400, 170));
        solar_panels_box->setMaximumSize(QSize(16777215, 16777215));
        solar_panels_box->setFrameShape(QFrame::Box);
        solar_panels_box->setFrameShadow(QFrame::Plain);
        verticalLayout_20 = new QVBoxLayout(solar_panels_box);
        verticalLayout_20->setSpacing(6);
        verticalLayout_20->setContentsMargins(11, 11, 11, 11);
        verticalLayout_20->setObjectName(QString::fromUtf8("verticalLayout_20"));
        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setSpacing(0);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        solar_current_label = new QLabel(solar_panels_box);
        solar_current_label->setObjectName(QString::fromUtf8("solar_current_label"));
        solar_current_label->setMaximumSize(QSize(16777215, 16777215));

        verticalLayout_13->addWidget(solar_current_label);

        solar_current_graph = new QCustomPlot(solar_panels_box);
        solar_current_graph->setObjectName(QString::fromUtf8("solar_current_graph"));
        solar_current_graph->setEnabled(true);
        solar_current_graph->setMinimumSize(QSize(300, 50));
        solar_current_graph->setMaximumSize(QSize(16777215, 50));
        solar_current_graph->setLayoutDirection(Qt::RightToLeft);

        verticalLayout_13->addWidget(solar_current_graph);


        verticalLayout_20->addLayout(verticalLayout_13);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setSpacing(5);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        solar_voltage1_label = new QLabel(solar_panels_box);
        solar_voltage1_label->setObjectName(QString::fromUtf8("solar_voltage1_label"));
        solar_voltage1_label->setEnabled(true);

        verticalLayout_9->addWidget(solar_voltage1_label);

        solar_voltage2_label = new QLabel(solar_panels_box);
        solar_voltage2_label->setObjectName(QString::fromUtf8("solar_voltage2_label"));
        solar_voltage2_label->setEnabled(true);

        verticalLayout_9->addWidget(solar_voltage2_label);

        solar_voltage3_label = new QLabel(solar_panels_box);
        solar_voltage3_label->setObjectName(QString::fromUtf8("solar_voltage3_label"));
        solar_voltage3_label->setEnabled(true);

        verticalLayout_9->addWidget(solar_voltage3_label);


        horizontalLayout_18->addLayout(verticalLayout_9);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalLayout_10->setContentsMargins(0, -1, -1, 40);
        solar_voltage4_label = new QLabel(solar_panels_box);
        solar_voltage4_label->setObjectName(QString::fromUtf8("solar_voltage4_label"));
        solar_voltage4_label->setEnabled(true);
        solar_voltage4_label->setMaximumSize(QSize(16777215, 16777215));

        verticalLayout_10->addWidget(solar_voltage4_label);

        solar_voltage5_label = new QLabel(solar_panels_box);
        solar_voltage5_label->setObjectName(QString::fromUtf8("solar_voltage5_label"));
        solar_voltage5_label->setEnabled(true);

        verticalLayout_10->addWidget(solar_voltage5_label);

        solar_voltage6_label = new QLabel(solar_panels_box);
        solar_voltage6_label->setObjectName(QString::fromUtf8("solar_voltage6_label"));

        verticalLayout_10->addWidget(solar_voltage6_label);

        solar_voltage7_label = new QLabel(solar_panels_box);
        solar_voltage7_label->setObjectName(QString::fromUtf8("solar_voltage7_label"));

        verticalLayout_10->addWidget(solar_voltage7_label);


        horizontalLayout_18->addLayout(verticalLayout_10);


        verticalLayout_20->addLayout(horizontalLayout_18);


        verticalLayout_3->addWidget(solar_panels_box);


        horizontalLayout_2->addLayout(verticalLayout_3);


        verticalLayout->addLayout(horizontalLayout_2);

        tabWidget->addTab(livedata, QString());
        temperatures = new QWidget();
        temperatures->setObjectName(QString::fromUtf8("temperatures"));
        verticalLayout_4 = new QVBoxLayout(temperatures);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_5 = new QLabel(temperatures);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMaximumSize(QSize(16777215, 30));

        verticalLayout_4->addWidget(label_5);

        temp_graph = new QCustomPlot(temperatures);
        temp_graph->setObjectName(QString::fromUtf8("temp_graph"));

        verticalLayout_4->addWidget(temp_graph);

        label_4 = new QLabel(temperatures);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMaximumSize(QSize(16777215, 30));

        verticalLayout_4->addWidget(label_4);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(20);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        horizontalLayout_14->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_14->setContentsMargins(0, -1, 0, -1);
        bus_temp_graph = new QCustomPlot(temperatures);
        bus_temp_graph->setObjectName(QString::fromUtf8("bus_temp_graph"));
        bus_temp_graph->setEnabled(true);

        horizontalLayout_14->addWidget(bus_temp_graph);

        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setSpacing(6);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        verticalLayout_14->setSizeConstraint(QLayout::SetDefaultConstraint);
        ambitemp = new QLabel(temperatures);
        ambitemp->setObjectName(QString::fromUtf8("ambitemp"));
        ambitemp->setMinimumSize(QSize(250, 60));
        ambitemp->setMaximumSize(QSize(250, 16777215));

        verticalLayout_14->addWidget(ambitemp);

        heatsink1 = new QLabel(temperatures);
        heatsink1->setObjectName(QString::fromUtf8("heatsink1"));
        heatsink1->setMinimumSize(QSize(250, 60));
        heatsink1->setMaximumSize(QSize(250, 16777215));

        verticalLayout_14->addWidget(heatsink1);

        heatsink2 = new QLabel(temperatures);
        heatsink2->setObjectName(QString::fromUtf8("heatsink2"));
        heatsink2->setMinimumSize(QSize(250, 60));
        heatsink2->setMaximumSize(QSize(250, 16777215));

        verticalLayout_14->addWidget(heatsink2);

        ltctemp = new QLabel(temperatures);
        ltctemp->setObjectName(QString::fromUtf8("ltctemp"));
        ltctemp->setMinimumSize(QSize(200, 60));
        ltctemp->setMaximumSize(QSize(200, 16777215));

        verticalLayout_14->addWidget(ltctemp);


        horizontalLayout_14->addLayout(verticalLayout_14);

        horizontalLayout_14->setStretch(0, 1);

        verticalLayout_4->addLayout(horizontalLayout_14);

        tabWidget->addTab(temperatures, QString());
        Solar_panels = new QWidget();
        Solar_panels->setObjectName(QString::fromUtf8("Solar_panels"));
        verticalLayout_18 = new QVBoxLayout(Solar_panels);
        verticalLayout_18->setSpacing(6);
        verticalLayout_18->setContentsMargins(11, 11, 11, 11);
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        verticalLayout_18->setContentsMargins(9, -1, -1, 150);
        solar_voltage_graph = new QCustomPlot(Solar_panels);
        solar_voltage_graph->setObjectName(QString::fromUtf8("solar_voltage_graph"));
        solar_voltage_graph->setMinimumSize(QSize(0, 0));
        solar_voltage_graph->setMaximumSize(QSize(16777215, 16777215));

        verticalLayout_18->addWidget(solar_voltage_graph);

        solar_current_label_2 = new QLabel(Solar_panels);
        solar_current_label_2->setObjectName(QString::fromUtf8("solar_current_label_2"));
        solar_current_label_2->setMinimumSize(QSize(300, 20));
        solar_current_label_2->setMaximumSize(QSize(16777215, 20));
        solar_current_label_2->setFont(font);

        verticalLayout_18->addWidget(solar_current_label_2);

        solar_current_graph_2 = new QCustomPlot(Solar_panels);
        solar_current_graph_2->setObjectName(QString::fromUtf8("solar_current_graph_2"));
        solar_current_graph_2->setMaximumSize(QSize(16777215, 60));
        solar_current_graph_2->setLayoutDirection(Qt::RightToLeft);

        verticalLayout_18->addWidget(solar_current_graph_2);

        tabWidget->addTab(Solar_panels, QString());
        cellset = new QWidget();
        cellset->setObjectName(QString::fromUtf8("cellset"));
        verticalLayout_22 = new QVBoxLayout(cellset);
        verticalLayout_22->setSpacing(6);
        verticalLayout_22->setContentsMargins(11, 11, 11, 11);
        verticalLayout_22->setObjectName(QString::fromUtf8("verticalLayout_22"));
        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setSpacing(0);
        horizontalLayout_23->setObjectName(QString::fromUtf8("horizontalLayout_23"));
        horizontalLayout_23->setContentsMargins(0, 0, 0, -1);
        verticalFrame = new QFrame(cellset);
        verticalFrame->setObjectName(QString::fromUtf8("verticalFrame"));
        verticalFrame->setMaximumSize(QSize(16777215, 16777215));
        verticalFrame->setFrameShape(QFrame::Box);
        verticalLayout_6 = new QVBoxLayout(verticalFrame);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_15 = new QLabel(verticalFrame);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setMaximumSize(QSize(16777215, 30));
        label_15->setFont(font);

        verticalLayout_6->addWidget(label_15);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        over_voltage_box_label = new QLabel(verticalFrame);
        over_voltage_box_label->setObjectName(QString::fromUtf8("over_voltage_box_label"));
        over_voltage_box_label->setMinimumSize(QSize(0, 0));
        over_voltage_box_label->setMaximumSize(QSize(300, 16777215));
        over_voltage_box_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        over_voltage_box_label->setIndent(1);

        horizontalLayout_20->addWidget(over_voltage_box_label);

        over_voltage_box = new QLineEdit(verticalFrame);
        over_voltage_box->setObjectName(QString::fromUtf8("over_voltage_box"));
        over_voltage_box->setMinimumSize(QSize(0, 0));
        over_voltage_box->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_20->addWidget(over_voltage_box);

        voltage_unit_1 = new QLabel(verticalFrame);
        voltage_unit_1->setObjectName(QString::fromUtf8("voltage_unit_1"));
        voltage_unit_1->setMinimumSize(QSize(0, 0));
        voltage_unit_1->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_20->addWidget(voltage_unit_1);


        verticalLayout_6->addLayout(horizontalLayout_20);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        under_voltage_box_label = new QLabel(verticalFrame);
        under_voltage_box_label->setObjectName(QString::fromUtf8("under_voltage_box_label"));
        under_voltage_box_label->setMinimumSize(QSize(0, 0));
        under_voltage_box_label->setMaximumSize(QSize(300, 16777215));
        under_voltage_box_label->setIndent(1);

        horizontalLayout_19->addWidget(under_voltage_box_label);

        under_voltage_box = new QLineEdit(verticalFrame);
        under_voltage_box->setObjectName(QString::fromUtf8("under_voltage_box"));
        under_voltage_box->setMinimumSize(QSize(0, 0));
        under_voltage_box->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_19->addWidget(under_voltage_box);

        voltage_unit_2 = new QLabel(verticalFrame);
        voltage_unit_2->setObjectName(QString::fromUtf8("voltage_unit_2"));
        voltage_unit_2->setMinimumSize(QSize(100, 20));
        voltage_unit_2->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_19->addWidget(voltage_unit_2);


        verticalLayout_6->addLayout(horizontalLayout_19);

        horizontalLayout_27 = new QHBoxLayout();
        horizontalLayout_27->setSpacing(6);
        horizontalLayout_27->setObjectName(QString::fromUtf8("horizontalLayout_27"));
        discharge_current_box_label = new QLabel(verticalFrame);
        discharge_current_box_label->setObjectName(QString::fromUtf8("discharge_current_box_label"));
        discharge_current_box_label->setMinimumSize(QSize(0, 0));
        discharge_current_box_label->setMaximumSize(QSize(300, 16777215));
        discharge_current_box_label->setIndent(1);

        horizontalLayout_27->addWidget(discharge_current_box_label);

        discharge_over_current_box = new QLineEdit(verticalFrame);
        discharge_over_current_box->setObjectName(QString::fromUtf8("discharge_over_current_box"));
        discharge_over_current_box->setMinimumSize(QSize(0, 0));
        discharge_over_current_box->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_27->addWidget(discharge_over_current_box);

        current_unit_1 = new QLabel(verticalFrame);
        current_unit_1->setObjectName(QString::fromUtf8("current_unit_1"));
        current_unit_1->setMinimumSize(QSize(100, 20));
        current_unit_1->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_27->addWidget(current_unit_1);


        verticalLayout_6->addLayout(horizontalLayout_27);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setSpacing(6);
        horizontalLayout_26->setObjectName(QString::fromUtf8("horizontalLayout_26"));
        charge_current_box_label = new QLabel(verticalFrame);
        charge_current_box_label->setObjectName(QString::fromUtf8("charge_current_box_label"));
        charge_current_box_label->setMinimumSize(QSize(0, 0));
        charge_current_box_label->setMaximumSize(QSize(300, 16777215));
        charge_current_box_label->setIndent(1);

        horizontalLayout_26->addWidget(charge_current_box_label);

        charge_over_current_box = new QLineEdit(verticalFrame);
        charge_over_current_box->setObjectName(QString::fromUtf8("charge_over_current_box"));
        charge_over_current_box->setMinimumSize(QSize(0, 0));
        charge_over_current_box->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_26->addWidget(charge_over_current_box);

        current_unit_2 = new QLabel(verticalFrame);
        current_unit_2->setObjectName(QString::fromUtf8("current_unit_2"));
        current_unit_2->setMinimumSize(QSize(100, 20));
        current_unit_2->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_26->addWidget(current_unit_2);


        verticalLayout_6->addLayout(horizontalLayout_26);

        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setSpacing(6);
        horizontalLayout_25->setObjectName(QString::fromUtf8("horizontalLayout_25"));
        over_heat_box_label = new QLabel(verticalFrame);
        over_heat_box_label->setObjectName(QString::fromUtf8("over_heat_box_label"));
        over_heat_box_label->setMinimumSize(QSize(0, 0));
        over_heat_box_label->setMaximumSize(QSize(300, 16777215));
        over_heat_box_label->setIndent(1);

        horizontalLayout_25->addWidget(over_heat_box_label);

        over_heat_box = new QLineEdit(verticalFrame);
        over_heat_box->setObjectName(QString::fromUtf8("over_heat_box"));
        over_heat_box->setMinimumSize(QSize(0, 0));
        over_heat_box->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_25->addWidget(over_heat_box);

        temp_unit_1 = new QLabel(verticalFrame);
        temp_unit_1->setObjectName(QString::fromUtf8("temp_unit_1"));
        temp_unit_1->setMinimumSize(QSize(100, 20));
        temp_unit_1->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_25->addWidget(temp_unit_1);


        verticalLayout_6->addLayout(horizontalLayout_25);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setSpacing(6);
        horizontalLayout_24->setObjectName(QString::fromUtf8("horizontalLayout_24"));
        allowed_disbalancing_label = new QLabel(verticalFrame);
        allowed_disbalancing_label->setObjectName(QString::fromUtf8("allowed_disbalancing_label"));
        allowed_disbalancing_label->setMinimumSize(QSize(0, 0));
        allowed_disbalancing_label->setMaximumSize(QSize(300, 16777215));

        horizontalLayout_24->addWidget(allowed_disbalancing_label);

        allowed_disbalancing_box = new QLineEdit(verticalFrame);
        allowed_disbalancing_box->setObjectName(QString::fromUtf8("allowed_disbalancing_box"));
        allowed_disbalancing_box->setMinimumSize(QSize(0, 0));
        allowed_disbalancing_box->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_24->addWidget(allowed_disbalancing_box);

        voltage_unit_5 = new QLabel(verticalFrame);
        voltage_unit_5->setObjectName(QString::fromUtf8("voltage_unit_5"));

        horizontalLayout_24->addWidget(voltage_unit_5);


        verticalLayout_6->addLayout(horizontalLayout_24);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        min_cell_voltage_balancing_label = new QLabel(verticalFrame);
        min_cell_voltage_balancing_label->setObjectName(QString::fromUtf8("min_cell_voltage_balancing_label"));
        min_cell_voltage_balancing_label->setMinimumSize(QSize(0, 0));
        min_cell_voltage_balancing_label->setMaximumSize(QSize(300, 16777215));

        horizontalLayout_22->addWidget(min_cell_voltage_balancing_label);

        min_cell_voltage_balancing_box = new QLineEdit(verticalFrame);
        min_cell_voltage_balancing_box->setObjectName(QString::fromUtf8("min_cell_voltage_balancing_box"));
        min_cell_voltage_balancing_box->setMinimumSize(QSize(0, 0));
        min_cell_voltage_balancing_box->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_22->addWidget(min_cell_voltage_balancing_box);

        voltage_unit_6 = new QLabel(verticalFrame);
        voltage_unit_6->setObjectName(QString::fromUtf8("voltage_unit_6"));

        horizontalLayout_22->addWidget(voltage_unit_6);


        verticalLayout_6->addLayout(horizontalLayout_22);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        label_2 = new QLabel(verticalFrame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMaximumSize(QSize(300, 16777215));

        horizontalLayout_21->addWidget(label_2);

        refresh_rate_box = new QLineEdit(verticalFrame);
        refresh_rate_box->setObjectName(QString::fromUtf8("refresh_rate_box"));
        refresh_rate_box->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_21->addWidget(refresh_rate_box);

        label_3 = new QLabel(verticalFrame);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_21->addWidget(label_3);


        verticalLayout_6->addLayout(horizontalLayout_21);

        check_balancing_allowed = new QCheckBox(verticalFrame);
        check_balancing_allowed->setObjectName(QString::fromUtf8("check_balancing_allowed"));
        sizePolicy.setHeightForWidth(check_balancing_allowed->sizePolicy().hasHeightForWidth());
        check_balancing_allowed->setSizePolicy(sizePolicy);
        check_balancing_allowed->setMinimumSize(QSize(0, 0));
        check_balancing_allowed->setMaximumSize(QSize(16777215, 16777215));
        check_balancing_allowed->setLayoutDirection(Qt::LeftToRight);

        verticalLayout_6->addWidget(check_balancing_allowed);

        horizontalFrame_5 = new QFrame(verticalFrame);
        horizontalFrame_5->setObjectName(QString::fromUtf8("horizontalFrame_5"));
        horizontalFrame_5->setMaximumSize(QSize(16777215, 16777215));
        horizontalFrame_5->setFrameShape(QFrame::Box);
        horizontalLayout_15 = new QHBoxLayout(horizontalFrame_5);
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        read_button = new QCommandLinkButton(horizontalFrame_5);
        read_button->setObjectName(QString::fromUtf8("read_button"));
        read_button->setMinimumSize(QSize(300, 50));
        read_button->setMaximumSize(QSize(300, 50));
        read_button->setIconSize(QSize(50, 50));

        horizontalLayout_15->addWidget(read_button);

        upload_button = new QCommandLinkButton(horizontalFrame_5);
        upload_button->setObjectName(QString::fromUtf8("upload_button"));
        upload_button->setMinimumSize(QSize(300, 50));
        upload_button->setMaximumSize(QSize(300, 50));
        upload_button->setIconSize(QSize(50, 50));

        horizontalLayout_15->addWidget(upload_button);


        verticalLayout_6->addWidget(horizontalFrame_5);


        horizontalLayout_23->addWidget(verticalFrame);


        verticalLayout_22->addLayout(horizontalLayout_23);

        tabWidget->addTab(cellset, QString());
        log2file = new QWidget();
        log2file->setObjectName(QString::fromUtf8("log2file"));
        tabWidget->addTab(log2file, QString());
        rawdata = new QWidget();
        rawdata->setObjectName(QString::fromUtf8("rawdata"));
        verticalLayout_19 = new QVBoxLayout(rawdata);
        verticalLayout_19->setSpacing(6);
        verticalLayout_19->setContentsMargins(11, 11, 11, 11);
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        rawdata_box = new QTextBrowser(rawdata);
        rawdata_box->setObjectName(QString::fromUtf8("rawdata_box"));

        verticalLayout_19->addWidget(rawdata_box);

        tabWidget->addTab(rawdata, QString());
        board = new QWidget();
        board->setObjectName(QString::fromUtf8("board"));
        battery_status_box_2 = new QFrame(board);
        battery_status_box_2->setObjectName(QString::fromUtf8("battery_status_box_2"));
        battery_status_box_2->setGeometry(QRect(20, 40, 1031, 71));
        battery_status_box_2->setFrameShape(QFrame::Box);
        battery_status_box_2->setFrameShadow(QFrame::Plain);
        horizontalLayout_3 = new QHBoxLayout(battery_status_box_2);
        horizontalLayout_3->setSpacing(50);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        Board_label = new QLabel(battery_status_box_2);
        Board_label->setObjectName(QString::fromUtf8("Board_label"));
        Board_label->setEnabled(true);
        QFont font1;
        font1.setPointSize(10);
        Board_label->setFont(font1);

        horizontalLayout_3->addWidget(Board_label);

        board_list = new QComboBox(battery_status_box_2);
        board_list->setObjectName(QString::fromUtf8("board_list"));

        horizontalLayout_3->addWidget(board_list);

        connect_button = new QCommandLinkButton(battery_status_box_2);
        connect_button->setObjectName(QString::fromUtf8("connect_button"));

        horizontalLayout_3->addWidget(connect_button);

        disconnect_button = new QCommandLinkButton(battery_status_box_2);
        disconnect_button->setObjectName(QString::fromUtf8("disconnect_button"));

        horizontalLayout_3->addWidget(disconnect_button);

        label = new QLabel(battery_status_box_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMaximumSize(QSize(200, 10));

        horizontalLayout_3->addWidget(label);

        board_name = new QLabel(battery_status_box_2);
        board_name->setObjectName(QString::fromUtf8("board_name"));
        board_name->setFont(font1);

        horizontalLayout_3->addWidget(board_name);

        bms_board_label = new QLabel(board);
        bms_board_label->setObjectName(QString::fromUtf8("bms_board_label"));
        bms_board_label->setEnabled(true);
        bms_board_label->setGeometry(QRect(20, 10, 151, 16));
        bms_board_label->setFont(font1);
        tabWidget->addTab(board, QString());

        verticalLayout_23->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        statusBar->setAutoFillBackground(false);
        statusBar->setSizeGripEnabled(true);
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "TSB - BMS V2.0", nullptr));
        actionArduino_Uno->setText(QCoreApplication::translate("MainWindow", "Board", nullptr));
        bat_current_label->setText(QCoreApplication::translate("MainWindow", "Battery Current: - ", nullptr));
        battery_status_label->setText(QCoreApplication::translate("MainWindow", "Battery Status:", nullptr));
        state_label->setText(QCoreApplication::translate("MainWindow", "Current State: - ", nullptr));
        soc_label->setText(QCoreApplication::translate("MainWindow", "State Of Charge: - ", nullptr));
        battery_voltage_label->setText(QCoreApplication::translate("MainWindow", "Battery Voltage: - ", nullptr));
        pack_temp_label->setText(QCoreApplication::translate("MainWindow", "Pack Temperature: -", nullptr));
        voltage_warning_label_1->setText(QCoreApplication::translate("MainWindow", "Voltage Warning:", nullptr));
        voltage_warning_label->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        current_warning_label_1->setText(QCoreApplication::translate("MainWindow", "Current Warning:", nullptr));
        current_warning_label->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        temperature_warning_label_1->setText(QCoreApplication::translate("MainWindow", "Temperature Warning:", nullptr));
        temperature_warning_label->setText(QCoreApplication::translate("MainWindow", " -", nullptr));
        balancing_label->setText(QCoreApplication::translate("MainWindow", "Balancing: ", nullptr));
        pre_charge_relay_label->setText(QCoreApplication::translate("MainWindow", "Pre-Charge Relay:", nullptr));
        motor_relay_label_1->setText(QCoreApplication::translate("MainWindow", "Motor Relay:", nullptr));
        motor_relay_label->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        solar_relay_label_1->setText(QCoreApplication::translate("MainWindow", "Solar Relays:", nullptr));
        solar_relay_label->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        charger_relay_label_1->setText(QCoreApplication::translate("MainWindow", "Charger Relay:", nullptr));
        charger_relay_label->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        solar_panels_label->setText(QCoreApplication::translate("MainWindow", "Solar Panel Status:", nullptr));
        solar_current_label->setText(QCoreApplication::translate("MainWindow", "Solar Panels Current: - ", nullptr));
        solar_voltage1_label->setText(QCoreApplication::translate("MainWindow", "Panel1 Voltage: - ", nullptr));
        solar_voltage2_label->setText(QCoreApplication::translate("MainWindow", "Panel2 Voltage: - ", nullptr));
        solar_voltage3_label->setText(QCoreApplication::translate("MainWindow", "Panel3 Voltage: - ", nullptr));
        solar_voltage4_label->setText(QCoreApplication::translate("MainWindow", "Panel4 Voltage: - ", nullptr));
        solar_voltage5_label->setText(QCoreApplication::translate("MainWindow", "Panel5 Voltage: - ", nullptr));
        solar_voltage6_label->setText(QCoreApplication::translate("MainWindow", "Panel6 Voltage: -", nullptr));
        solar_voltage7_label->setText(QCoreApplication::translate("MainWindow", "Panel7 Voltage: -", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(livedata), QCoreApplication::translate("MainWindow", "Live Data", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "Cells Temperatures", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "Busbars temperatures", nullptr));
        ambitemp->setText(QCoreApplication::translate("MainWindow", "Ambient Temperature: -", nullptr));
        heatsink1->setText(QCoreApplication::translate("MainWindow", "Heat Sink 1 Temperature: -", nullptr));
        heatsink2->setText(QCoreApplication::translate("MainWindow", "Heat Sink 2 Temperature: -", nullptr));
        ltctemp->setText(QCoreApplication::translate("MainWindow", "LTC Temperature: -", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(temperatures), QCoreApplication::translate("MainWindow", "Temperatures", nullptr));
        solar_current_label_2->setText(QCoreApplication::translate("MainWindow", "Solar Panels Current: - ", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Solar_panels), QCoreApplication::translate("MainWindow", "Solar Panels", nullptr));
        label_15->setText(QCoreApplication::translate("MainWindow", "BMS Parameters:", nullptr));
        over_voltage_box_label->setText(QCoreApplication::translate("MainWindow", "Over-Voltage Cutoff: ", nullptr));
        voltage_unit_1->setText(QCoreApplication::translate("MainWindow", "V", nullptr));
        under_voltage_box_label->setText(QCoreApplication::translate("MainWindow", "Under-Voltage Cutoff: ", nullptr));
        voltage_unit_2->setText(QCoreApplication::translate("MainWindow", "V", nullptr));
        discharge_current_box_label->setText(QCoreApplication::translate("MainWindow", "Discharge Over-Current Cutoff:", nullptr));
        current_unit_1->setText(QCoreApplication::translate("MainWindow", "A", nullptr));
        charge_current_box_label->setText(QCoreApplication::translate("MainWindow", "Charge Over-Current Cutoff:", nullptr));
        current_unit_2->setText(QCoreApplication::translate("MainWindow", "A", nullptr));
        over_heat_box_label->setText(QCoreApplication::translate("MainWindow", "Over-Heat Cutoff:", nullptr));
        temp_unit_1->setText(QCoreApplication::translate("MainWindow", "\302\272 C", nullptr));
        allowed_disbalancing_label->setText(QCoreApplication::translate("MainWindow", "Allowed Disbalancing", nullptr));
        voltage_unit_5->setText(QCoreApplication::translate("MainWindow", "mV", nullptr));
        min_cell_voltage_balancing_label->setText(QCoreApplication::translate("MainWindow", "Minimum Cell Voltage For Balancing", nullptr));
        voltage_unit_6->setText(QCoreApplication::translate("MainWindow", "V", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Refresh Rate", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "ms", nullptr));
        check_balancing_allowed->setText(QCoreApplication::translate("MainWindow", "Allowed Balancing", nullptr));
        read_button->setText(QCoreApplication::translate("MainWindow", "Read Board", nullptr));
        upload_button->setText(QCoreApplication::translate("MainWindow", "Upload", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(cellset), QCoreApplication::translate("MainWindow", "Cell Settings", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(log2file), QCoreApplication::translate("MainWindow", "Log to file", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(rawdata), QCoreApplication::translate("MainWindow", "Raw Data", nullptr));
        Board_label->setText(QCoreApplication::translate("MainWindow", "Boards Available:", nullptr));
        connect_button->setText(QCoreApplication::translate("MainWindow", "Connect", nullptr));
        disconnect_button->setText(QCoreApplication::translate("MainWindow", "Disconnect", nullptr));
        label->setText(QString());
        board_name->setText(QString());
        bms_board_label->setText(QCoreApplication::translate("MainWindow", "BMS Board:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(board), QCoreApplication::translate("MainWindow", "Board", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
