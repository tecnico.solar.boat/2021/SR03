# Foils

This Repository holds the code to put the boat ✈️. The PCB for this code is available [here](https://gitlab.com/tecnico.solar.boat/2021/SR03/-/tree/main/PCB's/FoilsV2)

Copyright (C) 2021  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute 
it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat


## NEMAS CAN IDs

|    Controller   | Node-ID | CAN-ID (600h + Node-ID )|
|:---------------:|:-------:|:-----------------------:| 
| Left            | 1       | 601                     |
| Right           | 2       | 602                     |
| Rear            | 3       | 603                     |


## PDO Mappings

### RX-PDOs

#### 1. CAN-ID: 200h + Node-ID :
* 6040h:00h (controlword)
#### 2. CAN-ID: 300h + Node-ID :
* 6060h:00h (mode of operation)
#### 3. CAN-ID: 400h + Node-ID :
* 6040h:00h (controlword)
* 607Ah:00h (target position)

### TX-PDOs

#### 1. CAN-ID: 180h + Node-ID:
* 6041h:00h (statusword)
* 6061h:00h (Modes Of Operation Display)

#### 2. CAN-ID: 280h + Node-ID:
* 6064h:00h (Position actual value)

- [X] ID 1
- [X] ID 2
- [X] ID 3

## Configuration Changes from Default Object Dictionary

### Heartbeat Activation:
* Object 1017h:00h -> 03E8h (1000ms)
- [X] ID 1
- [X] ID 2
- [X] ID 3

### PDOs Activation:
* Activate and Configure the PDOs with the above PDO Mappings
* On TX-PDO 1 and 2 configure Event timer : Object 1800h:05h -> 03E8h (1000 ms) and Object 1801h:05h -> 03E8h (1000 ms) 
* On TX-PDO 1 and 2 configure Inhibit time : Object 1800h:03h -> 01h (0.1 ms) and Object 1801h:03h -> 01h (0.1 ms)  
- [X] ID 1
- [X] ID 2
- [X] ID 3

### Profile Position Settings:
* Positioning option code: Object 60F2h -> 21h
* Profile Velocity: Object 6081h -> 01F4h (500rpm)
* Profile Acceleration: Object 6083h -> 01F4h (500rpm^2)
* Profile Deceleration: Object 6084h -> 01F4h (500rpm^2)
- [X] ID 1
- [X] ID 2
- [X] ID 3

### Homing Settings:
* Minimum current for block detection: Object 203A:01h -> 0032h (50mA)
* Homing Method: Object 6098h -> EFh (-17)
- [ ] ID 1
- [ ] ID 2
- [X] ID 3

### Transition to the Quick stop active state:
* Object 605Ah:00h -> 06h
- [X] ID 1
- [X] ID 2
- [X] ID 3

## New Controllers Objects (Manually created or exported from Matlab)

### Must have the following functions:
```c++
void heave_mode();
void roll_mode();
void reset();
void reset_heave(); 
```

### Must have the following variables:
```c++
volatile float angle_left;
volatile float angle_right;
volatile float angle_rear;
volatile float heave;
float h_ref;
volatile float roll;                           
volatile float pitch;                         
volatile float yaw;                            
volatile float gyrX;                           
volatile float gyrY;                         
volatile float gyrZ;                           
volatile float accX;                           
volatile float accY;                           
volatile float accZ;                           
volatile float velX;                           
volatile float velY;                          
volatile float velZ; 
volatile float boat_speed;
volatile float auto_com_angle;
volatile float auto_dif_angle;
bool flag_first_loop;
```
