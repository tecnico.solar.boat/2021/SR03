/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "../../include/functions.h"
#include "Nanotec_Controller.h"
#include "Foils_Controller.h"
#include "RunningMedian.h"
#include "Arduino.h"
#include "Senix.h"
#include "Xsens.h"
#include "Watchdog_t4.h"


// /**
//  * Object that implements the running median functions
//  * @param  {11} Array size:  
//  */
RunningMedian ultrasonic_samples = RunningMedian(11);

/**
 * Object that handles the CAN communication with nemas controllers
 */
NemasComms controller_comms;

/**
 * Objects that implement the controll functions of each nema controller
 * @param  {NEMAS*_ID} ID of the controller:  
 * @param  {controller_comms} The CAN communication object: 
 */
NemasController left_controller(LEFT_CONTROLLER_ID,&controller_comms);
NemasController right_controller(RIGHT_CONTROLLER_ID,&controller_comms);
NemasController back_controller(BACK_CONTROLLER_ID,&controller_comms);

/**
 * Object that implements the handler for receiving general CAN messages
 */
TSB_CAN canMessageHandler;

//CAN Interfaces
FlexCAN_T4<CAN1, RX_SIZE_256, TX_SIZE_16> Can1; 
FlexCAN_T4<CAN_FOILS, RX_SIZE_256, TX_SIZE_16> Can2;

/**
 * Objects that implement the interval timers
 */
IntervalTimer ultrasonic_read_timer;
IntervalTimer foil_controll_timer;
IntervalTimer send_can_timer;
IntervalTimer print_timer;

/**
 * Object that implements the Foils Controller
 */
FoilsController foilscontroller;

/**
 * Object that implements the ultrasonic sensor
 */
UltrasonicSensor ultrasonicsensor;

/**
 * Object that implements the rs232 Xsens ahrs
 */
XsensAHRS ahrs;

/**
 * Object that implements the Watch Dog Timer
 */
WDT_T4<WDT1> wdt;
WDT_timings_t wdt_config;


/**
 * Global variables
 */
bool flag_rear_stepper_active=false;
bool force_lowering=false;
bool new_angle=false;
volatile int32_t position=0;
unsigned long start_heave_time=0;
bool led_state = LOW;
unsigned long timer_disable_rear_stepper=0;
volatile float com_mode_roll=DEFAULT_FRONT_ANGLE;
float heave_gains[6] = {0,0,0,0,0,0};
float roll_gains[3] = {0,0,0}; 

/**
 * CAN messages definition (can0)
 */
CAN_message_t us_angles;
CAN_message_t controller_angles;
CAN_message_t setpoints_state_mode;
CAN_message_t gyrX_filt_roll_cgyrX_cgyrZ;
CAN_message_t gyrZ_filt;

#ifdef SERIAL_AHRS
CAN_message_t XCDI_EulerAngles, XCDI_RateOfTurn, XCDI_Acceleration,XCDI_Temperature, XCDI_Latitude, XCDI_MagneticField, XCDI_VelocityXYZ, XCDI_FreeAccXYZ;
#endif

void myCallback() {
  Serial.println("FEED THE DOG SOON, OR RESET!");
}

static uint32_t callback_test = 0;

/**
 * Initializations are done here
 * Code run only once
 */
void setup() {
	// delay(10000);
	//add controllers to the comunication node
	controller_comms.addController(left_controller.id);
	controller_comms.addController(right_controller.id);
	//controller_comms.addController(back_controller.id);

	//Led pin setup
	pinMode(LED_BUILTIN, OUTPUT);

	//Led pin initialization on OFF state
  	digitalWrite(LED_BUILTIN,LOW);

	//Initialization of the Serial ports
	Serial.begin(9600); //Serial port for usb debug

	//Logger 
	Log.begin(LOG_LEVEL_VERBOSE, &Serial);
	Log.setPrefix(printTimestamp); // Uncomment to get timestamps as prefix
    Log.setSuffix(printNewline); // Uncomment to get newline as suffix
	
	//Initialization of the Serial2 port to communicate with the ultrassonic sensor
	SenixSerial.begin(57600);
	SenixSerial.transmitterEnable(SERIAL2_DIR);
	SenixSerial.clear();
	pinMode(US_ENABLE_PIN,OUTPUT);

	//Initialization of the Serial3 port to communicate with the ahrs
	XsensSerial.begin(2000000);
	XsensSerial.clear();

	//Initialization of the CAN ports
	Can1.begin();
	Can1.setBaudRate(1000000);
	Can1.setMaxMB(16);
	Can1.enableFIFO();
	Can1.enableFIFOInterrupt();

	Can2.begin();
	Can2.setBaudRate(1000000);
	Can2.setMaxMB(16);
	Can2.enableFIFO();
	Can2.enableFIFOInterrupt();

	//Can ports are attached to to the respective handlers
    Can1.attachObj(&canMessageHandler);
	canMessageHandler.attachGeneralHandler();

	Can2.attachObj(&controller_comms);
	controller_comms.attachGeneralHandler();

	//Initialization of the Can messages (can0 only)
    init_CAN_messages();

	//Timers initialization
	send_can_timer.begin(send_CAN,0.1*1e6);
	foil_controll_timer.begin(controll,0.02*1e6);
	print_timer.begin(debug_print,1*1e6);

	//wait for controllers to boot
	left_controller.waitforBootUp();
	right_controller.waitforBootUp();
	//back_controller.waitforBootUp();

	//wait for nmt pre operational(maybe after initial tests controllers will start on operational)
	left_controller.waitforNMTState(CANOPEN_NMT_PREOPERATIONAL_STATE);
	right_controller.waitforNMTState(CANOPEN_NMT_PREOPERATIONAL_STATE);
	//back_controller.waitforNMTState(CANOPEN_NMT_PREOPERATIONAL_STATE);

	//switch nmt to operational(maybe after initial tests controllers will start on operational)
	left_controller.WriteNMT_Broadcast(CANOPEN_NMT_SWITCH_OPERATIONAL);
	right_controller.WriteNMT_Broadcast(CANOPEN_NMT_SWITCH_OPERATIONAL);
	//back_controller.WriteNMT_Broadcast(CANOPEN_NMT_SWITCH_OPERATIONAL);
	//wait for nmt operational(maybe after initial tests controllers will start on operational)
	left_controller.waitforNMTState(CANOPEN_NMT_OPERATIONAL_STATE);
	right_controller.waitforNMTState(CANOPEN_NMT_OPERATIONAL_STATE);	
	//back_controller.waitforNMTState(CANOPEN_NMT_OPERATIONAL_STATE);	

	//wait for first pdo message to confirm communication is ok
	left_controller.waitforfirstPDO();
	right_controller.waitforfirstPDO();
	//back_controller.waitforfirstPDO();

	//Reset all fault conditions on the nemas controllers
	left_controller.resetfaultConditions_PDO();
	right_controller.resetfaultConditions_PDO();
	//back_controller.resetfaultConditions_PDO();

	//Sets the nemas controller mode to the default mode
	left_controller.setOperatingMode_PDO(STEPPERS_STARTMODE);
	right_controller.setOperatingMode_PDO(STEPPERS_STARTMODE);
	//back_controller.setOperatingMode_PDO(STEPPERS_STARTMODE);

	//Sets the nemas controller state to the default start state
	left_controller.setState_PDO(STEPPERS_STARTSTATE);
	right_controller.setState_PDO(STEPPERS_STARTSTATE);
	//back_controller.setState_PDO(STEPPERS_STARTSTATE);
	// get the change of the encoder's value
	// difference of angles
	
	/*while(canMessageHandler.screen.last_left_angle == 0)
		delay(1);
	Serial.print("Left Previous angle: ");
	Serial.println(canMessageHandler.screen.last_left_angle);
	Serial.print("Left actual encoder: ");
	Serial.println(left_controller.getActualPosition_PDO());
	left_controller.homing_offset = -angle_to_steps_front_left(canMessageHandler.screen.last_left_angle, 0) + left_controller.getActualPosition_PDO();
	Serial.print("encoder offset: ");
	Serial.println(left_controller.homing_offset);
	// repeat for right
	while(canMessageHandler.screen.last_right_angle == 0)
		delay(1);
	Serial.print("Right Previous angle: ");
	Serial.println(canMessageHandler.screen.last_right_angle);
	Serial.print("Right actual encoder: ");
	Serial.println(right_controller.getActualPosition_PDO());
	right_controller.homing_offset = (canMessageHandler.screen.last_right_angle - steps_to_angle_front_right(right_controller.getActualPosition_PDO(), 0))*MAX_RIGHT_FRONTMOTOR_POSITION/(MAX_RIGHT_FOILANGLE - MIN_RIGHT_FOILANGLE);
	Serial.print("encoder offset: ");
	Serial.println(right_controller.homing_offset);
	// repeat for back
	//while(canMessageHandler.screen.last_back_angle == 0)
	//	delay(1);
	//back_controller.homing_offset = canMessageHandler.screen.last_back_angle - steps_to_angle_rear(back_controller.getActualPosition_PDO(), 0);
	//back_controller.homing_offset = back_controller.homing_offset*MAX_REARMOTOR_POSITION/(MAX_REAR_FOILANGLE - MIN_REAR_FOILANGLE);
	*/
	left_controller.homing_offset = 0;
	right_controller.homing_offset = 0;
	//back_controller.homing_offset = 0;
	foilscontroller.homing_done=true;

	//Initialize the foils controller mode and state
	foilscontroller.mode=CONTROLLER_INITIALMODE;
	foilscontroller.state=CONTROLLER_INITIALSTATE;
	//Gets the value of the rear foil angle from CAN into the foils controller object
	//f(!canMessageHandler.screen.back_foil_state)
	//foilscontroller.set_anglerear(canMessageHandler.screen.back_angle_setpoint);

	/* 
	* If the homing was already done, change the mode to profile position and then set
	* the state to operation enabled
	*/
	// if(foilscontroller.homing_done){
	// 	left_controller.setState_PDO(CANOPEN_OPERATION_ENABLED);
	// 	right_controller.setState_PDO(CANOPEN_OPERATION_ENABLED);
	// 	if(canMessageHandler.screen.back_foil_state){
	// 		back_controller.setState_PDO(CANOPEN_OPERATION_ENABLED);
	// 	}
	// }

	// Setup Watch Dog Timer 
	wdt_config.trigger = 10; /* in seconds, 0->128 */
	wdt_config.timeout = 20; /* in seconds, 0->128 */
	//wdt_config.callback = myCallback;
	wdt.begin(wdt_config);

	//enable_ultrasonic_sensors();
	//ultrasonic_read_timer.begin(us_sendRequest,0.02*1e6);
	//foilscontroller.ultrasonic_connected=true;
}

/**
 * Where the magic happens!
 */
void loop() {
	//Get height reference from CAN bus
	//foilscontroller.set_href(-canMessageHandler.screen.dist_setpoint);

	//Check if the received height reference value is between the limits, if not, the closest limit is imposed
	if (-canMessageHandler.screen.dist_setpoint < -MAX_HEIGHT_REF) foilscontroller.set_href(-MAX_HEIGHT_REF);
	else if (-canMessageHandler.screen.dist_setpoint > 0) foilscontroller.set_href(0);
	else foilscontroller.set_href(-canMessageHandler.screen.dist_setpoint);

	/*
	* If the foils controller is not ready and if the foils are not enabled,
	* process the screen mode change received from the CAN bus
	*/
	if(!foilscontroller.ready && !foilscontroller.foils_enabled && canMessageHandler.screen.mode_setpoint){
		//Change mode
		foilscontroller.mode++;
		foilscontroller.mode = foilscontroller.mode%N_MODES;
		canMessageHandler.screen.mode_setpoint=false;
	}

	if(!canMessageHandler.screen.back_foil_state){//&& !force_lowering
		if(foilscontroller.get_anglerear() != canMessageHandler.screen.back_angle_setpoint){
			foilscontroller.set_anglerear(canMessageHandler.screen.back_angle_setpoint);
			if((foilscontroller.mode==HEAVEMODE || foilscontroller.mode==FULLMODE) && !foilscontroller.foils_enabled && foilscontroller.homing_done) {
				// back_controller.setState_PDO(CANOPEN_OPERATION_ENABLED);
				//back_controller.setInstantAbsProfilePos_PDO(angle_to_steps_rear(foilscontroller.get_anglerear(), back_controller.homing_offset));
				timer_disable_rear_stepper=millis();
				flag_rear_stepper_active = true;			
			}
		}

		/*
		* After 3 seconds, since the rear foil angle was changed, if the controller is in operation enable
		* and foils are not enabled, disable the rear foil.
		*/
		if((millis()-timer_disable_rear_stepper>DISABLE_REAR_STEPPER_TIME) && !foilscontroller.foils_enabled && flag_rear_stepper_active == true){ 
			// back_controller.setState_PDO(CANOPEN_SWITCH_ON_DISABLED);
			flag_rear_stepper_active = false;
		}
	}


	/*
	* If a state change is received in the CAN bus, the following conditions 
	* change the foils controller state to the OFF or READY state.
	*/
	if(canMessageHandler.screen.controller_state){
		/*
		* If the foils controller is not ready, and the homing has been done
		* the following conditions process the foils controller state change
		* from OFF to READY
		*/
		if(!foilscontroller.ready && foilscontroller.homing_done){
			/*
			* If the foils controller mode is HEAVEMODE or FULLMODE enable the ultrassonic sensor.
			*/
			if(foilscontroller.mode==HEAVEMODE || foilscontroller.mode==FULLMODE){
				enable_ultrasonic_sensors();
				ultrasonic_read_timer.begin(us_sendRequest,0.02*1e6);
				foilscontroller.ultrasonic_connected=true;
				delay(1000);
				//if the communication with ultrassonic sensor is not working, disable ultrassonic sensor
				if(foilscontroller.get_heave()==0){
					disable_ultrasonic_sensors();
					ultrasonic_read_timer.end();
					ultrasonicsensor.value=0;
					foilscontroller.set_heave(0);
					foilscontroller.ultrasonic_connected=false;
				}
			}
			/*
			* If the communication with the ultrassonic sensor is working or the foils controller mode is MANUALMODE 
			* OR ROLLMODE, the foils controller ready flag is set to true, the controller reset is done, the front nemas
			* controllers move to the default angle and the controller state is changed from OFF to READY.
			*/ 
			if(foilscontroller.ultrasonic_connected || foilscontroller.mode==MANUALMODE || foilscontroller.mode==ROLLMODE){
				if(foilscontroller.mode == ROLLMODE){
					if(com_mode_roll < 5.5)
						foilscontroller.set_difanglelim(3+com_mode_roll);
					else if(com_mode_roll > 5.5)
						foilscontroller.set_difanglelim(14-com_mode_roll);
					else
						foilscontroller.set_difanglelim(8.5);
				}
				else
					foilscontroller.set_difanglelim(8.5);
				
				foilscontroller.rear_foil_enabled = canMessageHandler.screen.back_foil_state;
				foilscontroller.ready=true;
				foilscontroller.reset_controller(canMessageHandler.screen.back_foil_state);
				delay(1000);
				left_controller.setState_PDO(CANOPEN_OPERATION_ENABLED);
				right_controller.setState_PDO(CANOPEN_OPERATION_ENABLED);
				//back_controller.setState_PDO(CANOPEN_OPERATION_ENABLED);
				left_controller.setInstantAbsProfilePos_PDO(angle_to_steps_front_left(DEFAULT_FRONT_ANGLE,left_controller.homing_offset));
				right_controller.setInstantAbsProfilePos_PDO(angle_to_steps_front_right(DEFAULT_FRONT_ANGLE,right_controller.homing_offset));
				foilscontroller.state=READY;
			}
	
		/*
		* If the foils controller is ready, and the foils are not enabled
		* the following conditions process the foils controller state change
		* from READY to OFF
		*/
		}else if(foilscontroller.ready && !foilscontroller.foils_enabled){
			//switch mode operation
			if(foilscontroller.mode==HEAVEMODE || foilscontroller.mode==FULLMODE){
				disable_ultrasonic_sensors();
				ultrasonic_read_timer.end();
				foilscontroller.ultrasonic_connected=false;
				ultrasonicsensor.value=0;
				foilscontroller.set_heave(0);
			}
			
			foilscontroller.ready=false;
			left_controller.setState_PDO(CANOPEN_SWITCH_ON_DISABLED);
			right_controller.setState_PDO(CANOPEN_SWITCH_ON_DISABLED);
			//back_controller.setState_PDO(CANOPEN_SWITCH_ON_DISABLED);
			
			foilscontroller.state=OFF;
		}
		canMessageHandler.screen.controller_state=false;
	}

	/*
	* If a homing command is received in the CAN bus, the foils controller is not ready, and the foils are not enabled
	* the following condition calls a function to perform homing on all nemas controllers
	*/
	if(canMessageHandler.screen.homing && !foilscontroller.ready && !foilscontroller.foils_enabled){
		left_controller.performHoming_PDO();
		left_controller.homing_offset = left_controller.homing_offset;
		right_controller.performHoming_PDO();
		right_controller.homing_offset = right_controller.homing_offset;
		//back_controller.performHoming_PDO();
		//back_controller.homing_offset = 0;

		canMessageHandler.screen.homing=false;
	}

	/*
	* If the foils controller is ready, the foils controller is not enabled and the boat speed is greater or equal to the
	* foils start speed the following condition changes the foils controller state from READY to ON
	*/
	if(!foilscontroller.foils_enabled && foilscontroller.ready && (foilscontroller.get_boatspeed() >= FOILS_START_SPEED)){
		foilscontroller.set_flagfirstlooproll(true);
		foilscontroller.foils_enabled=true;
		foilscontroller.state=ON;
	}

	/*
	* If the the foils controller is enabled and the boat speed is less that the
	* foils stop speed the following condition changes the foils controller state from ON to READY.
	* The front nemas controllers also move to the default angle.
	*/
	if(foilscontroller.foils_enabled && foilscontroller.get_boatspeed() < FOILS_STOP_SPEED){
		foilscontroller.foils_enabled=false;
		foilscontroller.reset_controller(canMessageHandler.screen.back_foil_state);
		foilscontroller.state=READY;
		left_controller.setInstantAbsProfilePos_PDO(angle_to_steps_front_left(foilscontroller.get_angleleft(),left_controller.homing_offset));
		right_controller.setInstantAbsProfilePos_PDO(angle_to_steps_front_right(foilscontroller.get_angleright(),right_controller.homing_offset));
		if(canMessageHandler.screen.back_foil_state)
			back_controller.setInstantAbsProfilePos_PDO(angle_to_steps_rear(foilscontroller.get_anglerear(),back_controller.homing_offset));
	}

	/*
	* If the foils controller is ready and not enabled and also the foils controller mode is either MANUALMODE or ROLLMODE the 
	* following conditions enable the manual controll, by the pilot, of the front nemas controllers angle.
	*/
	if(foilscontroller.ready && !foilscontroller.foils_enabled && (foilscontroller.mode == MANUALMODE || foilscontroller.mode == ROLLMODE)){

		if(foilscontroller.get_angleright() != canMessageHandler.screen.front_angle_setpoint){
			//The left and right angles are received from the CAN bus
			foilscontroller.set_angleleft(canMessageHandler.screen.front_angle_setpoint);
			foilscontroller.set_angleright(canMessageHandler.screen.front_angle_setpoint);

			//Connditions to impose the max and min front foils angles
			if (foilscontroller.get_angleleft() > MAX_LEFT_FOILANGLE) foilscontroller.set_angleleft(MAX_LEFT_FOILANGLE);
			else if (foilscontroller.get_angleleft() < MIN_LEFT_FOILANGLE) foilscontroller.set_angleleft(MIN_LEFT_FOILANGLE);
			if (foilscontroller.get_angleright() > MAX_RIGHT_FOILANGLE) foilscontroller.set_angleright(MAX_RIGHT_FOILANGLE);
			else if (foilscontroller.get_angleright() < MIN_RIGHT_FOILANGLE) foilscontroller.set_angleright(MIN_RIGHT_FOILANGLE);
			
			//The foils controller auto common angle is set, since left and right angles are the same, the right angle is used
			com_mode_roll = foilscontroller.get_angleright();
			foilscontroller.set_initial_com_angle(foilscontroller.get_angleright());
			foilscontroller.set_autocomangle(foilscontroller.get_angleright());

			if(com_mode_roll < 5.5)
				foilscontroller.set_difanglelim(3+com_mode_roll);
			else if(com_mode_roll > 5.5)
				foilscontroller.set_difanglelim(14-com_mode_roll);
			else
				foilscontroller.set_difanglelim(8.5);
			

			//The front nemas controllers move accordingly to the angle manually set
			left_controller.setInstantAbsProfilePos_PDO(angle_to_steps_front_left(foilscontroller.get_angleleft(),left_controller.homing_offset));
			right_controller.setInstantAbsProfilePos_PDO(angle_to_steps_front_right(foilscontroller.get_angleright(),right_controller.homing_offset));
		
		}
			
		if(foilscontroller.get_anglerear() != canMessageHandler.screen.back_angle_setpoint && canMessageHandler.screen.back_foil_state){
			// if rear foil control is active
				
			foilscontroller.set_anglerear(canMessageHandler.screen.back_angle_setpoint);
			if (foilscontroller.get_anglerear() > MAX_REAR_FOILANGLE) foilscontroller.set_anglerear(MAX_REAR_FOILANGLE);
			else if (foilscontroller.get_anglerear() < MIN_REAR_FOILANGLE) foilscontroller.set_anglerear(MIN_REAR_FOILANGLE);

			//The foils controller auto common angle is set, since left and right angles are the same, the right angle is used
			//foilscontroller.set_autorearangle(foilscontroller.get_anglerear());

			// if rear foil control is active	
			//back_controller.setInstantAbsProfilePos_PDO(angle_to_steps_rear(foilscontroller.get_anglerear(),back_controller.homing_offset));
		}
	}

	//readfromkeyboard();

	// Reset watch dog timer
    if ( millis() - callback_test > 5500 ) {
		callback_test = millis();
		wdt.feed(); /* uncomment to feed the watchdog */
	}

	if(foilscontroller.mode == ROLLMODE || foilscontroller.mode == FULLMODE){ //change com mode angle while in roll mode
		if(com_mode_roll != canMessageHandler.screen.front_angle_setpoint)
			com_mode_roll = canMessageHandler.screen.front_angle_setpoint;
			if(com_mode_roll < 5.5)
				foilscontroller.set_difanglelim(3+com_mode_roll);
			else if(com_mode_roll > 5.5)
				foilscontroller.set_difanglelim(14-com_mode_roll);
			else
				foilscontroller.set_difanglelim(8.5);
		foilscontroller.set_initial_com_angle(com_mode_roll);
	}
	
	if(!foilscontroller.foils_enabled){
		for(int i=0;i<6;i++){
			if(canMessageHandler.screen.heave_gains[i] != heave_gains[i]){
				heave_gains[i] = canMessageHandler.screen.heave_gains[i];
				foilscontroller.set_heave_gains(heave_gains);
			}
		}
		for(int i=0;i<3;i++){
			if(canMessageHandler.screen.roll_gains[i] != roll_gains[i]){
				roll_gains[i] = canMessageHandler.screen.roll_gains[i];
				foilscontroller.set_roll_gains(roll_gains);
			}
		}
	}
	

}

/**
 * Where the magic really happens!
 * Interruption code that controlls the boat foils.
 */
void controll(){
	//Read data from AHRS
	getAHRSData();
	// if(lqrcontroller.boat_speed < HEAVE_STOP_SPEED) {
	// 	lqrcontroller.DIF_FOILANGLE = 3;
	// } else {
	// 	lqrcontroller.DIF_FOILANGLE = 1.5;
	// }

	/**
	* If the foils controller is enabled the following code controlls the boat foils
	*/

	//read encoder values
	//foilscontroller.set_realanglerear(steps_to_angle_rear(back_controller.getActualPosition_PDO(),back_controller.homing_offset));
	//foilscontroller.set_realanglerear(0);
	foilscontroller.set_realangleleft(steps_to_angle_front_left(left_controller.getActualPosition_PDO(),left_controller.homing_offset));
	foilscontroller.set_realangleright(steps_to_angle_front_right(right_controller.getActualPosition_PDO(),right_controller.homing_offset));

	if(foilscontroller.foils_enabled){
		/**º
		* If the foils controller is on ROLLMODE or FULLMODE the controller roll mode function is called
		*/
		if(foilscontroller.mode == ROLLMODE || foilscontroller.mode == FULLMODE) {
			foilscontroller.roll_controller();
		}
		/**
		* If the foils controller does not have the heave mode enabled, 
		* the foils controller mode is either HEAVEMODE or FULLMODE and the boat speed is
		* greater than the heave start speed, the heave mode is enabled.
		* The rear foil is also enabled.
		*/
		if(!foilscontroller.heave_enabled && (foilscontroller.mode == HEAVEMODE || foilscontroller.mode == FULLMODE) && foilscontroller.get_boatspeed() > HEAVE_START_SPEED) {
			foilscontroller.heave_enabled = true;
			foilscontroller.set_flagfirstloop(true);
			//start rear stepper if not active
			start_heave_time = millis();
		}
		/**
		* If the foils controller has the heave mode enabled and the boat speed is
		* less than the heave start speed, the heave mode is disabled.
		* The rear foil is also disabled.
		*/
		if (foilscontroller.heave_enabled && foilscontroller.get_boatspeed() < HEAVE_STOP_SPEED) {
			foilscontroller.heave_enabled = false;
			foilscontroller.reset_heave(foilscontroller.rear_foil_enabled);
			//stop rear stepper if rear_foil not active
			// if(!foilscontroller.rear_foil_enabled)
			// 	back_controller.setState_PDO(CANOPEN_SWITCH_ON_DISABLED);
		}
		/**
		* If the foils controller has the heave mode enabled and 1 second has elapsed from heave being enabled,
		* the heave mode function is called.
		*/
		if (foilscontroller.heave_enabled && (millis()-start_heave_time)>1000) {
			foilscontroller.heave_controller();
		}

		//The left and right foil angles are calculated when rollmode
		/*if(foilscontroller.mode == ROLLMODE && foilscontroller.foils_enabled == true){
			if(sqrt(pow(foilscontroller.get_autodifangle(),2)) > com_mode_roll-MIN_LEFT_FOILANGLE && com_mode_roll < 5.5){
				foilscontroller.set_autocomangle(com_mode_roll+sqrt(pow(foilscontroller.get_autodifangle(),2))-(com_mode_roll-MIN_LEFT_FOILANGLE));
			}
			else if(sqrt(pow(foilscontroller.get_autodifangle(),2)) > MAX_LEFT_FOILANGLE-com_mode_roll && com_mode_roll > 5.5){
				foilscontroller.set_autocomangle(com_mode_roll-sqrt(pow(foilscontroller.get_autodifangle(),2))+(MAX_LEFT_FOILANGLE-com_mode_roll));
			}
			else
				foilscontroller.set_autocomangle(com_mode_roll);
		}
		*/

		foilscontroller.set_angleleft(foilscontroller.get_autocomangle() + foilscontroller.get_autodifangle());
		if(foilscontroller.get_angleleft() < MIN_LEFT_FOILANGLE)
			foilscontroller.set_angleleft(MIN_LEFT_FOILANGLE);
		else if(foilscontroller.get_angleleft() > MAX_LEFT_FOILANGLE)
			foilscontroller.set_angleleft(MAX_LEFT_FOILANGLE);
		foilscontroller.set_angleright(foilscontroller.get_autocomangle() - foilscontroller.get_autodifangle());
		if(foilscontroller.get_angleright() < MIN_RIGHT_FOILANGLE)
			foilscontroller.set_angleright(MIN_RIGHT_FOILANGLE);
		else if(foilscontroller.get_angleright() > MAX_RIGHT_FOILANGLE)
			foilscontroller.set_angleright(MAX_RIGHT_FOILANGLE);

		if(foilscontroller.rear_foil_enabled)
			foilscontroller.set_anglerear(foilscontroller.get_autorearangle());
			
		left_controller.setInstantAbsProfilePos_PDO(angle_to_steps_front_left(foilscontroller.get_angleleft(),left_controller.homing_offset));
		right_controller.setInstantAbsProfilePos_PDO(angle_to_steps_front_right(foilscontroller.get_angleright(),right_controller.homing_offset));
		//back_controller.setInstantAbsProfilePos_PDO(angle_to_steps_rear(foilscontroller.get_anglerear(),back_controller.homing_offset));	

		new_angle = true;
	}
}

void getAHRSData(){
	//ultrasonic
	 if(SenixSerial.available()){
	 	if(foilscontroller.ultrasonic_connected){
	 		ultrasonicsensor.readSensor();
			
	 		//The sensor outputs a positive value of height, but since the controller needs a negative value, a negative value retrieved
	 		//Filter the sensor readings
	 		//ultrasonicsensor.value = ultrasonicsensor.value*cos(ahrs.roll) - DIST_US_TO_AHRS*sin(ahrs.pitch*PI/180); //US to ahrs
	 		if(ultrasonicsensor.value > 15 && ultrasonicsensor.value < 90){
	 			ultrasonic_samples.add(-1*ultrasonicsensor.value);
	 			foilscontroller.set_heave(ultrasonic_samples.getMedian());
	 			foilscontroller.last_heave = ultrasonic_samples.getMedian();
	 		}
	 		else{
	 			//ultrasonic_samples.add(-1*ultrasonicsensor.value);
	 			//foilscontroller.set_heave(ultrasonic_samples.getMedian());
	 			foilscontroller.set_heave(foilscontroller.last_heave);
	 		}
	 	}
	 	else{
	 		SenixSerial.read();
	 		ultrasonicsensor.value=0;
	 	}
	 }
	//AHRS
	//In the case we need to use old ahrs
	#ifdef SERIAL_AHRS 
	if(XsensSerial.available()){
		ahrs.readXsensMsg();
	}
	foilscontroller.set_rollpitchyaw(ahrs.roll,ahrs.pitch,ahrs.yaw);                   
    foilscontroller.set_gyr(ahrs.gyrX,ahrs.gyrY,ahrs.gyrZ);
	foilscontroller.set_vel(ahrs.velX,ahrs.velY,ahrs.velZ);
	foilscontroller.set_boatspeed(ahrs.vel);
	//foilscontroller.set_freeacc(ahrs.free_accX,ahrs.free_accY,ahrs.free_accZ);
	#else
	foilscontroller.set_rollpitchyaw(canMessageHandler.ahrs.roll,canMessageHandler.ahrs.pitch,canMessageHandler.ahrs.yaw);                   
    foilscontroller.set_gyr(canMessageHandler.ahrs.gyrX,canMessageHandler.ahrs.gyrY,canMessageHandler.ahrs.gyrZ);
	foilscontroller.set_vel(canMessageHandler.ahrs.velX,canMessageHandler.ahrs.velY,canMessageHandler.ahrs.velZ);
	foilscontroller.set_boatspeed(sqrt(pow(canMessageHandler.ahrs.velX,2) + pow(canMessageHandler.ahrs.velY,2) + pow(canMessageHandler.ahrs.velZ,2)));
	// foilscontroller.set_freeacc(canMessageHandler.ahrs.free_accX , canMessageHandler.ahrs.free_accY, canMessageHandler.ahrs.free_accZ);
	#endif
}

/**
 * Function that does the setup of all the CAN messages sent by the foils controller, the definition of ~
 * message id and message length are done in this function.
 */
void init_CAN_messages(){
	us_angles.flags.extended = 0;
	gyrX_filt_roll_cgyrX_cgyrZ.flags.extended = 0;
	gyrZ_filt.flags.extended = 0;
	controller_angles.flags.extended = 0;
	setpoints_state_mode.flags.extended = 0;
    us_angles.id = 0x602;
	setpoints_state_mode.id = 0x612;
	controller_angles.id = 0x622;
	gyrX_filt_roll_cgyrX_cgyrZ.id = 0x632;
	gyrZ_filt.id = 0x642;
	
	gyrZ_filt.len = 2;
	gyrX_filt_roll_cgyrX_cgyrZ.len = 8;
	controller_angles.len = 4;
	us_angles.len = 8;
	setpoints_state_mode.len = 8;

	#ifdef SERIAL_AHRS
		XCDI_EulerAngles.flags.extended = 0;
		XCDI_RateOfTurn.flags.extended = 0;
		XCDI_Acceleration.flags.extended = 0;
		XCDI_Temperature.flags.extended = 0;
		XCDI_Latitude.flags.extended = 0;
		XCDI_MagneticField.flags.extended = 0;
		XCDI_VelocityXYZ.flags.extended = 0;
		XCDI_FreeAccXYZ.flags.extended = 0;

		XCDI_EulerAngles.len = 6;
		XCDI_RateOfTurn.len = 6;
		XCDI_Acceleration.len = 6;
		XCDI_Temperature.len = 2;
		XCDI_Latitude.len = 8;
		XCDI_MagneticField.len = 6;
		XCDI_VelocityXYZ.len = 6;
		XCDI_FreeAccXYZ.len = 6;

		XCDI_EulerAngles.id = 0x434;
		XCDI_RateOfTurn.id = 0x444;
		XCDI_Acceleration.id = 0x454;
		XCDI_Temperature.id = 0x664;
		XCDI_Latitude.id = 0x674;
		XCDI_MagneticField.id = 0x684;
		XCDI_VelocityXYZ.id = 0x494;
		XCDI_FreeAccXYZ.id = 0x4A4;
	#endif

}

/**
 * Function that sends the CAN messages to the boat CAN bus
 */
void send_CAN(){
	int32_t ind = 0;
	//US_angles
	ind=0;
	buffer_append_float16(us_angles.buf, -1*foilscontroller.get_heave(),1e2, &ind);
	buffer_append_float16(us_angles.buf, steps_to_angle_front_left(left_controller.getActualPosition_PDO(),left_controller.homing_offset),1e2, &ind);
	buffer_append_float16(us_angles.buf, steps_to_angle_front_right(right_controller.getActualPosition_PDO(),right_controller.homing_offset),1e2, &ind);
	buffer_append_float16(us_angles.buf, 0,1e2, &ind);
	Can1.write(us_angles);
	
	ind = 0;
	buffer_append_float16(controller_angles.buf, foilscontroller.get_angleleft(),1e2, &ind);
	buffer_append_float16(controller_angles.buf, foilscontroller.get_angleright(),1e2, &ind);
	Can1.write(controller_angles);

	ind = 0;
	buffer_append_float16(gyrX_filt_roll_cgyrX_cgyrZ.buf, foilscontroller.get_gyrXfilt(),1e2, &ind);
	buffer_append_float16(gyrX_filt_roll_cgyrX_cgyrZ.buf, foilscontroller.get_roll(),1e2, &ind);
	buffer_append_float16(gyrX_filt_roll_cgyrX_cgyrZ.buf, foilscontroller.get_gyrX(),1e2, &ind);
	buffer_append_float16(gyrX_filt_roll_cgyrX_cgyrZ.buf, foilscontroller.get_gyrZ(),1e2, &ind);
	Can1.write(gyrX_filt_roll_cgyrX_cgyrZ);

	ind = 0;
	buffer_append_float16(gyrZ_filt.buf, foilscontroller.get_gyrZfilt(),1e2, &ind);

	Can1.write(gyrZ_filt);

	//Setpoints_state_mode
	ind=0;
	buffer_append_uint8(setpoints_state_mode.buf,foilscontroller.state,&ind);
	buffer_append_uint8(setpoints_state_mode.buf,foilscontroller.mode,&ind);
	buffer_append_uint8(setpoints_state_mode.buf,(uint8_t)-foilscontroller.get_href(),&ind);
	if(foilscontroller.mode==MANUALMODE) buffer_append_float16(setpoints_state_mode.buf,foilscontroller.get_angleright(),1e1, &ind);
	else buffer_append_float16(setpoints_state_mode.buf,foilscontroller.get_anglerear(),1e1, &ind);
	buffer_append_float16(setpoints_state_mode.buf,foilscontroller.get_boatspeed(),1e2, &ind);
	buffer_append_uint8(setpoints_state_mode.buf, tempmonGetTemp(), &ind);
	//Serial.println(tempmonGetTemp());
	Can1.write(setpoints_state_mode);

	#ifdef SERIAL_AHRS
		ind = 0;
		buffer_append_float16(XCDI_EulerAngles.buf,ahrs.roll,pow(2,7),&ind);
		buffer_append_float16(XCDI_EulerAngles.buf,ahrs.pitch,pow(2,7),&ind);
		buffer_append_float16(XCDI_EulerAngles.buf,ahrs.yaw,pow(2,7),&ind);
		Can1.write(XCDI_EulerAngles);

		ind = 0;
		buffer_append_float16(XCDI_RateOfTurn.buf,ahrs.gyrX,pow(2,9),&ind);
		buffer_append_float16(XCDI_RateOfTurn.buf,ahrs.gyrY,pow(2,9),&ind);
		buffer_append_float16(XCDI_RateOfTurn.buf,ahrs.gyrZ,pow(2,9),&ind);
		Can1.write(XCDI_RateOfTurn);

		ind = 0;
		buffer_append_float16(XCDI_Acceleration.buf,ahrs.accX,pow(2,8),&ind);
		buffer_append_float16(XCDI_Acceleration.buf,ahrs.accY,pow(2,8),&ind);
		buffer_append_float16(XCDI_Acceleration.buf,ahrs.accZ,pow(2,8),&ind);
		Can1.write(XCDI_Acceleration);

		ind = 0;
		buffer_append_float16(XCDI_Temperature.buf,ahrs.temperature,pow(2,8),&ind);
		Can1.write(XCDI_Temperature);

		ind = 0;
		buffer_append_float32(XCDI_Latitude.buf,ahrs.lat,pow(2,24),&ind);
		buffer_append_float32(XCDI_Latitude.buf,ahrs.lon,pow(2,23),&ind);
		Can1.write(XCDI_Latitude);

		ind = 0;
		buffer_append_float16(XCDI_MagneticField.buf,ahrs.magX,pow(2,10),&ind);
		buffer_append_float16(XCDI_MagneticField.buf,ahrs.magY,pow(2,10),&ind);
		buffer_append_float16(XCDI_MagneticField.buf,ahrs.magZ,pow(2,10),&ind);
		Can1.write(XCDI_MagneticField);

		ind = 0;
		buffer_append_float16(XCDI_VelocityXYZ.buf,ahrs.velX,pow(2,6),&ind);
		buffer_append_float16(XCDI_VelocityXYZ.buf,ahrs.velY,pow(2,6),&ind);
		buffer_append_float16(XCDI_VelocityXYZ.buf,ahrs.velZ,pow(2,6),&ind);
		Can1.write(XCDI_VelocityXYZ);

		ind = 0;
		buffer_append_float16(XCDI_FreeAccXYZ.buf,ahrs.free_accX,pow(2,8),&ind);
		buffer_append_float16(XCDI_FreeAccXYZ.buf,ahrs.free_accY,pow(2,8),&ind);
		buffer_append_float16(XCDI_FreeAccXYZ.buf,ahrs.free_accZ,pow(2,8),&ind);
		Can1.write(XCDI_FreeAccXYZ);
	#endif
}

void debug_print(){
	digitalWrite(LED_BUILTIN,led_state);
	led_state=!led_state;
	Serial.println((String)"Left: " + left_controller.getActualPosition_PDO() + " Right: " + right_controller.getActualPosition_PDO());
	/*Serial.print("Kh[0]:  ");
	Serial.print(foilscontroller.);
	Serial.print("  Kh[1]:  ");
	Serial.print(heave_gains[1]);
	Serial.print("  Kh[2]:  ");
	Serial.print(heave_gains[2]);
	Serial.print("  Kh[3]:  ");
	Serial.print(heave_gains[3]);
	Serial.print("  Kh[4]:  ");
	Serial.print(heave_gains[4]);
	Serial.print("  Kh[5]:  ");
	Serial.print(heave_gains[5]);
	Serial.println("");
	*/

	//Serial.print("Real angle rear:  ");
	//Serial.print(foilscontroller.get_realanglerear());
	//Serial.print("Real angle left:  ");
	//Serial.print(foilscontroller.get_realangleleft());
	//Serial.print("Real angle right:  ");
	//Serial.print(foilscontroller.get_realangleright());
	
	//Log.trace("Ultrasonic: %F",foilscontroller.get_heave());
	//ahrs.printXsens();
}

// /**
//  * The following function reads the ultrasonic sensor value 
//  */
// void serialEvent2(){
// 	if(foilscontroller.ultrasonic_connected){
// 		ultrasonicsensor.readSensor();
// 		//The sensor outputs a positive value of height, but since the controller needs a negative value, a negative value retrieved
// 		//Filter the sensor readings
// 		if(ultrasonicsensor.value > 15 && ultrasonicsensor.value < 90){
// 			ultrasonic_samples.add(-1*ultrasonicsensor.value);
// 			foilscontroller.set_heave(ultrasonic_samples.getMedian());
// 			foilscontroller.last_heave = ultrasonic_samples.getMedian();
// 		}
// 		else
// 			foilscontroller.set_heave(foilscontroller.last_heave);
// 			//calculate heave at center of mass
// 			//lqrcontroller.heave = -( readSensor() - DIST_US_TO_CM*sin(-AHRS.pitch*PI/180) );
// 	}else{
// 		SenixSerial.read();
// 		ultrasonicsensor.value=0;
// 	}
	
// }

// void serialEvent3(){
// 	ahrs.readXsensMsg();
// }


// void serialEvent(){
// 	readfromkeyboard();
// }

void readfromkeyboard(){

	/*
	* DEBUG AND TEST 
	* The following code is only to be used in debug or test scenarios (normally when the boat is not on water).
	* It enables the foils movement controll using the computer keyboard (USE WITH CAUTION!)
	*/
	switch (Serial.read()){
		//MODES
		//Switch on disabled
	/*	case '1':
			left_controller.setState_PDO(CANOPEN_SWITCH_ON_DISABLED);
			right_controller.setState_PDO(CANOPEN_SWITCH_ON_DISABLED);
			break;
		//Ready to switch on
		case '2':
			left_controller.setState_PDO(CANOPEN_READY_TO_SWITCH_ON);
			right_controller.setState_PDO(CANOPEN_READY_TO_SWITCH_ON);
			break;
		//Switched on
		case '3':
			left_controller.setState_PDO(CANOPEN_SWITCHED_ON);
			right_controller.setState_PDO(CANOPEN_SWITCHED_ON);
			break;
		//Operation enabled
		case '4':
			left_controller.setState_PDO(CANOPEN_OPERATION_ENABLED);
			right_controller.setState_PDO(CANOPEN_OPERATION_ENABLED);
			break;
		//Quick stop
		case '5':
			left_controller.setState_PDO(CANOPEN_QUICK_STOP_ACTIVE);
			right_controller.setState_PDO(CANOPEN_QUICK_STOP_ACTIVE);
			break;
		//Mode Homing
		case '6':
			left_controller.setOperatingMode_PDO(CANOPEN_OPERATION_MODE_HOMING);
			right_controller.setOperatingMode_PDO(CANOPEN_OPERATION_MODE_HOMING);
			break;
		//Mode Profile Position
		case '7':
			left_controller.setOperatingMode_PDO(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
			right_controller.setOperatingMode_PDO(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
			break;
	*/	//position increment
		case 'w':
			foilscontroller.set_boatspeed(foilscontroller.get_boatspeed()+1);
			Serial.print("speed: ");
			Serial.println(foilscontroller.get_boatspeed());
			break;
		//position decrement
		case 's':
			foilscontroller.set_boatspeed(foilscontroller.get_boatspeed()-1);
			Serial.print("speed: ");
			Serial.println(foilscontroller.get_boatspeed());
			break;
		//follow target position
	//	case 'g':
	//		left_controller.setInstantAbsProfilePos_PDO(position);
//			right_controller.setInstantAbsProfilePos_PDO(position);
//			break;
		case 'h':
			// left_controller.performHoming_PDO();
			// right_controller.performHoming_PDO();
			//back_controller.performHoming_PDO();
			break;
//		case 'u':
//			Log.trace("ultrasonic ON!");
//			ultrasonic_read_timer.begin(us_sendRequest,0.02*1e6);
//			enable_ultrasonic_sensors();
//			foilscontroller.ultrasonic_connected=true;
//			break;
/*		case 'i':
			Log.trace("ultrasonic OFF!");
			ultrasonic_read_timer.end();
			disable_ultrasonic_sensors();
			foilscontroller.ultrasonic_connected=false;
			ultrasonic_read_timer.end();
			foilscontroller.set_heave(0);
			ultrasonicsensor.value=0;
			break;
		*/case 't':
			position+=1800;
			break;
		case 'y':
			position-=1800;
			break;
		// case 'l':
			// uint8_t value[4];
			// left_controller.ReadSDO(0x6081,0x00,value);
			// Serial.println((String)"Left Velocity: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// right_controller.ReadSDO(0x6081,0x00,value);
			// Serial.println((String)"Right Velocity: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// back_controller.ReadSDO(0x6081,0x00,value);
			// Serial.println((String)"Back Velocity: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			
			// left_controller.WriteSDO(0x6081, 0x00, 4, 0x1F4);
			// right_controller.WriteSDO(0x6081, 0x00, 4, 0x1F4);

			// left_controller.ReadSDO(0x6081,0x00,value);
			// Serial.println((String)"Left Velocity: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// right_controller.ReadSDO(0x6081,0x00,value);
			// Serial.println((String)"Right Velocity: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// back_controller.ReadSDO(0x6081,0x00,value);
			// Serial.println((String)"Back Velocity: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			
			// left_controller.ReadSDO(0x6083,0x00,value);
			// Serial.println((String)"Left Acceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// right_controller.ReadSDO(0x6083,0x00,value);
			// Serial.println((String)"Right Acceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// back_controller.ReadSDO(0x6083,0x00,value);
			// Serial.println((String)"Back Acceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			
			// left_controller.WriteSDO(0x6083, 0x00, 4, 0x1F4);
			// right_controller.WriteSDO(0x6083, 0x00, 4, 0x1F4);

			// left_controller.ReadSDO(0x6083,0x00,value);
			// Serial.println((String)"Left Acceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// right_controller.ReadSDO(0x6083,0x00,value);
			// Serial.println((String)"Right Acceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// back_controller.ReadSDO(0x6083,0x00,value);
			// Serial.println((String)"Back Acceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			
			
			// left_controller.ReadSDO(0x6084,0x00,value);
			// Serial.println((String)"Left Deceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// right_controller.ReadSDO(0x6084,0x00,value);
			// Serial.println((String)"Right Deceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// back_controller.ReadSDO(0x6084,0x00,value);
			// Serial.println((String)"Back Deceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			
			// left_controller.WriteSDO(0x6084, 0x00, 4, 0x1F4);
			// right_controller.WriteSDO(0x6084, 0x00, 4, 0x1F4);

			// left_controller.ReadSDO(0x6084,0x00,value);
			// Serial.println((String)"Left Deceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// right_controller.ReadSDO(0x6084,0x00,value);
			// Serial.println((String)"Right Deceleration: " + value[0] + " " + value[1] + " " + value[2] + " " + value[3]);
			// back_controller.ReadSDO(0x6084,0x00,value);
			
			// break;
		default:
			Serial.read();
			break;
	}
}