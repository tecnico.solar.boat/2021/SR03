/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

//
// File: foils_controller2_GS.cpp
//
// Code generated for Simulink model 'foils_controller2_GS'.
//
// Model version                  : 1.23
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Thu Apr 15 14:43:42 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: NXP->Cortex-M4
// Code generation objectives:
//    1. Execution efficiency
//    2. RAM efficiency
// Validation result: Not run
//
#include "Controller_2_GS.h"

//
// Output and update for atomic system:
//    '<Root>/Discrete derivative1'
//    '<Root>/Discrete derivative2'
//
void Controller2GS::Discretederivative1(const real_T rtu_f[2],
  real_T rty_fdot[2], DW_Discretederivative1 *localDW)
{
  real_T rtb_TSamp_idx_0;
  real_T rtb_TSamp_idx_1;

  // SampleTimeMath: '<S7>/TSamp'
  //
  //  About '<S7>/TSamp':
  //   y = u * K where K = 1 / ( w * Ts )

  rtb_TSamp_idx_0 = rtu_f[0] * 50.0;
  rtb_TSamp_idx_1 = rtu_f[1] * 50.0;

  // Step: '<S1>/Step'
  if ((((&rtM)->Timing.clockTick0) * 0.02) < 0.01) {
    // Switch: '<S1>/Switch' incorporates:
    //   Constant: '<S1>/Constant'

    rty_fdot[0] = 0.0;
    rty_fdot[1] = 0.0;
  } else {
    // Switch: '<S1>/Switch' incorporates:
    //   Sum: '<S7>/Diff'
    //   UnitDelay: '<S7>/UD'
    //
    //  Block description for '<S7>/Diff':
    //
    //   Add in CPU
    //
    //  Block description for '<S7>/UD':
    //
    //   Store in Global RAM

    rty_fdot[0] = rtb_TSamp_idx_0 - localDW->UD_DSTATE[0];
    rty_fdot[1] = rtb_TSamp_idx_1 - localDW->UD_DSTATE[1];
  }

  // End of Step: '<S1>/Step'

  // Update for UnitDelay: '<S7>/UD'
  //
  //  Block description for '<S7>/UD':
  //
  //   Store in Global RAM

  localDW->UD_DSTATE[0] = rtb_TSamp_idx_0;
  localDW->UD_DSTATE[1] = rtb_TSamp_idx_1;
}

//
// Output and update for atomic system:
//    '<S4>/MATLAB Function'
//    '<S5>/MATLAB Function'
//
void Controller2GS::MATLABFunction(real_T rtu_u, real_T *rty_y,
  real_T *rty_y5)
{
  real_T y;
  if (rtu_u < 5.0) {
    y = 0.0;
  } else if (rtu_u > 7.0) {
    y = 1.0;
  } else {
    y = (rtu_u - 5.0) * 0.5;
  }

  *rty_y5 = 1.0 - y;
  *rty_y = y;
}

//
// System initialize for atomic system:
//    synthesized block
//    synthesized block
//
void Controller2GS::MovingAverage_Init(DW_MovingAverage
  *localDW)
{
  dsp_simulink_MovingAverage *obj;
  g_dsp_private_SlidingWindowAver *obj_0;
  int32_T i;

  // Start for MATLABSystem: '<S4>/Moving Average'
  localDW->obj.matlabCodegenIsDeleted = true;
  localDW->obj.isInitialized = 0;
  localDW->obj.NumChannels = -1;
  localDW->obj.matlabCodegenIsDeleted = false;
  localDW->objisempty = true;
  obj = &localDW->obj;
  localDW->obj.isSetupComplete = false;
  localDW->obj.isInitialized = 1;
  localDW->obj.NumChannels = 1;
  obj->_pobj0.isInitialized = 0;
  localDW->obj.pStatistic = &obj->_pobj0;
  localDW->obj.isSetupComplete = true;
  localDW->obj.TunablePropsChanged = false;

  // InitializeConditions for MATLABSystem: '<S4>/Moving Average'
  obj_0 = localDW->obj.pStatistic;
  if (obj_0->isInitialized == 1) {
    obj_0->pCumSum = 0.0;
    for (i = 0; i < 9; i++) {
      obj_0->pCumSumRev[i] = 0.0;
    }

    obj_0->pCumRevIndex = 1.0;
  }

  // End of InitializeConditions for MATLABSystem: '<S4>/Moving Average'
}

//
// Output and update for atomic system:
//    synthesized block
//    synthesized block
//
void Controller2GS::MovingAverage(real_T rtu_0,
  DW_MovingAverage *localDW)
{
  g_dsp_private_SlidingWindowAver *obj;
  real_T csumrev[9];
  real_T csum;
  real_T cumRevIndex;
  real_T z;
  int32_T i;

  // MATLABSystem: '<S4>/Moving Average'
  if (localDW->obj.TunablePropsChanged) {
    localDW->obj.TunablePropsChanged = false;
  }

  obj = localDW->obj.pStatistic;
  if (obj->isInitialized != 1) {
    obj->isSetupComplete = false;
    obj->isInitialized = 1;
    obj->pCumSum = 0.0;
    for (i = 0; i < 9; i++) {
      obj->pCumSumRev[i] = 0.0;
    }

    obj->pCumRevIndex = 1.0;
    obj->isSetupComplete = true;
    obj->pCumSum = 0.0;
    for (i = 0; i < 9; i++) {
      obj->pCumSumRev[i] = 0.0;
    }

    obj->pCumRevIndex = 1.0;
  }

  cumRevIndex = obj->pCumRevIndex;
  csum = obj->pCumSum;
  for (i = 0; i < 9; i++) {
    csumrev[i] = obj->pCumSumRev[i];
  }

  csum += rtu_0;
  z = csumrev[static_cast<int32_T>(cumRevIndex) - 1] + csum;
  csumrev[static_cast<int32_T>(cumRevIndex) - 1] = rtu_0;
  if (cumRevIndex != 9.0) {
    cumRevIndex++;
  } else {
    cumRevIndex = 1.0;
    csum = 0.0;
    for (i = 7; i >= 0; i--) {
      csumrev[i] += csumrev[i + 1];
    }
  }

  obj->pCumSum = csum;
  for (i = 0; i < 9; i++) {
    obj->pCumSumRev[i] = csumrev[i];
  }

  obj->pCumRevIndex = cumRevIndex;

  // MATLABSystem: '<S4>/Moving Average'
  localDW->MovingAverage_p = z / 10.0;
}

// Function for MATLAB Function: '<Root>/MATLAB Function1'
void Controller2GS::cosd(real_T *x)
{
  real_T absx;
  real_T b_x;
  int8_T n;
  if (rtIsInf(*x) || rtIsNaN(*x)) {
    *x = (rtGetNaN());
  } else {
    b_x = rt_remd_snf(*x, 360.0);
    absx = std::abs(b_x);
    if (absx > 180.0) {
      if (b_x > 0.0) {
        b_x -= 360.0;
      } else {
        b_x += 360.0;
      }

      absx = std::abs(b_x);
    }

    if (absx <= 45.0) {
      b_x *= 0.017453292519943295;
      n = 0;
    } else if (absx <= 135.0) {
      if (b_x > 0.0) {
        b_x = (b_x - 90.0) * 0.017453292519943295;
        n = 1;
      } else {
        b_x = (b_x + 90.0) * 0.017453292519943295;
        n = -1;
      }
    } else if (b_x > 0.0) {
      b_x = (b_x - 180.0) * 0.017453292519943295;
      n = 2;
    } else {
      b_x = (b_x + 180.0) * 0.017453292519943295;
      n = -2;
    }

    switch (n) {
     case 0:
      *x = std::cos(b_x);
      break;

     case 1:
      *x = -std::sin(b_x);
      break;

     case -1:
      *x = std::sin(b_x);
      break;

     default:
      *x = -std::cos(b_x);
      break;
    }
  }
}

// Function for MATLAB Function: '<S6>/MATLAB Function'
void Controller2GS::sind(real_T *x)
{
  real_T absx;
  real_T c_x;
  int8_T n;
  if (rtIsInf(*x) || rtIsNaN(*x)) {
    *x = (rtGetNaN());
  } else {
    c_x = rt_remd_snf(*x, 360.0);
    absx = std::abs(c_x);
    if (absx > 180.0) {
      if (c_x > 0.0) {
        c_x -= 360.0;
      } else {
        c_x += 360.0;
      }

      absx = std::abs(c_x);
    }

    if (absx <= 45.0) {
      c_x *= 0.017453292519943295;
      n = 0;
    } else if (absx <= 135.0) {
      if (c_x > 0.0) {
        c_x = (c_x - 90.0) * 0.017453292519943295;
        n = 1;
      } else {
        c_x = (c_x + 90.0) * 0.017453292519943295;
        n = -1;
      }
    } else if (c_x > 0.0) {
      c_x = (c_x - 180.0) * 0.017453292519943295;
      n = 2;
    } else {
      c_x = (c_x + 180.0) * 0.017453292519943295;
      n = -2;
    }

    switch (n) {
     case 0:
      *x = std::sin(c_x);
      break;

     case 1:
      *x = std::cos(c_x);
      break;

     case -1:
      *x = -std::cos(c_x);
      break;

     default:
      *x = -std::sin(c_x);
      break;
    }
  }
}

// Model step function
void Controller2GS::step()
{
  // local block i/o variables
  real_T rtb_v[3];
  real_T absx_0[9];
  real_T rtb_TmpSignalConversionAtGain1I[5];
  real_T rtb_Gain1[2];
  real_T rtb_Gain5[2];
  real_T rtb_Switch[2];
  real_T ab;
  real_T absx;
  real_T bb;
  real_T cb;
  real_T d;
  real_T db;
  real_T e;
  real_T eb;
  real_T f;
  real_T g;
  real_T h;
  real_T i;
  real_T j;
  real_T k;
  real_T l;
  real_T m;
  real_T n_0;
  real_T o;
  real_T p;
  real_T q;
  real_T r;
  real_T rtb_y;
  real_T rtb_z_cm;
  real_T s;
  real_T t;
  real_T u;
  real_T v;
  real_T w;
  real_T y;
  int32_T i_0;
  int32_T i_1;
  int8_T n;

  // Outputs for Atomic SubSystem: '<Root>/Subsystem'
  // MATLAB Function: '<S6>/MATLAB Function' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/Roll'
  //   Inport: '<Root>/Vx'
  //   Inport: '<Root>/Vy'
  //   Inport: '<Root>/Vz'
  //   Inport: '<Root>/Yaw'

  absx = rtU.Pitch;
  cosd(&absx);
  rtb_y = rtU.Yaw;
  cosd(&rtb_y);
  d = rtU.Pitch;
  cosd(&d);
  e = rtU.Yaw;
  sind(&e);
  f = rtU.Pitch;
  sind(&f);
  g = rtU.Roll;
  sind(&g);
  h = rtU.Pitch;
  sind(&h);
  i = rtU.Yaw;
  cosd(&i);
  j = rtU.Roll;
  cosd(&j);
  k = rtU.Yaw;
  sind(&k);
  l = rtU.Roll;
  sind(&l);
  m = rtU.Pitch;
  sind(&m);
  n_0 = rtU.Yaw;
  sind(&n_0);
  o = rtU.Roll;
  cosd(&o);
  p = rtU.Yaw;
  cosd(&p);
  q = rtU.Roll;
  sind(&q);
  r = rtU.Pitch;
  cosd(&r);
  s = rtU.Roll;
  cosd(&s);
  t = rtU.Pitch;
  sind(&t);
  u = rtU.Yaw;
  cosd(&u);
  v = rtU.Roll;
  sind(&v);
  w = rtU.Yaw;
  sind(&w);
  rtb_z_cm = rtU.Roll;
  cosd(&rtb_z_cm);
  y = rtU.Pitch;
  sind(&y);
  ab = rtU.Yaw;
  sind(&ab);
  bb = rtU.Roll;
  sind(&bb);
  cb = rtU.Yaw;
  cosd(&cb);
  db = rtU.Roll;
  cosd(&db);
  eb = rtU.Pitch;
  cosd(&eb);
  absx_0[0] = absx * rtb_y;
  absx_0[3] = d * e;
  absx_0[6] = -f;
  absx_0[1] = g * h * i - j * k;
  absx_0[4] = l * m * n_0 + o * p;
  absx_0[7] = q * r;
  absx_0[2] = s * t * u + v * w;
  absx_0[5] = rtb_z_cm * y * ab - bb * cb;
  absx_0[8] = db * eb;
  for (i_0 = 0; i_0 < 3; i_0++) {
    rtb_v[i_0] = 0.0;
    rtb_v[i_0] += absx_0[i_0] * rtU.Vx;
    rtb_v[i_0] += absx_0[i_0 + 3] * rtU.Vy;
    rtb_v[i_0] += absx_0[i_0 + 6] * rtU.Vz;
  }

  // End of MATLAB Function: '<S6>/MATLAB Function'
  // End of Outputs for SubSystem: '<Root>/Subsystem'

  // MATLAB Function: '<Root>/MATLAB Function1' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/Roll'
  //   Inport: '<Root>/z'

  if (rtIsInf(rtU.Pitch) || rtIsNaN(rtU.Pitch)) {
    rtb_z_cm = (rtGetNaN());
  } else {
    rtb_z_cm = rt_remd_snf(rtU.Pitch, 360.0);
    absx = std::abs(rtb_z_cm);
    if (absx > 180.0) {
      if (rtb_z_cm > 0.0) {
        rtb_z_cm -= 360.0;
      } else {
        rtb_z_cm += 360.0;
      }

      absx = std::abs(rtb_z_cm);
    }

    if (absx <= 45.0) {
      rtb_z_cm *= 0.017453292519943295;
      n = 0;
    } else if (absx <= 135.0) {
      if (rtb_z_cm > 0.0) {
        rtb_z_cm = (rtb_z_cm - 90.0) * 0.017453292519943295;
        n = 1;
      } else {
        rtb_z_cm = (rtb_z_cm + 90.0) * 0.017453292519943295;
        n = -1;
      }
    } else if (rtb_z_cm > 0.0) {
      rtb_z_cm = (rtb_z_cm - 180.0) * 0.017453292519943295;
      n = 2;
    } else {
      rtb_z_cm = (rtb_z_cm + 180.0) * 0.017453292519943295;
      n = -2;
    }

    rtb_z_cm = std::tan(rtb_z_cm);
    if ((n == 1) || (n == -1)) {
      absx = 1.0 / rtb_z_cm;
      rtb_z_cm = -(1.0 / rtb_z_cm);
      if (rtIsInf(rtb_z_cm) && (n == 1)) {
        rtb_z_cm = absx;
      }
    }
  }

  absx = rtU.Pitch;
  cosd(&absx);
  rtb_y = rtU.Roll;
  cosd(&rtb_y);
  rtb_z_cm = (rtU.heave * absx + rtb_z_cm * 3.15) / rtb_y;

  // End of MATLAB Function: '<Root>/MATLAB Function1'

  // SignalConversion generated from: '<Root>/Gain1' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/q'

  rtb_TmpSignalConversionAtGain1I[0] = rtb_v[0];
  rtb_TmpSignalConversionAtGain1I[1] = rtb_v[2];
  rtb_TmpSignalConversionAtGain1I[2] = rtU.q;
  rtb_TmpSignalConversionAtGain1I[3] = rtU.Pitch;
  rtb_TmpSignalConversionAtGain1I[4] = rtb_z_cm;

  // Gain: '<Root>/Gain1'
  for (i_0 = 0; i_0 < 2; i_0++) {
    rtb_Gain1[i_0] = 0.0;
    for (i_1 = 0; i_1 < 5; i_1++) {
      rtb_Gain1[i_0] += rtConstP_controller2gs.Gain1_Gain[(i_1 << 1) + i_0] *
        rtb_TmpSignalConversionAtGain1I[i_1];
    }
  }

  // End of Gain: '<Root>/Gain1'

  // Outputs for Atomic SubSystem: '<Root>/Discrete derivative1'
  Discretederivative1(rtb_Gain1, rtb_Switch, &rtDW.Discretederivative1_h);

  // End of Outputs for SubSystem: '<Root>/Discrete derivative1'

  // Gain: '<Root>/Gain5'
  for (i_0 = 0; i_0 < 2; i_0++) {
    rtb_Gain5[i_0] = 0.0;
    for (i_1 = 0; i_1 < 5; i_1++) {
      rtb_Gain5[i_0] += rtConstP_controller2gs.Gain5_Gain[(i_1 << 1) + i_0] *
        rtb_TmpSignalConversionAtGain1I[i_1];
    }
  }

  // End of Gain: '<Root>/Gain5'

  // Outputs for Atomic SubSystem: '<Root>/Discrete derivative2'
  Discretederivative1(rtb_Gain5, rtb_Gain1, &rtDW.Discretederivative2);

  // End of Outputs for SubSystem: '<Root>/Discrete derivative2'

  // Outputs for Atomic SubSystem: '<Root>/Scheduler'
  MovingAverage(rtb_v[0], &rtDW.MovingAverage_p);

  // MATLAB Function: '<S4>/MATLAB Function'
  MATLABFunction(rtDW.MovingAverage_p.MovingAverage_p, &absx, &rtb_y);

  // Product: '<S4>/Product1'
  rtb_Switch[0] *= absx;

  // Product: '<S4>/Product'
  rtb_Gain1[0] *= rtb_y;

  // Product: '<S4>/Product1'
  d = rtb_Switch[1] * absx;

  // Product: '<S4>/Product'
  e = rtb_y * rtb_Gain1[1];

  // End of Outputs for SubSystem: '<Root>/Scheduler'

  // Sum: '<Root>/Sum2' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/heave_ref'
  //   Inport: '<Root>/pitch_ref'

  rtb_Gain5[0] = rtU.heave_ref - rtb_z_cm;
  rtb_Gain5[1] = rtU.pitch_ref - rtU.Pitch;

  // Outputs for Atomic SubSystem: '<Root>/Scheduler1'
  MovingAverage(rtb_v[0], &rtDW.MovingAverage_pn);

  // MATLAB Function: '<S5>/MATLAB Function'
  MATLABFunction(rtDW.MovingAverage_pn.MovingAverage_p, &rtb_y, &absx);

  // End of Outputs for SubSystem: '<Root>/Scheduler1'

  // Saturate: '<Root>/Saturation1'
  if (rtDW.DiscreteTimeIntegrator1_DSTATE > 11.0) {
    // Outport: '<Root>/com_mode'
    rtY.com_mode = 11.0;
  } else if (rtDW.DiscreteTimeIntegrator1_DSTATE < 0.0) {
    // Outport: '<Root>/com_mode'
    rtY.com_mode = 0.0;
  } else {
    // Outport: '<Root>/com_mode'
    rtY.com_mode = rtDW.DiscreteTimeIntegrator1_DSTATE;
  }

  if (rtDW.DiscreteTimeIntegrator2_DSTATE > MAX_REAR_FOILANGLE) {
    // Outport: '<Root>/rear_foil '
    rtY.rear_foil = MAX_REAR_FOILANGLE;
  } else if (rtDW.DiscreteTimeIntegrator2_DSTATE < MIN_REAR_FOILANGLE) {
    // Outport: '<Root>/rear_foil '
    rtY.rear_foil = MIN_REAR_FOILANGLE;
  } else {
    // Outport: '<Root>/rear_foil '
    rtY.rear_foil = rtDW.DiscreteTimeIntegrator2_DSTATE;
  }

  if (rtDW.DiscreteTimeIntegrator1_DSTATE > 11.0) {
    rtb_z_cm = 11.0;
  } else if (rtDW.DiscreteTimeIntegrator1_DSTATE < 0.0) {
    rtb_z_cm = 0.0;
  } else {
    rtb_z_cm = rtDW.DiscreteTimeIntegrator1_DSTATE;
  }

  // Outputs for Atomic SubSystem: '<Root>/Scheduler1'
  // Outputs for Atomic SubSystem: '<Root>/Scheduler'
  // Update for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' incorporates:
  //   Gain: '<Root>/Gain3'
  //   Gain: '<Root>/Gain4'
  //   Product: '<S5>/Product'
  //   Product: '<S5>/Product1'
  //   Sum: '<Root>/Sum1'
  //   Sum: '<Root>/Sum4'
  //   Sum: '<S4>/Add'
  //   Sum: '<S5>/Add'

  rtDW.DiscreteTimeIntegrator1_DSTATE += (((0.0 -
    (rtDW.DiscreteTimeIntegrator1_DSTATE - rtb_z_cm)) - ((4.1319904490571311 * //Kih7(1,6:7)
    rtb_Gain5[0] + -0.017530816207593444 * rtb_Gain5[1]) * rtb_y +
    (63.328830742875468 * rtb_Gain5[0] + -2.4365457476604053 * rtb_Gain5[1]) * //Kih5(1,6:7)
    absx)) - (rtb_Switch[0] + rtb_Gain1[0])) * 0.02;

  // End of Outputs for SubSystem: '<Root>/Scheduler'
  // End of Outputs for SubSystem: '<Root>/Scheduler1'

  // Saturate: '<Root>/Saturation1'
  if (rtDW.DiscreteTimeIntegrator2_DSTATE > MAX_REAR_FOILANGLE) {
    rtb_z_cm = MAX_REAR_FOILANGLE;
  } else if (rtDW.DiscreteTimeIntegrator2_DSTATE < MIN_REAR_FOILANGLE) {
    rtb_z_cm = MIN_REAR_FOILANGLE;
  } else {
    rtb_z_cm = rtDW.DiscreteTimeIntegrator2_DSTATE;
  }

  // Outputs for Atomic SubSystem: '<Root>/Scheduler1'
  // Outputs for Atomic SubSystem: '<Root>/Scheduler'
  // Update for DiscreteIntegrator: '<Root>/Discrete-Time Integrator2' incorporates:
  //   Gain: '<Root>/Gain3'
  //   Gain: '<Root>/Gain4'
  //   Product: '<S5>/Product'
  //   Product: '<S5>/Product1'
  //   Sum: '<Root>/Sum1'
  //   Sum: '<Root>/Sum4'
  //   Sum: '<S4>/Add'
  //   Sum: '<S5>/Add'

  rtDW.DiscreteTimeIntegrator2_DSTATE += (((0.0 -
    (rtDW.DiscreteTimeIntegrator2_DSTATE - rtb_z_cm)) - ((0.10828315758965269 * //Kih7(2,6:7)
    rtb_Gain5[0] + 0.41810036959076 * rtb_Gain5[1]) * rtb_y +
    (31.455670343204417 * rtb_Gain5[0] + 4.9054301360387953 * rtb_Gain5[1]) *   //Kih5(2,6:7)
    absx)) - (d + e)) * 0.02;

  // End of Outputs for SubSystem: '<Root>/Scheduler'
  // End of Outputs for SubSystem: '<Root>/Scheduler1'

  // Update absolute time for base rate
  // The "clockTick0" counts the number of times the code of this task has
  //  been executed. The resolution of this integer timer is 0.02, which is the step size
  //  of the task. Size of "clockTick0" ensures timer will not overflow during the
  //  application lifespan selected.

  (&rtM)->Timing.clockTick0++;
}

// Model initialize function
void Controller2GS::initialize()
{
  // Registration code

  // initialize non-finites
  rt_InitInfAndNaN(sizeof(real_T));

  // InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' 
  rtDW.DiscreteTimeIntegrator1_DSTATE = 0;

  // InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator2' 
  rtDW.DiscreteTimeIntegrator2_DSTATE = 0;

  // SystemInitialize for Atomic SubSystem: '<Root>/Scheduler'
  MovingAverage_Init(&rtDW.MovingAverage_p);

  // End of SystemInitialize for SubSystem: '<Root>/Scheduler'

  // SystemInitialize for Atomic SubSystem: '<Root>/Scheduler1'
  MovingAverage_Init(&rtDW.MovingAverage_pn);

  // End of SystemInitialize for SubSystem: '<Root>/Scheduler1'
}

// Model initialize function
void Controller2GS::reset()
{
  // Registration code

  // InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' 
  rtDW.DiscreteTimeIntegrator1_DSTATE = 0;

  // InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator2' 
  rtDW.DiscreteTimeIntegrator2_DSTATE = 0;

  // SystemInitialize for Atomic SubSystem: '<Root>/Scheduler'
  MovingAverage_Init(&rtDW.MovingAverage_p);

  // End of SystemInitialize for SubSystem: '<Root>/Scheduler'

  // SystemInitialize for Atomic SubSystem: '<Root>/Scheduler1'
  MovingAverage_Init(&rtDW.MovingAverage_pn);

  // End of SystemInitialize for SubSystem: '<Root>/Scheduler1'
  (&rtM)->Timing.clockTick0 = 0;
  
  rtY.com_mode = 0;
  rtY.rear_foil = 0;

}

// Constructor
Controller2GS::Controller2GS() :
  rtU(),
  rtY(),
  rtDW(),
  rtM()
{
  // Currently there is no constructor body generated.
}

// Destructor
Controller2GS::~Controller2GS()
{
  // Currently there is no destructor body generated.
}

// Real-Time Model get method
Controller2GS::RT_MODEL * Controller2GS::
  getRTM()
{
  return (&rtM);
}

//
// File trailer for generated code.
//
// [EOF]
//
