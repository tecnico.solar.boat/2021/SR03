/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

//
// File: foils_controller2.h
//
// Code generated for Simulink model 'foils_controller2'.
//
// Model version                  : 1.20
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Thu Apr 15 14:26:48 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: NXP->Cortex-M4
// Code generation objectives:
//    1. Execution efficiency
//    2. RAM efficiency
// Validation result: Not run
//
#ifndef CONTROLLER_2_H
#define CONTROLLER_2_H
#include <stddef.h>
#include <cfloat>
#include <cmath>
#include "controllers_include.h"
#include "../../include/config.h"

// Class declaration for model foils_controller2
class Controller2
{
  // public data and function members
 public:
  // Block signals and states (default storage) for system '<Root>'
  typedef struct {
    real_T UD_DSTATE[2];               // '<S4>/UD'
    real_T DiscreteTimeIntegrator1_DSTATE;// '<Root>/Discrete-Time Integrator1'
    real_T DiscreteTimeIntegrator2_DSTATE;// '<Root>/Discrete-Time Integrator2'
  } DW;

  // Constant parameters (default storage)
  typedef struct {
    // Expression: Kih(:,1:5)
    //  Referenced by: '<Root>/Gain1'

    real_T Gain1_Gain[10];
  } ConstP;

  // External inputs (root inport signals with default storage)
  typedef struct {
    real_T heave_ref;                  // '<Root>/heave_ref'
    real_T q;                          // '<Root>/q'
    real_T Pitch;                      // '<Root>/Pitch'
    real_T heave;                          // '<Root>/z'
    real_T pitch_ref;                  // '<Root>/pitch_ref'
    real_T Roll;                       // '<Root>/Roll'
    real_T Vx;                         // '<Root>/Vx'
    real_T Vy;                         // '<Root>/Vy'
    real_T Vz;                         // '<Root>/Vz'
    real_T Yaw;                        // '<Root>/Yaw'
  } ExtU;

  // External outputs (root outports fed by signals with default storage)
  typedef struct {
    real_T com_mode;                   // '<Root>/com_mode'
    real_T rear_foil;                  // '<Root>/rear_foil '
  } ExtY;

  // Real-time Model Data Structure
  struct RT_MODEL {
    const char_T * volatile errorStatus;

    //
    //  Timing:
    //  The following substructure contains information regarding
    //  the timing information for the model.

    struct {
      uint32_T clockTick0;
    } Timing;
  };

  // External inputs
  ExtU rtU;

  // External outputs
  ExtY rtY;

  // model initialize function
  void initialize();

  // model step function
  void step();

  // reset controller
  void reset();

  // Constructor
  Controller2();

  // Destructor
  ~Controller2();

  // Real-Time Model get method
  Controller2::RT_MODEL * getRTM();

  // private data and function members
 private:
  // Block signals and states
  DW rtDW;

  // Real-Time Model
  RT_MODEL rtM;

  // private member function(s) for subsystem '<Root>'
  void cosd(real_T *x);
  void sind(real_T *x);
}

;

// Constant parameters (default storage)
extern const Controller2::ConstP rtConstP_controller2;

//-
//  These blocks were eliminated from the model due to optimizations:
//
//  Block '<S4>/Data Type Duplicate' : Unused code path elimination
//  Block '<Root>/Gain2' : Eliminated nontunable gain of 1


//-
//  The generated code includes comments that allow you to trace directly
//  back to the appropriate location in the model.  The basic format
//  is <system>/block_name, where system is the system number (uniquely
//  assigned by Simulink) and block_name is the name of the block.
//
//  Use the MATLAB hilite_system command to trace the generated code back
//  to the model.  For example,
//
//  hilite_system('<S3>')    - opens system 3
//  hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
//
//  Here is the system hierarchy for this model
//
//  '<Root>' : 'foils_controller2'
//  '<S1>'   : 'foils_controller2/Discrete derivative1'
//  '<S2>'   : 'foils_controller2/MATLAB Function1'
//  '<S3>'   : 'foils_controller2/Subsystem'
//  '<S4>'   : 'foils_controller2/Discrete derivative1/Discrete Derivative'
//  '<S5>'   : 'foils_controller2/Subsystem/MATLAB Function'

#endif                                 // RTW_HEADER_foils_controller2_h_

//
// File trailer for generated code.
//
// [EOF]
//
