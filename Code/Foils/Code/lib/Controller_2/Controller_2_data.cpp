/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

//
// File: foils_controller2_data.cpp
//
// Code generated for Simulink model 'foils_controller2'.
//
// Model version                  : 1.20
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Thu Apr 15 14:26:48 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: NXP->Cortex-M4
// Code generation objectives:
//    1. Execution efficiency
//    2. RAM efficiency
// Validation result: Not run
//
#include "Controller_2.h"

// Constant parameters (default storage)
const Controller2::ConstP rtConstP_controller2 = {
  // Expression: Kih(:,1:5)
  //  Referenced by: '<Root>/Gain1'

  { 1.626394354731671, 0.95422824043013044, -0.97771018761899064,
    -0.26818682821498796, 0.0485731562164147, -0.028202138549715681,
    1.131146779966943, -0.49312699833188139, -17.934827483982595,
    -3.82891885311863 }
};

//
// File trailer for generated code.
//
// [EOF]
//
