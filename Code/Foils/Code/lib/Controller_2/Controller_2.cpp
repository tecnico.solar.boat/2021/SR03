/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

//
// File: foils_controller2.cpp
//
// Code generated for Simulink model 'foils_controller2'.
//
// Model version                  : 1.20
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Thu Apr 15 14:26:48 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: NXP->Cortex-M4
// Code generation objectives:
//    1. Execution efficiency
//    2. RAM efficiency
// Validation result: Not run
//
#include "Controller_2.h"

// Function for MATLAB Function: '<Root>/MATLAB Function1'
void Controller2::cosd(real_T *x)
{
  real_T absx;
  real_T b_x;
  int8_T n;
  if (rtIsInf(*x) || rtIsNaN(*x)) {
    *x = (rtGetNaN());
  } else {
    b_x = rt_remd_snf(*x, 360.0);
    absx = std::abs(b_x);
    if (absx > 180.0) {
      if (b_x > 0.0) {
        b_x -= 360.0;
      } else {
        b_x += 360.0;
      }

      absx = std::abs(b_x);
    }

    if (absx <= 45.0) {
      b_x *= 0.017453292519943295;
      n = 0;
    } else if (absx <= 135.0) {
      if (b_x > 0.0) {
        b_x = (b_x - 90.0) * 0.017453292519943295;
        n = 1;
      } else {
        b_x = (b_x + 90.0) * 0.017453292519943295;
        n = -1;
      }
    } else if (b_x > 0.0) {
      b_x = (b_x - 180.0) * 0.017453292519943295;
      n = 2;
    } else {
      b_x = (b_x + 180.0) * 0.017453292519943295;
      n = -2;
    }

    switch (n) {
     case 0:
      *x = std::cos(b_x);
      break;

     case 1:
      *x = -std::sin(b_x);
      break;

     case -1:
      *x = std::sin(b_x);
      break;

     default:
      *x = -std::cos(b_x);
      break;
    }
  }
}

// Function for MATLAB Function: '<S3>/MATLAB Function'
void Controller2::sind(real_T *x)
{
  real_T absx;
  real_T c_x;
  int8_T n;
  if (rtIsInf(*x) || rtIsNaN(*x)) {
    *x = (rtGetNaN());
  } else {
    c_x = rt_remd_snf(*x, 360.0);
    absx = std::abs(c_x);
    if (absx > 180.0) {
      if (c_x > 0.0) {
        c_x -= 360.0;
      } else {
        c_x += 360.0;
      }

      absx = std::abs(c_x);
    }

    if (absx <= 45.0) {
      c_x *= 0.017453292519943295;
      n = 0;
    } else if (absx <= 135.0) {
      if (c_x > 0.0) {
        c_x = (c_x - 90.0) * 0.017453292519943295;
        n = 1;
      } else {
        c_x = (c_x + 90.0) * 0.017453292519943295;
        n = -1;
      }
    } else if (c_x > 0.0) {
      c_x = (c_x - 180.0) * 0.017453292519943295;
      n = 2;
    } else {
      c_x = (c_x + 180.0) * 0.017453292519943295;
      n = -2;
    }

    switch (n) {
     case 0:
      *x = std::sin(c_x);
      break;

     case 1:
      *x = std::cos(c_x);
      break;

     case -1:
      *x = -std::cos(c_x);
      break;

     default:
      *x = -std::sin(c_x);
      break;
    }
  }
}

// Model step function
void Controller2::step()
{
  real_T absx_0[9];
  real_T rtb_TmpSignalConversionAtSFun_e[5];
  real_T rtb_TmpSignalConversionAtSFun_n[3];
  real_T rtb_TSamp[2];
  real_T tmp[2];
  real_T ab;
  real_T absx;
  real_T bb;
  real_T cb;
  real_T d;
  real_T db;
  real_T e;
  real_T eb;
  real_T f;
  real_T g;
  real_T h;
  real_T i;
  real_T j;
  real_T k;
  real_T l;
  real_T m;
  real_T n_0;
  real_T o;
  real_T p;
  real_T q;
  real_T r;
  real_T rtb_Switch_idx_1;
  real_T rtb_z_cm;
  real_T s;
  real_T t;
  real_T u;
  real_T v;
  real_T w;
  real_T y;
  int32_T i_0;
  int32_T i_1;
  int8_T n;

  // Outputs for Atomic SubSystem: '<Root>/Subsystem'
  // MATLAB Function: '<S3>/MATLAB Function' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/Roll'
  //   Inport: '<Root>/Vx'
  //   Inport: '<Root>/Vy'
  //   Inport: '<Root>/Vz'
  //   Inport: '<Root>/Yaw'

  absx = rtU.Pitch;
  cosd(&absx);
  rtb_Switch_idx_1 = rtU.Yaw;
  cosd(&rtb_Switch_idx_1);
  d = rtU.Pitch;
  cosd(&d);
  e = rtU.Yaw;
  sind(&e);
  f = rtU.Pitch;
  sind(&f);
  g = rtU.Roll;
  sind(&g);
  h = rtU.Pitch;
  sind(&h);
  i = rtU.Yaw;
  cosd(&i);
  j = rtU.Roll;
  cosd(&j);
  k = rtU.Yaw;
  sind(&k);
  l = rtU.Roll;
  sind(&l);
  m = rtU.Pitch;
  sind(&m);
  n_0 = rtU.Yaw;
  sind(&n_0);
  o = rtU.Roll;
  cosd(&o);
  p = rtU.Yaw;
  cosd(&p);
  q = rtU.Roll;
  sind(&q);
  r = rtU.Pitch;
  cosd(&r);
  s = rtU.Roll;
  cosd(&s);
  t = rtU.Pitch;
  sind(&t);
  u = rtU.Yaw;
  cosd(&u);
  v = rtU.Roll;
  sind(&v);
  w = rtU.Yaw;
  sind(&w);
  rtb_z_cm = rtU.Roll;
  cosd(&rtb_z_cm);
  y = rtU.Pitch;
  sind(&y);
  ab = rtU.Yaw;
  sind(&ab);
  bb = rtU.Roll;
  sind(&bb);
  cb = rtU.Yaw;
  cosd(&cb);
  db = rtU.Roll;
  cosd(&db);
  eb = rtU.Pitch;
  cosd(&eb);
  absx_0[0] = absx * rtb_Switch_idx_1;
  absx_0[3] = d * e;
  absx_0[6] = -f;
  absx_0[1] = g * h * i - j * k;
  absx_0[4] = l * m * n_0 + o * p;
  absx_0[7] = q * r;
  absx_0[2] = s * t * u + v * w;
  absx_0[5] = rtb_z_cm * y * ab - bb * cb;
  absx_0[8] = db * eb;
  for (i_0 = 0; i_0 < 3; i_0++) {
    rtb_TmpSignalConversionAtSFun_n[i_0] = absx_0[i_0 + 6] * rtU.Vz +
      (absx_0[i_0 + 3] * rtU.Vy + absx_0[i_0] * rtU.Vx);
  }

  // End of MATLAB Function: '<S3>/MATLAB Function'
  // End of Outputs for SubSystem: '<Root>/Subsystem'

  // MATLAB Function: '<Root>/MATLAB Function1' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/Roll'
  //   Inport: '<Root>/z'

  if (rtIsInf(rtU.Pitch) || rtIsNaN(rtU.Pitch)) {
    rtb_z_cm = (rtGetNaN());
  } else {
    rtb_z_cm = rt_remd_snf(rtU.Pitch, 360.0);
    absx = std::abs(rtb_z_cm);
    if (absx > 180.0) {
      if (rtb_z_cm > 0.0) {
        rtb_z_cm -= 360.0;
      } else {
        rtb_z_cm += 360.0;
      }

      absx = std::abs(rtb_z_cm);
    }

    if (absx <= 45.0) {
      rtb_z_cm *= 0.017453292519943295;
      n = 0;
    } else if (absx <= 135.0) {
      if (rtb_z_cm > 0.0) {
        rtb_z_cm = (rtb_z_cm - 90.0) * 0.017453292519943295;
        n = 1;
      } else {
        rtb_z_cm = (rtb_z_cm + 90.0) * 0.017453292519943295;
        n = -1;
      }
    } else if (rtb_z_cm > 0.0) {
      rtb_z_cm = (rtb_z_cm - 180.0) * 0.017453292519943295;
      n = 2;
    } else {
      rtb_z_cm = (rtb_z_cm + 180.0) * 0.017453292519943295;
      n = -2;
    }

    rtb_z_cm = std::tan(rtb_z_cm);
    if ((n == 1) || (n == -1)) {
      absx = 1.0 / rtb_z_cm;
      rtb_z_cm = -(1.0 / rtb_z_cm);
      if (rtIsInf(rtb_z_cm) && (n == 1)) {
        rtb_z_cm = absx;
      }
    }
  }

  absx = rtU.Pitch;
  cosd(&absx);
  rtb_Switch_idx_1 = rtU.Roll;
  cosd(&rtb_Switch_idx_1);
  rtb_z_cm = (rtU.heave * absx + rtb_z_cm * 3.15) / rtb_Switch_idx_1;

  // End of MATLAB Function: '<Root>/MATLAB Function1'

  // SignalConversion generated from: '<Root>/Gain1' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/q'

  rtb_TmpSignalConversionAtSFun_e[0] = rtb_TmpSignalConversionAtSFun_n[0];
  rtb_TmpSignalConversionAtSFun_e[1] = rtb_TmpSignalConversionAtSFun_n[2];
  rtb_TmpSignalConversionAtSFun_e[2] = rtU.q;
  rtb_TmpSignalConversionAtSFun_e[3] = rtU.Pitch;
  rtb_TmpSignalConversionAtSFun_e[4] = rtb_z_cm;

  // Outputs for Atomic SubSystem: '<Root>/Discrete derivative1'
  for (i_0 = 0; i_0 < 2; i_0++) {
    // Gain: '<Root>/Gain1'
    tmp[i_0] = 0.0;
    for (i_1 = 0; i_1 < 5; i_1++) {
      tmp[i_0] += rtConstP_controller2.Gain1_Gain[(i_1 << 1) + i_0] *
        rtb_TmpSignalConversionAtSFun_e[i_1];
    }

    // End of Gain: '<Root>/Gain1'

    // SampleTimeMath: '<S4>/TSamp'
    //
    //  About '<S4>/TSamp':
    //   y = u * K where K = 1 / ( w * Ts )

    rtb_TSamp[i_0] = tmp[i_0] * 50.0;
  }

  // Step: '<S1>/Step'
  if ((((&rtM)->Timing.clockTick0) * 0.02) < 0.01) {
    // Switch: '<S1>/Switch' incorporates:
    //   Constant: '<S1>/Constant'

    absx = 0.0;
    rtb_Switch_idx_1 = 0.0;
  } else {
    // Switch: '<S1>/Switch' incorporates:
    //   Sum: '<S4>/Diff'
    //   UnitDelay: '<S4>/UD'
    //
    //  Block description for '<S4>/Diff':
    //
    //   Add in CPU
    //
    //  Block description for '<S4>/UD':
    //
    //   Store in Global RAM

    absx = rtb_TSamp[0] - rtDW.UD_DSTATE[0];
    rtb_Switch_idx_1 = rtb_TSamp[1] - rtDW.UD_DSTATE[1];
  }

  // End of Step: '<S1>/Step'

  // Update for UnitDelay: '<S4>/UD'
  //
  //  Block description for '<S4>/UD':
  //
  //   Store in Global RAM

  rtDW.UD_DSTATE[0] = rtb_TSamp[0];
  rtDW.UD_DSTATE[1] = rtb_TSamp[1];

  // End of Outputs for SubSystem: '<Root>/Discrete derivative1'

  // Saturate: '<Root>/Saturation1'
  if (rtDW.DiscreteTimeIntegrator2_DSTATE > MAX_REAR_FOILANGLE) {
    y = MAX_REAR_FOILANGLE;
  } else if (rtDW.DiscreteTimeIntegrator2_DSTATE < MIN_REAR_FOILANGLE) {
    y = MIN_REAR_FOILANGLE;
  } else {
    y = rtDW.DiscreteTimeIntegrator2_DSTATE;
  }

  // Sum: '<Root>/Sum2' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/heave_ref'
  //   Inport: '<Root>/pitch_ref'

  rtb_z_cm = rtU.heave_ref - rtb_z_cm;
  d = rtU.pitch_ref - rtU.Pitch;

  // Saturate: '<Root>/Saturation1'
  if (rtDW.DiscreteTimeIntegrator1_DSTATE > 11.0) {
    // Outport: '<Root>/com_mode'
    rtY.com_mode = 11.0;
  } else if (rtDW.DiscreteTimeIntegrator1_DSTATE < 0.0) {
    // Outport: '<Root>/com_mode'
    rtY.com_mode = 0.0;
  } else {
    // Outport: '<Root>/com_mode'
    rtY.com_mode = rtDW.DiscreteTimeIntegrator1_DSTATE;
  }

  // Outport: '<Root>/rear_foil ' incorporates:
  //   Saturate: '<Root>/Saturation1'

  rtY.rear_foil = y;

  // Saturate: '<Root>/Saturation1'
  if (rtDW.DiscreteTimeIntegrator1_DSTATE > 11.0) {
    e = 11.0;
  } else if (rtDW.DiscreteTimeIntegrator1_DSTATE < 0.0) {
    e = 0.0;
  } else {
    e = rtDW.DiscreteTimeIntegrator1_DSTATE;
  }

  // Update for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' incorporates:
  //   Gain: '<Root>/Gain'
  //   Sum: '<Root>/Sum1'
  //   Sum: '<Root>/Sum4'

  rtDW.DiscreteTimeIntegrator1_DSTATE += (((0.0 -
    (rtDW.DiscreteTimeIntegrator1_DSTATE - e)) - (4.1319904490571311 * rtb_z_cm  //Kih(1,6:7)
    + -0.017530816207593444 * d)) - absx) * 0.02;

  // Update for DiscreteIntegrator: '<Root>/Discrete-Time Integrator2' incorporates:
  //   Gain: '<Root>/Gain'
  //   Saturate: '<Root>/Saturation1'
  //   Sum: '<Root>/Sum1'
  //   Sum: '<Root>/Sum4'

  rtDW.DiscreteTimeIntegrator2_DSTATE += (((0.0 -
    (rtDW.DiscreteTimeIntegrator2_DSTATE - y)) - (0.10828315758965269 * rtb_z_cm //Kih(2,6:7)
    + 0.41810036959076 * d)) - rtb_Switch_idx_1) * 0.02;

  // Update absolute time for base rate
  // The "clockTick0" counts the number of times the code of this task has
  //  been executed. The resolution of this integer timer is 0.02, which is the step size
  //  of the task. Size of "clockTick0" ensures timer will not overflow during the
  //  application lifespan selected.

  (&rtM)->Timing.clockTick0++;
}

// Model initialize function
void Controller2::initialize()
{
  // Registration code

  // initialize non-finites
  rt_InitInfAndNaN(sizeof(real_T));

  // InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' 
  rtDW.DiscreteTimeIntegrator1_DSTATE = 0;

  // InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator2' 
  rtDW.DiscreteTimeIntegrator2_DSTATE = 0;
}

void Controller2::reset()
{
  (&rtM)->Timing.clockTick0 = 0; //reset timer
  // InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' 
  rtDW.DiscreteTimeIntegrator1_DSTATE = 0;

  // InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator2' 
  rtDW.DiscreteTimeIntegrator2_DSTATE = 0;
  rtY.com_mode = 0;
  rtY.rear_foil = 0;
}

// Constructor
Controller2::Controller2() :
  rtU(),
  rtY(),
  rtDW(),
  rtM()
{
  // Currently there is no constructor body generated.
}

// Destructor
Controller2::~Controller2()
{
  // Currently there is no destructor body generated.
}

// Real-Time Model get method
Controller2::RT_MODEL * Controller2::getRTM()
{
  return (&rtM);
}

//
// File trailer for generated code.
//
// [EOF]
//
