/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "Foils_Controller.h"

FoilsController::FoilsController(){
    ready=false;
    foils_enabled=false;
    heave_enabled=false;
    rear_foil_enabled = false;
    homing_done=false;

    last_heave = -23;
    mode=0;
    state=0;

    ultrasonic_connected=false;

    angle_left=DEFAULT_FRONT_ANGLE;
    angle_right=DEFAULT_FRONT_ANGLE;
    angle_rear=DEFAULT_REAR_ANGLE;
}

void FoilsController::set_href(float href){
    if(Basile) controller0.h_ref=href;
    else controller1.rtU.heave_ref=href;
}

float FoilsController::get_href(){
    if(Basile) return controller0.h_ref;
    else return controller1.rtU.heave_ref;
}

void FoilsController::set_heave(float heave){
    if(Basile) controller0.heave=heave;
    else controller1.rtU.heave=heave;
}

float FoilsController::get_heave(){
    if(Basile) return controller0.heave;
    else return controller1.rtU.heave;
}

void FoilsController::set_boatspeed(float speed){
    if(Basile) controller0.boat_speed=speed;
    else{
        controller1.rtU.Vx=speed;
        controller1.rtU.Yaw=0;
    }
}

float FoilsController::get_boatspeed(){
    if(Basile) return controller0.boat_speed;
    else return controller1.rtU.Vx;
}

void FoilsController::set_anglerear(float anglerear){
    if(Basile) controller0.angle_rear=anglerear;
    else angle_rear=anglerear;
}

float FoilsController::get_anglerear(){
    if(Basile) return controller0.angle_rear;
    else return angle_rear;
}

void FoilsController::set_angleleft(float angleleft){
    if(Basile) controller0.angle_left=angleleft;
    else angle_left=angleleft;
}

float FoilsController::get_angleleft(){
    if(Basile) return controller0.angle_left;
    else return angle_left;
}

void FoilsController::set_angleright(float angleright){
    if(Basile) controller0.angle_right=angleright;
    else angle_right=angleright;
}

float FoilsController::get_angleright(){
    if(Basile) return controller0.angle_right;
    else return angle_right;
}

void FoilsController::set_autocomangle(float autocomangle){
    if(Basile) controller0.auto_com_angle=autocomangle;
    else controller1.rtY.com_mode=autocomangle;
}

void FoilsController::set_autorearangle(float autorearangle){
    controller1.rtY.rear_foil=autorearangle;
}

float FoilsController::get_autocomangle(){
    if(Basile) return controller0.auto_com_angle;
    else return controller1.rtY.com_mode;

}

float FoilsController::get_autorearangle(){
    if (Basile) return 0; 
    return controller1.rtY.rear_foil;
}

float FoilsController::get_autodifangle(){
    return controller0.auto_dif_angle;
}

void FoilsController::set_flagfirstloop(bool flag){
    if(Basile) controller0.flag_first_loop=flag;
}

void FoilsController::set_flagfirstlooproll(bool flag){
    if(Basile) controller0.flag_first_loop_roll=flag;
}

void FoilsController::set_rollpitchyaw(float roll, float pitch, float yaw){
    controller0.roll=roll;
    controller0.pitch=pitch;
    controller0.yaw=yaw;
    if(!Basile){
        controller1.rtU.Roll=roll;
        controller1.rtU.Pitch=pitch;
        controller1.rtU.Yaw=0;
    }
    
}

void FoilsController::set_gyr(float gyrx, float gyry, float gyrz){
    controller0.gyrX=gyrx;
    controller0.gyrY=gyry;
    controller0.gyrZ=gyrz;
    if(!Basile){
        controller1.rtU.q=gyry;
    }
    
}

void FoilsController::set_vel(float velx, float vely, float velz){
    controller0.velX=velx;
    controller0.velY=vely;
    controller0.velZ=velz;
    if(!Basile){
        controller1.rtU.Vz=velz;
    }
}

void FoilsController::reset_controller(bool rear_active){
    controller0.reset(); // Roll is from Basile's code, so everything must always reset
    if(!Basile){
        controller1.reset();
        angle_left=controller0.initial_com_angle;
	    angle_right=controller0.initial_com_angle;
        if(rear_active)
            angle_rear = DEFAULT_REAR_ANGLE;
    }
}

void FoilsController::reset_heave(bool rear_active){
    if(Basile) 
        controller0.reset_heave();
    else {
        controller1.reset();
        angle_left=controller0.initial_com_angle;
	    angle_right=controller0.initial_com_angle;
        if(rear_active)
            angle_rear = DEFAULT_REAR_ANGLE;
    }
}

void FoilsController::roll_controller(){
    controller0.roll_mode();
}
void FoilsController::heave_controller(){
    if(Basile) controller0.heave_mode();
    else controller1.step();
}

void FoilsController::set_heave_gains(float* heave_gains){
    controller0.Kh[0] = heave_gains[0];
    controller0.Kh[1] = heave_gains[1];
    controller0.Kh[2] = heave_gains[2];
    controller0.Kh[3] = -heave_gains[3];
    controller0.Kh[4] = -heave_gains[4];
    controller0.Kh[5] = -heave_gains[5];
}

void FoilsController::set_roll_gains(float* roll_gains){
    controller0.Kr[0] = roll_gains[0];
    controller0.Kr[1] = roll_gains[1];
    controller0.Kr[2] = roll_gains[2];
}

void FoilsController::set_realanglerear(float realanglerear){
    controller0.real_angle_rear = realanglerear;
}
float FoilsController::get_realanglerear(){
    return controller0.real_angle_rear;
}
void FoilsController::set_realangleleft(float realangleleft){
    controller0.real_angle_left = realangleleft;
}
float FoilsController::get_realangleleft(){
    return controller0.real_angle_left;
}
void FoilsController::set_realangleright(float realangleright){
    controller0.real_angle_right = realangleright;
}
float FoilsController::get_realangleright(){
    return controller0.real_angle_right;
}
        
void FoilsController::set_difanglelim(float difanglelim){
    controller0.dif_angle_lim = difanglelim;
}

float FoilsController::get_gyrXfilt(){
    return controller0.gyrX_filt;
}

float FoilsController::get_gyrZfilt(){
    return controller0.yawrate_filt;
}

float FoilsController::get_roll(){
    return controller0.roll;
}

float FoilsController::get_gyrX(){
    return controller0.gyrX;
}

float FoilsController::get_gyrZ(){
    return controller0.gyrZ;
}


void FoilsController::set_initial_com_angle(float initial_com_angle){
    controller0.initial_com_angle = initial_com_angle;
}

/**
 *
 * Function to convert angle of attack into motor position
 * 
 * @param  {float} angle : 
 * @return {int32_t}     : 
 */
int32_t angle_to_steps_front_left(float angle, int32_t offset){
    //int32_t mapped_steps = map(angle,MIN_COM_FOILANGLE-DIF_FOILANGLE,MAX_COM_FOILANGLE+DIF_FOILANGLE,MAX_MOTOR_POSITION,MIN_MOTOR_POSITION);
    //int32_t mapped_steps = map(angle,MIN_LEFT_FOILANGLE,MAX_LEFT_FOILANGLE,MIN_LEFT_FRONTMOTOR_POSITION+offset,MAX_LEFT_FRONTMOTOR_POSITION+offset);
    //if (mapped_steps > MAX_LEFT_FRONTMOTOR_POSITION+offset) mapped_steps = MAX_LEFT_FRONTMOTOR_POSITION+offset;
    //else if (mapped_steps < MIN_LEFT_FRONTMOTOR_POSITION+offset) mapped_steps = MIN_LEFT_FRONTMOTOR_POSITION+offset;
    int32_t mapped_steps=0;
    mapped_steps = map(angle,MIN_LEFT_FOILANGLE,MAX_LEFT_FOILANGLE,MIN_LEFT_FRONTMOTOR_POSITION+offset,MAX_LEFT_FRONTMOTOR_POSITION+offset);
    
    if (mapped_steps > MAX_LEFT_FRONTMOTOR_POSITION+offset) mapped_steps = MAX_LEFT_FRONTMOTOR_POSITION+offset;
    else if (mapped_steps < MIN_LEFT_FRONTMOTOR_POSITION+offset) mapped_steps = MIN_LEFT_FRONTMOTOR_POSITION+offset;

    return mapped_steps;
}
/**
 *
 * Function to convert motor position into angle of attack (front foils)
 * 
 * @param  {int32_t} steps : 
 * @return {float}         : 
 */
float steps_to_angle_front_left(int32_t steps, int32_t offset){
    // The cast is needed to make sure MAP returns a float, otherwise it will only return an integer
    //return map((float)steps,MAX_MOTOR_POSITION,MIN_MOTOR_POSITION,MIN_COM_FOILANGLE-DIF_FOILANGLE,MAX_COM_FOILANGLE+DIF_FOILANGLE);
    return map((float)steps,MIN_LEFT_FRONTMOTOR_POSITION+offset,MAX_LEFT_FRONTMOTOR_POSITION+offset,MIN_LEFT_FOILANGLE,MAX_LEFT_FOILANGLE);
}

/**
 *
 * Function to convert angle of attack into motor position
 * 
 * @param  {float} angle : 
 * @return {int32_t}     : 
 */
int32_t angle_to_steps_front_right(float angle, int32_t offset){
    //int32_t mapped_steps = map(angle,MIN_COM_FOILANGLE-DIF_FOILANGLE,MAX_COM_FOILANGLE+DIF_FOILANGLE,MAX_MOTOR_POSITION,MIN_MOTOR_POSITION);
    int32_t mapped_steps = map(angle,MIN_RIGHT_FOILANGLE,MAX_RIGHT_FOILANGLE,MIN_RIGHT_FRONTMOTOR_POSITION-offset,MAX_RIGHT_FRONTMOTOR_POSITION-offset);
    if (mapped_steps > MAX_RIGHT_FRONTMOTOR_POSITION-offset) mapped_steps = MAX_RIGHT_FRONTMOTOR_POSITION-offset;
    else if (mapped_steps < MIN_RIGHT_FRONTMOTOR_POSITION-offset) mapped_steps = MIN_RIGHT_FRONTMOTOR_POSITION-offset;
    return -1*mapped_steps;
}
/**
 *
 * Function to convert motor position into angle of attack (front foils)
 * 
 * @param  {int32_t} steps : 
 * @return {float}         : 
 */
float steps_to_angle_front_right(int32_t steps, int32_t offset){
    // The cast is needed to make sure MAP returns a float, otherwise it will only return an integer
    //return map((float)steps,MAX_MOTOR_POSITION,MIN_MOTOR_POSITION,MIN_COM_FOILANGLE-DIF_FOILANGLE,MAX_COM_FOILANGLE+DIF_FOILANGLE);
    return map((float)-1*steps,MIN_RIGHT_FRONTMOTOR_POSITION-offset,MAX_RIGHT_FRONTMOTOR_POSITION-offset,MIN_RIGHT_FOILANGLE,MAX_RIGHT_FOILANGLE);
    
}
/**
 *
 * Function to convert motor position to angle of attack (rear foil)
 * 
 * @param  {int32_t} steps : 
 * @return {float}         : 
 */
float steps_to_angle_rear(int32_t steps, int32_t offset){
    // The cast is needed to make sure MAP returns a float, otherwise it will only return an integer
    return map((float)steps,MIN_REARMOTOR_POSITION+offset,MAX_REARMOTOR_POSITION+offset,MIN_REAR_FOILANGLE,MAX_REAR_FOILANGLE);
}

/**
 *
 * Function to convert angle of attack to motor position for rear foil
 * 
 * @param  {float} angle : 
 * @return {int32_t}     : 
 */
int32_t angle_to_steps_rear(float angle, int32_t offset){
    int32_t mapped_steps = map(angle,MIN_REAR_FOILANGLE,MAX_REAR_FOILANGLE,MIN_REARMOTOR_POSITION+offset,MAX_REARMOTOR_POSITION+offset);
    if (mapped_steps > MAX_REARMOTOR_POSITION+offset) mapped_steps = MAX_REARMOTOR_POSITION+offset;
    else if (mapped_steps < MIN_REARMOTOR_POSITION + offset) mapped_steps = MIN_REARMOTOR_POSITION +offset;
    return mapped_steps;
}

