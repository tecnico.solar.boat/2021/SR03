/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "rtwtypes.h"
#include <stddef.h>
#include <cfloat>
#include <cmath>

#ifndef CONTROLLERS_INCLUDE
#define CONTROLLERS_INCLUDE

#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

struct tag_80AwAGWuE9fVrIKDJpJKoC
{
  int32_T isInitialized;
  boolean_T isSetupComplete;
  real_T pCumSum;
  real_T pCumSumRev[9];
  real_T pCumRevIndex;
};

typedef tag_80AwAGWuE9fVrIKDJpJKoC g_dsp_private_SlidingWindowAver;


struct tag_PMfBDzoakfdM9QAdfx2o6D
{
  uint32_T f1[8];
};

typedef tag_PMfBDzoakfdM9QAdfx2o6D cell_wrap;

struct tag_RoFcBL3cgrPNAPUZAtdOnE
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
  boolean_T TunablePropsChanged;
  cell_wrap inputVarSize;
  g_dsp_private_SlidingWindowAver *pStatistic;
  int32_T NumChannels;
  g_dsp_private_SlidingWindowAver _pobj0;
};

typedef tag_RoFcBL3cgrPNAPUZAtdOnE dsp_simulink_MovingAverage;

typedef struct {
  struct {
    uint32_T wordH;
    uint32_T wordL;
  } words;
} BigEndianIEEEDouble;

typedef struct {
  struct {
    uint32_T wordL;
    uint32_T wordH;
  } words;
} LittleEndianIEEEDouble;

typedef struct {
  union {
    real32_T wordLreal;
    uint32_T wordLuint;
  } wordL;
} IEEESingle;

real_T rtGetInf(void);
real32_T rtGetInfF(void);
real_T rtGetMinusInf(void);
real32_T rtGetMinusInfF(void);
real_T rtGetNaN(void);
real32_T rtGetNaNF(void);   
real_T rt_remd_snf(real_T u0, real_T u1);
void rt_InitInfAndNaN(size_t realSize);
boolean_T rtIsInf(real_T value);
boolean_T rtIsInfF(real32_T value);
boolean_T rtIsNaN(real_T value);
boolean_T rtIsNaNF(real32_T value);
#endif