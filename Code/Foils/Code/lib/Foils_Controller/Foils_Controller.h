/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef FOILS_CONTROLLER_H
#define FOILS_CONTROLLER_H

#include "Arduino.h"
#include "../../include/config.h"
#include "LQR.h"
#include "Controller.h"
#include "Controller_2.h"
#include "Controller_2_GS.h"

class FoilsController
{
    public:

        FoilsController();

        bool ready;
        bool foils_enabled;
        bool heave_enabled;
        bool rear_foil_enabled;
        bool homing_done;

        uint8_t mode;
        uint8_t state;

        bool ultrasonic_connected;

        void set_href(float href);
        float get_href();
        void set_heave(float heave);
        float get_heave();
        void set_boatspeed(float speed);
        float get_boatspeed();
        void set_anglerear(float anglerear);
        float get_anglerear();
        void set_angleleft(float angleleft);
        float get_angleleft();
        void set_angleright(float angleright);
        float get_angleright();

        void set_realanglerear(float realanglerear);
        float get_realanglerear();
        void set_realangleleft(float realangleleft);
        float get_realangleleft();
        void set_realangleright(float realangleright);
        float get_realangleright();

        void set_difanglelim(float difanglelim);
        float get_gyrXfilt();
        float get_gyrZfilt();
        float get_roll();
        float get_gyrX();
        float get_gyrZ();

        void set_autocomangle(float autocomangle);
        void set_autorearangle(float autorearangle);
        float get_autocomangle();
        float get_autorearangle();
        float get_autodifangle();
        void set_initial_com_angle(float initial_com_angle);
        
        void set_flagfirstloop(bool flag);
        void set_flagfirstlooproll(bool flag);

        void set_rollpitchyaw(float roll, float pitch, float yaw);
        void set_gyr(float gyrx, float gyry, float gyrz);
        void set_vel(float velx, float vely, float velz);


        void reset_controller(bool rear_active);
        void reset_heave(bool rear_active);

        void roll_controller();
        void heave_controller();

        void set_heave_gains(float* heave_gains);
        void set_roll_gains(float* roll_gains);

        //controllers
        LQRController controller0;
        Controller controller1;
        Controller2 controller3;
        Controller2GS controller2;

        volatile float angle_left;
        volatile float angle_right;
        volatile float angle_rear;
        volatile float last_heave;

    private:
};

int32_t angle_to_steps_front_left(float angle, int32_t offset);
float steps_to_angle_front_left(int32_t steps, int32_t offset);
int32_t angle_to_steps_front_right(float angle, int32_t offset);
float steps_to_angle_front_right(int32_t steps, int32_t offset);
float steps_to_angle_rear(int32_t steps, int32_t offset);
int32_t angle_to_steps_rear(float angle, int32_t offset);

#endif