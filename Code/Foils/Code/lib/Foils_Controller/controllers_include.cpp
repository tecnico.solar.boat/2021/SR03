/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "controllers_include.h"

#define NumBitsPerChar                 8U

//
// Initialize rtNaN needed by the generated code.
// NaN is initialized as non-signaling. Assumes IEEE.
//
real_T rtGetNaN(void){
    size_t bitsPerReal = sizeof(real_T) * (NumBitsPerChar);
    real_T nan = 0.0;
    if(bitsPerReal == 32U) {
        nan = rtGetNaNF();
    }else{
        union {
        LittleEndianIEEEDouble bitVal;
        real_T fltVal;
        } tmpVal;

        tmpVal.bitVal.words.wordH = 0xFFF80000U;
        tmpVal.bitVal.words.wordL = 0x00000000U;
        nan = tmpVal.fltVal;
    }
    return nan;
}

//
// Initialize rtNaNF needed by the generated code.
// NaN is initialized as non-signaling. Assumes IEEE.
//
real32_T rtGetNaNF(void){
    IEEESingle nanF = { { 0 } };

    nanF.wordL.wordLuint = 0xFFC00000U;
    return nanF.wordL.wordLreal;
}

// Test if value is infinite
boolean_T rtIsInf(real_T value){
    return (boolean_T)((value==rtGetInf() || value==rtGetMinusInf()) ? 1U : 0U);
}

// Test if single-precision value is infinite
boolean_T rtIsInfF(real32_T value){
    return (boolean_T)(((value)==rtGetInfF() || (value)==rtGetMinusInfF()) ? 1U : 0U);
}

// Test if value is not a number
boolean_T rtIsNaN(real_T value){
    boolean_T result = (boolean_T) 0;
    size_t bitsPerReal = sizeof(real_T) * (NumBitsPerChar);
    if (bitsPerReal == 32U){
        result = rtIsNaNF((real32_T)value);
    }else{
        union {
        LittleEndianIEEEDouble bitVal;
        real_T fltVal;
        } tmpVal;

        tmpVal.fltVal = value;
        result = (boolean_T)((tmpVal.bitVal.words.wordH & 0x7FF00000) ==
                            0x7FF00000 &&
                            ( (tmpVal.bitVal.words.wordH & 0x000FFFFF) != 0 ||
                            (tmpVal.bitVal.words.wordL != 0) ));
    }
    return result;
}

// Test if single-precision value is not a number
boolean_T rtIsNaNF(real32_T value){
    IEEESingle tmp;
    tmp.wordL.wordLreal = value;
    return (boolean_T)( (tmp.wordL.wordLuint & 0x7F800000) == 0x7F800000 &&
                        (tmp.wordL.wordLuint & 0x007FFFFF) != 0 );
}

//
// Initialize rtInf needed by the generated code.
// Inf is initialized as non-signaling. Assumes IEEE.
//
real_T rtGetInf(void){
    size_t bitsPerReal = sizeof(real_T) * (NumBitsPerChar);
    real_T inf = 0.0;
    if (bitsPerReal == 32U) {
        inf = rtGetInfF();
    }else{
        union {
        LittleEndianIEEEDouble bitVal;
        real_T fltVal;
        } tmpVal;

        tmpVal.bitVal.words.wordH = 0x7FF00000U;
        tmpVal.bitVal.words.wordL = 0x00000000U;
        inf = tmpVal.fltVal;
    }
    return inf;
}

//
// Initialize rtInfF needed by the generated code.
// Inf is initialized as non-signaling. Assumes IEEE.
//
real32_T rtGetInfF(void){
    IEEESingle infF;
    infF.wordL.wordLuint = 0x7F800000U;
    return infF.wordL.wordLreal;
}

//
// Initialize rtMinusInf needed by the generated code.
// Inf is initialized as non-signaling. Assumes IEEE.
//
real_T rtGetMinusInf(void){
    size_t bitsPerReal = sizeof(real_T) * (NumBitsPerChar);
    real_T minf = 0.0;
    if (bitsPerReal == 32U) {
        minf = rtGetMinusInfF();
    }else{
        union{
        LittleEndianIEEEDouble bitVal;
        real_T fltVal;
        } tmpVal;
        tmpVal.bitVal.words.wordH = 0xFFF00000U;
        tmpVal.bitVal.words.wordL = 0x00000000U;
        minf = tmpVal.fltVal;
    }
    return minf;
}

//
// Initialize rtMinusInfF needed by the generated code.
// Inf is initialized as non-signaling. Assumes IEEE.
//
real32_T rtGetMinusInfF(void){
    IEEESingle minfF;
    minfF.wordL.wordLuint = 0xFF800000U;
    return minfF.wordL.wordLreal;
}


real_T rt_remd_snf(real_T u0, real_T u1){
    real_T u1_0;
    real_T y;
    if(rtIsNaN(u0) || rtIsNaN(u1) || rtIsInf(u0)){
        y = (rtGetNaN());
    }else if(rtIsInf(u1)){
        y = u0;
    }else{
        if(u1 < 0.0){
            u1_0 = std::ceil(u1);
        }else{
            u1_0 = std::floor(u1);
        }

        if((u1 != 0.0) && (u1 != u1_0)){
            u1_0 = std::abs(u0 / u1);
            if(!(std::abs(u1_0 - std::floor(u1_0 + 0.5)) > DBL_EPSILON * u1_0)) {
                y = 0.0 * u0;
            }else{
                y = std::fmod(u0, u1);
            }
        }else{
            y = std::fmod(u0, u1);
        }
    }
    return y;
}