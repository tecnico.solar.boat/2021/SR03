/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

//
// File: foil_controller.cpp
//
// Code generated for Simulink model 'foil_controller'.
//
// Model version                  : 1.22
// Simulink Coder version         : 9.4 (R2020b) 29-Jul-2020
// C/C++ source code generated on : Thu Apr 15 13:58:47 2021
//
// Target selection: ert.tlc
// Embedded hardware selection: NXP->Cortex-M4
// Code generation objectives:
//    1. Execution efficiency
//    2. RAM efficiency
// Validation result: Not run
//
#include "Controller.h"

// Function for MATLAB Function: '<Root>/MATLAB Function1'
void Controller::cosd(real_T *x)
{
  real_T absx;
  real_T b_x;
  int8_T n;
  if (rtIsInf(*x) || rtIsNaN(*x)) {
    *x = (rtGetNaN());
  } else {
    b_x = rt_remd_snf(*x, 360.0);
    absx = std::abs(b_x);
    if (absx > 180.0) {
      if (b_x > 0.0) {
        b_x -= 360.0;
      } else {
        b_x += 360.0;
      }

      absx = std::abs(b_x);
    }

    if (absx <= 45.0) {
      b_x *= 0.017453292519943295;
      n = 0;
    } else if (absx <= 135.0) {
      if (b_x > 0.0) {
        b_x = (b_x - 90.0) * 0.017453292519943295;
        n = 1;
      } else {
        b_x = (b_x + 90.0) * 0.017453292519943295;
        n = -1;
      }
    } else if (b_x > 0.0) {
      b_x = (b_x - 180.0) * 0.017453292519943295;
      n = 2;
    } else {
      b_x = (b_x + 180.0) * 0.017453292519943295;
      n = -2;
    }

    switch (n) {
     case 0:
      *x = std::cos(b_x);
      break;

     case 1:
      *x = -std::sin(b_x);
      break;

     case -1:
      *x = std::sin(b_x);
      break;

     default:
      *x = -std::cos(b_x);
      break;
    }
  }
}

// Function for MATLAB Function: '<S3>/MATLAB Function'
void Controller::sind(real_T *x)
{
  real_T absx;
  real_T c_x;
  int8_T n;
  if (rtIsInf(*x) || rtIsNaN(*x)) {
    *x = (rtGetNaN());
  } else {
    c_x = rt_remd_snf(*x, 360.0);
    absx = std::abs(c_x);
    if (absx > 180.0) {
      if (c_x > 0.0) {
        c_x -= 360.0;
      } else {
        c_x += 360.0;
      }

      absx = std::abs(c_x);
    }

    if (absx <= 45.0) {
      c_x *= 0.017453292519943295;
      n = 0;
    } else if (absx <= 135.0) {
      if (c_x > 0.0) {
        c_x = (c_x - 90.0) * 0.017453292519943295;
        n = 1;
      } else {
        c_x = (c_x + 90.0) * 0.017453292519943295;
        n = -1;
      }
    } else if (c_x > 0.0) {
      c_x = (c_x - 180.0) * 0.017453292519943295;
      n = 2;
    } else {
      c_x = (c_x + 180.0) * 0.017453292519943295;
      n = -2;
    }

    switch (n) {
     case 0:
      *x = std::sin(c_x);
      break;

     case 1:
      *x = std::cos(c_x);
      break;

     case -1:
      *x = -std::cos(c_x);
      break;

     default:
      *x = -std::sin(c_x);
      break;
    }
  }
}

// Model step function
void Controller::step()
{
  real_T absx_0[9];
  real_T rtb_TmpSignalConversionAtSFun_e[5];
  real_T rtb_TmpSignalConversionAtSFun_n[3];
  real_T ab;
  real_T absx;
  real_T bb;
  real_T cb;
  real_T d;
  real_T db;
  real_T e;
  real_T eb;
  real_T f;
  real_T g;
  real_T h;
  real_T i;
  real_T j;
  real_T k;
  real_T l;
  real_T m;
  real_T n_0;
  real_T o;
  real_T p;
  real_T q;
  real_T r;
  real_T rtb_TSamp;
  real_T rtb_z_cm;
  real_T s;
  real_T t;
  real_T u;
  real_T v;
  real_T w;
  real_T y;
  int32_T i_0;
  int8_T n;

  // Outputs for Atomic SubSystem: '<Root>/Subsystem'
  // MATLAB Function: '<S3>/MATLAB Function' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/Roll'
  //   Inport: '<Root>/Vx'
  //   Inport: '<Root>/Vy'
  //   Inport: '<Root>/Vz'
  //   Inport: '<Root>/Yaw'

  absx = rtU.Pitch;
  cosd(&absx);
  rtb_TSamp = rtU.Yaw;
  cosd(&rtb_TSamp);
  d = rtU.Pitch;
  cosd(&d);
  e = rtU.Yaw;
  sind(&e);
  f = rtU.Pitch;
  sind(&f);
  g = rtU.Roll;
  sind(&g);
  h = rtU.Pitch;
  sind(&h);
  i = rtU.Yaw;
  cosd(&i);
  j = rtU.Roll;
  cosd(&j);
  k = rtU.Yaw;
  sind(&k);
  l = rtU.Roll;
  sind(&l);
  m = rtU.Pitch;
  sind(&m);
  n_0 = rtU.Yaw;
  sind(&n_0);
  o = rtU.Roll;
  cosd(&o);
  p = rtU.Yaw;
  cosd(&p);
  q = rtU.Roll;
  sind(&q);
  r = rtU.Pitch;
  cosd(&r);
  s = rtU.Roll;
  cosd(&s);
  t = rtU.Pitch;
  sind(&t);
  u = rtU.Yaw;
  cosd(&u);
  v = rtU.Roll;
  sind(&v);
  w = rtU.Yaw;
  sind(&w);
  rtb_z_cm = rtU.Roll;
  cosd(&rtb_z_cm);
  y = rtU.Pitch;
  sind(&y);
  ab = rtU.Yaw;
  sind(&ab);
  bb = rtU.Roll;
  sind(&bb);
  cb = rtU.Yaw;
  cosd(&cb);
  db = rtU.Roll;
  cosd(&db);
  eb = rtU.Pitch;
  cosd(&eb);
  absx_0[0] = absx * rtb_TSamp;
  absx_0[3] = d * e;
  absx_0[6] = -f;
  absx_0[1] = g * h * i - j * k;
  absx_0[4] = l * m * n_0 + o * p;
  absx_0[7] = q * r;
  absx_0[2] = s * t * u + v * w;
  absx_0[5] = rtb_z_cm * y * ab - bb * cb;
  absx_0[8] = db * eb;
  for (i_0 = 0; i_0 < 3; i_0++) {
    rtb_TmpSignalConversionAtSFun_n[i_0] = absx_0[i_0 + 6] * rtU.Vz +
      (absx_0[i_0 + 3] * rtU.Vy + absx_0[i_0] * rtU.Vx);
  }

  // End of MATLAB Function: '<S3>/MATLAB Function'
  // End of Outputs for SubSystem: '<Root>/Subsystem'

  // MATLAB Function: '<Root>/MATLAB Function1' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/Roll'
  //   Inport: '<Root>/heave'

  if (rtIsInf(rtU.Pitch) || rtIsNaN(rtU.Pitch)) {
    rtb_z_cm = (rtGetNaN());
  } else {
    rtb_z_cm = rt_remd_snf(rtU.Pitch, 360.0);
    absx = std::abs(rtb_z_cm);
    if (absx > 180.0) {
      if (rtb_z_cm > 0.0) {
        rtb_z_cm -= 360.0;
      } else {
        rtb_z_cm += 360.0;
      }

      absx = std::abs(rtb_z_cm);
    }

    if (absx <= 45.0) {
      rtb_z_cm *= 0.017453292519943295;
      n = 0;
    } else if (absx <= 135.0) {
      if (rtb_z_cm > 0.0) {
        rtb_z_cm = (rtb_z_cm - 90.0) * 0.017453292519943295;
        n = 1;
      } else {
        rtb_z_cm = (rtb_z_cm + 90.0) * 0.017453292519943295;
        n = -1;
      }
    } else if (rtb_z_cm > 0.0) {
      rtb_z_cm = (rtb_z_cm - 180.0) * 0.017453292519943295;
      n = 2;
    } else {
      rtb_z_cm = (rtb_z_cm + 180.0) * 0.017453292519943295;
      n = -2;
    }

    rtb_z_cm = std::tan(rtb_z_cm);
    if ((n == 1) || (n == -1)) {
      absx = 1.0 / rtb_z_cm;
      rtb_z_cm = -(1.0 / rtb_z_cm);
      if (rtIsInf(rtb_z_cm) && (n == 1)) {
        rtb_z_cm = absx;
      }
    }
  }

  absx = rtU.Pitch;
  cosd(&absx);
  rtb_TSamp = rtU.Roll;
  cosd(&rtb_TSamp);
  rtb_z_cm = (rtU.heave * absx + rtb_z_cm * 3.15) / rtb_TSamp;

  // End of MATLAB Function: '<Root>/MATLAB Function1'

  // SignalConversion generated from: '<Root>/Gain1' incorporates:
  //   Inport: '<Root>/Pitch'
  //   Inport: '<Root>/q'

  rtb_TmpSignalConversionAtSFun_e[0] = rtb_TmpSignalConversionAtSFun_n[0];
  rtb_TmpSignalConversionAtSFun_e[1] = rtb_TmpSignalConversionAtSFun_n[2];
  rtb_TmpSignalConversionAtSFun_e[2] = rtU.q;
  rtb_TmpSignalConversionAtSFun_e[3] = rtU.Pitch;
  rtb_TmpSignalConversionAtSFun_e[4] = rtb_z_cm;

  // Gain: '<Root>/Gain1'
  absx = 0.0;
  for (i_0 = 0; i_0 < 5; i_0++) {
    absx += rtConstP_controller.Gain1_Gain[i_0] * rtb_TmpSignalConversionAtSFun_e[i_0];
  }

  // Outputs for Atomic SubSystem: '<Root>/Discrete derivative1'
  // SampleTimeMath: '<S4>/TSamp' incorporates:
  //   Gain: '<Root>/Gain1'
  //
  //  About '<S4>/TSamp':
  //   y = u * K where K = 1 / ( w * Ts )

  rtb_TSamp = absx * 50.0;

  // Step: '<S1>/Step'
  if ((((&rtM)->Timing.clockTick0) * 0.02) < 0.01) {
    // Switch: '<S1>/Switch' incorporates:
    //   Constant: '<S1>/Constant'

    absx = 0.0;
  } else {
    // Switch: '<S1>/Switch' incorporates:
    //   Sum: '<S4>/Diff'
    //   UnitDelay: '<S4>/UD'
    //
    //  Block description for '<S4>/Diff':
    //
    //   Add in CPU
    //
    //  Block description for '<S4>/UD':
    //
    //   Store in Global RAM

    absx = rtb_TSamp - rtDW.UD_DSTATE;
  }

  // End of Step: '<S1>/Step'

  // Update for UnitDelay: '<S4>/UD'
  //
  //  Block description for '<S4>/UD':
  //
  //   Store in Global RAM

  rtDW.UD_DSTATE = rtb_TSamp;

  // End of Outputs for SubSystem: '<Root>/Discrete derivative1'

  // Saturate: '<Root>/Saturation1' incorporates:
  //   DiscreteIntegrator: '<Root>/Discrete-Time Integrator1'

  if (rtDW.DiscreteTimeIntegrator1_DSTATE > 11.0) {
    rtb_TSamp = 11.0;
  } else if (rtDW.DiscreteTimeIntegrator1_DSTATE < 0) {
    rtb_TSamp = 0;
  } else {
    rtb_TSamp = rtDW.DiscreteTimeIntegrator1_DSTATE;
  }

  // End of Saturate: '<Root>/Saturation1'

  // Outport: '<Root>/com_mode'
  rtY.com_mode = rtb_TSamp;

  // Update for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' incorporates:
  //   Gain: '<Root>/Gain'
  //   Inport: '<Root>/heave_ref'
  //   Sum: '<Root>/Sum1'
  //   Sum: '<Root>/Sum2'
  //   Sum: '<Root>/Sum4'

  rtDW.DiscreteTimeIntegrator1_DSTATE += (((0.0 -
    (rtDW.DiscreteTimeIntegrator1_DSTATE - rtb_TSamp)) - (rtU.heave_ref -
    rtb_z_cm) * 2.74) - absx) * 0.02; //Kih(6) erro

  // Update absolute time for base rate
  // The "clockTick0" counts the number of times the code of this task has
  //  been executed. The resolution of this integer timer is 0.02, which is the step size
  //  of the task. Size of "clockTick0" ensures timer will not overflow during the
  //  application lifespan selected.

  (&rtM)->Timing.clockTick0++;
  rtY.rear_foil = 0;
}

// Model initialize function
void Controller::initialize()
{
  // Registration code

  // initialize non-finites
  rt_InitInfAndNaN(sizeof(real_T));

  // InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' 
  rtDW.DiscreteTimeIntegrator1_DSTATE = 0;
}

void Controller::reset()
{
  (&rtM)->Timing.clockTick0 = 0; //reset timer
  // InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' 
  rtDW.DiscreteTimeIntegrator1_DSTATE = 0;
  rtY.com_mode = 0;
}

// Constructor
Controller::Controller() :
  rtU(),
  rtY(),
  rtDW(),
  rtM()
{
  // Currently there is no constructor body generated.
}

// Destructor
Controller::~Controller()
{
  // Currently there is no destructor body generated.
}

// Real-Time Model get method
Controller::RT_MODEL * Controller::getRTM()
{
  return (&rtM);
}

//
// File trailer for generated code.
//
// [EOF]
//
