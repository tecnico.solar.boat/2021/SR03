/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef _Xsens_h
#define _Xsens_h

#include <Arduino.h>
#include "buffer.h"
#include "../../include/config.h"

#define Preamble 0xFA
#define BID 0xFF
#define MTData2 0x36

enum XDI{UtcTime = 0x1010, EulerAngles = 0x2030, Acceleration = 0x4020, 
RateOfTurn = 0x8020, LatLon = 0x5040, VelocityXYZ = 0xD010, FreeAccXYZ = 0x4030, 
MagneticField = 0xC020, Temperature = 0x810, GnssSatInfo = 0x7020};

class XsensAHRS
{
    public:
		uint32_t ns;
		float yaw;		// Float
		float pitch;	// Float
		float roll;		// Float
		float accX;		// Float
		float accY;		// Float
		float accZ;		// Float
		float gyrX;		// Float
		float gyrY;		// Float
		float gyrZ;		// Float
		float lat;		// Float
		float lon;		// Float
		float velX;		// Float
		float velY;		// Float
		float velZ;		// Float
		float vel;		// Float
		uint8_t fixType;
		float free_accX;		// Float
		float free_accY;		// Float
		float free_accZ;		// Float
		float magX;
		float magY;
		float magZ;
		float temperature;
		uint8_t numStats;
		
		int readXsensMsg();
		void parseXsensMsg( byte *msg, int size);
		void printXsens();

		String createAHRSdata();
		void init_Xsensmessages();
		void send_Xsensmessages();
		void inclination_reset();
		void alignment_reset();

    private:
};

#endif