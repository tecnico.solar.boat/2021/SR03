/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "Xsens.h"

#define PACKET_START1 	0xFA
#define PACKET_START2 	0xFF
#define MTDATA			0x36

unsigned long timeout_xsens=0;

int XsensAHRS::readXsensMsg(){
	byte inByte = 0x00;
	int size = 0, index = 0, checksum=0;
	bool receiving = false, msg_begin = false, msg_got_mtdata = false, msg_good = false;
	XsensSerial.clear();
	timeout_xsens=millis();
	while(!msg_good){
		if(XsensSerial.available()){
			// Serial.println("Xsens Available");
			inByte = XsensSerial.read();
			if(receiving && msg_begin && msg_got_mtdata){
				size = inByte;
				msg_good = true;
				break;
			}
			else if((inByte == MTDATA) && receiving && msg_begin){
				msg_got_mtdata = true;
			}
			else if((inByte == PACKET_START2) && receiving){
				msg_begin = true;
			}
			else if(inByte == PACKET_START1 && !receiving){
				receiving = true;
			}
		}
		if(millis() - timeout_xsens > XSENS_TIMEOUT){
			// Serial.println("Xsens TimeOut");
			return -1;
		} 
	}

	if(msg_good){
		byte msg[size];
		checksum += 0xFF + 0x36 + size;
		while(index < size + 1){
			if(XsensSerial.available()){
				inByte = XsensSerial.read();
				msg[index++] = inByte;
				checksum += inByte;
			}
		}

		if ( ( checksum & 0xF ) == 0 ) {
			parseXsensMsg(msg, size);
			return 0;
		}
		else{
			// Serial.println("Xsens Failed Checksum");
			return -1;
		}
	}
	// Serial.println("Xsens Message not good");
	return -1;
}

void XsensAHRS::parseXsensMsg( byte *msg, int size){
	int i = 0;
	int32_t ind=0;

	// Serial.println(size);
	// for(uint8_t k = 0; k < size +1; k++)
	// {
	// 	Serial.print(msg[k], HEX);
	// 	Serial.print(" ");
	// }
	// Serial.println();
	// Serial.println();

	while (i < size){
		uint16_t dataID = (msg[i] << 8) + msg[i+1];
		// Serial.println(i);
		switch (dataID){
			case (uint16_t)XDI::UtcTime:
				ind=i+3;
				ns = buffer_get_uint32(msg,&ind);
				
				// AHRSHex.ns = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];
				// this->ns = AHRSHex.ns;

				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::EulerAngles:
				ind=i+3;
				roll = buffer_get_float32_auto(msg,&ind);
				pitch = buffer_get_float32_auto(msg,&ind);
				yaw = buffer_get_float32_auto(msg,&ind);

				// AHRSHex.roll = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];
				// AHRSHex.pitch = (msg[i+7] << 24) + (msg[i+8] << 16) + (msg[i+9] << 8) + msg[i+10];
				// AHRSHex.yaw = (msg[i+11] << 24) + (msg[i+12] << 16) + (msg[i+13] << 8) + msg[i+14];
				// this->roll = (float)AHRSHex.roll;
				// this->pitch = (float)AHRSHex.pitch;
				// this->yaw = (float)AHRSHex.yaw;

				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::Acceleration:
				ind=i+3;
				accX=buffer_get_float32_auto(msg,&ind);
				accY=buffer_get_float32_auto(msg,&ind);
				accZ=buffer_get_float32_auto(msg,&ind);

				// AHRSHex.accX = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];
				// AHRSHex.accY = (msg[i+7] << 24) + (msg[i+8] << 16) + (msg[i+9] << 8) + msg[i+10];
				// AHRSHex.accZ = (msg[i+11] << 24) + (msg[i+12] << 16) + (msg[i+13] << 8) + msg[i+14];
				// this->accX = *(float*)& AHRSHex.accX;
				// this->accY = *(float*)& AHRSHex.accY;
				// this->accZ = *(float*)& AHRSHex.accZ;

				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::RateOfTurn:
				ind=i+3;
				gyrX=buffer_get_float32_auto(msg,&ind);
				gyrY=buffer_get_float32_auto(msg,&ind);
				gyrZ=buffer_get_float32_auto(msg,&ind);
				// AHRSHex.gyrX = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];
				// AHRSHex.gyrY = (msg[i+7] << 24) + (msg[i+8] << 16) + (msg[i+9] << 8) + msg[i+10];
				// AHRSHex.gyrZ = (msg[i+11] << 24) + (msg[i+12] << 16) + (msg[i+13] << 8) + msg[i+14];
				// this->gyrX = *(float*)& AHRSHex.gyrX;
				// this->gyrY = *(float*)& AHRSHex.gyrY;
				// this->gyrZ = *(float*)& AHRSHex.gyrZ;

				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::LatLon:
				ind=i+3;
				lat=buffer_get_float32_auto(msg,&ind);
				lon=buffer_get_float32_auto(msg,&ind);

				// AHRSHex.lat = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];
				// AHRSHex.lon = (msg[i+7] << 24) + (msg[i+8] << 16) + (msg[i+9] << 8) + msg[i+10];
				// this->lat = *(float*)& AHRSHex.lat;
				// this->lon = *(float*)& AHRSHex.lon;

				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::VelocityXYZ:
				ind=i+3;
				velX=buffer_get_float32_auto(msg,&ind);
				velY=buffer_get_float32_auto(msg,&ind);
				velZ=buffer_get_float32_auto(msg,&ind);
				vel = sqrt( pow(velX,2) + pow(velY,2) + pow(velZ,2) ); // Computes actual velocity in m/s
				
				// AHRSHex.velX = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];
				// AHRSHex.velY = (msg[i+7] << 24) + (msg[i+8] << 16) + (msg[i+9] << 8) + msg[i+10];
				// AHRSHex.velZ = (msg[i+11] << 24) + (msg[i+12] << 16) + (msg[i+13] << 8) + msg[i+14];
				// this->velX = *(float*)& AHRSHex.velX;
				// this->velY = *(float*)& AHRSHex.velY;
				// this->velZ = *(float*)& AHRSHex.velZ;
		
				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::FreeAccXYZ:
				ind=i+3;
				free_accX=buffer_get_float32_auto(msg,&ind);
				free_accY=buffer_get_float32_auto(msg,&ind);
				free_accZ=buffer_get_float32_auto(msg,&ind);
				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::MagneticField:
				ind=i+3;
				magX=buffer_get_float32_auto(msg,&ind);
				magY=buffer_get_float32_auto(msg,&ind);
				magZ=buffer_get_float32_auto(msg,&ind);
				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::Temperature:
				ind=i+3;
				temperature=buffer_get_float32_auto(msg,&ind);
				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::GnssSatInfo:
				ind=i+3+4; // ignore itow
				numStats = buffer_get_uint8(msg,&ind);
				i = i + msg[i+2] + 3;
				break;
			default:
				Serial.println("Default Xsens.cpp parseXsensMsg()"); // Mandar para o cartao SD
				Serial.println(dataID, HEX);
				// for(uint8_t k = 0; k < 60; k++)
				// {
				// 	Serial.print(msg[k], HEX);
				// 	Serial.print(" ");
				// }
				// Serial.println();
				// Serial.println();

				i = i + msg[i+2] + 3;
				break;
		}
	}
}

void XsensAHRS::inclination_reset(){
	XsensSerial.write(0xFA);
	XsensSerial.write(0xFF);
	XsensSerial.write(0xA4);
	XsensSerial.write(0x02);
	XsensSerial.write(0x00);
	XsensSerial.write(0x03);
	XsensSerial.write(0x58);
}

void XsensAHRS::alignment_reset(){
	XsensSerial.write(0xFA);
	XsensSerial.write(0xFF);
	XsensSerial.write(0xA4);
	XsensSerial.write(0x02);
	XsensSerial.write(0x00);
	XsensSerial.write(0x04);
	XsensSerial.write(0x57);
}

void XsensAHRS::printXsens(){
	Serial.println();
	Serial.println(" AHRS output:");
	Serial.print("Time (ns): "); Serial.println(this->ns);
	Serial.print("Yaw: "); Serial.println(this->yaw, 10);
	Serial.print("Pitch: "); Serial.println(this->pitch, 10);
	Serial.print("Roll: "); Serial.println(this->roll, 10);
	Serial.print("AccX: "); Serial.println(this->accX, 10);
	Serial.print("AccY: "); Serial.println(this->accY, 10);
	Serial.print("AccZ: "); Serial.println(this->accZ, 10);
	Serial.print("FreeAccX: "); Serial.println(this->free_accX, 10);
	Serial.print("FreeAccY: "); Serial.println(this->free_accY, 10);
	Serial.print("FreeAccZ: "); Serial.println(this->free_accZ, 10);
	Serial.print("GyroX: "); Serial.println(this->gyrX, 10);
	Serial.print("GyroY: "); Serial.println(this->gyrY, 10);
	Serial.print("GyroZ: "); Serial.println(this->gyrZ, 10);
	Serial.print("Lat: "); Serial.println(this->lat, 10);
	Serial.print("Lon: "); Serial.println(this->lon, 10);
	Serial.print("VelX: "); Serial.println(this->velX, 10);
	Serial.print("VelY: "); Serial.println(this->velY, 10);
	Serial.print("VelZ: "); Serial.println(this->velZ, 10);
	Serial.print("Temp: "); Serial.println(this->temperature, 10);
	Serial.print("Num Stat: "); Serial.println(this->numStats);
	Serial.println();
}

String XsensAHRS::createAHRSdata(){
	String Data;
	Data=String(this->yaw, 7)+";"+String( - this->pitch, 7)+";"+String(this->roll, 7)+";"
		+String(this->accX, 7)+";"+String(this->accY, 7)+";"+String(this->accZ, 7)+";"+String(this->gyrX, 7)+";"
		+String(this->gyrY, 7)+";"+String(this->gyrZ, 7)+";"+String(this->lat, 10)+";"+String(this->lon, 10)+";"
		+String(this->velX, 7)+";"+String(this->velY, 7)+";"+String(this->velZ, 7)+";"+String(this->vel, 7);
	return Data;
}
