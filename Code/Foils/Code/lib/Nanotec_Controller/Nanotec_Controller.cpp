/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "Nanotec_Controller.h"

/**
 * NemasComms::NemasComms
 * 
 * Constructor for communication object
 * 
 */
NemasComms::NemasComms() {
}

void NemasComms::addController(uint8_t controller_id){
    this->controllers[controller_id].id = controller_id;
    this->controllers[controller_id].boot_up=false;
}

/**
 * NemasComms 
 * 
 * Function to send a message
 * 
 * @param  {CAN_message_t} message : message to be sent
 */
void NemasComms::SendMessage(CAN_message_t &message){
    if(CAN_SEQUENTIAL_WRITE) message.seq=1;
    Can.write(message);
    return;
}

/**
 * NemasComms 
 * 
 * Function to Receive a message
 * 
 * @param  {CAN_message_t} frame : message received
 */
void NemasComms::RecvMessage(CAN_message_t &frame){
    switch(frame.id & CANOPEN_MESSAGE_ID_MASK){
        case CANOPEN_BOOTUP_ID:
            if(frame.len==0) process_BootupMessage(frame);
            else process_HeartBeatMessage(frame);
            break;
        case CANOPEN_SDO_ID:
            process_SDOMessage(frame);
            break;
        case TX_PDO_MAPPING1_ID:
            process_TXPDO1Message(frame);
            break;
        case TX_PDO_MAPPING2_ID:
            process_TXPDO2Message(frame);
            break;
        case CANOPEN_EMCY_ID:
            process_EMCYMessage(frame);
            break;
        default:
            break;
    }
}

/**
 * NemasComms 
 * 
 *  Frame Handler
 * 
 * @param  {CAN_message_t} frame : 
 * @param  {int} mailbox         : 
 * @param  {uint8_t} controller  : 
 * @return {bool}                : 
 */
bool NemasComms::frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller){
    RecvMessage(frame);
    return true;
}

void NemasComms::process_BootupMessage(CAN_message_t &frame){
    uint8_t index = (frame.id & CANOPEN_CONTROLLER_ID_MASK);
    controllers[index].boot_up=true;
}

void NemasComms::process_HeartBeatMessage(CAN_message_t &frame){
    uint8_t index = (frame.id & CANOPEN_CONTROLLER_ID_MASK);
    controllers[index].boot_up=true;
    controllers[index].nmt_state=frame.buf[0];
}

void NemasComms::process_SDOMessage(CAN_message_t &frame){
    uint8_t index = (frame.id & CANOPEN_CONTROLLER_ID_MASK);
    if(controllers[index].sdorequest.id == frame.id 
        && (controllers[index].sdorequest.index & 0xFF) == frame.buf[1] 
        && (controllers[index].sdorequest.index >> 8) == frame.buf[2] 
        && controllers[index].sdorequest.subindex == frame.buf[3]){
            controllers[index].sdorequest.frame=frame;
            controllers[index].sdorequest.received=true;
    }
}

void NemasComms::process_TXPDO1Message(CAN_message_t &frame){
    uint8_t index = (frame.id & CANOPEN_CONTROLLER_ID_MASK);
    controllers[index].statusword = ((uint16_t) frame.buf[1]) << 8 | ((uint16_t) frame.buf[0]);
    controllers[index].mode = frame.buf[2];
}

void NemasComms::process_TXPDO2Message(CAN_message_t &frame){
    uint8_t index = (frame.id & CANOPEN_CONTROLLER_ID_MASK);
    controllers[index].position_actual_value =  ((uint32_t) frame.buf[3]) << 24 | ((uint32_t) frame.buf[2]) << 16 | ((uint32_t) frame.buf[1]) << 8 | ((uint32_t) frame.buf[0]);
}

void NemasComms::process_EMCYMessage(CAN_message_t &frame){
}


/**
 * NemasController::NemasController 
 * 
 * Constructor for controller object
 * 
 * @param  {uint8_t} nemas_id       : id of the controller
 * @param  {NemasComms*} comms_node : communication node
 */
NemasController::NemasController(uint8_t nemas_id,NemasComms* comms_node){
    this->node = comms_node;
    this->id = nemas_id;
    homing_offset = 0;
}

/**
 * NemasController 
 * 
 * Function to write NMT state
 * 
 * @param  {uint8_t} state : controller desired NMT state
 */
void NemasController::WriteNMT(uint8_t state){
    //can message
    CAN_message_t msg;

    //8 bytes frame
    msg.flags.extended=0;

    //Data Length 8 bytes
    msg.len = 2;

    //CAN ID
    msg.id = CANOPEN_NMT_ID;

    //CMD
    msg.buf[0] = state;

    //Node ID
    msg.buf[1] = id;

    //Send frame
    node->SendMessage(msg);
}

/**
 * NemasController 
 * 
 * Function to write NMT state to all controllers (Broadcast)
 * 
 * @param  {uint8_t} state : controller desired NMT state
 */
void NemasController::WriteNMT_Broadcast(uint8_t state){
    //can message
    CAN_message_t msg;
    //8 bytes frame
    msg.flags.extended=0;

    //Data Length 8 bytes
    msg.len = 2;

    //CAN ID
    msg.id = CANOPEN_NMT_ID;

    //CMD
    msg.buf[0] = state;

    //Node ID
    msg.buf[1] = 0x00;

    //Send frame
    node->SendMessage(msg);
}

/**
 * NemasController 
 * 
 * Function to write a SYNC message
 * 
 */
void NemasController::WriteSYNC(){
    CAN_message_t msg;
    //8 bytes frame
    msg.flags.extended=0;

    //Data Length 8 bytes
    msg.len = 0;

    //CAN ID
    msg.id = CANOPEN_SYNC_ID + id;

    //Send frame
    node->SendMessage(msg);
}

/**
 * NemasController 
 * 
 * Function to write SDO requests
 * 
 * @param  {uint16_t} index    : sdo index
 * @param  {uint8_t} subindex  : sdo subindex
 * @param  {uint8_t} data_size : sdo data size
 * @param  {int32_t} data      : sdo data to write
 * @return {bool}              : sdo request success info
 */
bool NemasController::WriteSDO(uint16_t index, uint8_t subindex, uint8_t data_size, int32_t data){
    CAN_message_t msg;
    //8 bytes frame
    msg.flags.extended=0;

    //Data Length 8 bytes
    msg.len = 8;

    //CAN ID
    msg.id = CANOPEN_SDO_DOWNLOAD_REQUEST + id;

    //CMD Byte0
    switch(data_size){
        case CANOPEN_MSG_SIZE_8_BITS : msg.buf[0] = CANOPEN_SDO_CMD_1_BYTE; break;
        case CANOPEN_MSG_SIZE_16_BITS : msg.buf[0] = CANOPEN_SDO_CMD_2_BYTE; break;
        case CANOPEN_MSG_SIZE_24_BITS: msg.buf[0] = CANOPEN_SDO_CMD_3_BYTE; break;
        case CANOPEN_MSG_SIZE_32_BITS : msg.buf[0] = CANOPEN_SDO_CMD_4_BYTE; break;
        default: return false;
    }

    //Index Byte1 >> Index LSB, Byte2 >> Index MSB
    msg.buf[1] = index & 0xFF;
    msg.buf[2] = index >> 8;

    //Sub-Index Byte 3
    msg.buf[3] = subindex;

    //Data Byte4 >> Data LSB, Byte7 >> Data MSB
    msg.buf[4] = data & 0xFF;
    msg.buf[5] = (data >> 8) & 0xFF;
    msg.buf[6] = (data >> 16) & 0xFF;
    msg.buf[7] = (data >> 24) & 0xFF;

    node->controllers[id].sdorequest.id=CANOPEN_SDO_DOWNLOAD_REPLY+id;
    node->controllers[id].sdorequest.index=index;
    
    node->controllers[id].sdorequest.subindex=subindex;

	//Send frame
    node->SendMessage(msg);

    //wait for response
    unsigned long send_time = millis();
    while(!node->controllers[id].sdorequest.received){
        if(millis()-send_time > CANOPEN_SDO_REPLY_TIMEOUT){
            Log.error("Controller ID: %d, Timeout Write SDO!",id);
            return false;
        }
    }

    //received
    node->controllers[id].sdorequest.received=false;
    msg=node->controllers[id].sdorequest.frame;

    //check for errors
    switch(msg.buf[0]){
        case CANOPEN_SDO_REPLY_OK: return true;
        case CANOPEN_SDO_REPLY_ERROR:
            Log.error("Controller ID: %d, SDO WRITE ERROR: %d | %d | %d | %d ",id,msg.buf[7],msg.buf[6],msg.buf[5],msg.buf[4]);
            return false;
        default: return false;
    }
}

/**
 * NemasController 
 * 
 * Function to read SDO requests
 * 
 * @param  {u_int16_t} index   : sdo index
 * @param  {u_int8_t} subindex : sdo subindex
 * @param  {u_int8_t*} data    : sdo data read
 * @return {bool}              : sdo request success info
 */
bool NemasController::ReadSDO(u_int16_t index, u_int8_t subindex, u_int8_t* data){
    CAN_message_t msg;
    //8 bytes frame
    msg.flags.extended=0;

    //Data Length 8 bytes
    msg.len = 8;

    //CAN ID
    msg.id = CANOPEN_SDO_UPLOAD_REQUEST + id;

    //CMD Byte0 >> 0x40
    msg.buf[0] =  0x40;

    //Index Byte1 >> Index LSB, Byte2 >> Index MSB
    msg.buf[1] = index & 0xFF;
    msg.buf[2] = index >> 8;

    //Sub-Index Byte 3
    msg.buf[3] = subindex;

    //Data Byte4-Byte7 >> 0x00
    msg.buf[4] = 0x00;
    msg.buf[5] = 0x00;
    msg.buf[6] = 0x00;
    msg.buf[7] = 0x00;

    //post a new request
    node->controllers[id].sdorequest.id=CANOPEN_SDO_UPLOAD_REPLY+id;
    node->controllers[id].sdorequest.index=index;
    node->controllers[id].sdorequest.subindex=subindex;

	//Send frame
    node->SendMessage(msg);
    Log.trace("Controller ID: %d, SDO frame sent!",id);

    //wait for response
    unsigned long send_time = millis();
    while(!(node->controllers[id].sdorequest.received)){
        if(millis()-send_time > CANOPEN_SDO_REPLY_TIMEOUT){
            Log.error("Controller ID: %d, Timeout Read SDO!",id);
            return false;
        }
    }

    //received
    node->controllers[id].sdorequest.received=false;
    msg=node->controllers[id].sdorequest.frame;

    //check for errors
    switch(msg.buf[0]){
        case CANOPEN_SDO_REPLY_1_BYTE:
            data[0] = 0x00;
            data[1] = 0x00;
            data[2] = 0x00;
            data[3] = msg.buf[4];
            return true;
        case CANOPEN_SDO_REPLY_2_BYTE:
            data[0] = 0x00;
            data[1] = 0x00;
            data[2] = msg.buf[5];
            data[3] = msg.buf[4];
            return true;
        case CANOPEN_SDO_REPLY_3_BYTE:
            data[0] = 0x00;
            data[1] = msg.buf[6];
            data[2] = msg.buf[5];
            data[3] = msg.buf[4];
            return true;
        case CANOPEN_SDO_REPLY_4_BYTE:
            data[0] = msg.buf[7];
            data[1] = msg.buf[6];
            data[2] = msg.buf[5];
            data[3] = msg.buf[4];
            return true;
        case CANOPEN_SDO_REPLY_ERROR:
            Log.error("Controller ID: %d, SDO READ ERROR: %d | %d | %d | %d ",id,msg.buf[7],msg.buf[6],msg.buf[5],msg.buf[4]);
            return false;
        default: return false;
    }
}

/**
 * NemasController 
 * 
 * Function to write a PDO request
 * 
 * @param  {uint8_t} mapping : request mapping
 */
void NemasController::WritePDO(uint8_t mapping){
    //CAN message
    CAN_message_t msg;

    //8 bytes frame
    msg.flags.extended=0;

    //Data Length 8 bytes
    msg.len = 8;

    //CAN ID
    switch(mapping){
        case PDO_RX_MAPPING1:
            msg.len = 2;
            msg.id=CANOPEN_RX_PDO_MAPPING1 + id;
            //controlword
            msg.buf[0] = pdorxmapping.controlword & 0xFF;
            msg.buf[1] = (pdorxmapping.controlword >> 8) & 0xFF;
            break;
        case PDO_RX_MAPPING2:
            msg.len = 1;
            msg.id=CANOPEN_RX_PDO_MAPPING2 + id;
            //mode of operation
            msg.buf[0] = pdorxmapping.mode_operation & 0xFF;
            break;
        case PDO_RX_MAPPING3:
            msg.len = 6;
            msg.id=CANOPEN_RX_PDO_MAPPING3 + id;
            //controlword
            msg.buf[0] = pdorxmapping.controlword & 0xFF;
            msg.buf[1] = (pdorxmapping.controlword >> 8) & 0xFF;
            //target position
            msg.buf[2] = pdorxmapping.target_position & 0xFF;
            msg.buf[3] = (pdorxmapping.target_position >> 8) & 0xFF;
            msg.buf[4] = (pdorxmapping.target_position >> 16) & 0xFF;
            msg.buf[5] = (pdorxmapping.target_position >> 24) & 0xFF;
            break;
        default:
            return;
    }
    //Send frame
    node->SendMessage(msg);
    return;
}

bool NemasController::waitforBootUp(){
    elapsedMillis setup_timeout=0;
    while(!getBootUpState()){
		if(setup_timeout>BOOTUP_TIMEOUT){
			setup_timeout=0;
			Log.error("Controller ID: %d, Wait for Boot Up timeout!",id);
			return false;
		}
	}
    Log.trace("Controller ID: %d, Boot Up Ok!",id);
    return true;
}

bool NemasController::waitforNMTState(uint8_t state){
    elapsedMillis setup_timeout=0;
    while(getNmtState()!=state){
		if(setup_timeout>NMT_TIMEOUT){
			setup_timeout=0;
			Log.error("Controller ID: %d, Wait for NMT state timeout!",id);
			return false;
		}
	}
    Log.trace("Controller ID: %d, NMT State Ok!",id);
    return true;
}

bool NemasController::waitforfirstPDO(){
    elapsedMillis setup_timeout=0;
    while(getState_PDO()==0){
		if(setup_timeout>FIRST_PDO_TIMEOUT){
			setup_timeout=0;
			Log.error("Controller ID: %d, Wait for first PDO message timeout!",id);
			return false;
		}
	}
    Log.trace("Controller ID: %d, First PDO Message Ok!",id);
    return true;
}

void NemasController::resetfaultConditions_SDO(){
    WriteSDO(CANOPEN_CONTROL_WORD,0,CANOPEN_MSG_SIZE_16_BITS,CANOPEN_RESET_FAULT_CONDITIONS);
}

void NemasController::resetfaultConditions_PDO(){
    setPDOVariable_ControlWord(CANOPEN_RESET_FAULT_CONDITIONS);
    WritePDO(PDO_RX_MAPPING1);
}

bool NemasController::getBootUpState(){
    return node->controllers[id].boot_up;
}

uint8_t NemasController::getNmtState(){
    return node->controllers[id].nmt_state;
}

int32_t NemasController::getActualPosition_PDO(){
    return node->controllers[id].position_actual_value;
}

uint16_t NemasController::getStatusWord_PDO(){
    return node->controllers[id].statusword;
}

int8_t NemasController::getModeOperation_PDO(){
    return node->controllers[id].mode;
}

/**
 * NemasController 
 * 
 * Function that gets the controller actual position
 * 
 * @param  {int32_t} position : controller position
 * @return {bool}             : operation success info
 */
bool NemasController::getActualPosition_SDO(int32_t &position){
    uint8_t data[4];
    int32_t ind=0;
    if(ReadSDO(0x6064,0x00,data)){
        position = buffer_get_int32(data,&ind);
        return true;
    }
    return false;
}

/**
 * NemasController 
 * 
 * Function that gets the Iq current of the controller
 * 
 * @param  {int32_t} current : controller iq current
 * @return {bool}            : operation success info
 */
bool NemasController::getCurrentIq_SDO(int32_t &current){
    uint8_t data[4];
    int32_t ind=0;
    if(ReadSDO(0x2039,0x02,data)){
        current = buffer_get_int32(data,&ind);
        return true;
    }
    return false;
}

/**
 * NemasController 
 * 
 * Function that gets the current state of the controller
 * 
 * @param  {uint8_t} state : controller state
 * @return {bool}          : operation success info
 */
bool NemasController::getState_SDO(uint8_t &state){
    uint8_t data[4];
    int32_t ind=0;
    if(ReadSDO(CANOPEN_STATUS_WORD,0x00,data)){
        state=(buffer_get_int32(data,&ind) & STATE_MASK);
        return true;
    }
    return false;
}

uint8_t NemasController::getState_PDO(){
    return getStatusWord_PDO() & STATE_MASK;
}

bool NemasController::getTARG_BIT_PDO(){
    if((getStatusWord_PDO() & TARG_MASK)==TARG_MASK) return 1;
    else return 0;
}

bool NemasController::getSetpointAck_PDO(){
    if((getStatusWord_PDO() & SETPOINT_ACK_MASK)==SETPOINT_ACK_MASK) return 1;
    else return 0;
}

uint16_t NemasController::getHomingState_PDO(){
    return getStatusWord_PDO() & HOMING_MASK;
}






/**
 * NemasController 
 * 
 * Function that changes the controller state based on a the controllers state machine
 * 
 * @param  {uint8_t} state : controller desired state
 * @return {bool}               : state change success info
 */
bool NemasController::setState_SDO(uint8_t state){
    unsigned long start_time=0;
    uint8_t actual_state=0;
    uint8_t wait_state=0;
    if(getState_SDO(actual_state)) wait_state=actual_state;
    else return false;
    start_time=millis();
    while(!(millis()-start_time > SDO_RESPONSE_TIMEOUT)){
        if(getState_SDO(actual_state)){
            if(wait_state!=actual_state) continue;
            switch(actual_state){
                case CANOPEN_SWITCH_ON_DISABLED:
                    switch(state){
                        case CANOPEN_SWITCH_ON_DISABLED:
                            return true;
                            break;
                        case CANOPEN_READY_TO_SWITCH_ON:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_2)) return false;
                            wait_state=CANOPEN_READY_TO_SWITCH_ON;
                            break;
                        case CANOPEN_SWITCHED_ON:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_2)) return false;
                            wait_state=CANOPEN_READY_TO_SWITCH_ON;
                            break;
                        case CANOPEN_OPERATION_ENABLED:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_2)) return false;
                            wait_state=CANOPEN_READY_TO_SWITCH_ON;
                            break;
                        case CANOPEN_QUICK_STOP_ACTIVE:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_2)) return false;
                            state=CANOPEN_SWITCH_ON_DISABLED;
                            wait_state=CANOPEN_READY_TO_SWITCH_ON;
                            break;
                        default:
                            return false;
                            break;
                    }
                    break;
                case CANOPEN_READY_TO_SWITCH_ON:
                    switch(state){
                        case CANOPEN_SWITCH_ON_DISABLED:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_7)) return false;
                            wait_state=CANOPEN_SWITCH_ON_DISABLED;
                            break;
                        case CANOPEN_READY_TO_SWITCH_ON:
                            return true;
                            break;
                        case CANOPEN_SWITCHED_ON:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_3)) return false;
                            wait_state=CANOPEN_SWITCHED_ON;
                            break;
                        case CANOPEN_OPERATION_ENABLED:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_3)) return false;
                            wait_state=CANOPEN_SWITCHED_ON;
                            break;
                        case CANOPEN_QUICK_STOP_ACTIVE:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_3)) return false;
                            wait_state=CANOPEN_SWITCHED_ON;
                            break;
                        default:
                            return false;
                            break;
                    }
                    break;
                case CANOPEN_SWITCHED_ON:
                    switch(state){
                        case CANOPEN_SWITCH_ON_DISABLED:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_10)) return false;
                            wait_state=CANOPEN_SWITCH_ON_DISABLED;
                            break;
                        case CANOPEN_READY_TO_SWITCH_ON:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_6)) return false;
                            wait_state=CANOPEN_READY_TO_SWITCH_ON;
                            break;
                        case CANOPEN_SWITCHED_ON:
                            return true;
                            break;
                        case CANOPEN_OPERATION_ENABLED:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_4)) return false;
                            wait_state=CANOPEN_OPERATION_ENABLED;
                            break;
                        case CANOPEN_QUICK_STOP_ACTIVE:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_4)) return false;
                            wait_state=CANOPEN_OPERATION_ENABLED;
                            break;
                        default:
                            return false;
                            break;
                    }
                    break;
                case CANOPEN_OPERATION_ENABLED:
                    switch(state){
                        case CANOPEN_SWITCH_ON_DISABLED:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_9)) return false;
                            wait_state=CANOPEN_SWITCH_ON_DISABLED;
                            break;
                        case CANOPEN_READY_TO_SWITCH_ON:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_8)) return false;
                            wait_state=CANOPEN_READY_TO_SWITCH_ON;
                            break;
                        case CANOPEN_SWITCHED_ON:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_5)) return false;
                            wait_state=CANOPEN_SWITCHED_ON;
                            break;
                        case CANOPEN_OPERATION_ENABLED:
                            return true;
                            break;
                        case CANOPEN_QUICK_STOP_ACTIVE:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_11)) return false;
                            wait_state=CANOPEN_SWITCH_ON_DISABLED;
                            break;
                        default:
                            return false;
                            break;
                    }
                    break;
                case CANOPEN_FAULT:
                    switch(state){
                        case CANOPEN_SWITCH_ON_DISABLED:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_15_T0)) return false;
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_15_T1)) return false;
                            wait_state=CANOPEN_SWITCH_ON_DISABLED;
                            break;
                        case CANOPEN_READY_TO_SWITCH_ON:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_15_T0)) return false;
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_15_T1)) return false;
                            wait_state=CANOPEN_SWITCH_ON_DISABLED;
                            break;
                        case CANOPEN_SWITCHED_ON:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_15_T0)) return false;
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_15_T1)) return false;
                            wait_state=CANOPEN_SWITCH_ON_DISABLED;
                            break;
                        case CANOPEN_OPERATION_ENABLED:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_15_T0)) return false;
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_15_T1)) return false;
                            wait_state=CANOPEN_SWITCH_ON_DISABLED;
                            break;
                        case CANOPEN_QUICK_STOP_ACTIVE:
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_15_T0)) return false;
                            if(!setStateCommand_SDO(CANOPEN_STATE_TRANSITION_15_T1)) return false;
                            wait_state=CANOPEN_SWITCH_ON_DISABLED;
                            break;
                        default:
                            return false;
                            break;
                    }
                    break;
                default:
                    return false;
                    break;
            }
        }else return false;
    }
    return false;
}

bool NemasController::setState_PDO(uint8_t state){
    unsigned long start_time=0;
    uint8_t actual_state=0;
    uint8_t wait_state=0;
    wait_state = getState_PDO();
    start_time=millis();
    while(!(millis()-start_time > PDO_RESPONSE_TIMEOUT)){
        actual_state=getState_PDO();
        if(wait_state!=actual_state) continue;
        switch(actual_state){
            case CANOPEN_SWITCH_ON_DISABLED:
                switch(state){
                    case CANOPEN_SWITCH_ON_DISABLED:
                        Log.trace("Controller ID: %d, State Change to SWITCH_ON_DISABLED OK!",id);
                        return true;
                        break;
                    case CANOPEN_READY_TO_SWITCH_ON:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_2);
                        wait_state=CANOPEN_READY_TO_SWITCH_ON;
                        break;
                    case CANOPEN_SWITCHED_ON:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_2);
                        wait_state=CANOPEN_READY_TO_SWITCH_ON;
                        break;
                    case CANOPEN_OPERATION_ENABLED:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_2);
                        wait_state=CANOPEN_READY_TO_SWITCH_ON;
                        break;
                    case CANOPEN_QUICK_STOP_ACTIVE:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_2);
                        state=CANOPEN_SWITCH_ON_DISABLED;
                        wait_state=CANOPEN_READY_TO_SWITCH_ON;
                        break;
                    default:
                        Log.error("Controller ID: %d, PDO set state ERROR!",id);
                        return false;
                        break;
                }
                break;
            case CANOPEN_READY_TO_SWITCH_ON:
                switch(state){
                    case CANOPEN_SWITCH_ON_DISABLED:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_7);
                        wait_state=CANOPEN_SWITCH_ON_DISABLED;
                        break;
                    case CANOPEN_READY_TO_SWITCH_ON:
                        Log.trace("Controller ID: %d, State Change to CANOPEN_READY_TO_SWITCH_ON OK!",id);
                        return true;
                        break;
                    case CANOPEN_SWITCHED_ON:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_3);
                        wait_state=CANOPEN_SWITCHED_ON;
                        break;
                    case CANOPEN_OPERATION_ENABLED:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_3);
                        wait_state=CANOPEN_SWITCHED_ON;
                        break;
                    case CANOPEN_QUICK_STOP_ACTIVE:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_3);
                        wait_state=CANOPEN_SWITCHED_ON;
                        break;
                    default:
                        Log.error("Controller ID: %d, PDO set state ERROR!",id);
                        return false;
                        break;
                }
                break;
            case CANOPEN_SWITCHED_ON:
                switch(state){
                    case CANOPEN_SWITCH_ON_DISABLED:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_10);
                        wait_state=CANOPEN_SWITCH_ON_DISABLED;
                        break;
                    case CANOPEN_READY_TO_SWITCH_ON:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_6);
                        wait_state=CANOPEN_READY_TO_SWITCH_ON;
                        break;
                    case CANOPEN_SWITCHED_ON:
                        Log.trace("Controller ID: %d, State Change to CANOPEN_SWITCHED_ON OK!",id);
                        return true;
                        break;
                    case CANOPEN_OPERATION_ENABLED:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_4);
                        wait_state=CANOPEN_OPERATION_ENABLED;
                        break;
                    case CANOPEN_QUICK_STOP_ACTIVE:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_4);
                        wait_state=CANOPEN_OPERATION_ENABLED;
                        break;
                    default:
                        Log.error("Controller ID: %d, PDO set state ERROR!",id);
                        return false;
                        break;
                }
                break;
            case CANOPEN_OPERATION_ENABLED:
                switch(state){
                    case CANOPEN_SWITCH_ON_DISABLED:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_9);
                        wait_state=CANOPEN_SWITCH_ON_DISABLED;
                        break;
                    case CANOPEN_READY_TO_SWITCH_ON:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_8);
                        wait_state=CANOPEN_READY_TO_SWITCH_ON;
                        break;
                    case CANOPEN_SWITCHED_ON:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_5);
                        wait_state=CANOPEN_SWITCHED_ON;
                        break;
                    case CANOPEN_OPERATION_ENABLED:
                        Log.trace("Controller ID: %d, State Change to CANOPEN_OPERATION_ENABLED OK!",id);
                        return true;
                        break;
                    case CANOPEN_QUICK_STOP_ACTIVE:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_11);
                        wait_state=CANOPEN_QUICK_STOP_ACTIVE;
                        break;
                    default:
                        Log.error("Controller ID: %d, PDO set state ERROR!",id);
                        return false;
                        break;
                }
                break;
            case CANOPEN_QUICK_STOP_ACTIVE:
                switch(state){
                    case CANOPEN_SWITCH_ON_DISABLED:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_12);
                        wait_state=CANOPEN_SWITCH_ON_DISABLED;
                        break;
                    case CANOPEN_READY_TO_SWITCH_ON:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_12);
                        wait_state=CANOPEN_READY_TO_SWITCH_ON;
                        break;
                    case CANOPEN_SWITCHED_ON:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_12);
                        wait_state=CANOPEN_SWITCHED_ON;
                        break;
                    case CANOPEN_OPERATION_ENABLED:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_16_T0);
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_16_T1);
                        wait_state=CANOPEN_SWITCHED_ON;
                        break;
                    case CANOPEN_QUICK_STOP_ACTIVE:
                        Log.trace("Controller ID: %d, State Change to CANOPEN_QUICK_STOP_ACTIVE OK!",id);
                        return true;
                        break;
                    default:
                        Log.error("Controller ID: %d, PDO set state ERROR!",id);
                        return false;
                        break;
                }
                break;
            case CANOPEN_FAULT:
                switch(state){
                    case CANOPEN_SWITCH_ON_DISABLED:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_15_T0);
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_15_T1);
                        wait_state=CANOPEN_SWITCH_ON_DISABLED;
                        break;
                    case CANOPEN_READY_TO_SWITCH_ON:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_15_T0);
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_15_T1);
                        wait_state=CANOPEN_SWITCH_ON_DISABLED;
                        break;
                    case CANOPEN_SWITCHED_ON:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_15_T0);
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_15_T1);
                        wait_state=CANOPEN_SWITCH_ON_DISABLED;
                        break;
                    case CANOPEN_OPERATION_ENABLED:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_15_T0);
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_15_T1);
                        wait_state=CANOPEN_SWITCH_ON_DISABLED;
                        break;
                    case CANOPEN_QUICK_STOP_ACTIVE:
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_15_T0);
                        setStateCommand_PDO(CANOPEN_STATE_TRANSITION_15_T1);
                        wait_state=CANOPEN_SWITCH_ON_DISABLED;
                        break;
                    default:
                        Log.error("Controller ID: %d, PDO set state ERROR!",id);
                        return false;
                        break;
                }
                break;
            default:
                Log.error("Controller ID: %d, PDO set state ERROR!",id);
                return false;
                break;
        }
    }
    Log.error("Controller ID: %d, PDO set state Timeout!",id);
    return false;
}

void NemasController::setPDOVariable_TargePosition(int32_t targetposition){
    pdorxmapping.target_position=targetposition;
}

void NemasController::setPDOVariable_ControlWord(uint16_t controlword){
    pdorxmapping.controlword=controlword;
}

void NemasController::setPDOVariable_ModeOfOperation(int8_t mode_operation){
    pdorxmapping.mode_operation=mode_operation;
    return;
}

void NemasController::setStateCommand_PDO(uint16_t command){
    setPDOVariable_ControlWord(command);
    WritePDO(PDO_RX_MAPPING1);
    return;
}

bool NemasController::setStateCommand_SDO(uint16_t command){
    return WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,command);
}


bool NemasController::setOperatingMode_PDO(int32_t mode){
    unsigned long start_time=0;
    setState_PDO(CANOPEN_SWITCH_ON_DISABLED);
    setPDOVariable_ModeOfOperation(mode);
    WritePDO(PDO_RX_MAPPING2);
    start_time=millis();
    while(!(millis()-start_time > PDO_RESPONSE_TIMEOUT)){
        if(mode==getModeOperation_PDO()){
            Log.trace("Controller ID: %d, Set Operating Mode OK!",id);
            return true;
        } 
        else continue;
    }
    Log.error("Controller ID: %d, Timeout Setting operating mode!",id);
    return false;
}

/**
 * NemasController 
 * 
 * Function that sets the controller operating mode 
 * 
 * @param  {int32_t} mode : controller desired mode
 * @return {bool}         : operation success info
 */
bool NemasController::setOperatingMode_SDO(int32_t mode){
    uint8_t data[4];
    int32_t ind=0;
    if(!setState_SDO(CANOPEN_SWITCH_ON_DISABLED)) return false;
    if(!WriteSDO(CANOPEN_SET_MODES_OF_OPERATIONS,0x00,CANOPEN_MSG_SIZE_8_BITS,mode)) return false;
    //READ SDO (confirm if the operation mode is correctly set)
    if(ReadSDO(CANOPEN_GET_MODES_OF_OPERATIONS,0x00,data)){
        if(mode == buffer_get_int32(data,&ind)) return true;
    }
    return false;
}
/**
 * NemasController 
 * 
 * Function that sets the controller profile position
 * 
 * @param  {int32_t} position : controller desired profile position
 * @return {bool}             : operation success info
 */
bool NemasController::setTargetPosition_SDO(int32_t position){
    if(!WriteSDO(CANOPEN_TARGET_POSITION,0x00,CANOPEN_MSG_SIZE_32_BITS,position)) return false;
    return true;
}
/**
 * NemasController 
 * 
 * Function that sets the controller absolute position instantly
 * 
 * @param  {int32_t} position : controller desired instant absolute position
 * @return {bool}             : operation success info
 */
bool NemasController::setInstantAbsProfilePos_SDO(int32_t position){
    if(!setTargetPosition_SDO(position)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x3F)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x0F)) return false;
    return true;
}
/**
 * NemasController
 * 
 * Function that sets the controller absolute position, waiting for other position changes to complete
 * 
 * @param  {int32_t} position : controller desired complete absolute position
 * @return {bool}             : operation success info
 */
bool NemasController::setCompletedAbsProfilePos_SDO(int32_t position){
    if(!setTargetPosition_SDO(position)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x1F)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x0F)) return false;
    return true;
}

bool NemasController::setCompletedAbsProfilePos_PDO(int32_t position){
    setPDOVariable_ControlWord(0x1F);
    setPDOVariable_TargePosition(position);
    WritePDO(PDO_RX_MAPPING3);
    return true;
}

/**
 * NemasController 
 * 
 * Function that sets the controller relative position, waiting for other position changes to complete
 * 
 * @param  {int32_t} position :  controller desired complete relative position
 * @return {bool}             :  operation success info
 */
bool NemasController::setCompletedRelProfilePos_SDO(int32_t position){
    if(!setTargetPosition_SDO(position)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x5F)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x4F)) return false;
    return true;
}

/**
 * NemasController 
 * 
 * Function that performs the controller homing routine
 * 
 * @param  {int} zsearch_rpm     : motor rpm when searching for z index
 * @param  {int} zsearch_current : motor current when searching for z index
 * @param  {int} final_rpm       : motor rpm after the homing is performed
 * @param  {int} final_current   : motor current after the homing is performed
 * @return {bool}                : operation success info 
 */
bool NemasController::Homing_SDO(int zsearch_rpm, int zsearch_current, int final_rpm, int final_current){
    uint8_t data[4];

    bool homing = false;
    bool index_found = false;

    unsigned long last_time=0;

    unsigned long time=0;

    //check if openloop is available bit15 (bit 8 data[2])
    ReadSDO(CANOPEN_STATUS_WORD,0x00,data);
    if((data[2] & 0x80) == 0x80){ //if closed loop available enter here
        //set closed loop
        WriteSDO(CANOPEN_MOTOR_SUBMODE_SELECT,0x00,CANOPEN_MSG_SIZE_32_BITS,0x01);
        //homing command
        WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x1F);
        //Debug
        //Serial.println("closed loop enabled! homing straight away");
    }else{
        //Debug
        //Serial.println("closed loop not enabled! let's find Z");
        setState_SDO(CANOPEN_SWITCH_ON_DISABLED);
        //set open loop 
        WriteSDO(CANOPEN_MOTOR_SUBMODE_SELECT,0x00,CANOPEN_MSG_SIZE_32_BITS,0x00);
        //change motor max current
        WriteSDO(CANOPEN_MAX_CURRENT,0x00,CANOPEN_MSG_SIZE_32_BITS,zsearch_current);
        //change profile position speed
        WriteSDO(CANOPEN_PROFILE_VELOCITY,0x00,CANOPEN_MSG_SIZE_32_BITS,zsearch_rpm);

        //profile position
        setOperatingMode_SDO(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
		setState_SDO(CANOPEN_OPERATION_ENABLED);
        //1 rotation left
        setCompletedRelProfilePos_SDO(360);
        //1 rotation right
        setCompletedRelProfilePos_SDO(-360);

        last_time=millis();

        //check open loop
        while(!index_found){
            ReadSDO(CANOPEN_STATUS_WORD,0x00,data);
            if(((data[2] & 0x80) == 0x80)) {
                //Debug
                //Serial.println("Z found!");
                index_found=true;
            }
            time=millis();
		    if(time - last_time > 10000) { //10 seconds before timeout
                //Debug
                //Serial.println("timeout searching for Z");
                setState_SDO(CANOPEN_SWITCH_ON_DISABLED);
                // reset max current 
                WriteSDO(CANOPEN_MAX_CURRENT,0x00,CANOPEN_MSG_SIZE_32_BITS,final_current);
                //reset profile position speed
                WriteSDO(CANOPEN_PROFILE_VELOCITY,0x00,CANOPEN_MSG_SIZE_32_BITS,final_rpm);
                return false;
            }
        }
        setState_SDO(CANOPEN_SWITCH_ON_DISABLED);
        //set closed loop
        WriteSDO(CANOPEN_MOTOR_SUBMODE_SELECT,0x00,CANOPEN_MSG_SIZE_32_BITS,0x01);

        // reset max current 
        WriteSDO(CANOPEN_MAX_CURRENT,0x00,CANOPEN_MSG_SIZE_32_BITS,final_current);
        //reset profile position speed
        WriteSDO(CANOPEN_PROFILE_VELOCITY,0x00,CANOPEN_MSG_SIZE_32_BITS,final_rpm);



        setState_SDO(CANOPEN_OPERATION_MODE_HOMING);
        setState_SDO(CANOPEN_OPERATION_ENABLED);
        WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x1F);
    }

    last_time=millis();
    //Debug
    //Serial.println("homing...");
    while(!homing){
        //homing cycle
        ReadSDO(CANOPEN_STATUS_WORD,0x00,data);
        if((data[2] & 0x04) == 0x04) {
            homing=true;
            //Debug
            //Serial.println("homing done!");
        }
        time=millis();
		if(time - last_time > TIMEOUT_HOMING) {
            //Debug
            //Serial.println("homing timeout");
            return false;
        }

    }
    return true;
}
/**
 * NemasController 
 * 
 * Function to interrupt homing
 * 
 * @return {bool}  : operation success info
 */
bool NemasController::stopHoming(){
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x0F)) return false;
    return true;
}

/**
 * NemasController 
 * 
 * Function that changes the controller position with a pdo request
 * 
 * @param  {int32_t} position : controller desired position
 * @return {bool}             : operation success info
 */
void NemasController::setInstantAbsProfilePos_PDO(int32_t position){
    setPDOVariable_ControlWord(0x3F);
    setPDOVariable_TargePosition(position);
    WritePDO(PDO_RX_MAPPING3);
    //wait for bit 12 return to 0
    elapsedMillis timer=0;
    while(getSetpointAck_PDO()){
        if(timer>SET_POSITION_TIMEOUT){
            Log.error("Controller ID: %d, setInstantAbsProfilePos Timeout!", id);
            break;
        }
    }
    //Log.trace("setInstantAbsProfilePos OK!");
}

/**
 * NemasController 
 * 
 * Function that writes a value to a user variable in the controller memory
 * 
 * @param  {uint8_t} subindex : subindex of the variable to be written
 * @param  {int32_t} value    : variable value
 * @return {bool}             : operation success info
 */
bool NemasController::WriteToN5(uint8_t subindex,int32_t value){
    if(!WriteSDO(CUSTOMER_STORAGE_AREA_ID,subindex,CANOPEN_MSG_SIZE_32_BITS,value)) return false;
    return true;
}
/**
 * NemasController 
 * 
 * Function that reads a value from a user variable in the controller memory
 * 
 * @param  {uint8_t} subindex : subindex of the variable to be read
 * @param  {int32_t} value    : variable value
 * @return {bool}             : operation success info
 */
bool NemasController::ReadFromN5(uint8_t subindex,int32_t &value){
    uint8_t data[4];
    int32_t ind=0;
    if(ReadSDO(CUSTOMER_STORAGE_AREA_ID,subindex,data)){
        value = buffer_get_int32(data,&ind);
        return true;
    }
    return false;
}



/**
 * NemasController 
 * 
 * Function for homing with open loop control and a limit switch
 * 
 * @return {bool}  : operation success info
 */
bool NemasController::backfoilHoming(){
    uint8_t data[4];

    bool homing = false;

    unsigned long last_time=0;

    WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x1F);
    delay(2000);

    last_time=millis();

    //Serial.println("homing...");
    while(!homing){
        //homing cycle
        ReadSDO(CANOPEN_STATUS_WORD,0x00,data);
        if((data[2] & 0x04) == 0x04) {
            homing=true;
            //Serial.println("homing done!");
        }
		if(millis()- last_time > TIMEOUT_HOMING) {
            //Serial.println("homing timeout");
            return false;
        }

    }
    return true;
}

bool NemasController::performHoming_PDO(){
    //if(getHomingState_PDO()==0x1400 || getHomingState_PDO()==0x1000){ //homing again comment if needed
     //   Log.trace("Controller ID: %d, Homing already performed!",id);
      //  return true;
    //}
    setOperatingMode_PDO(CANOPEN_OPERATION_MODE_HOMING);
    setState_PDO(CANOPEN_OPERATION_ENABLED);
    setPDOVariable_ControlWord(0x1F);
    WritePDO(PDO_RX_MAPPING1);
    elapsedMillis timeout;
    while(getHomingState_PDO()!=0){
        if(timeout>HOMING_TIMEOUT){
			timeout=0;
			Log.error("Controller ID: %d, Homing timeout!",id);
			return false;
		}
    }
    while(getHomingState_PDO()!=0x1400){
        if(timeout>HOMING_TIMEOUT){
			timeout=0;
			Log.error("Controller ID: %d, Homing timeout!",id);
			return false;
		}
    }
    if(getHomingState_PDO()==0x1400 || getHomingState_PDO()==0x1000){
        Log.trace("Controller ID: %d, Homing OK!",id);
        setState_PDO(CANOPEN_SWITCH_ON_DISABLED);
        delay(1000);
        homing_offset=getActualPosition_PDO();
        setOperatingMode_PDO(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
        return true;    
    }
    else{
        Log.error("Controller ID: %d, Homing Error!",id);
        return false;
    }
}

/*Auxiliary functions*/

void printTimestamp(Print* _logOutput) {
  char c[12];
  sprintf(c, "%10lu ", millis());
  _logOutput->print(c);
}

void printNewline(Print* _logOutput) {
  _logOutput->print('\n');
}
