/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef NEMAS_CONTROLLER_H
#define NEMAS_CONTROLLER_H

#include "Arduino.h"
#include "CAN_TSB.h"
#include "../../include/config.h"
#include <ArduinoLog.h>

struct SdoRequest {
    volatile bool received;
    volatile uint32_t id;
    volatile uint16_t index;
    volatile uint8_t subindex;
    CAN_message_t frame;
};

struct EMCYMessage {
    volatile uint8_t error_code[2];
    volatile uint8_t error_register;
    volatile uint8_t manufacturer_error_code[5];
};

struct PdoRXMapping {
    volatile uint16_t controlword;
    volatile int8_t mode_operation;
    volatile int32_t target_position;
};

struct Nanotec_Controller {
    volatile uint8_t id;
    volatile bool boot_up;
    volatile uint8_t nmt_state;
    volatile uint16_t statusword;
    volatile int8_t mode;
    volatile int32_t position_actual_value;
    EMCYMessage emcymessage;
    SdoRequest sdorequest;
};

class NemasComms : public CANListener {
public:
    NemasComms();
    Nanotec_Controller controllers[N_CONTROLLERS];
    FlexCAN_T4<CAN_FOILS, RX_SIZE_256, TX_SIZE_16> Can;
    bool frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller);
    void SendMessage(CAN_message_t &message);
    void RecvMessage(CAN_message_t &frame);
    void addController(uint8_t controller_id);
    void process_BootupMessage(CAN_message_t &frame);
    void process_HeartBeatMessage(CAN_message_t &frame);
    void process_SDOMessage(CAN_message_t &frame);
    void process_TXPDO1Message(CAN_message_t &frame);
    void process_TXPDO2Message(CAN_message_t &frame);
    void process_EMCYMessage(CAN_message_t &frame);
private:
};

class NemasController {
    public:               
        NemasController(uint8_t id, NemasComms* comms_node);
        void WriteNMT(uint8_t state);
        void WriteNMT_Broadcast(uint8_t state);
        void WriteSYNC();
        bool WriteSDO(uint16_t index,uint8_t subIndex,uint8_t data_size,int32_t data);
        bool ReadSDO(uint16_t index,uint8_t subIndex,uint8_t* data);
        void WritePDO(uint8_t mapping);

        bool getBootUpState();
        uint8_t getNmtState();

        //gets
        int32_t getActualPosition_PDO();
        uint16_t getStatusWord_PDO();
        int8_t getModeOperation_PDO();
        uint8_t getState_PDO();
        bool getTARG_BIT_PDO();
        uint16_t getHomingState_PDO();
        bool getSetpointAck_PDO();
        
        bool getActualPosition_SDO(int32_t &position);
        bool getCurrentIq_SDO(int32_t &current);
        bool getState_SDO(uint8_t &state);

        //Sets
        void setPDOVariable_TargePosition(int32_t targetposition);
        void setPDOVariable_ControlWord(uint16_t controlword);
        void setPDOVariable_ModeOfOperation(int8_t mode);
        void setInstantAbsProfilePos_PDO(int32_t position);
        bool setState_PDO(uint8_t state);
        void setStateCommand_PDO(uint16_t command);
        bool setOperatingMode_PDO(int32_t mode);
        bool setCompletedAbsProfilePos_PDO(int32_t position);

        bool setState_SDO(uint8_t state);
        bool setStateCommand_SDO(uint16_t command);
        bool setOperatingMode_SDO(int32_t mode);
        bool setTargetPosition_SDO(int32_t position);
        bool setInstantAbsProfilePos_SDO(int32_t position);
        bool setCompletedAbsProfilePos_SDO(int32_t position);
        bool setCompletedRelProfilePos_SDO(int32_t position);

        bool waitforBootUp();
        bool waitforNMTState(uint8_t state);
        bool waitforfirstPDO();

        void resetfaultConditions_SDO();
        void resetfaultConditions_PDO();

        bool Homing_SDO(int zsearch_rpm, int zsearch_current, int final_rpm, int final_current);

        bool performHoming_PDO();

        bool stopHoming();

        bool WriteToN5(uint8_t subindex,int32_t value);
        bool ReadFromN5(uint8_t subindex,int32_t &value);
        bool backfoilHoming();
        volatile PdoRXMapping pdorxmapping;
        float actualPosition;
        float current_Iq;
        uint8_t id;
        int32_t homing_offset;
    private:
        NemasComms* node;
};

#define CANOPEN_RESET_FAULT_CONDITIONS 0x80

//ID MASK
#define CANOPEN_MESSAGE_ID_MASK 0xFFF0
#define CANOPEN_CONTROLLER_ID_MASK 0xF

//MSG IDS

/*****CANOPEN SDO**********/
#define CANOPEN_SDO_ID 0x580

/*****CANOPEN PDO TX********/
#define TX_PDO_MAPPING1_ID 0x180
#define TX_PDO_MAPPING2_ID 0x280

/*****CANOPEN BOOTUP**********/
#define CANOPEN_BOOTUP_ID 0x700

/*****CANOPEN PDO TX*******/
#define TX_PDO_MAPPING1 0x180
#define TX_PDO_MAPPING2 0x280
#define TX_PDO_MAPPING3 0x380
#define TX_PDO_MAPPING4 0x480

/*****CANOPEN EMCY*******/
#define CANOPEN_EMCY_ID 0x80

//Message Sizes
#define CANOPEN_MSG_SIZE_8_BITS    0X01
#define CANOPEN_MSG_SIZE_16_BITS   0X02
#define CANOPEN_MSG_SIZE_24_BITS   0x03
#define CANOPEN_MSG_SIZE_32_BITS   0X04

/*****CANOPEN NMT*******/
#define CANOPEN_NMT_ID   0x000
#define CANOPEN_NMT_BOOTUP_STATE 0x00
#define CANOPEN_NMT_PREOPERATIONAL_STATE 0x7F
#define CANOPEN_NMT_OPERATIONAL_STATE 0x05

//States CMDs
#define CANOPEN_NMT_SWITCH_OPERATIONAL      0x01
#define CANOPEN_NMT_SWITCH_STOP             0x02
#define CANOPEN_NMT_SWITCH_PREOPERATIONAL   0x80
#define CANOPEN_NMT_RESET_NODE              0x81
#define CANOPEN_NMT_RESET_COMMUNICATION     0x82

/*****CANOPEN SYNC*******/
#define CANOPEN_SYNC_ID 0x80

/*****CANOPEN PDO RX*******/
#define CANOPEN_RX_PDO_MAPPING1_CONFIG 0x1400
#define CANOPEN_RX_PDO_MAPPING2_CONFIG 0x1401
#define CANOPEN_RX_PDO_MAPPING3_CONFIG 0x1402
#define CANOPEN_RX_PDO_MAPPING4_CONFIG 0x1403

#define CANOPEN_RX_PDO_SUBINDEX_TRANSMISSION_TYPE 0x02
#define CANOPEN_RX_PDO_TRANSMISSION_TYPE_ASSYNC 0xFF
#define CANOPEN_RX_PDO_TRANSMISSION_TYPE_SYNC 0x00

#define CANOPEN_RX_PDO_MAPPING1 0x200
#define CANOPEN_RX_PDO_MAPPING2 0x300
#define CANOPEN_RX_PDO_MAPPING3 0x400

//PDO_IDS
#define PDO_RX_MAPPING1 0x01
#define PDO_RX_MAPPING2 0x02
#define PDO_RX_MAPPING3 0x03

/*****CANOPEN SDO*******/
#define CANOPEN_SDO_DOWNLOAD_REQUEST    0x600
#define CANOPEN_SDO_UPLOAD_REQUEST      0x600
#define CANOPEN_SDO_DOWNLOAD_REPLY      0x580
#define CANOPEN_SDO_UPLOAD_REPLY        0x580

//CANOPEN SDO CMDs
#define CANOPEN_SDO_CMD_1_BYTE 0x2F
#define CANOPEN_SDO_CMD_2_BYTE 0x2B
#define CANOPEN_SDO_CMD_3_BYTE 0x27
#define CANOPEN_SDO_CMD_4_BYTE 0x23

//CANOPEN SDO REPLY
#define CANOPEN_SDO_REPLY_OK     0x60
#define CANOPEN_SDO_REPLY_ERROR  0x80

//CANOPEN SDO REPLY CMD
#define CANOPEN_SDO_REPLY_1_BYTE 0x4F
#define CANOPEN_SDO_REPLY_2_BYTE 0x4B
#define CANOPEN_SDO_REPLY_3_BYTE 0x47
#define CANOPEN_SDO_REPLY_4_BYTE 0x43

//States Of Operation
#define STATE_MASK              0x6F

#define TARG_MASK 0x400
#define HOMING_MASK 0x3400

#define SETPOINT_ACK_MASK 0x1000

//Controller States
#define CANOPEN_NOT_READY_TO_SWITCH_ON	0x00
#define CANOPEN_SWITCH_ON_DISABLED	    0x40
#define CANOPEN_READY_TO_SWITCH_ON 	    0x21
#define CANOPEN_SWITCHED_ON		        0x23
#define CANOPEN_OPERATION_ENABLED	    0x27
#define CANOPEN_QUICK_STOP_ACTIVE	    0x07
#define CANOPEN_FAULT_REACTION_ACTIVE   0x0F
#define CANOPEN_FAULT 			        0x08

// Transition Commands			     
#define CANOPEN_STATE_TRANSITION_2	0x06
#define CANOPEN_STATE_TRANSITION_3	0x07  
#define CANOPEN_STATE_TRANSITION_4	0x0F 
#define CANOPEN_STATE_TRANSITION_5	0x07 
#define CANOPEN_STATE_TRANSITION_6	0x06
#define CANOPEN_STATE_TRANSITION_7	0x00 
#define CANOPEN_STATE_TRANSITION_8	0x06
#define CANOPEN_STATE_TRANSITION_9	0x00 
#define CANOPEN_STATE_TRANSITION_10	0x00 
#define CANOPEN_STATE_TRANSITION_11	0x02
#define CANOPEN_STATE_TRANSITION_12	0x00
#define CANOPEN_STATE_TRANSITION_15_T0	0x00  
#define CANOPEN_STATE_TRANSITION_15_T1	0x80
#define CANOPEN_STATE_TRANSITION_16_T0	0x0B
#define CANOPEN_STATE_TRANSITION_16_T1	0x0F  
          

//TIMEOUTS
#define CANOPEN_SDO_REPLY_TIMEOUT   50 //5 ms
#define STATE_MACHINE_TIMEOUT       0x32 //50 ms
#define TIMEOUT_HOMING              15000 //15000 ms

//OPERATION MODES
#define CANOPEN_OPERATION_MODE_PROFILE_POSITION 1
#define CANOPEN_OPERATION_MODE_HOMING 6


//MODES SET GET
#define CANOPEN_SET_MODES_OF_OPERATIONS		0x6060
#define CANOPEN_GET_MODES_OF_OPERATIONS		0x6061

//PROFILE POSITION
#define CANOPEN_TARGET_POSITION 0x607A

//POSITION ACTUAL VALUE
#define CANOPEN_POSITION_ACTUAL_VALUE 0x6064

//STATUS WORDS
#define CANOPEN_CONTROL_WORD    0x6040
#define CANOPEN_STATUS_WORD     0x6041

//PROFILE VELOCITY
#define CANOPEN_PROFILE_VELOCITY 0x6081

//MAX CURRENT
#define CANOPEN_MAX_CURRENT 0x2031

//Motor Drive Submode Select
#define CANOPEN_MOTOR_SUBMODE_SELECT 0x3202

//Customer Storage Area
#define CUSTOMER_STORAGE_AREA_ID 0x2701

//Functions
void printTimestamp(Print* _logOutput);
void printNewline(Print* _logOutput);

#endif