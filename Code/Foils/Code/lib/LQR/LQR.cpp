/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "LQR.h"

LQRController::LQRController(){

	//globals for the com_mode controller
	KX_PREV = 0.0;
	KZ_PREV = 0.0;
	ALFA = INITIAL_AOA_HEAVE; // initial condition of integrator
	SATURATION = 0.0;
	LAST_TIME_COM = 0.0;
	
	K_sat_com = 3; //saturation 

	//globals for the dif_mode controller
	KROLL_PREV = 0.0;
	KROLLDOT_PREV = 0.0;
	ALFA_DIF = 0.0;
	SATURATION_DIF = 0.0;
	LAST_TIME_DIF = 0.0;
	dif_angle_lim = 8.5;
	initial_com_angle = 5.5;
	             
	K_sat_dif = 3; 

	//Kpitch 0.8
	//KpitchRate 0.1
	// Kspeed 1.4
	// KspeedZ 0.6
	// Kheave 10.3
	// Kheave int 3.5

	//PID
	//Kp 49 Ki 3 Kd 15

	angle_left=DEFAULT_FRONT_ANGLE;
	angle_right=DEFAULT_FRONT_ANGLE;
	angle_rear = DEFAULT_REAR_ANGLE;

	heave=0.0;

	h_ref=0.0;
	roll_reference=0.0;
	yawrate_filt = 0.0;
	gyrX_filt = 0.0;
	gyrY_filt = 0.0;
	boat_speed = 0.0;

	auto_com_angle = 0.0;
	auto_dif_angle = 0.0;

	flag_first_loop = false;
	flag_first_loop_roll = false;
}

float LQRController::com_mode() {
	float com_foil=0.0;
    float Kz=0.0;
    float Kx=0.0;
    float Ke=0.0;
    float u_dot=0.0;
	float deriv_Kz=0.0;
	float deriv_Kx=0.0;
	float real_com = 0.0;

	float delta_t = (millis() - LAST_TIME_COM)/1000;	
	if (delta_t > 0.02) delta_t = 0.02;
	LAST_TIME_COM = millis();

	/*if(-heave - DIST_US_TO_REAR*sin(pitch*PI/180) > MAX_REAR_LIMIT)  
		h_ref = -(MAX_REAR_LIMIT+DIST_US_TO_REAR*sin(pitch*PI/180)); 
	else if(-heave - DIST_US_TO_FF*sin(pitch*PI/180) > MAX_FF_LIMIT)  
		h_ref = -(MAX_FF_LIMIT+DIST_US_TO_FF*sin(pitch*PI/180));
	*/
	gyrY_filt = (rads_to_degs(gyrY)*gyrY_filt_a) + (1-gyrY_filt_a)*gyrY_filt;

	Kz = heave/100*Kh[4]; //heave values are negative (downpointing referencial) and in cm
	Kx = (1)*pitch*Kh[0] + (1)*rads_to_degs(gyrY)*Kh[1] + boat_speed*Kh[2] + (1)*velZ*Kh[3]; //ahrs is now in NED
	Ke = (h_ref/100 - heave/100)*Kh[5];


	// calculate derivatives
	deriv_Kx = (Kx-KX_PREV)/delta_t;
	deriv_Kz = (Kz-KZ_PREV)/delta_t;

	if(flag_first_loop) { // for first loop ignore derivatives, they may be infinite
		deriv_Kx = 0.0;
		deriv_Kz = 0.0;
		flag_first_loop = false;
	}


	// calculate alfa
	u_dot = (Ke - deriv_Kx - deriv_Kz);
	if(boat_speed > 6.4) //Gain scheduling
		u_dot = u_dot*6.4*6.4/(boat_speed*boat_speed);
	//if(u_dot >= 50) //slew rate anti windup
	//	u_dot = 50;
	//else if(u_dot <= -50)
	//	u_dot = -50;
	u_dot = u_dot - SATURATION*K_sat_com; //anti windup
	ALFA = ALFA + u_dot*delta_t;  // alfa = int(u_dot);

	// saturation block
	if(ALFA > MAX_RIGHT_FOILANGLE-sqrt(pow(auto_dif_angle,2))) { //change to MAX_FRONTFOIL_ANGLE - auto_dif_angle
	    com_foil=MAX_RIGHT_FOILANGLE-sqrt(pow(auto_dif_angle,2));
	}else if (ALFA < MIN_LEFT_FOILANGLE + sqrt(pow(auto_dif_angle,2))) { //change to MIN_FRONTFOIL_ANGLE + auto_dif_angle
	    com_foil=MIN_LEFT_FOILANGLE + sqrt(pow(auto_dif_angle,2));
	}else {
	    com_foil=ALFA;
	}

	real_com = (real_angle_left+real_angle_right)/2;
	// update global variables
	SATURATION = ALFA - real_com;
	KX_PREV = Kx;
	KZ_PREV = Kz;


	return com_foil;
}



float LQRController::dif_mode() {
	float dif_val=0.0;
    float Kroll=0.0;
    float Krolldot=0.0;
    float Ke=0.0;
    float u_dot=0.0;
	float deriv_Kroll=0.0;
	float deriv_Krolldot=0.0;
	float real_dif = 0.0;

	float delta_t = (millis() - LAST_TIME_DIF)/1000;	
	if (delta_t > 0.02) delta_t = 0.02;
	LAST_TIME_DIF = millis();

	//filter yaw rate and compute roll reference
	yawrate_filt = (rads_to_degs(gyrZ)*EMA_ALPHA) + (1-EMA_ALPHA)*yawrate_filt;
	gyrX_filt = (rads_to_degs(gyrX)*gyrX_filt_a) + (1-gyrX_filt_a)*gyrX_filt;
	roll_reference = ROLLREFGAIN*yawrate_filt; //removed -sign because in NED, positive roll equals positive yaw
	//limit roll ref saturation
	if(roll_reference > MAX_ROLL) {
	    roll_reference= MAX_ROLL;
	}else if (roll_reference < -MAX_ROLL) {
	    roll_reference= -MAX_ROLL;
	}
	
	// multiply gains
	Kroll = roll*Kr[0];
	Krolldot = gyrX_filt*Kr[1];
	Ke = (roll_reference - roll)*Kr[2]; 

	// calculate derivatives
	deriv_Kroll = (Kroll-KROLL_PREV)/delta_t;
	deriv_Krolldot = (Krolldot-KROLLDOT_PREV)/delta_t;

	if(flag_first_loop_roll) { // for first loop ignore derivatives, they may be infinite
		deriv_Kroll = 0.0;
		deriv_Krolldot = 0.0;
		flag_first_loop_roll = false;
	}

	// calculate alfa
	u_dot = Ke - deriv_Kroll - deriv_Krolldot;  
	if(boat_speed > 6.4) //Gain scheduling
		u_dot = u_dot*6.4*6.4/(boat_speed*boat_speed);
	//if(u_dot >= 50)
	//	u_dot = 50;
	//else if(u_dot <= -50)
	//	u_dot = -50;
	u_dot = u_dot - SATURATION_DIF*K_sat_dif;
	ALFA_DIF = ALFA_DIF + u_dot*delta_t;  // alfa = int(u_dot);

	// saturation block
	if(ALFA_DIF > dif_angle_lim) {
	    dif_val=dif_angle_lim;
	}else if (ALFA_DIF < -dif_angle_lim) {
	    dif_val= -dif_angle_lim;
	}else {
	    dif_val=ALFA_DIF;
	}

	real_dif = (real_angle_left-real_angle_right)/2;
	// update global variables
	SATURATION_DIF = ALFA_DIF - real_dif;
	KROLL_PREV = Kroll;
	KROLLDOT_PREV = Krolldot;
	
	return dif_val;
}



void LQRController::full_mode(){
	auto_com_angle = com_mode();
	auto_dif_angle = dif_mode();
}

void LQRController::heave_mode(){
	auto_com_angle = com_mode();
}

void LQRController::roll_mode(){
	auto_dif_angle = dif_mode();
}


void LQRController::reset(){
	KX_PREV=0;
	KZ_PREV=0;
	SATURATION=0;
	ALFA=initial_com_angle;

	KROLL_PREV=0;
	KROLLDOT_PREV=0;
	SATURATION_DIF=0;
	ALFA_DIF=0;

	auto_com_angle = initial_com_angle;
	auto_dif_angle = 0;

	angle_left=initial_com_angle;
	angle_right=initial_com_angle;

}

void LQRController::reset_heave(){
	KX_PREV=0;
	KZ_PREV=0;
	SATURATION=0;
	ALFA=initial_com_angle;
	auto_com_angle=initial_com_angle;
}

float LQRController::rads_to_degs(float rads){
	return rads*180/PI;
}




