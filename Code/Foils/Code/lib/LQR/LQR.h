/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef LQR_H
#define LQR_H

#include "Arduino.h"
#include "../../include/config.h"

class LQRController
{
    public:
        LQRController();

        float com_mode();
        float dif_mode();

        void full_mode();
        void heave_mode();
        void roll_mode();

        void reset();
        void reset_heave();

        float rads_to_degs(float rads);

        //foils angles
        volatile float angle_left;
	    volatile float angle_right;
        volatile float angle_rear;

        //foils real angles
        volatile float real_angle_left;
	    volatile float real_angle_right;
        volatile float real_angle_rear;

        volatile float initial_com_angle;

        volatile float heave;
        float h_ref;
        float roll_reference;
        float yawrate_filt;

        volatile float roll;                           // Roll in range ±180º
        volatile float pitch;                          // Pitch in range ±90º
        volatile float yaw;                            // Yaw in range ±180º

        volatile float gyrX;                           // gyrX in range ±35 rad/s
        volatile float gyrY;                           // gyrY in range ±35 rad/s
        volatile float gyrZ;                           // gyrZ in range ±35 rad/s

        volatile float velX;                           // velX in range ±500 m/s
        volatile float velY;                           // velY in range ±500 m/s
        volatile float velZ; 

        volatile float boat_speed;

        volatile float auto_com_angle;
        volatile float auto_dif_angle;

        bool flag_first_loop;
        bool flag_first_loop_roll;

        float Kh[6];
        float Kr[3];

        float dif_angle_lim;
        float gyrX_filt;
        float gyrY_filt;

    private:

        //globals for the com_mode controller
        float KX_PREV;
        float KZ_PREV;
        float ALFA;
        float SATURATION;
        float LAST_TIME_COM;
        float K_sat_com;

        //globals for the dif_mode controller
        float KROLL_PREV;
        float KROLLDOT_PREV;
        float KGYROZ_PREV;
        float ALFA_DIF;
        float SATURATION_DIF;
        float LAST_TIME_DIF;
        float K_sat_dif;

};




#endif
