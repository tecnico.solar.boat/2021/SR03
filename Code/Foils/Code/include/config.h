/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef CONFIG_H
#define CONFIG_H

#define CAN_FOILS CAN2

//ID starts at 1 and ends at N_CONTROLLERS, make things simple!
#define N_CONTROLLERS 3
#define LEFT_CONTROLLER_ID 1
#define RIGHT_CONTROLLER_ID 2
#define BACK_CONTROLLER_ID 3

//Number of modes
#define N_MODES 4

//Modes of controller
#define MANUALMODE 0
#define ROLLMODE 1
#define HEAVEMODE 2
#define FULLMODE 3

//States of controller
#define OFF 0
#define HOMING 1
#define READY 2
#define ON 3
#define ERROR 4

//Controller Initial mode and state
#define CONTROLLER_INITIALMODE ROLLMODE
#define CONTROLLER_INITIALSTATE OFF

//Steppers Controllers Initial modes and states
#define STEPPERS_STARTMODE CANOPEN_OPERATION_MODE_PROFILE_POSITION
#define STEPPERS_STARTSTATE CANOPEN_SWITCH_ON_DISABLED

//TIMERS and TIMEOUTS
#define DISABLE_REAR_STEPPER_TIME 10000 //ms
#define PDO_RESPONSE_TIMEOUT 1000 //ms
#define SDO_RESPONSE_TIMEOUT 50 //ms
#define BOOTUP_TIMEOUT 5000 //ms
#define NMT_TIMEOUT 5000 //ms
#define FIRST_PDO_TIMEOUT 5000 //ms
#define HOMING_TIMEOUT 5000 //ms
#define SET_POSITION_TIMEOUT 10 //ms

//FOILS START and STOP SPEEDs ms
#define FOILS_START_SPEED 2 //basically roll start speed
#define HEAVE_START_SPEED 5.8//4
#define HEAVE_STOP_SPEED 5.5//3.5
#define FOILS_STOP_SPEED 2 //basically roll stop speed

//boat max height reference
#define MAX_HEIGHT_REF 85

//Default angles, max angles, min angles
#define DEFAULT_REAR_ANGLE 0
#define DEFAULT_FRONT_ANGLE 5.5
#define DEFAULT_FRONT_ANGLE_ROLL -3
#define MIN_REAR_FOILANGLE -3
#define MAX_REAR_FOILANGLE 10
#define MIN_FRONT_FOILANGLE -3
#define MAX_FRONT_FOILANGLE 14

#define MIN_RIGHT_FOILANGLE -5.00
#define MAX_RIGHT_FOILANGLE 16.65
#define MIN_LEFT_FOILANGLE -3.65
#define MAX_LEFT_FOILANGLE 14
 
#define INITIAL_AOA_HEAVE 5.5 //initial aoa for heave controller (integrator output)
#define ROLLREFGAIN 0.3
#define EMA_ALPHA 0.05
#define gyrX_filt_a 0.05
#define gyrY_filt_a 0.05
#define MAX_ROLL 3

#define MAX_COM_FOILANGLE 9
#define MIN_COM_FOILANGLE 2

#define DIF_FOILANGLE 8.5 //8.5 for rollmode preference

//Motors max and min positions
#define MIN_REARMOTOR_POSITION 0
#define MAX_REARMOTOR_POSITION 57000
#define MIN_LEFT_FRONTMOTOR_POSITION 0
#define MAX_LEFT_FRONTMOTOR_POSITION 15000
#define MIN_RIGHT_FRONTMOTOR_POSITION 0
#define MAX_RIGHT_FRONTMOTOR_POSITION 17345



//zone 0 - 12000
//#define MIN_LEFT_FRONTMOTOR_POSITION 0
//#define MAX_LEFT_FRONTMOTOR_POSITION 12000
//#define MIN_LEFT_FOILANGLE -3
//#define MAX_LEFT_FOILANGLE 14
//zone 12000 - 18500
//#define MIN_LEFT_FRONTMOTOR_POSITION1 12000
//#define MAX_LEFT_FRONTMOTOR_POSITION1 20000
//#define MIN_LEFT_FOILANGLE1 11
//#define MAX_LEFT_FOILANGLE1 14


//Ultrasonic Sensor
#define SERIAL2_DIR	9
#define SenixSerial Serial2
#define TIMEOUT_SENSOR 80
#define US_ENABLE_PIN 17
#define DIST_US_TO_AHRS 330 //distance between ultrasonic and ahrs
#define DIST_FF_TO_AHRS 142
#define DIST_AHRS_TO_REAR 212 //ahrs rear
#define DIST_US_TO_FF 188
#define DIST_US_TO_REAR 542
#define MAX_FF_LIMIT 85
#define MAX_REAR_LIMIT 80

//CAN
#define CAN_SEQUENTIAL_WRITE 0

#define Basile 1

// Serial to use with the Xsens AHRS
#define XsensSerial Serial3
#define XSENS_TIMEOUT 30

#endif