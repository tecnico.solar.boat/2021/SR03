/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Arduino.h>
#include <Wire.h>
#include "Linduino.h"
#include "Timer.h"

#include "PumpController.h"
#include "ADT7470.h"

#include "LTC2984.h"
// #include "LED.h"

#include "CAN_TSB.h"

#define PUMP_MOSFET 41

Timer t;
TSB_CAN canMessageHandler;
float TEMP_M1, TEMP_M2, TEMP_WC_IN, TEMP_WC_OUT, WATER_FLOW;
uint8_t PUMP_PWM, WARNING = 0;
uint16_t PUMP_TACH;
CAN_message_t temperatures, state; // One for each CAN ID that needs to be sent
void serialEvent();

void initCAN_Messages(){ // Initialise the message fixed parameters, do it for all messages
  temperatures.id = 0x60A;
  temperatures.len = 8;

  state.id = 0x61A;
  state.len = 5;
}

void sendCAN_Messages(){ // Call this function preodically to send the messages use IntervalTimer
  int32_t ind = 4;

  // buffer_append_float16(temperatures.buf, TEMP_M1, 1e1, &ind); // Populate the message data 
  // buffer_append_float16(temperatures.buf, TEMP_M2, 1e1, &ind); // Populate the message data
  buffer_append_float16(temperatures.buf, TEMP_WC_IN, 1e1, &ind); // Populate the message data
  buffer_append_float16(temperatures.buf, TEMP_WC_OUT, 1e1, &ind); // Populate the message data 
  Can0.write(temperatures); // Send message (call this for every message to be sent)

  ind = 0;
  buffer_append_uint8(state.buf, PUMP_PWM, &ind); // Populate the message data 
  buffer_append_uint16(state.buf, PUMP_TACH, &ind); // Populate the message data
  buffer_append_float8(state.buf, WATER_FLOW, 1e1, &ind); // Populate the message data 
  buffer_append_uint8(state.buf, WARNING, &ind); // Populate the message data
  Can0.write(state); // Send message (call this for every message to be sent)

}



void setup()
{
  Serial.begin(115200);         // Initialize the serial port to the PC
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(QUIKEVAL_CS, OUTPUT); // Configure chip select pin on Linduino
  pinMode(PUMP_MOSFET, OUTPUT);
  digitalWrite(PUMP_MOSFET, HIGH);
  /* Let LED_BUILTIN toggle every 1000th millisecond with start
	condition HIGH */
	t.oscillate(LED_BUILTIN, 1000, HIGH);

// -------------- Configure CAN  --------------------------------------
  
  initCAN_Messages(); // Call the function to initialise all you CAN Messages
  // Setup the CAN
  if( Can0.init(CAN_BPS_1000K)){
    Serial.println("CAN Init OK!");
	}
	else{
    Serial.println("CAN Init Failed!");
  }
  Can0.attachObj(&canMessageHandler); // Register CAN_TSB as a CAN Listener
  Can0.setNumTXBoxes(1);    // Use all MOb (only 6x on the ATmegaxxM1 series) for receiving.

  //standard  
  Can0.setRXFilter(1, 0, 0, false);       //catch all mailbox
  Can0.setRXFilter(2, 0, 0, false);       //catch all mailbox
  Can0.setRXFilter(3, 0, 0, false);       //catch all mailbox
  Can0.setRXFilter(4, 0, 0, false);       //catch all mailbox
  Can0.setRXFilter(5, 0, 0, false);       //catch all mailbox

  canMessageHandler.attachGeneralHandler(); // Attach CAN_TSB as a General Handler for all Mailboxes
  // This can also be done for individual Mailboxes
  

// -------------- Configure the ADT7470 -------------------------------

  PumpSetup(canMessageHandler.bms.maxTemp);

// -------------- Configure the LTC2984 -------------------------------

  LTC2894_Setup();

// -------------- Configure LEDs -------------------------------

  // LEDsetup();
}



void loop()
{
  t.update(); // Updates the timer

  // --------------      LTC2984      -------------------------------------
  // measure_channel(CHIP_SELECT, 4, TEMPERATURE);      // Ch 4: Thermistor Custom Table
  // measure_channel(CHIP_SELECT, 6, TEMPERATURE);      // Ch 6: Thermistor Custom Table
  measure_channel(CHIP_SELECT, 8, TEMPERATURE);      // Ch 8: Thermistor Custom Table
  measure_channel(CHIP_SELECT, 10, TEMPERATURE);     // Ch 10: Thermistor Custom Table
  measure_channel(CHIP_SELECT, 19, VOLTAGE);         // Ch 19: Direct ADC

  // Serial.println(WARNING);
  // Serial.println(PUMP_PWM);
  // Serial.println(PUMP_TACH);
  // // --------------      Pump       -------------------------------------
  PumpControl(canMessageHandler.bms.maxTemp);
  // LEDloop();

  // // -------------- CAN Messages -----------------------------------------
  sendCAN_Messages();
  delay(500);

  //serialEvent();

  if(canMessageHandler.screen.pumpMOSFET == true){
    digitalWrite(PUMP_MOSFET, !digitalRead(PUMP_MOSFET));
    canMessageHandler.screen.pumpMOSFET = false;
  }

}

// void serialEvent(){
//   switch (Serial.read())
//   {
//   case 'w':
//     canMessageHandler.bms.maxTemp += 2;
//     // Serial.println(canMessageHandler.bms.maxTemp);
//     break;
//   case 's':
//     canMessageHandler.bms.maxTemp -= 2;
//     // Serial.println(canMessageHandler.bms.maxTemp);
//   break;
//   default:
//     break;
//   }

// }

// #include <Wire.h>
// #include "ADT7470.h"

// ADT7470 ADT(ADT7470_ADDR_FLOAT);



// void testStart()
// {
//   Serial.println(F("ADT7470 testStart"));
//   ADT.powerUp();
//   ADT.startMonitoring();
//   Serial.println();
//   Serial.println();
//   delay(1000);
// }


// void testRevision()
// {
//   Serial.print("ADT7470_LIB_VERSION:\t");
//   Serial.println(ADT7470_LIB_VERSION);

//   Serial.print("ADT7470 getRevision:\t");
//   Serial.println(ADT.getRevision());
//   Serial.print("ADT7470 getDeviceID:\t");
//   Serial.println(ADT.getDeviceID());
//   Serial.print("ADT7470 getCompanyID:\t");
//   Serial.println(ADT.getCompanyID());
//   Serial.println();
//   Serial.println();
//   delay(10);
// }

// void testTemp()
// {
//   Serial.println(F("ADT7470 testTemp 0..9"));

//   Serial.print("temp:");
//   for (uint8_t i = 0; i < 10; i++)
//   {
//     Serial.print("\t");
//     Serial.print(ADT.getTemperature(i));
//   }
//   Serial.println();
//   Serial.print("max:\t");
//   Serial.println(ADT.getMaxTemperature());

//   Serial.println();
//   Serial.println();
//   delay(10);
// }

// void testPWM()
// {
//   Serial.println(F("ADT7470 getPWM 0..3"));
//   Serial.print(F("set:"));
//   for (int i = 0; i < 10; i++)
//   {
//     uint8_t pwm = random(255);
//     ADT.setPWM(3, pwm);
//     Serial.print("\t");
//     Serial.print(pwm);
//   }
//   Serial.println();

//   Serial.print(F("get:"));
//   for (int i = 0; i < 10; i++)
//   {
//     uint8_t pwm = ADT.getPWM(3);
//     Serial.print("\t");
//     Serial.print(pwm);
//   }
//   Serial.println();
//   Serial.println();
//   delay(10);
// }

// void testTach()
// {
//   uint8_t ppr[4];
//   Serial.println(F("ADT7470 testTach 0..3"));
//   Serial.print(F("getPPR: "));
//   for (uint8_t i = 0; i < 4; i++)
//   {
//     ppr[i] = ADT.getPulsesPerRevolution(i);
//     Serial.print("\t");
//     Serial.print(ppr[i]);
//   }
//   Serial.println();

//   Serial.print(F("setPPR: "));
//   for (uint8_t i = 0; i < 4; i++)
//   {
//     ADT.setPulsesPerRevolution(i, ppr[i]);
//     bool b = (ppr[i] == ADT.getPulsesPerRevolution(i));
//     Serial.print("\t");
//     Serial.print(b ? "T" : "F");          // expect TTTT
//   }
//   Serial.println();

//   ADT.setSlowTach();
//   Serial.println(F("setSlowTach"));
//   Serial.print(F("getTach:"));
//   for (uint8_t i = 0; i < 4; i++)
//   {
//     uint16_t tach = ADT.getTach(i);
//     Serial.print("\t");
//     Serial.print(tach);
//   }
//   Serial.println();
//   Serial.print(F("getRPM :"));
//   for (int i = 0; i < 4; i++)
//   {
//     uint32_t rpm = ADT.getRPM(i);
//     Serial.print("\t");
//     Serial.print(rpm);
//   }
//   Serial.println();

//   ADT.setFastTach();
//   Serial.println(F("setFastTach"));
//   Serial.print(F("getTach:"));
//   for (uint8_t i = 0; i < 4; i++)
//   {
//     uint16_t tach = ADT.getTach(i);
//     Serial.print("\t");
//     Serial.print(tach);
//   }
//   Serial.println();
//   Serial.print(F("getRPM :"));
//   for (int i = 0; i < 4; i++)
//   {
//     uint32_t rpm = ADT.getRPM(i);
//     Serial.print("\t");
//     Serial.print(rpm);
//   }
//   Serial.println();
//   Serial.println();
//   delay(10);
// }


// void testFanSpeed()
// {
//   Serial.println(F("ADT7470 testFanSpeed"));
//   Serial.print("low:\t");
//   for (uint8_t i = 0; i < 8; i++)
//   {
//     ADT.setFanLowFreq(i);
//     Serial.print(i);
//     Serial.print("\t");
//     delay(1000);
//   }
//   Serial.println();

//   Serial.print("high:\t");
//   for (uint8_t i = 0; i < 8; i++)
//   {
//     ADT.setFanHighFreq(i);
//     Serial.print(i);
//     Serial.print("\t");
//     delay(1000);
//   }
//   Serial.println();
//   ADT.setFanHighFreq(2);

//   Serial.println();
//   Serial.println();
//   delay(10);
// }

// void testStop()
// {
//   Serial.println(F("ADT7470 testStop"));
//   ADT.stopMonitoring();
//   ADT.powerDown();
//   delay(2000);
//   // TODO how to check if it is down - datasheet.
// }

// void setup()
// {
//   delay(5000);
//   Wire.begin();

//   Serial.begin(115200);
//   Serial.print(F("\n\nStart "));
//   Serial.println(__FILE__);
//   Serial.println();

//   if (!ADT.isConnected())
//   {
//     Serial.println("Cannot connect ADT7470...\n");
//     // while(1);
//   }
//   // else
//   {
//     testStart();
//     testRevision();
//     // testTemp();
//     testPWM();
//     // testTach();
//     // testFanSpeed();


//     testStop();
//   }
//   Serial.println("Done");
// }

// void loop()
// {
// }