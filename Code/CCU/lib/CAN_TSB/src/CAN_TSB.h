/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef CAN_TSB_h
#define CAN_TSB_h

#if defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)
#include "../../avr_can/src/avr_can.h"
#include <Arduino.h>
#elif defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
#include "../utility/FlexCAN_T4.h"
#include <TimeLib.h>
#endif

#include "../utility/ArduinoJson.h"
#include "buffer.h"

#define MAXEQ(A, B) (A) = max((A), (B))
#define MAX_NUM_NODES 15

class Throttle_Data
{
public:
  int8_t value;                  // Resquested power [-100;100]
  uint8_t status;                       // Status bits

  StaticJsonDocument<256> toJson();     // Subject to size change...
};

// --------------------------------------------------------

class BMS_Data
{

public:
  uint8_t status;                       // Status of the battery
  uint16_t ltc_status;                  // Status of the LTC
  bool *cells_balancing;                // Which cells are balancing
  uint8_t warnings;                     // Warnings

  float soc;                            // Battery State of Charge
  float voltage;                        // Battery pack voltage
  float current;                        // Battery pack current
  float solar_current;                  // Solar Panels current

  float *cellvoltages;                  // Cell Voltages
  float *solar_voltages;                // Solar Array Voltages
  float *temperatures;                  // Temperatures
  float maxTemp;                        // Cells and Bus Bars Max Temperature
  
  StaticJsonDocument<4096> toJson();    // Subject to size change... (Maybe 2048?)
};

// --------------------------------------------------------

class Foils_Data
{

public:
  float dist;                           // Distance to water in meters
  float fake_dist;                      // Distance measured by the breakout
  float left_angle;                     // Angle of the Left foil
  float right_angle;                    // Angle of the Right foil
  uint8_t state;                        // Controller state
  uint8_t mode;                         // Controller mode

  float back_angle;                     // Angle of the Rear foil
  uint8_t height_ref;                   // Height Reference used by the controller in Heave or Full mode
  float angle_ref;                      // Front Foils Angle Reference
  float speed;                          // Boat speed in m/s
  int8_t teensyTemp;

  StaticJsonDocument<512> toJson();     // Subject to size change...
};

// --------------------------------------------------------

class Motor_Data
{

public:
  float motor_current;                  // Motor Current
  float input_current;                  // VESC Input Current

  float duty_cycle;                     // VESC duty cycle
  int32_t rpm;                          // Motor rpm
  float voltage;                        // VESC input voltage

  float temp_mos_1;                     // VESC Mos 1 Temp
  float temp_mos_2;                     // VESC Mos 2 Temp
  float temp_mos_3;                     // VESC Mos 3 Temp
  float temp_mos_max;
  float temp_mot;                       // Motor temperature

  uint8_t fault;                        // VESC Fault code
  uint8_t id;                           // VESC id
  int16_t asked_current;                // Throttle asked current
  int16_t pid_speed;                    // Throttle PID speed

  StaticJsonDocument<512> toJson();     // Subject to size change...
};

// --------------------------------------------------------

class Xsens_MTi680G_Data
{

public:
  #if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
	  tmElements_t utc_time;
	#endif
  float utc_time_centisecond;
  uint32_t unix_time;

  uint32_t status_word;                 // Contains the 32bit status word

  uint8_t error_code;                   // If 0x01 Output Buffer is full, at least one Message was dropped

  float roll;                           // Roll in range ±180º
  float pitch;                          // Pitch in range ±90º
  float yaw;                            // Yaw in range ±180º

  float gyrX;                           // gyrX in range ±35 rad/s
  float gyrY;                           // gyrY in range ±35 rad/s
  float gyrZ;                           // gyrZ in range ±35 rad/s

  float accX;                           // accX in range ±100 m/s2
  float accY;                           // accY in range ±100 m/s2
  float accZ;                           // accZ in range ±100 m/s2

  float temperature;                    // AHRS internal Temperature in ºC

  float lat;                            // Boat Latitude in decimal degrees format (DD), ±90º
  float lon;                            // Boat Longitude in decimal degrees format (DD), ±180º

  float velX;                           // velX in range ±500 m/s
  float velY;                           // velY in range ±500 m/s
  float velZ;                           // velZ in range ±500 m/s

  float magx;
  float magy;
  float magz;

  float free_accX;
  float free_accY;
  float free_accZ;

  StaticJsonDocument<512> toJson();
};

// --------------------------------------------------------

class Screen_Data
{
public:
  // Motors related stuff
  int16_t current_threshold;            // Dual motor current threshold
  float pid_speed_setpoint;             // Speed reference for the speed pid controller
  volatile bool pid_state;              // PID ON / OFF
  float speed_Kp, speed_Ki, speed_Kd;   // Gains for speed PID
  volatile bool newGains;               // Signal that new gains have arrived

  // Foils related stuff

  volatile bool controller_state;       // Controller ON / OFF
  volatile bool homing;                 // Homing
  volatile bool mode_setpoint;                // Controller Mode
  float front_angle_setpoint;  
  float back_angle_setpoint;               // Front Foils Angle
  uint8_t dist_setpoint;                // Boat hight
  volatile bool back_foil_state;        // Rear Foil State: 0 - Manual ; 1 - Auto (Active)

  float last_left_angle, last_right_angle, last_back_angle; // Angles before last shutdown

  float heave_gains[6];
  float roll_gains[3];

  //SD Card related
  volatile bool new_kvaser_file;

  //BMS related stuff
  volatile bool solarRelay;

  //CCU related stuff
  volatile bool pumpMOSFET;
};

// --------------------------------------------------------

// Current shunt stuff:

// Struct for IVT_Msg_Result messages
typedef struct
{
  bool OCS;                             // Set if OCS is true
  bool this_result;                     // Set if result is out of (spec-) range, or has reduced precision or measurement-error
  bool any_result;                      // Set if any result has a measurement-error
  bool system;                          // Set if there is a system error, sensor functionality is not ensured.
  int32_t value;
} IVT_S_Result;

class IVT_S_Data
{
public:
  IVT_S_Result current;
  IVT_S_Result voltage1;
  IVT_S_Result voltage2;
  IVT_S_Result voltage3;
  IVT_S_Result temperature;
  IVT_S_Result power;
  IVT_S_Result As;
  IVT_S_Result Wh;
  
  StaticJsonDocument<512> toJson();
};


class Fuse_Board_Data
{
public:
  float current_24;
  float current_48;

  StaticJsonDocument<256> toJson();
};

class Cooling_Control_Unit_Data
{
  public:
		uint8_t temp_M1;                    // Motor 1 Temperature
		uint8_t temp_M2;                    // Motor 2 Temperature
		uint8_t temp_WC_IN;                 // Water Cooling In Temp
		uint8_t temp_WC_OUT;                // Water Cooling Out Temp

		uint8_t pump_pwm;                   // Pump speed 0 - 100%
		uint16_t pump_tach;                 // Pump speed in RPMs
		float water_flow;                   // Water Flow in l/min
		
    StaticJsonDocument<512> toJson();
};

class CANWiFi_Data
{
  public:
		float TeensyTemp;                 // Teensy CPU Temp
		
    StaticJsonDocument<256> toJson();
};

class Steering_Encoder_Data
{
  public:
    float value;
    
    StaticJsonDocument<256> toJson();     // Subject to size change...
};

class TSB_CAN : public CANListener
{
private:
  #if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
	static time_t getTeensy3Time();
	#endif
  bool receivedTime = false;
  uint16_t cells_balancing;
  // Array that hold the time of the last received message for each Source ID
  unsigned long received_time[MAX_NUM_NODES];

public:
  int *filters;
  CAN_message_t message;
  BMS_Data bms;
  Foils_Data foils;
  Motor_Data motor1;
  Motor_Data motor2;
  Xsens_MTi680G_Data ahrs;
  Throttle_Data throttle;
  Screen_Data screen;
  IVT_S_Data shunt_bat;
  IVT_S_Data shunt_solar;
  Fuse_Board_Data fuse_board;
  Steering_Encoder_Data encoder;
  Cooling_Control_Unit_Data ccu;
  CANWiFi_Data canWiFi;

  TSB_CAN();
  void receive_Message(CAN_message_t &frame);
  void printFrame(CAN_message_t &frame, int mailbox);
  #if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
    bool frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller); //overrides the parent version so we can actually do something 
  #elif defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)
    void gotFrame(CAN_message_t *frame, int mailbox); //overrides the parent version so we can actually do something
  #endif
  void check_valid_data();
  void reset_data(uint8_t source_id);
};

#endif
