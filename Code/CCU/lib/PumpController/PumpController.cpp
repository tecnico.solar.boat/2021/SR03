/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "PumpController.h"

ADT7470 ADT(ADT7470_ADDR_FLOAT);

//Define Variables we'll be connecting to
double Setpoint, Input, Output;

//Specify the links and initial tuning parameters
double Kp=5, Ki=10, Kd=1;
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, REVERSE);

void testStart()
{
  Serial.println(F("ADT7470 testStart"));
  ADT.powerUp();
  ADT.startMonitoring();
  ADT.setFanHighFreq(3);
  ADT.setPulsesPerRevolution(3, 2);
  Serial.println((String)"Pulses Per Revolution: " + ADT.getPulsesPerRevolution(3));
  ADT.setFastTach();
  Serial.println();
  Serial.println();
}

void testRevision()
{
  Serial.print("ADT7470_LIB_VERSION:\t");
  Serial.println(ADT7470_LIB_VERSION);

  Serial.print("ADT7470 getRevision:\t");
  Serial.println(ADT.getRevision());
  Serial.print("ADT7470 getDeviceID:\t");
  Serial.println(ADT.getDeviceID());
  Serial.print("ADT7470 getCompanyID:\t");
  Serial.println(ADT.getCompanyID());
  Serial.println();
  Serial.println();
  delay(10);
}

void testPWM()
{
  Serial.println(F("----- ADT7470 PWM TEST -----"));
  Serial.println(F("Setting PWM to 0 ..."));
  ADT.setPWM(3, 0);
  delay(5000);
  Serial.println("PWM:");
  Serial.println(ADT.getPWM(3));
  Serial.println();


  for (int i = 0; i < 255; i+= 50)
  {
    Serial.print(F("set:"));
    uint8_t pwm = i;
    ADT.setPWM(3, pwm);
    Serial.print("\t");
    Serial.print(pwm);
    delay(500);
    Serial.print(F("get:"));
    Serial.print("\t");
    Serial.println(ADT.getPWM(3));
    Serial.println((String)"RPM: " + ADT.getRPM(3));
  }
  Serial.println();

}

void testTach(){}

void testFanSpeed()
{
  Serial.println(F("ADT7470 testFanSpeed"));

  Serial.print("high:\t");
  for (uint8_t i = 0; i < 8; i++)
  {
    ADT.setFanHighFreq(i);
    Serial.print(i);
    Serial.print("\t");
    delay(1000);
  }
  Serial.println();
  ADT.setFanHighFreq(2);

  Serial.println();
  Serial.println();
  delay(10);
}

void PumpSetup(uint8_t maxTemp)
{
  delay(5000);
  Wire.begin();

  Serial.print(F("\n\nStart "));
  Serial.println(__FILE__);
  Serial.println();

  if (!ADT.isConnected())
    Serial.println("Cannot connect ADT7470...\n");
  else{
    testStart();
    // testPWM();
  }
  // initialize the variables we're linked to
  Input = maxTemp;
  ADT.setPWM(3, Output);
  Setpoint = 32;

  // turn the PID on
  myPID.SetMode(AUTOMATIC);

  Serial.println("Done");
}

void PumpControl(uint8_t maxTemp)
{
  Input = maxTemp;
  // myPID.Compute();
  ADT.setPWM(3, 255);

  PUMP_PWM = ADT.getPWM(3);
  // Serial.print((String)"PWM: " + ADT.getPWM(3));

  PUMP_TACH = ADT.getRPM(3);
  // Serial.print((String)"\tRPM: " + ADT.getRPM(3));
  // Serial.print((String)"\tTemperature: " + maxTemp);
  // Serial.println();
  // Serial.println();
}