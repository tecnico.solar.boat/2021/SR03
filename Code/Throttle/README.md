# Throttle

This repo holds the code for TSB's CAN Throttle based on Torqueedo Remote Throttle. The PCB for this code is available [here](https://gitlab.com/tecnico.solar.boat/2021/SR03/-/tree/main/PCB's/Throttle_CAN)

The throttle directly communicates over CAN to the VESCs 75/300 issuing commands to control the boat's motors. Each VESC needs to be configured to use CAN and each one must be assigned a unique CAN ID. In our case CAN ID 1 e CAN ID 2.

Copyright (C) 2021  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute 
it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat
