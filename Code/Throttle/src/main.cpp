/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/


#include <Arduino.h>
#include "AS5045.h"
#include "CAN_TSB.h"
#include <Timer.h>
#include <PID_v1.h>
#include "Config.h"
#include "VescCAN.h"

// CHANGE THESE AS APPROPRIATE
#define CSpin   14
#define CLKpin  18
#define DOpin   19

AS5045 enc (CSpin, CLKpin, DOpin) ;

TSB_CAN Can_tsb; // Can_tsb.motor1.voltage < 20 -> neutral_to_start 

CAN_message_t can_message;

VescCAN motor1(MOTOR1_ID);
VescCAN motor2(MOTOR2_ID);

int angle;
int start_angle;
int start_angle2;
int8_t throttle;
uint8_t status;
bool neutral_to_start;

 Timer t;


//Define Variables for the speed PID
double PIDdesiredspeed, PIDactualspeed, PIDcurrent;
float speedSetpoint = 8;
float maximum_current = MAXIMUM_CURRENT_SINGLE;

//Create the PID class
PID speedPID(&PIDactualspeed, &PIDcurrent, &PIDdesiredspeed,Kp,Ki,Kd, DIRECT);

int reverse_flag=0;
bool dualMotorStartFlag = false;
float motor_current = 0;
bool wasStopped = true;
bool startUpEnd = false;
unsigned long startUpEndTime = 0;
unsigned long lastIncrementTime=0;
float sweepCurrent = 60;

void init_CAN_m();
void send_CAN_m(void* context); 
void neutral_to_start_f();
void dual_Motor_alg(float current);

void setup ()
{
	status = 0;
	neutral_to_start = false;
	Serial.begin (115200) ;   // NOTE BAUD RATE

	init_CAN_m();

	if( Can0.begin(CAN_BPS_1000K)){
		Serial.println("CAN Init OK!");
	}
	else{
		Serial.println("CAN Init Failed!");
	}
	Can0.attachObj(&Can_tsb);
	
	Can0.setNumTXBoxes(1);    // Use all MOb (only 6x on the ATmegaxxM1 series) for receiving.
	
	Can0.watchFor();
	// Can0.setRXFilter(0x1b01,0x1b01,true);
	Can_tsb.attachGeneralHandler();

	

	pinMode(13, OUTPUT);
	if (!enc.begin ()){
		Serial.println ("Error setting up AS5045") ;
	}
	
	/* led HIGH until start sequence is done */
	neutral_to_start_f(); 
	Serial.print("Exit start");

	speedPID.SetMode(AUTOMATIC);

	/* Let LED_BUILTIN toggle every 1000th millisecond with start
	condition HIGH */
	t.oscillate(LED_BUILTIN, 1000, HIGH);

	t.every(100,send_CAN_m,(void*)1);

}

void loop ()
{
	angle = (int) round(enc.read() * 0.08789);
	// /*reverse*/
	if (angle>=90){
		if (wasStopped)
			throttle = map( angle ,148,90,-100,-20); 
		else
			throttle = map( angle ,148,90,-100,-10); 
	}
	// /*neutral*/
	if (angle==88 || angle==89){				
		throttle = 0;
		wasStopped = true;
	}
	/*forward*/
	if (angle<=87){
		if (wasStopped)
			throttle = map( angle ,87,17,25,100);								
		else
			throttle = map( angle ,87,17,10,100);
	}

	if (wasStopped == true && throttle != 0 && (millis() - lastIncrementTime) > 100 ){
		// Serial.println(sweepCurrent);
		lastIncrementTime = millis();
		motor1.setMotorCurrent(sweepCurrent);
		sweepCurrent += 1;
		if (sweepCurrent > 70 || abs(Can_tsb.motor1.rpm) > 1000 ){
			startUpEnd = true;
			wasStopped = false;
			startUpEndTime = millis();
			sweepCurrent = 60;
		}
		
	}
	else{
		motor_current = throttle * maximum_current/100;
		if(motor_current < 0) reverse_flag = 1; else reverse_flag = 0;
		//check for dual motor state
		if ( motor_current < Can_tsb.screen.current_threshold ){
		    dualMotorStartFlag = false;
		}

		// Compute PID limits based on current_threshold
		if (Can_tsb.screen.current_threshold > MAXIMUM_CURRENT_SINGLE)
		{
			speedPID.SetOutputLimits(0,MAXIMUM_CURRENT_SINGLE);
			maximum_current = MAXIMUM_CURRENT_SINGLE;
		}
		else{
		speedPID.SetOutputLimits(0,MAXIMUM_CURRENT_DUAL);
		maximum_current = MAXIMUM_CURRENT_DUAL;
		}
		
		//check screen flag for PID or not PID

		/*for a PID to control current depending on the desired speed we need 3 things
		- first read actual speed from CAN bus
		- second define speed range and translate throttle to a desired speed
		- third compute PID to output new current value to send to motors*/
		PIDactualspeed = Can_tsb.foils.speed;
		//get desired speed from throttle (convert motor current to speed value)
		if(reverse_flag == 0) {
			PIDdesiredspeed = throttle*speedSetpoint/100;
		} else PIDdesiredspeed = 0; //do not try to use the PID if going in reverse
		speedPID.Compute();
		
		switch(reverse_flag){
			case 0: // If going forward - here with PID
				if(Can_tsb.screen.pid_state) {
					if( abs(PIDcurrent) >= Can_tsb.screen.current_threshold ){ // Asked current bigger than dual motor threshold
						dual_Motor_alg(PIDcurrent);
					}else{ // Only single motor required
						motor1.setMotorCurrent(PIDcurrent);
						motor2.setMotorCurrent(0);
					}
					if( throttle == 0 ){
						speedPID.SetMode(MANUAL);
						motor1.setMotorCurrent(0);
						PIDcurrent = 0;
					}
					else{
						speedPID.SetMode(AUTOMATIC);
					}
				} else { // Here without PID
					if( abs(motor_current) >= Can_tsb.screen.current_threshold ){ // Asked current bigger than dual motor threshold
						dual_Motor_alg(motor_current);
					}else{ // Only single motor required
						motor1.setMotorCurrent(motor_current);
						motor2.setMotorCurrent(0);
					}
				}
				break;
			case 1: // If going in reverse divide current by 3
				motor1.setMotorCurrent(motor_current);
				break;
		}

		if (Can_tsb.screen.pid_speed_setpoint != speedSetpoint){
			speedSetpoint = Can_tsb.screen.pid_speed_setpoint;
		}
	}      
	
	if(Can_tsb.motor1.voltage < 30)
		neutral_to_start_f();

	if(Can_tsb.screen.newGains == true){ // New PID gains arrived
		speedPID.SetTunings(Can_tsb.screen.speed_Kp, Can_tsb.screen.speed_Ki, Can_tsb.screen.speed_Kd );
		Can_tsb.screen.newGains = false;
	}
	// Serial.println((String)"kp: " + speedPID.GetKp() + " ki: " + speedPID.GetKi() + " kd: " + speedPID.GetKd());
	/* Updates the timer*/
	t.update(); 
}

void neutral_to_start_f( ){

	status = 0;
	while(neutral_to_start != true || Can_tsb.motor1.voltage<30){
		// Serial.println(Can_tsb.motor1.voltage);
		motor1.setMotorCurrent(0);

		start_angle = (int) round(enc.read()*0.08789);
		send_CAN_m((void*)1);

		delay(250);
		
		start_angle2 = (int) round(enc.read()*0.08789);
		send_CAN_m((void*)1);

		if ((start_angle == 88 || start_angle == 89) && (start_angle2 == 88 || start_angle2 == 89)){ 
			neutral_to_start = true;
		}

		if ((start_angle < 88 || start_angle > 89) && (start_angle2 < 88 || start_angle2 > 89)){ 
			neutral_to_start = false;
		}
		delay(250);
	}

	/* initial sequence ends*/
	status = 1; 
}

void init_CAN_m(){

	can_message.id=0x10;
	can_message.len = 2;
}

void send_CAN_m(void* context){
	/* Serial.print ("\t");
	Serial.print("Throttle: ");
	Serial.print ("\t");
	Serial.print(angle);
	Serial.print ("\t");
	Serial.println (throttle,DEC); */
	int32_t ind = 0;
	
	buffer_append_int8(can_message.buf,throttle,&ind);
	buffer_append_uint8(can_message.buf,status,&ind);
	Can0.write(can_message);
	// Serial.println("Sent");
}


void dual_Motor_alg(float current){
    if (Can_tsb.motor2.rpm < Can_tsb.motor1.rpm - 100 && dualMotorStartFlag == false){
        motor1.setMotorCurrent(current);
		motor2.setMotorCurrent(CURRENT_INIT);
    } else{
        dualMotorStartFlag = true;
        // transition phase - motor 1 stays at threshold value, motor 2 increases with asked_current-threshold as new increment over CURRENT INIT
        if ((current - Can_tsb.screen.current_threshold + CURRENT_INIT) < Can_tsb.screen.current_threshold){
            motor1.setMotorCurrent(Can_tsb.screen.current_threshold);
			motor2.setMotorCurrent(current - Can_tsb.screen.current_threshold);
        } 
        // when motor 2 reaches threshold both motors divide power equally
        else{
			motor1.setMotorCurrent(current*0.3333);
			motor2.setMotorCurrent(current*0.6667);
        }
    }
}
