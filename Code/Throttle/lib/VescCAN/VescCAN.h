/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "CAN_TSB.h"

class VescCAN {  
    private:
        int32_t id;
        CAN_message_t setCurrent;
        CAN_message_t setCurrentRelative;
        CAN_message_t setDutyCycle;
        CAN_message_t setBrakeCurrent;
        CAN_message_t setRPMs;
        CAN_message_t setBrakeCurrentRelative;
        CAN_message_t setMotorCurrentLimit;
        CAN_message_t setStoreMotorCurrentLimit;
        CAN_message_t setInputCurrentLimit;
        CAN_message_t setStoreInputCurrentLimit;

    public:             
        VescCAN(int VescId);
        void setMotorCurrent(float current);
        void setMotorCurrentPercentage(int32_t percentage);
};