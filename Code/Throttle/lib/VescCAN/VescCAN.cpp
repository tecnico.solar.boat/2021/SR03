/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "VescCAN.h"

VescCAN::VescCAN(int VescId){
    id = VescId;

    setCurrent.extended = true;
    setCurrentRelative.extended = true;
    setDutyCycle.extended = true;
    setBrakeCurrent.extended = true;
    setRPMs.extended = true;
    setBrakeCurrentRelative.extended = true;
    setMotorCurrentLimit.extended = true;
    setStoreMotorCurrentLimit.extended = true;
    setInputCurrentLimit.extended = true;
    setStoreInputCurrentLimit.extended = true;

    setCurrent.len = 4;
    setCurrentRelative.len = 4;
    setDutyCycle.len = 4;
    setBrakeCurrent.len = 4;
    setRPMs.len = 4;
    setBrakeCurrentRelative.len = 8;
    setMotorCurrentLimit.len = 8;
    setStoreMotorCurrentLimit.len = 8;
    setInputCurrentLimit.len = 8;
    setStoreInputCurrentLimit.len = 8;

    setCurrent.id = 0x100 | VescId;
    setCurrentRelative.id = 0xA00 | VescId;
    setDutyCycle.id = 0x000 | VescId;
    setBrakeCurrent.id = 0x200 | VescId;
    setRPMs.id = 0x300 | VescId;
    setBrakeCurrentRelative.id = 0xB00 | VescId;
    setMotorCurrentLimit.id = 0x1500 | VescId;
    setStoreMotorCurrentLimit.id = 0x1600 | VescId;
    setInputCurrentLimit.id = 0x1700 | VescId;
    setStoreInputCurrentLimit.id = 0x1800 | VescId;
}

void VescCAN::setMotorCurrent(float current){
	int32_t ind = 0;
	buffer_append_int32( setCurrent.buf , current*1000, &ind);
    // Serial.println((String)"setMotorCurrent: " + current + " ID: " + this->id);
    Can0.write(setCurrent);
}

void VescCAN::setMotorCurrentPercentage(int32_t percentage){
	int32_t ind = 0;
	buffer_append_int32( setCurrentRelative.buf, percentage*1000, &ind);
    Can0.write(setCurrentRelative);
}