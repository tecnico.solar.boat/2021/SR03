# TSB SR03's CAN protocol

This is the code for the CAN cummunication bettween modules inside the boat.

Copyright (C) 2021  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat

⚠️ **CAN TSB now uses tonton81's FlexCAN_T4 library check the example at the end of this README** ⚠️

⚠️ **CAN TSB now uses 4 bits for Source and 5 bits for Data.** ⚠️

⚠️ **CAN ID 0x510 is FORBIDDEN since this is reserved for Isabellenhütte's Current Shunt internal use.** ⚠️

⚠️ **CAN ID 0x0 is FORBIDDEN since this is used to configure the steering encoder** ⚠️

#### Index

- [Message structure](#Mensagens)
- [ID structure](#Estrutura do ID)
- [Code](#Código)

<a name="Mensagens"/>

## Message structure
![JC](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/CAN-Bus-frame_in_base_format_without_stuffbits.svg/2560px-CAN-Bus-frame_in_base_format_without_stuffbits.svg.png)

On the code, only ID, DL (message length) and DB (message content) need to be defined.

<a name="ID"/>

### ID structure

| Priority bits | Data ID | Source ID |
| :-----------: | :-----: | :-------: |
|    2 bits     | 5 bits  |  4 bits   |


#### Priority bits

- Defines the message priority
- 00 maximum priority
- 11 lowest priority

#### Data ID

- Defines the message content
- With respect to each device

#### Source ID

- Defines which node sent the message
- Together with the Data ID defines which message was sent

| Source ID (4 bits) |                  Node                   |
| :----------------: | :-------------------------------------: |
|        0x0         |        [Throttle](#THROTTLE_MSG)        |
|        0x1         |          [Solar BMS](#BMS_MSG)          |
|        0x2         |           [Foils](#FOILS_MSG)           |
|         -          |           [VESCs](#VESCs_MSG)           |
|        0x4         |      [Xsens MTi-680G](#Xsens_MSG)       |
|        0x5         |          [Screen](#SCREEN_MSG)          |
|        0x6         | [Battery Current Shunt](#SHUNT_BAT_MSG) |
|        0x7         |        [Fuse Board](#FUSES_MSG)         |
|        0x8         | [Solar Current Shunt](#SHUNT_SOLAR_MSG) |
|        0x9         |      [Steering Encoder](#ENCODER)       |
|        0xA         |               [CCU](#CCU)               |
|        0xB         |          [CANWiFi](#CANWiFi)            |
|        0xC		 |			[MPPT_LT](#MPPT_LT)			   |
|        0xF         |          [Torque Sensor](#TS)           |

<br/>

Scale is scale argument you need to pass to the  decoding function see: **[buffer.c](https://gitlab.com/tecnico.solar.boat/2021/SR03/-/blob/main/Code/Libraries/CAN_TSB_Library/src/buffer.cpp)** If there is no scale the int function must be used. Otherwise, float functions must be used.

<a name="THROTTLE_MSG"/>

## Throttle Messages:
### status (CAN ID: 0x10)
**LEN = 2**

| Offset | Buffer Function |  Name  | Scale |          Description          |
| :----: | :-------------: | :----: | :---: | :---------------------------: |
|   0    |     int8_t      | value  |   -   |  Requested power \[-100;100\] |
|   1    |     uint8_t     | Status |   -   |          Status bits          |

#### Status bits:
| Position | Message |                   Description                   |
| :------: | :-----: | :---------------------------------------------: |
|    7     |    -    |                                                 |
|    6     |    -    |                                                 |
|    5     |    -    |                                                 |
|    4     |    -    |                                                 |
|    3     |    -    |                                                 |
|    2     |    -    |                                                 |
|    1     |    -    |                                                 |
|    0     |  Zero   | If set, throttle has gone to zero after startup |


<a name="BMS_MSG"/>

## Solar BMS Messages:
### status (CAN ID: 0x601)
**LEN = 6**

| Offset | Buffer Function |       Name       | Scale |            Description            |
| :----: | :-------------: | :--------------: | :---: | :-------------------------------: |
|   0    |     uint8_t     |      Status      |   -   |       Status of the battery       |
|   1    |    uint16_t     |    LTC Status    |   -   |         Status of the LTC         |
|   3    |     uint8_t     | Cells Balancing0 |   -   |  Which cells are balancing [1:8]  |
|   4    |     uint8_t     | Cells Balancing1 |   -   | Which cells are balancing [9:12] |
|   5    |     uint8_t     |     Warnings     |   -   |             Warnings              |

### SOC_Voltage_Currents (CAN ID: 0x611)
**LEN = 8**

| Offset | Buffer Function |      Name       | Scale |       Description       |
| :----: | :-------------: | :-------------: | :---: | :---------------------: |
|   0    |     float16     | State of Charge |  1e2  | Battery State of Charge |
|   2    |     float16     |     Voltage     |  5e2  | Battery pack voltage    |
|   4    |     float16     |     Current     |  1e2  | Battery pack current    |
|   6    |     float16     |  Solar Current  |  1e2  | Solar current           |


### cellvoltages0(CAN ID: 0x621)
**LEN = 8**

| Offset | Buffer Function |   Name   | Scale | Description  |
| :----: | :-------------: | :------: | :---: | :----------: |
|   0    |     float16     | voltage1 |  1e4  | cell voltage |
|   2    |     float16     | voltage2 |       | cell voltage |
|   4    |     float16     | voltage3 |       | cell voltage |
|   6    |     float16     | voltage4 |       | cell voltage |

### cellvoltages1(CAN ID: 0x631)
**LEN = 8**

| Offset | Buffer Function |   Name   | Scale | Description  |
| :----: | :-------------: | :------: | :---: | :----------: |
|   0    |     float16     | voltage5 |  1e4  | cell voltage |
|   2    |     float16     | voltage6 |       | cell voltage |
|   4    |     float16     | voltage7 |       | cell voltage |
|   6    |     float16     | voltage8 |       | cell voltage |

### cellvoltages2(CAN ID: 0x641)
**LEN = 8**

| Offset | Buffer Function |   Name    | Scale | Description  |
| :----: | :-------------: | :-------: | :---: | :----------: |
|   0    |     float16     | voltage9  |  1e4  | cell voltage |
|   2    |     float16     | voltage10 |       | cell voltage |
|   4    |     float16     | voltage11 |       | cell voltage |
|   6    |     float16     | voltage12 |       | cell voltage |

### SolarPanelVoltages0(CAN ID: 0x651)
**LEN = 8**

| Offset | Buffer Function | Name  | Scale |      Description      |
| :----: | :-------------: | :---: | :---: | :-------------------: |
|   0    |     float16     |  SP1  |  1e3  | solar array 1 voltage |
|   2    |     float16     |  SP2  |       | solar array 2 voltage |
|   4    |     float16     |  SP3  |       | solar array 3 voltage |
|   6    |     float16     |  SP4  |       | solar array 4 voltage |

### SolarPanelVoltages1(CAN ID: 0x661)
**LEN = 2**

| Offset | Buffer Function | Name  | Scale |     Description       |
| :----: | :-------------: | :---: | :---: | :-------------------: |
|   0    |     float16     |  SP5  |  1e3  | solar array 5 voltage |

### temperatures0 (CAN ID: 0x671)
**LEN = 8**

| Offset | Buffer Function |         Name          |     Scale      | Description |
| :----: | :-------------: | :-------------------: | :------------: | :---------: |
|   0    |     uint8_t     | Bus Bar 1 temperature | round to units | temperature |
|   1    |     uint8_t     | Bus Bar 2 temperature |                | temperature |
|   2    |     uint8_t     | Bus Bar 3 temperature |                | temperature |
|   3    |     uint8_t     | Bus Bar 4 temperature |                | temperature |
|   4    |     uint8_t     | Bus Bar 5 temperature |                | temperature |
|   5    |     uint8_t     | Bus Bar 6 temperature |                | temperature |
|   6    |     uint8_t     | Bus Bar 7 temperature |                | temperature |
|   7    |     uint8_t     | Bus Bar 8 temperature |                | temperature |

### temperatures1 (CAN ID: 0x681)
**LEN = 8**

| Offset | Buffer Function |          Name          |     Scale      | Description |
| :----: | :-------------: | :--------------------: | :------------: | :---------: |
|   0    |     uint8_t     | Bus Bar 9 temperature  | round to units | temperature |
|   1    |     uint8_t     | Bus Bar 10 temperature |                | temperature |
|   2    |     uint8_t     | Bus Bar 11 temperature |                | temperature |
|   3    |     uint8_t     | Bus Bar 12 temperature |                | temperature |
|   4    |     uint8_t     | Bus Bar 13 temperature |                | temperature |
|   5    |     uint8_t     |   Cell 1 temperature   |                | temperature |
|   6    |     uint8_t     |   Cell 2 temperature   |                | temperature |
|   7    |     uint8_t     |   Cell 3 temperature   |                | temperature |

### temperatures2 (CAN ID: 0x691)
**LEN = 8**

| Offset | Buffer Function |        Name         |     Scale      | Description |
| :----: | :-------------: | :-----------------: | :------------: | :---------: |
|   0    |     uint8_t     | Cell 4 temperature  | round to units | temperature |
|   1    |     uint8_t     | Cell 5 temperature  |                | temperature |
|   2    |     uint8_t     | Cell 6 temperature  |                | temperature |
|   3    |     uint8_t     | Cell 7 temperature  |                | temperature |
|   4    |     uint8_t     | Cell 8 temperature  |                | temperature |
|   5    |     uint8_t     | Cell 9 temperature  |                | temperature |
|   6    |     uint8_t     | Cell 10 temperature |                | temperature |
|   7    |     uint8_t     | Cell 11 temperature |                | temperature |

### temperatures3 (CAN ID: 0x6A1)
**LEN = 8**

| Offset | Buffer Function |        Name         |     Scale      | Description |
| :----: | :-------------: | :-----------------: | :------------: | :---------: |
|   0    |     uint8_t     | Cell 12 temperature | round to units | temperature |
|   1    |     uint8_t     | Cell 13 temperature |                | temperature |
|   2    |     uint8_t     | Cell 14 temperature |                | temperature |
|   3    |     uint8_t     | Cell 15 temperature |                | temperature |
|   4    |     uint8_t     | Cell 16 temperature |                | temperature |
|   5    |     uint8_t     | Cell 17 temperature |                | temperature |
|   6    |     uint8_t     | Cell 18 temperature |                | temperature |
|   7    |     uint8_t     | Cell 19 temperature |                | temperature |


### temperatures4 (CAN ID: 0x6B1)
**LEN = 6**

| Offset | Buffer Function |        Name         |     Scale      | Description |
| :----: | :-------------: | :-----------------: | :------------: | :---------: |
|   0    |     uint8_t     | Cell 20 temperature | round to units | temperature |
|   1    |     uint8_t     | Cell 21 temperature |                | temperature |
|   2    |     uint8_t     | Cell 22 temperature |                | temperature |
|   3    |     uint8_t     | Cell 23 temperature |                | temperature |
|   4    |     uint8_t     | Cell 24 temperature |                | temperature |
|   5    |     uint8_t     | Cell/bus bar max temp |                | temperature |

### temperatures5 (CAN ID: 0x6C1)
**LEN = 4**

| Offset | Buffer Function |     Name     |     Scale      | Description |
| :----: | :-------------: | :----------: | :------------: | :---------: |
|   0    |     uint8_t     | Ambient Temp | round to units | temperature |
|   1    |     uint8_t     |  HeatSink 1  |                | temperature |
|   2    |     uint8_t     |  HeatSink 2  |                | temperature |
|   3    |     uint8_t     |   LTC temp   |                | temperature |

<a name="FOILS_MSG"/>

## Foils Messages:

### US_angles(CAN ID: 0x602)
**LEN = 8**

| Offset | Buffer Function |    Name     | Scale |         Description         |
| :----: | :-------------: | :---------: | :---: | :-------------------------: |
|   0    |     float16     |    dist     |  1e2  | Distance to water in meters |
|   2    |     float16     | left_angle  |  1e2  |   Angle of the Left foil    |
|   4    |     float16     | right_angle |  1e2  |   Angle of the Right foil   |
|   6    |     float16     | back_angle  |  1e2  |   Angle of the Rear foil    |

### Setpoints_state_mode (CAN ID: 0x612)
**LEN = 8**

| Offset | Buffer Function |       Name       | Scale |                          Description                          |
| :----: | :-------------: | :--------------: | :---: | :-----------------------------------------------------------: |
|   0    |     uint8_t     | 	   State      |   -   |                       Controller state                        |
|   1    |     uint8_t     |       Mode       |   -   |                        Controller mode                        |
|   2    |     uint8_t     | Height Reference |   -   | Height Reference used by the controller in Heave or Full mode |
|   3    |     float16     | Angle Reference  |  1e1  |                     Foils Angle Reference *                   |
|   5    |     float16     |      Speed       |  1e2  |                       Boat speed in m/s                       |
|   7    |     uint8_t     |  Teensy Temp     |   -   |                  Teensy 4.0 CPU Temperature                   |

*Angle of back or front foils depending on the mode. Only front angle for manual mode.

<a name="VESCs_MSG"/>

## VESCs messages:

#### These messages are now broadcasted by the VESCs directly to the boat's CAN Bus, **they use EXTENDED CAN IDs**. The VESC with ID 1 (messages 0x901, 0xE01, 0xF01 and 0x1001) is the one that controls the LOW POWER motor. The VESC with ID 2 (messages 0x902, 0xE02, 0xF02 and 0x1002) is the one that controls the HIGH POWER motor.

### VESC1_current (CAN EXT ID: 0x901)
**LEN = 8**

| Offset | Buffer Function |     Name      | Scale |        Description         |
| :----: | :-------------: | :-----------: | :---: | :------------------------: |
|   0    |     float32     | motor_current |  1e2  |      Motor 1 Current       |
|   4    |     float32     | input_current |  1e2  | VESC Motor 1 Input Current |


### VESC1_duty_rpm_voltage (CAN EXT ID: 0xE01)
**LEN = 8**

| Offset | Buffer Function |    Name    | Scale |        Description         |
| :----: | :-------------: | :--------: | :---: | :------------------------: |
|   0    |     float16     | duty_cycle |  1e3  |  VESC Motor 1 duty cycle   |
|   2    |     int32_t     |    rpm     |   -   |        Motor 1 rpm         |
|   6    |     float16     |  voltage   |  1e1  | VESC Motor 1 input voltage |


### VESC1_temperatures (CAN EXT ID: 0xF01)
**LEN = 8**

| Offset | Buffer Function |     Name     | Scale |        Description        |
| :----: | :-------------: | :----------: | :---: | :-----------------------: |
|   0    |     float16     |  temp_mos_1  |  1e1  |  VESC Motor 1 Mos 1 Temp  |
|   2    |     float16     |  temp_mos_2  |  1e1  |  VESC Motor 1 Mos 2 Temp  |
|   4    |     float16     |  temp_mos_3  |  1e1  |  VESC Motor 1 Mos 3 Temp  |
|   6    |     float16     | temp_mos_max |  1e1  | VESC Motor 1 Mos Max Temp |


### VESC1_fault_id_asked_current (CAN EXT ID: 0x1001)
**LEN = 2**

| Offset | Buffer Function |     Name      | Scale |       Description       |
| :----: | :-------------: | :-----------: | :---: | :---------------------: |
|   0    |     uint8_t     |     fault     |   -   | VESC Motor 1 Fault code |
|   1    |     uint8_t     |      id       |   -   |     VESC Motor 1 id     |
|   2    |     float16     |    temp_mot   |  1e1  |   Motor 1 Temperature   |


### VESC2_current (CAN EXT ID: 0x902)
**LEN = 8**

| Offset | Buffer Function |     Name      | Scale |        Description         |
| :----: | :-------------: | :-----------: | :---: | :------------------------: |
|   0    |     float32     | motor_current |  1e2  |      Motor 2 Current       |
|   4    |     float32     | input_current |  1e2  | VESC Motor 2 Input Current |


### VESC2_duty_rpm_voltage (CAN ID: 0xE02)
**LEN = 8**

| Offset | Buffer Function |    Name    | Scale |        Description         |
| :----: | :-------------: | :--------: | :---: | :------------------------: |
|   0    |     float16     | duty_cycle |  1e3  |  VESC Motor 2 duty cycle   |
|   2    |     int32_t     |    rpm     |   -   |        Motor 2 rpm         |
|   6    |     float16     |  voltage   |  1e1  | VESC Motor 2 input voltage |


### VESC2_temperatures (CAN EXT ID: 0xF02)
**LEN = 8**

| Offset | Buffer Function |     Name     | Scale |        Description        |
| :----: | :-------------: | :----------: | :---: | :-----------------------: |
|   0    |     float16     |  temp_mos_1  |  1e1  |  VESC Motor 2 Mos 1 Temp  |
|   2    |     float16     |  temp_mos_2  |  1e1  |  VESC Motor 2 Mos 2 Temp  |
|   4    |     float16     |  temp_mos_3  |  1e1  |  VESC Motor 2 Mos 3 Temp  |
|   6    |     float16     | temp_mos_max |  1e1  | VESC Motor 2 Mos Max Temp |


### VESC2_fault_id (CAN ID: 0x1002)
**LEN = 2**

| Offset | Buffer Function |     Name      | Scale |       Description       |
| :----: | :-------------: | :-----------: | :---: | :---------------------: |
|   0    |     uint8_t     |     fault     |   -   | VESC Motor 2 Fault code |
|   1    |     uint8_t     |       id      |   -   |     VESC Motor 2 id     |
|   2    |     float16     |    temp_mot   |  1e1  |   Motor 2 Temperature   |


<a name="Xsens_MSG"/>

## Xsens MTi-680G Messages:
### XCID_UTC (CAN ID: 0x04)
**LEN = 8**

| Offset | Buffer Function |    Name     | Scale |   Description   |
| :----: | :-------------: | :---------: | :---: | :-------------: |
|   0    |     uint8_t     |    Year     |   1   |   Year in UTC   |
|   1    |     uint8_t     |    Month    |   1   |  Month in UTC   |
|   2    |     uint8_t     |     Day     |   1   |   Day in UTC    |
|   3    |     uint8_t     |    Hour     |   1   |  Hours in UTC   |
|   4    |     uint8_t     |   Minute    |   1   | Minutes in UTC  |
|   5    |     uint8_t     |   Second    |   1   | Seconds in UTC  |
|   6    |    uint16_t     | CentiSecond |  1e4  | Tenth ms in UTC |

### XCDI_StatusWord (CAN ID: 0x614)
**LEN = 4**

| Offset | Buffer Function |    Name    | Scale |          Description           |
| :----: | :-------------: | :--------: | :---: | :----------------------------: |
|   0    |    uint32_t     | StatusWord |   -   | Contains the 32bit status word |

Refer to XDI_StatusWord at http://mti.helpdocsonline.com/messages

### XCID_Error (CAN ID: 0x624)
**LEN = 1**

| Offset | Buffer Function |    Name    | Scale |                           Description                           |
| :----: | :-------------: | :--------: | :---: | :-------------------------------------------------------------: |
|   0    |     uint8_t     | Error Code |   -   | If 0x01 Output Buffer is full, at least one Message was dropped |

### XCDI_EulerAngles (CAN ID: 0x434)
**LEN = 6**

| Offset | Buffer Function | Name  | Scale |     Description     |
| :----: | :-------------: | :---: | :---: | :-----------------: |
|   0    |     float16     | Roll  |  2<sup>7</sup>  | Roll in range ±180º |
|   2    |     float16     | Pitch |  2<sup>7</sup>  | Pitch in range ±90º |
|   4    |     float16     |  Yaw  |  2<sup>7</sup>  | Yaw in range ±180º  |

### XCDI_RateOfTurn (CAN ID: 0x444)
**LEN = 6**

| Offset | Buffer Function | Name  | Scale |       Description       |
| :----: | :-------------: | :---: | :---: | :---------------------: |
|   0    |     float16     | gyrX  |  2<sup>9</sup>  | gyrX in range ±35 rad/s |
|   2    |     float16     | gyrY  |  2<sup>9</sup>  | gyrY in range ±35 rad/s |
|   4    |     float16     | gyrZ  |  2<sup>9</sup>  | gyrY in range ±35 rad/s |

### XCDI_Acceleration (CAN ID: 0x454)
**LEN = 6**

| Offset | Buffer Function | Name  | Scale |            Description             |
| :----: | :-------------: | :---: | :---: | :--------------------------------: |
|   0    |     float16     | accX  |  2<sup>8</sup>  | accX in range ±100 m/s<sup>2</sup> |
|   2    |     float16     | accY  |  2<sup>8</sup>  | accY in range ±100 m/s<sup>2</sup> |
|   4    |     float16     | accZ  |  2<sup>8</sup>  | accZ in range ±100 m/s<sup>2</sup> |

### XCDI_Temperature (CAN ID: 0x664)
**LEN = 2**

| Offset | Buffer Function |    Name     | Scale |           Description           |
| :----: | :-------------: | :---------: | :---: | :-----------------------------: |
|   0    |     float16     | Temperature |  2<sup>8</sup>  | AHRS internal Temperature in ºC |

### XCDI_Latitude (CAN ID: 0x674)
**LEN = 8**

| Offset | Buffer Function | Name  | Scale |                     Description                      |
| :----: | :-------------: | :---: | :---: | :--------------------------------------------------: |
|   0    |     float32     |  lat  | 2<sup>24</sup>  |  Boat Latitude in decimal degrees format (DD), ±90º  |
|   4    |     float32     |  lon  | 2<sup>23</sup>  | Boat Longitude in decimal degrees format (DD), ±180º |


### XCDI_MagneticField (CAN ID: 0x684)
**LEN = 6**

| Offset | Buffer Function | Name  | Scale |            Description             |
| :----: | :-------------: | :---: | :---: | :--------------------------------: |
|   0    |     float16     | magX  |  2<sup>10</sup>  | magX in range ±32 a.u.  |
|   2    |     float16     | magY  |  2<sup>10</sup>  | magY in range ±32 a.u.  |
|   4    |     float16     | magZ  |  2<sup>10</sup>  | magZ in range ±32 a.u.  |

### XCDI_VelocityXYZ (CAN ID: 0x494)
**LEN = 6**

| Offset | Buffer Function | Name  | Scale |      Description       |
| :----: | :-------------: | :---: | :---: | :--------------------: |
|   0    |     float16     | velX  |  2<sup>6</sup>  | velX in range ±500 m/s |
|   2    |     float16     | velY  |  2<sup>6</sup>  | velY in range ±500 m/s |
|   4    |     float16     | velZ  |  2<sup>6</sup>  | velZ in range ±500 m/s |

### XCDI_FreeAccXYZ (CAN ID: 0x4A4)
**LEN = 6**

| Offset | Buffer Function | Name  | Scale |      Description       |
| :----: | :-------------: | :---: | :---: | :--------------------: |
|   0    |     float16     | free_accX  |  2<sup>8</sup>  | FreeAccX in range ±100 m/s<sup>2</sup>|
|   2    |     float16     | free_accY  |  2<sup>8</sup>  | FreeAccY in range ±100 m/s<sup>2</sup>|
|   4    |     float16     | free_accZ  |  2<sup>8</sup>  | FreeAccZ in range ±100 m/s<sup>2</sup>|

<a name="SCREEN_MSG"/>

## Screen Messages:
### current_threshold (CAN ID: 0x605)
**LEN = 5**

| Offset | Buffer Function |        Name         | Scale |                 Description                  |
| :----: | :-------------: | :-----------------: | :---: | :------------------------------------------: |
|   0    |     int16_t     |  current_threshold  |   -   |         Dual motor current threshold         |
|   2    |     float16     | pid_speed_reference |  1e1  | Speed reference for the speed pid controller |
|   4    |                 |  [Messages set 1]   |   -   |                                              |

#### Messages set 1:
| Position |  Message  |  Description |
|:--------:|:---------:|:------------:|
|     7    |     -     |              |
|     6    |     -     |              |
|     5    |     -     |              |
|     4    |     -     |              |
|     3    |     -     |              |
|     2    |     -     |              |
|     1    |     -     |              |
|     0    | pid_state | PID ON / OFF |

### new_kvaser_file (CAN ID: 0x615)
**LEN = 0**
Content does not matter, just send message with empty buffer.

### foils_controller (CAN ID: 0x625)
**LEN = 6**

| Offset | Buffer Function |         Name         | Scale |    Description    |
|:------:|:---------------:|:--------------------:|:-----:|:-----------------:|
|    0   |     float16     |  back_angle_setpoint |  1e1  |  Back Foils Angle |
|    2   |     float16     | front_angle_setpoint |  1e1  | Front Foils Angle |
|    4   |     uint8_t     |     dist_setpoint    |   -   |     Boat hight    |
|    5   |                 |   [Messages set 1]   |   -   |                   |


#### Messages set 1:

| Position |      Message     |                   Description                   |
|:--------:|:----------------:|:-----------------------------------------------:|
|     7    |         -        |                                                 |
|     6    |         -        |                                                 |
|     5    |         -        |                                                 |
|     4    |         -        |                                                 |
|     3    | controller_state |               Controller ON / OFF               |
|     2    |      homing      |                      Homing                     |
|     1    |  controller_mode |                 Controller Mode                 |
|     0    |  back_foil_state | Rear Foil State: 0 - Manual ; 1 - Auto (Active) |

### toogle_solar_relay (CAN ID: 0x635)
Content does not matter, just send a message with an empty buffer.

### PID_gains (CAN ID: 0x645)
**LEN = 6**

| Offset | Buffer Function |  Name  | Scale |        Description       |
|:------:|:---------------:|:------:|:-----:|:------------------------:|
|    0   |     float16     | PID_KI |  1e1  |   Integral gain of PID   |
|    2   |     float16     | PID_KD |  1e1  |  Differential gain of PID |
|    4   |     float16     | PID_KP |  1e1  | Proportional gain of PID |


### last_foils_angles (CAN ID: 0x655)
**LEN = 6**

| Offset | Buffer Function |       Name       | Scale |                    Description                    |
|:------:|:---------------:|:----------------:|:-----:|:-------------------------------------------------:|
|    0   |     float16     |  last_left_angle |  1e2  |  Last Angle of the Left foil before boat shutdown |
|    2   |     float16     | last_right_angle |  1e2  | Last Angle of the Right foil before boat shutdown |
|    4   |     float16     |  last_back_angle |  1e2  |  Last Angle of the Rear foil before boat shutdown |

### toogle_cooling_pump (CAN ID: 0x665)
Content does not matter, just send a message with an empty buffer.

### Screen_Gain_Heave (CAN ID: 0x675)
**LEN = 6**

| Offset | Buffer Function |    Name    | Scale | Description |
|:------:|:---------------:|:----------:|:-----:|:-----------:|
|    0   |      float8     |   KPitch   |  1e1  |             |
|    1   |      float8     | KPitchRate |  1e1  |             |
|    2   |      float8     |   KSpeed   |  1e1  |             |
|    3   |      float8     |   KSpeedZ  |  1e1  |             |
|    4   |      float8     |   KHeave   |  1e1  |             |
|    5   |      float8     |  KHeaveInt |  1e1  |             |

### Screen_Gain_Roll (CAN ID: 0x685)
**LEN = 3**

| Offset | Buffer Function |  Name  | Scale | Description |
|:------:|:---------------:|:------:|:-----:|:-----------:|
|    0   |      float8     |  KRoll |  1e1  |             |
|    1   |      float8     |  KGyro |  1e1  |             |
|    2   |      float8     | KError |  1e1  |             |

<a name="SHUNT_BAT_MSG"/>

## IVT-S Battery Current Shunt (Isabellenhütte) Messages:

For more information regarding these sensor's messages check [these documents](https://gitlab.com/tecnico.solar.boat/2021/SR03/-/tree/main/Datasheets/Isabellenhutte%20-%20IVT-Series%20Current%20Shunt). Be aware that we are not using the default CAN ID's shown on Isabellenhütte documents.

### IVT_Msg_Command (CAN ID: 0x406)
**LEN = 8**

Function commands, SET and GET commands A command-ID-byte is included for identification.

### IVT_Msg_Debug (CAN ID: 0x510)
**LEN = 8**

The ID of this message cannot be changed and it is reserved for Isabellenhütte internal use. So we should not use this id in our messages.

### IVT_Msg_Response (CAN ID: 0x506)
**LEN = 8**

Response to SET and GET command messages A response-ID-byte is included for identification.

### IVT_Msg_Result_I (CAN ID: 0x606)
**LEN = 6**

Current measurement in mA.

### IVT_Msg_Result_U1 (CAN ID: 0x616)
**LEN = 6**

Voltage 1 in mV. **This is the voltage channel used to compute the Power and the Energy.**

### IVT_Msg_Result_U2 (CAN ID: 0x626)
**LEN = 6**

Voltage 2 in mV.

### IVT_Msg_Result_U3 (CAN ID: 0x636)
**LEN = 6**

Voltage 3 in mV.

### IVT_Msg_Result_T (CAN ID: 0x646)
**LEN = 6**

Temperature in ºC the scale is 0.1 ºC.

### IVT_Msg_Result_W (CAN ID: 0x656)
**LEN = 6**

Power (referring to current and voltage U1) in W.

### IVT_Msg_Result_As (CAN ID: 0x666)
**LEN = 6**

Current counter in As.

### IVT_Msg_Result_Wh (CAN ID: 0x676)
**LEN = 6**

Energy counter (referring to current and voltage U1) in Wh.

**All IVT_Msg_Result messages share the following structure:**

<img src="Auxiliary%20Files/IVT-S_Results.png"  width="60%">

MuxID description for IVT_Msg_Result:

<img src="Auxiliary%20Files/IVT-S_MuxID.png"  width="40%">

<a name="FUSES_MSG"/>

## Fuse Board Messages:
### status (CAN ID: 0x607)
**LEN = 2**

| Offset | Buffer Function |    Name    | Scale |          Description          |
| :----: | :-------------: | :--------: | :---: | :---------------------------: |
|   0    |     float8      | current_24 |  1e1  | current in 24V system in A/10 |
|   1    |     float8      | current_48 |  1e1  | current in 48V system in A/10 |


<a name="SHUNT_SOLAR_MSG"/>

## IVT-S SOLAR Current Shunt (Isabellenhütte) Messages:

For more information regarding this sensor's messages check [this documents](https://gitlab.com/tecnico.solar.boat/2021/SR03/-/tree/main/Datasheets/Isabellenhutte%20-%20IVT-Series%20Current%20Shunt). Be aware that we are not using the default CAN ID's shown on Isabellenhütte documents.

### IVT_Msg_Command (CAN ID: 0x408)
**LEN = 8**

Function commands, SET and GET commands A command-ID-byte is included for identification.

### IVT_Msg_Debug (CAN ID: 0x510)
**LEN = 8**

The ID of this message cannot be changed and it is reserved for Isabellenhütte internal use. So we should not use this id in our messages.

### IVT_Msg_Response (CAN ID: 0x508)
**LEN = 8**

Response to SET and GET command messages A response-ID-byte is included for identification.

### IVT_Msg_Result_I (CAN ID: 0x608)
**LEN = 6**

Current measurement in mA.

### IVT_Msg_Result_U1 (CAN ID: 0x618)
**LEN = 6**

Voltage 1 in mV. **This is the voltage channel used to compute the Power and the Energy.**

### IVT_Msg_Result_U2 (CAN ID: 0x628)
**LEN = 6**

Voltage 2 in mV.

### IVT_Msg_Result_U3 (CAN ID: 0x638)
**LEN = 6**

Voltage 3 in mV.

### IVT_Msg_Result_T (CAN ID: 0x648)
**LEN = 6**

Temperature in ºC the scale is 0.1 ºC.

### IVT_Msg_Result_W (CAN ID: 0x658)
**LEN = 6**

Power (referring to current and voltage U1) in W.

### IVT_Msg_Result_As (CAN ID: 0x668)
**LEN = 6**

Current counter in As.

### IVT_Msg_Result_Wh (CAN ID: 0x678)
**LEN = 6**

Energy counter (referring to current and voltage U1) in Wh.

**All IVT_Msg_Result messages share the following structure:**

<img src="Auxiliary%20Files/IVT-S_Results.png"  width="60%">

MuxID description for IVT_Msg_Result:

<img src="Auxiliary%20Files/IVT-S_MuxID.png"  width="40%">



<a name="ENCODER"/>

## Steering Encoder Messages:
Sensor has been configured with Node Number (NN): 9 and a baud rate of 1000 kbits/s.

### Boot up message (CAN ID: 0x709)

### Encoder Reply message (CAN ID: 0x589)

The encoder replies to requests by sending messages with this CAN ID. You can find the encoder documentation [here](https://gitlab.com/tecnico.solar.boat/2021/SR03/-/tree/main/Datasheets/Encoder).

### Preoperational mode (CAN ID: 0x0) 

This message is sent to the encoder in order to change its state. In this mode the encoder doesn't send its PDO's.

**LEN = 2**
| Offset |   0   |   1   |
| :----: | :---: | :---: |
| Value  | 0x80  | 0x09  |

### Start-operational mode (CAN ID: 0x0)

This message is sent to the encoder in order to change its state. This is the mode where we what to have the encoder running at.

**LEN = 2**
| Offset |   0   |   1   |
| :----: | :---: | :---: |
| Value  | 0x01  | 0x09  |

### Set preset value to 0 (CAN ID: 0x609)

This message is sent to the encoder in order to change its preset value. **The screen should have a button to send this message and thus zero the encoder**.

**LEN = 8**
| Offset |   0   |   1   |   2   |   3   |   4   |   5   |   6   |   7   |
| :----: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Value  | 0x23  | 0x03  | 0x60  | 0x00  | 0x00  | 0x00  | 0x00  | 0x00  |

### Save parameters to non-volatile memory (CAN ID: 0x609)

This message is sent to the encoder in order to save its parameters to its non-volatile memory.

**LEN = 8**
| Offset |   0   |   1   |   2   |   3   |   4   |   5   |   6   |   7   |
| :----: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Value  | 0x23  | 0x10  | 0x10  | 0x01  | 0x73  | 0x61  | 0x76  | 0x65  |

### value (CAN ID: 0x619)

This message is the 1st transmit PDO and is broadcasted every 2 ms.

**LEN = 4**

| Offset | Buffer Function |      Name      | Scale |       Description        |
| :----: | :-------------: | :------------: | :---: | :----------------------: |
|   0    |     int32_t     | position_value |   -   | Raw Value of the encoder |



<a name="CCU"/>

## Cooling Control Unit (CCU) messages:

## temperatures (CAN ID: 0x60A)
**LEN = 8**

| Offset | Buffer Function |    Name     | Scale |      Description       |
| :----: | :-------------: | :---------: | :---: | :--------------------: |
|   0    |     float16     |   temp_M1   |  1e1  |  Motor 1 Temperature   |
|   2    |     float16     |   temp_M2   |  1e1  |  Motor 2 Temperature   |
|   4    |     float16     | temp_WC_IN  |  1e1  | Water Cooling In Temp  |
|   6    |     float16     | temp_WC_OUT |  1e1  | Water Cooling Out Temp |

## state (CAN ID: 0x61A)
**LEN = 5**

| Offset | Buffer Function |    Name    | Scale |     Description     |
| :----: | :-------------: | :--------: | :---: | :-----------------: |
|   0    |     uint8_t     |  pump_pwm  |   -   | Pump speed 0 - 100% |
|   1    |    uint16_t     | pump_tach  |   -   | Pump speed in RPMs  |
|   3    |     float8      | water_flow |  1e1  | Water Flow in l/min |
|   4    |                 | State Bits |   -   |  |

#### State Bits:

(If 1 warning / fault is happening)

| Position |  Message  |  Description |
|:--------:|:-----------:|:------------:|
|     7    |     -     |              |
|     6    |     -     |              |
|     5    |     -     |              |
|     4    | m2_overtemp_warn  | High Power Motor above 85 ºC, reduce throttle    |
|     3    | m2_overtemp_fault | High Power Motor above 90 ºC, shutdown mandatory |
|     2    | m1_overtemp_warn  | Low Power Motor above 85 ºC, reduce throttle     |
|     1    | m1_overtemp_fault | Low Power Motor above 90 ºC, shutdown mandatory  |
|     0    | no_flow           | 0 L/min flow, shutdown mandatory                 |

<a name="CANWiFi"/>

## CANWiFi messages:

## temperature (CAN ID: 0x60B)
**LEN = 2**

| Offset | Buffer Function |    Name    | Scale |         Description           |
| :----: | :-------------: | :--------: | :---: | :---------------------------: |
|   0    |     float16     |     Temp   |  1e1  |  Teensy 3.2 CPU Temperature   |

<a name="MPPT_LT"/>

## MPPT LT8491 messages:

## MPPT1_msg1 (CAN ID: 0x60C)
**LEN = 8**
| Offset | Buffer Function |         Name         | Scale |    Description    |
|:------:|:---------------:|:--------------------:|:-----:|:-----------------:|
|    0   |     uint8_t     |    STAT_CHARGER_1	  |   -   |   |
|    1   |     uint8_t     |    STAT_SYSTEM_1     |   -   |   |
|    2   |     uint8_t     |    STAT_SUPPLY_1     |   -   |   |
|    3   |     uint8_t     |   STAT_CHARG_FAULT   |   -   |   |
|    4   |     float16     |        P_OUT_1       |  1e2  | Power OUT in W  |
|    6   |     float16     |        P_IN_1        |  1e2  | Power IN in W   |

## MPPT1_msg2 (CAN ID: 0x61C)
**LEN = 8**
| Offset | Buffer Function |         Name         | Scale |      Description     |
|:------:|:---------------:|:--------------------:|:-----:|:--------------------:|
|    0   |     float16     |        eff_1         |  1e0  | Efficiency 0-100%     |
|    2   |     float16     |        I_OUT_1       |  1e2  | Output current in A  |
|    4   |     float16     |        I_IN_1        |  1e2  | Input current in A   |
|    6   |     float16     |        V_BAT_1       |  1e2  | Battery Voltage in V |

## MPPT1_msg3 (CAN ID: 0x62C)
**LEN = 2**
| Offset | Buffer Function |         Name         | Scale |      Description     |
|:------:|:---------------:|:--------------------:|:-----:|:--------------------:|
|    0   |     float16     |        V_PAN_1       |  1e2  | Panel Voltage in V   |



<a name="TS"/>

## Torque Sensor (TS) messages:

### Zero (CAN ID: 0x60F)

This message is sent to the sensor in order to zero its torque output.

**LEN = 8**
| Offset |   0   |   1   |   2   |   3   |   4   |   5   |   6   |   7   |
| :----: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Value  | 0x00  | 0x00  | 0x00  | 0x00  | 0x00  | 0x00  | 0x00  | 0x00  |

### Torque (CAN ID: 0x61F)
**LEN = 4**

| Offset | Buffer Function |    Name    | Scale |      Description      |
| :----: | :-------------: | :--------: | :---: | :-------------------: |
|   0    |     float32_t   |  torque    |  1e3  | Measured torque in Nm |

### Speed (CAN ID: 0x62F)
**LEN = 4**

| Offset | Buffer Function |    Name    | Scale |      Description       |
| :----: | :-------------: | :--------: | :---: | :--------------------: |
|   0    |     uint32_t    |  speed     |   -   | Measured speed in RPMs |



<a name="Código"/>

## Code

- **[CAN_TSB](https://gitlab.com/tecnico.solar.boat/2021/SR03/-/tree/main/Code/Libraries/CAN_TSB_Library)**


#### Requirements 

- **[VSCode](https://code.visualstudio.com/)**
- **[Platformio](https://platformio.org/)**
- **[tonton81's FlexCAN_T4](https://github.com/tonton81/FlexCAN_T4)**


### Example

```c++
#include <Arduino.h>
#include "CAN_TSB.h"
#if defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)
  #include "avr_can.h"
#endif

TSB_CAN canMessageHandler;
CAN_message_t to_send; // One for each CAN ID that needs to be sent 

#if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
  // Declare your CAN interface, CAN0 available for Teensy 3.2/5 CAN 0 and 1 for Teensy 3.6 
  // CAN 1, 2 and 3 available for Teensy 4.X
  FlexCAN_T4<CAN0, RX_SIZE_256, TX_SIZE_16> Can0; // <- This name can be whatever you like but this is the name used to send commands to the interface
#endif

void initCAN_Messages(){ // Initialize the message fixed parameters, do it for all messages
  to_send.flags.extended = 0;
  to_send.id = 0x04;
  to_send.len = 4;
  // if you want to be sure that messages are sent in the order they are generated set this to 1
  // otherwise you don't need to set it.
  // to_send.seq = 1; 
}

void sendCAN_Messages(){ // Call this function periodically to send the messages use IntervalTimer
  // Lets show how it works for sending the time
  unsigned long time_now = now(); // Get current time
  int32_t ind = 0; 
  buffer_append_uint32(to_send.buf, time_now, &ind); // Populate the message data 
  Can0.write(to_send); // Send message (call this for every message to be sent)
}

void setup(void){
	Serial.begin(9600);
	initCAN_Messages(); // Call the function to initialize all you CAN Messages
	// Setup the CAN
	#if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
		Can0.begin(); // Initialize CAN BUS
		Can0.setBaudRate(1000000); // Define CAN BaudRate

		// FIFO allows ordered receptions, Mailboxes can receive in any order as long as a slot is empty. 
		// You can have FIFO and Mailboxes combined, for example, assign a Mailbox to receive a filtered ID in 
		// it's own callback, so a separate callback will fire when your critical frame is received. 
		// With FIFO alone you have only 1 callback essentially. 
		// In either case Teensy is fast enough to receive all critical frames without filters 
		// unless you are in non-interrupt mode or your loop code is causing delays...

		// Either use Mailboxes or FIFO if you are not using filters
		Can0.setMaxMB(16); // Teensy 3.X have only 16 Mailboxes per CAN Channel Teensy 4.X has 64
		Can0.enableMBInterrupts();

		// Can0.enableFIFO();
		// Can0.enableFIFOInterrupt();

	#elif defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)
		if( Can0.init(CAN_BPS_1000K)){
			Serial.println("CAN Init OK!");
		}
		else{
			Serial.println("CAN Init Failed!");
		}
	#endif
	Can0.attachObj(&canMessageHandler);
	
	#if defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)  
		Can0.setNumTXBoxes(1);    // Use all MOb (only 6x on the ATmegaxxM1 series) for receiving.

		//standard  
		Can0.setRXFilter(1, 0, 0, false);       //catch all mailbox
		Can0.setRXFilter(2, 0, 0, false);       //catch all mailbox
		Can0.setRXFilter(3, 0, 0, false);       //catch all mailbox
		Can0.setRXFilter(4, 0, 0, false);       //catch all mailbox
		Can0.setRXFilter(5, 0, 0, false);       //catch all mailbox
	#endif
		canMessageHandler.attachGeneralHandler();

	#if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
		Can0.mailboxStatus(); // This prints the mailboxes status, is also works when using FIFO
	#endif
  
}

void loop(void)
{
  // If you want to print something available on the CAN Bus check the CAN_TSB.h to see the structs used.
  // As an example lets print the battery current
  Serial.println(canMessageHandler.bms.current);

  // To send messages:
  sendCAN_Messages();
  delay(1000);
}
```

---
